# Rotas de Desenvolvimento

Aqui estão todas as rotas usadas no desenvolvimento e que são chamadas fora do contexto dos endpoints da API.

## E-mails

Visualização dos e-mails enviados pelo sistema.

Quando informar o `id` de alguma entidade, tente sempre colocar o `id` que corresponde a alguma entidade que possui os dados corretos, do contrário pode apresentar informações erradas.

> Se o pedido com `id` 10 estiver *cancelado*, pode ser que sejam apresentados dados errados se tentar visualizar esse pedido por algum email que não seja de cancelamento.

### Convite de acesso

#### dev/mail/account/invite

Cria um usuário falso e um token falso.

#### dev/mail/account/invite/{id}

Mostra o usuário com o `id` informado e um token falso.

### Nota fiscal aprovada

#### dev/mail/invoices/confirmation

Mostra a primeira nota fiscal que encontrar.

#### dev/mail/invoices/confirmation/{id}

Mostra a nota fiscal com o `id` informado.

### Notas fiscais exportadas

#### dev/mail/invoices

Cria um usuário falso para teste que recebe as notas exportadas.

#### dev/mail/invoices/{id}

Mostra os dados do usuário baseados no `id` informado.

### Nota fiscal rejeitada

#### dev/mail/invoices/rejected

Mostra a primeira nota fiscal que encontrar.

#### dev/mail/invoices/rejected/{id}

Mostra a nota fiscal com o `id` informado.

### Pedido cancelado

#### dev/mail/orders/cancelled

Mostra o primeiro pedido que encontrar.

#### dev/mail/orders/cancelled/{id}

Mostra o pedido com o `id` informado.

### Pedido completo

#### dev/mail/orders/completed

Mostra o primeiro pedido que encontrar.

#### dev/mail/orders/completed/{id}

Mostra o pedido com o `id` informado.

### Pedido encaminhado

#### dev/mail/orders/sent

Mostra o primeiro pedido que encontrar.

#### dev/mail/orders/sent/{id}

Mostra o pedido com o `id` informado.

### Pedidos exportados

#### dev/mail/myorders

Cria um usuário falso para teste que recebe as notas exportadas.

#### dev/mail/myorders/{id}

Mostra os dados do usuário baseados no `id` informado.

### Promoção iniciada

#### dev/mail/sale

Busca a primeira promoção que encontrar e o primeiro usuário vinculado.

#### dev/mail/sale/{id}

Mostra a promoção com o `id` informado e o primeiro usuário vinculado.

### Recuperação de conta

#### dev/mail/account/recover

Cria um usuário falso e um token falso.

#### dev/mail/account/recover/{id}

Mostra o usuário com o `id` informado e um token falso.
