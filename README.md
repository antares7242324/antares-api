# Meeg Antares Backend

<!-- testando ci/cd -->

## Documentação da API

Podem ser encontrados arquivos contendo as documentações do projeto para o backend dentro deste repositório no diretório `resources/docs/`.

---

> TODO: Adicionar detalhes sobre a documentação do padrão JSON:API para os parâmetros das queries.

---

## Variáveis de Ambiente

Existem algumas variáveis de ambiente muito importantes que devem ser adicionadas ao arquivo `.env`.

- ANTARES_EXTRA_DISCOUNTS - Um texto com uma lista de percentuais referentes ao **Desconto adicional** a ser configurado na edição de usuário. A lista deve estar no formato: `ANTARES_EXTRA_DISCOUNTS=0,0.10,0.15,0.20,0.25`. Caso não seja informado nenhum valor, aparecerá como valor vazio.

## Retornos dos Endpoints

Os endpoints devem sempre retornar o mais parecidos com as especificações encontradas na [JSON:API](https://jsonapi.org/).

Pelo menos todas as propriedades retornadas devem ser escritas usando o padrão **camelCase**.

Além disso, os endpoints possuem um retorno específico que está sendo usado na aplicação. Eles devem retornar usando a mesma estrutura de json exemplificada adiante:

```json
{
    "data": "array",
    "success": "boolean",
    "message": "string|null"
}
```

>**Observações**
>
>- A propriedade `data` pode retornar tanto `null` quanto um `array vazio`.
>- A propriedade `success` sempre será um boolean `true` ou `false`.
>- A propriedade `message` pode ser `null` caso não tenha mensagem.

Quando for retornar os dados dos endpoints, sempre procure usar algum **Resource** usando a estrutura já citada, e invocando o método *additionals()* quando necessário. Caso o uso de resource não for necessário, use os retornos do trait **HttpResponses**, tais como *success()* ou *error()*.

### Estrutura dos Dados

---

> TODO: Alterar os detalhes da estrutura de dados e adicionar exemplos mais reais.

---

Os dados dentro da propriedade `data` sempre devem retornar com a seguinte estrutura:

```json
{
    "data": {
        "id": "string (mostra apenas se houver ID)",
        "type": "string (tipo do recurso, sempre mostra)",
        "attributes": {
            "(objeto com todos os atributos do recurso)"
        }
    },
    "success": "boolean",
    "message": "string|null"
}
```

Essa estrutura tem que se repetir também caso um recurso possua outro recurso com um tipo. Por exemplo: Caso esteja mostrando os dados de um usuário e uma lista de pedidos vinculados, os pedidos também tem que estar na mesma estrutura.

## Envio de E-mail

Estamos atualmente usando o **Mailgun** para fazer os envios de e-mail. Para configurá-lo, precisamos pelo menos adicionar as seguintes variáveis no arquivo `.env`:

- MAIL_MAILER=mailgun - Colocar o serviço mailgun.
- MAILGUN_DOMAIN=domain - O domínio registrado.
- MAILGUN_SECRET=api secret key - A chave da API.

Além disso, se estiver usando algum *template blade* para criar o texto dos e-mails, então coloque na seguinte estrutura:

```php
resources\views\mails\{..dirs}\{template}
```

Onde `dirs` são quaisquer diretórios informando qual tipo ou grupo de e-mails o template pertence, e `template` é o arquivo blade de fato.

## Outros detalhes do Backend

Existem outras informações e detalhes relacionados apenas à programação no backend que estão todos nesta seção.

### Observers

Este app usa **observer patterns** para executar todos os processos internos que devem ser ativados em eventos. Eles são:

- **Enviar o pedido/orçamento para o Portal Antares ERP** em `OrderObserver`
- **Salvar o status antigo quando o usuário for atualizado** em `UserObserver`
- **Registrar a atividade do status do pedido** em `OrderObserver`
- **Registrar a atividade da promoção encerrada** em `SaleObserver`
- **Registrar a atividade quando o desconto principal do usuário for alterado** em `UserObserver`
- **Registrar a atividade quando o desconto atual do usuário for alterado** em `UserObserver`
- **Registrar a atividade quando a lista de preços do usuário for alterada** em `UserObserver`
- **Registar a atividade de atualização do status da nota fiscal** em `InvoiceObserver`
- **Registrar a atividade de criação de uma compensação para o usuário** em `CompensationObserver`
- **Registrar a atividade de remoção de uma compensação do usuário** em `InvoiceObserver`

## Recursos

### Documentações

- [Scribe](https://scribe.knuckles.wtf/laravel) - Gerador de documentação de API. Segue um [tutorial de vídeo](https://www.youtube.com/watch?v=a3nQrBEtufw) de demonstração
- [Laravel](https://laravel.com/docs/9.x) - Documentação do laravel 9.x
- [JSON:API](https://jsonapi.org/) - Especificação para criar APIs em JSON.

### Tutoriais

- [Laravel API Crash Course With Sanctum | Laravel Sanctum Full Tutorial | How to Build Laravel API](https://www.youtube.com/watch?v=TzAJfjCn7Ks) - Vídeo completo sobre Laravel API (9.x) com Sanctum em inglês
- [Enviando e-mail com Laravel e API do Mailgun](https://www.youtube.com/watch?v=o01XFVYItnw) - Vídeo sobre como configurar o Mailgun para usar no Laravel
- [New in Laravel 9.35: Alternate New Mailable Syntax](https://www.youtube.com/watch?v=h7oDAViX90M) - Vídeo mostrando a sintaxe mais recente das classes *Mailables* que tem agora no Laravel.

### Pesquisar Depois

- [Laravel Nova](https://nova.laravel.com/) - Painel de administrador
- [Laravel Pint](https://www.youtube.com/watch?v=5khyIHIYIK4) - Code styling usando linha de comando

## Comandos do Artisan

`antares:notify-sales`: Notifica o status da promoção com base na data informada. Se não for informada a data, usará a data atual.

`antares:reset`: Apaga todos os registros do banco de dados.

`antares:users.start`: Adiciona os usuários iniciais no sistema.

`antares:items.update`: Importa e atualiza todos os produtos que existem no Antares ERP.

`antares:prices.update {--code}`: Importa e atualiza todas as tabelas de preços do Antares ERP, ou apenas a tabela com o código informado.

`antares:users.priceup`: Importa e atualiza todos os produtos na tabela de preços de todos os usuários do sistema.
