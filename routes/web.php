<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Webdev\MailController;
use App\Http\Controllers\Webdev\TestController;
use Rap2hpoutre\LaravelLogViewer\LogViewerController;
use App\Http\Controllers\Antares\RetryFailedOrdersController;
use App\Http\Controllers\Antares\RetryUpdateOrderStatusController;

Route::get('/', function () {
   return redirect()->route('scribe');
});

Route::middleware('auth.basic')->group(function () {

   // Logs
   Route::get('logs', [LogViewerController::class, 'index']);

   // Documentação
   Route::view('docs', 'scribe.index')
      ->name('scribe');

   // Dev
   Route::prefix('dev')->name('dev')->group(function () {

      // Mails
      Route::prefix('mail')->name('.mail')->group(function () {

         Route::prefix('myorders')->name('.myorders')->group(function () {

            Route::get('', [MailController::class, 'myorders']);

            Route::get('{id}', [MailController::class, 'myorders'])
               ->name('.id');
         });

         Route::prefix('invoices')->name('.invoices')->group(function () {

            Route::get('', [MailController::class, 'invoices']);

            Route::get('{id}', [MailController::class, 'invoices'])
               ->name('.id');

            Route::get('confirmation', [MailController::class, 'invoiceConfirmation'])
               ->name('.confirmation');

               Route::get('confirmation/{id}', [MailController::class, 'invoiceConfirmation'])
               ->name('.confirmation.id');

            Route::get('rejected', [MailController::class, 'invoiceRejected'])
               ->name('.rejected');

            Route::get('rejected/{id}', [MailController::class, 'invoiceRejected'])
               ->name('.rejected.id');

         });

         Route::prefix('orders')->name('.orders')->group(function () {

            Route::get('sent', [MailController::class, 'orderSent'])
               ->name('.sent');

            Route::get('sent/{id}', [MailController::class, 'orderSent'])
               ->name('.sent.id');

            Route::get('completed', [MailController::class, 'orderCompleted'])
               ->name('.completed');

            Route::get('completed/{id}', [MailController::class, 'orderCompleted'])
               ->name('.completed.id');

            Route::get('cancelled', [MailController::class, 'orderCancelled'])
               ->name('.cancelled');

            Route::get('cancelled/{id}', [MailController::class, 'orderCancelled'])
               ->name('.cancelled.id');

         });

         Route::prefix('account')->name('.account')->group(function () {

            Route::get('invite', [MailController::class, 'accountInvite'])
               ->name('.invite');

            Route::get('invite/{id}', [MailController::class, 'accountInvite'])
               ->name('.invite.id');

            Route::get('recover', [MailController::class, 'accountRecover'])
               ->name('.recover');

            Route::get('recover/{id}', [MailController::class, 'accountRecover'])
               ->name('.recover.id');

         });

         Route::prefix('sale')->name('.sale')->group(function () {

            Route::get('', [MailController::class, 'sale']);

            Route::get('{id}', [MailController::class, 'sale'])
               ->name('.id');

         });

      });

   });

   // Antares ERP
   Route::prefix('antares')->group(function () {

      // Tentar conectar novamente com Antares ERP todos os pedidos com erro.
      Route::get('orders/failed/retry', RetryFailedOrdersController::class);

      Route::get('orders/status/retry', RetryUpdateOrderStatusController::class);
   });

   // Tests
   // Route::get('test', [TestController::class, 'test']);

});
