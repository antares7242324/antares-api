<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MailerAuthController;

Route::post('login', LoginController::class)
    ->name('.login');

Route::post('activate', [AuthController::class, 'activate'])
    ->name('.activate');

Route::post('block', [AuthController::class, 'block'])
    ->name('.block');

Route::post('recover', [AuthController::class, 'recover'])
    ->name('.recover');

// Mailer
Route::prefix('mail')->name('.mail')->group(function() {

    Route::post('recover', [MailerAuthController::class, 'recover'])
        ->name('.recover');

});
