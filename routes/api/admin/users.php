<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserStoreController;
use App\Http\Controllers\MailerAuthController;
use App\Http\Controllers\Users\ShowUserController;
use App\Http\Controllers\Users\ListUsersController;
use App\Http\Controllers\Users\ListUserStatementsController;

// Lista os usuários.
Route::get('', ListUsersController::class)
    ->name('.index');

// Adiciona um usuário
Route::post('', [UserController::class, 'store'])
    ->name('.store');

// Lista os statements de todos os usuários.
Route::get('statements', ListUserStatementsController::class)
    ->name('.statements');

// Single User
Route::prefix('{user}')->group(function () {

    // Detalha um usuário.
    Route::get('', ShowUserController::class)
        ->name('.show');

    // Atualiza um usuário.
    Route::patch('', [UserController::class, 'update'])
        ->name('.update');

    // Mailer
    Route::prefix('mail')->name('.mail')->group(function () {

        Route::post('invite', [MailerAuthController::class, 'invite'])
            ->name('.invite');

    });

    // Stores
    Route::prefix('stores')->name('.stores')->group(function () {

        Route::post('', [UserStoreController::class, 'store'])
            ->name('.store');

        Route::delete('{store}', [UserStoreController::class, 'destroy'])
            ->name('.destroy');

    });

});
