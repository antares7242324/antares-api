<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SettingsController;

Route::get('discounts', [SettingsController::class, 'discounts'])
    ->name('.discounts');