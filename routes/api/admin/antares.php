<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Antares\RetryFailedOrdersController;
use App\Http\Controllers\Antares\RetryUpdateOrderStatusController;

Route::post('orders/failed/retry', RetryFailedOrdersController::class)
    ->name('.orders.failed.retry');

Route::post('orders/status/retry', RetryUpdateOrderStatusController::class)
    ->name('.orders.status.retry');