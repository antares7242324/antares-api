<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;

Route::get('', [ItemController::class, 'index'])
    ->name('.index');

Route::post('', [ItemController::class, 'store'])
    ->name('.store');

Route::get('{item}', [ItemController::class, 'show'])
    ->name('.show');

Route::patch('{item}', [ItemController::class, 'update'])
    ->name('.update');

Route::delete('{item}', [ItemController::class, 'destroy'])
    ->name('.destroy');
