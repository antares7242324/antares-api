<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InvoiceController;

Route::get('', [InvoiceController::class, 'index'])
    ->name('.index');

Route::post('export', [InvoiceController::class, 'export'])
    ->name('.export');

Route::patch('many', [InvoiceController::class, 'updateMany'])
    ->name('.updateMany');

Route::get('{invoice}', [InvoiceController::class, 'show'])
    ->name('.show');

Route::patch('{invoice}', [InvoiceController::class, 'update'])
    ->name('.update');