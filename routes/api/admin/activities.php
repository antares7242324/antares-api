<?php

use App\Http\Controllers\Activities\ListActivitiesController;
use Illuminate\Support\Facades\Route;

Route::get('', ListActivitiesController::class)
    ->name('.index');