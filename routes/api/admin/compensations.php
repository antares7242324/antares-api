<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompensationController;

Route::get('', [CompensationController::class, 'index'])
    ->name('.index');

Route::post('', [CompensationController::class, 'store'])
    ->name('.store');

Route::post('/many', [CompensationController::class, 'storeMany'])
    ->name('.store-many');

Route::get('{compensation}', [CompensationController::class, 'show'])
    ->name('.show');

Route::patch('{compensation}', [CompensationController::class, 'update'])
    ->name('.update');

Route::delete('{compensation}', [CompensationController::class, 'destroy'])
    ->name('.destroy');