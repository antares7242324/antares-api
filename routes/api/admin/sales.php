<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaleController;

Route::post('', [SaleController::class, 'store'])
    ->name('.store');

Route::patch('{sale}', [SaleController::class, 'update'])
    ->name('.update');

Route::delete('{sale}', [SaleController::class, 'destroy'])
    ->name('destroy');