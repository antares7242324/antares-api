<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;

Route::get('', [OrderController::class, 'index'])
    ->name('.index');

Route::post('', [OrderController::class, 'store'])
    ->name('.store');

Route::patch('', [OrderController::class, 'updateMany'])
    ->name('.updateMany');

Route::get('{order}', [OrderController::class, 'show'])
    ->name('.show');

Route::patch('{order}', [OrderController::class, 'update'])
    ->name('.update');

Route::post('{order}/copy', [OrderController::class, 'copy'])
    ->name('.copy');
