<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HuntController;

Route::get('', [HuntController::class, 'index'])
    ->name('.index');