<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PriceController;

Route::get('', [PriceController::class, 'index'])
    ->name('.index');

Route::post('{code_erp}', [PriceController::class, 'update'])
    ->name('.update');
