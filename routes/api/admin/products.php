<?php

use Illuminate\Support\Facades\Route;

// Items
Route::prefix('items')->name('.items')
    ->group(base_path('routes/api/admin/products.items.php'));

// Prices
Route::prefix('prices')->name('.prices')
    ->group(base_path('routes/api/admin/products.prices.php'));
