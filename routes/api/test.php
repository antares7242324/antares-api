<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Tests\TestController;
use App\Http\Controllers\Webdev\UpdateItemsPortal;
use App\Http\Controllers\Tests\CancelOrderTestController;
use App\Http\Controllers\Tests\CreateOrderTestController;
use App\Http\Controllers\Tests\CheckEveryOrderStatusController;

Route::middleware('access.admin')->group(function () {

    // Rota principal de teste.
    Route::get('', TestController::class)->name('test');

    // Criar pedido.
    Route::post('orders/create', CreateOrderTestController::class)
        ->name('test.orders.create');

    // Cancelar pedido.
    Route::post('orders/cancel', CancelOrderTestController::class)
        ->name('test.orders.cancel');

    // Atualizar itens do portal.
    Route::get('portal/items', UpdateItemsPortal::class)
        ->name('test.portal.items');

    // Manualmente invoca o job para verificar status dos pedidos.
    Route::get('portal/orders/status', CheckEveryOrderStatusController::class)
        ->name('test.portal.orders.status');
});