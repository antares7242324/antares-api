<?php

use App\Http\Controllers\MyUser\ListMyStatementsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyUserController;

// Authentication
Route::prefix('auth')->name('auth')
    ->group(base_path('routes/api/client/auth.php'));

// Items
Route::prefix('items')->name('items')
    ->group(base_path('routes/api/client/items.php'));

// My Account
Route::prefix('my')->name('my')->group(function () {

    // Activities
    Route::prefix('activities')->name('.activities')
        ->group(base_path('routes/api/client/my.activities.php'));

    // Big Numbers
    Route::get('bignumbers', [MyUserController::class, 'bigNumbers'])
        ->name('.bignumbers');

    // Compensations
    Route::prefix('compensations')->name('.compensations')
        ->group(base_path('routes/api/client/my.compensations.php'));

    // Invoices
    Route::prefix('invoices')->name('.invoices')
        ->group(base_path('routes/api/client/my.invoices.php'));

    // Items
    Route::prefix('items')->name('.items')
        ->middleware('user.pricelist')
        ->group(base_path('routes/api/client/my.items.php'));

    // Orders
    Route::prefix('orders')->name('.orders')
        ->group(base_path('routes/api/client/my.orders.php'));

    // Price List
    Route::prefix('pricelists')->name('.pricelists')
        ->group(base_path('routes/api/client/my.pricelists.php'));

    // Statements
    Route::get('statements', ListMyStatementsController::class)
        ->name('.statements');

    // User
    Route::prefix('user')->name('.user')->group(function () {

        Route::get('', [MyUserController::class, 'show'])
            ->name('.show');

    });
});

// Sales
Route::prefix('sales')->name('sales')
    ->group(base_path('routes/api/client/sales.php'));
