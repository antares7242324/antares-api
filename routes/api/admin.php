<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\Webdev\ShowConfigController;

Route::middleware('access.admin')->group(function () {

    // Activities
    Route::prefix('activities')->name('activities')
        ->group(base_path('routes/api/admin/activities.php'));

    // Antares configurations and processes.
    Route::prefix('antares')->name('antares')
        ->group(base_path('routes/api/admin/antares.php'));

    // Big Numbers
    Route::get('bignumbers', [InformationController::class, 'bignumbers'])
        ->name('.bignumbers');

    // Hunts
    Route::prefix('hunts')->name('hunts')
        ->group(base_path('routes/api/admin/hunts.php'));

    // Invoices
    Route::prefix('invoices')->name('invoices')
        ->group(base_path('routes/api/admin/invoices.php'));

    // Orders
    Route::prefix('orders')->name('orders')
        ->group(base_path('routes/api/admin/orders.php'));

    // Products
    Route::prefix('products')->name('products')
        ->group(base_path('routes/api/admin/products.php'));

    // Sales
    Route::prefix('sales')->name('sales')
        ->group(base_path('routes/api/admin/sales.php'));

    // Settings
    Route::prefix('settings')->name('settings')
        ->group(base_path('routes/api/admin/settings.php'));

    // Users
    Route::prefix('users')->name('users')
        ->group(base_path('routes/api/admin/users.php'));

    // Config data
    Route::prefix('config')->name('config')
        ->group(function () {
            Route::get('', ShowConfigController::class)
                ->name('.index');
        });

});

// Compensations
// Por algum motivo insano esse foi o único jeito de agrupar essas rotas,
// mesmo que agrupe pelo prefixo 'users' e separe em dois grupos com
// middleware diferentes, para de reconhecer esse grupo.
// Por algum motivo simplesmente não funciona fazer o agrupamento com 'user/'
// sob circunstância alguma. Não faz sentido.
// O ideal seria a rota ser 'users/compensations'.
Route::prefix('compensations')->name('compensations')
    ->middleware('access.logistics')
    ->group(base_path('routes/api/admin/compensations.php'));