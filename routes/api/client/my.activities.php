<?php

use App\Http\Controllers\MyActivityController;
use Illuminate\Support\Facades\Route;

Route::get('', [MyActivityController::class, 'index'])
    ->name('.index');