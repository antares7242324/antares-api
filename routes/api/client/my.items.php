<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyItemController;

Route::get('', [MyItemController::class, 'index'])
    ->name('.index');

Route::get('{price}', [MyItemController::class, 'show'])
    ->name('.show');
