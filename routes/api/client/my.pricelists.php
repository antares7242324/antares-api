<?php

use App\Http\Controllers\MyPrices\ShowMyPriceListController;
use Illuminate\Support\Facades\Route;

Route::get('', ShowMyPriceListController::class)
    ->name('.show');