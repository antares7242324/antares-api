<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaleController;

Route::get('', [SaleController::class, 'index'])
    ->name('.index');

Route::get('{sale}', [SaleController::class, 'show'])
    ->name('.show');
