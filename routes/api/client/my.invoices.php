<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyInvoiceController;
use App\Http\Controllers\MyInvoiceMediaController;
use App\Http\Controllers\MyInvoices\LoadInvoiceItemsHuntController;
use App\Http\Controllers\MyInvoices\CheckInvoiceItemCodesController;
use App\Http\Controllers\MyInvoices\StoreInvoiceController;

Route::get('', [MyInvoiceController::class, 'index'])
    ->name('.index');

// Adiciona uma nota fiscal para o usuário logado.
Route::post('', StoreInvoiceController::class)->name('.store');

Route::get('{invoice}', [MyInvoiceController::class, 'show'])
    ->name('.show');

Route::prefix('media')->name('.media')->group(function () {

    Route::post('', [MyInvoiceMediaController::class, 'upload'])
        ->name('.upload');

    Route::get('{media}/load', LoadInvoiceItemsHuntController::class)
        ->name('.load');

    // Verifica se os itens da nota fiscal possuem os códigos informados.
    Route::get('{media}/check', CheckInvoiceItemCodesController::class)
        ->name('.check');
});
