<?php

use App\Http\Controllers\MyCompensationController;
use Illuminate\Support\Facades\Route;

Route::get('', [MyCompensationController::class, 'index'])
    ->name('.index');

Route::get('{compensation}', [MyCompensationController::class, 'show'])
    ->name('.show');
