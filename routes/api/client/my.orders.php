<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyOrderController;
use App\Http\Controllers\MyOrders\CreateMyOrderController;
use App\Http\Controllers\MyOrders\ListMyOrdersController;
use App\Http\Controllers\MyOrders\SyncMyOrderItemsController;
use App\Http\Controllers\MyOrders\UpdateMyOrderController;

Route::get('', ListMyOrdersController::class)
    ->name('.index');

Route::post('', CreateMyOrderController::class)
    ->name('.store');

Route::patch('', [MyOrderController::class, 'updateMany'])
    ->name('.updateMany');

Route::post('export', [MyOrderController::class, 'exportMany'])
    ->name('.exportMany');

Route::get('{order}', [MyOrderController::class, 'show'])
    ->name('.show')
    ->middleware('ownership:order');

Route::post('{order}', UpdateMyOrderController::class)
    ->name('.update')
    ->middleware('ownership:order');

Route::post('{order}/copy', [MyOrderController::class, 'copy'])
    ->name('.copy')
    ->middleware('ownership:order');

Route::post('{order}/items', SyncMyOrderItemsController::class)
    ->name('.items.sync')
    ->middleware('ownership:order');
