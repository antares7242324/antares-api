<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Items\ShowItemController;
use App\Http\Controllers\Items\ListItemsController;
use App\Http\Controllers\Items\ShowItemCodeController;
use App\Http\Controllers\Items\ListItemCodesController;
use App\Http\Controllers\Items\StoreItemCodeController;
use App\Http\Controllers\Items\DeleteItemCodeController;
use App\Http\Controllers\Items\ToggleConfirmationItemCodesController;

// Codes
Route::prefix('codes')->name('.codes')->group(function () {

    Route::get('', ListItemCodesController::class)
        ->name('.index');

    Route::post('', StoreItemCodeController::class)
        ->name('.store');

    Route::post('confirmation', ToggleConfirmationItemCodesController::class)
        ->name('.confirmation');

    Route::get('{userItem}', ShowItemCodeController::class)
        ->withTrashed()
        ->name('.show');

    Route::delete('{userItem}', DeleteItemCodeController::class)
        ->name('.delete');

});

Route::get('', ListItemsController::class)
    ->name('.index');

Route::get('{item}', ShowItemController::class)
    ->name('.show');
