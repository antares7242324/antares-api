<?php

use Illuminate\Support\Facades\Route;

// Authentication
// Route::prefix('auth')->group(function() {
//     Route::post('login', [AuthController::class, 'login'])
//         ->withoutMiddleware([PreventBlockedUser::class, PreventInactiveUser::class]);

//     Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
// });

// Public
Route::prefix('auth')->name('auth')
    ->group(base_path('routes/api/public/auth.php'));

// App
Route::middleware('app')->group(function () {

    // Admin
    Route::prefix('admin')
        ->group(base_path('routes/api/admin.php'));

    // Test
    Route::prefix('test')
        ->group(base_path('routes/api/test.php'));

    // Client
    Route::prefix('')
        ->group(base_path('routes/api/client.php'));

});
