<?php

use App\Models\Hunt\Invoice;
use App\Models\Hunt\InvoiceItem;
use App\Models\Product\Item;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignId('invoice_id')
                ->constrained(Invoice::TABLE)
                ->onDelete('cascade')
                ->comment("The invoice foreign key from ".Invoice::TABLE." table.");

            $table->foreignId('item_id')
                ->constrained(Item::TABLE)
                ->onDelete('cascade')
                ->comment("The item foreign key from ".Item::TABLE." table.");

            $table->unsignedBigInteger('quantity')
                ->default(0)
                ->comment("The quantity of same items in the invoice.");

            $table->decimal('unit_price')
                ->default(0.00)
                ->comment("The item's unit price in the invoice.");

            $table->decimal('unit_discounted_price')
                ->default(0.00)
                ->comment("The item's unit discounted price in the invoice.");

            $table->decimal('subtotal')
                ->default(0.00)
                ->comment("The summed price of all items based on their quantity in the invoice.");

            $table->decimal('refund')
                ->default(0.00)
                ->comment("The refunded amount from subtotal based on the user discount in the invoice.");

            $table->string('description')
                ->comment("The item's description in the invoice.");

            $table->string('code')
                ->comment("The item's code in the invoice.");

            $table->string('sku')
                ->nullable()
                ->comment("The item's sku in the invoice.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(InvoiceItem::TABLE);
    }
};
