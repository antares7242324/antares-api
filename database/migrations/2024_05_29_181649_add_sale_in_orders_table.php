<?php

use App\Models\Sale\Sale;
use App\Models\Order\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn(app(Order::class)->getTable(), 'sale_id', function (Blueprint $table) {
            $table->foreignIdFor(Sale::class, 'sale_id')
                ->after('current_discount')
                ->nullable()
                ->comment("The sale foreign key from ".Sale::TABLE." table.");
        });

        Schema::whenTableDoesntHaveColumn(app(Order::class)->getTable(), 'sale_discount', function (Blueprint $table) {
            $table->unsignedDecimal('sale_discount')
                ->after('sale_id')
                ->default(0.00)
                ->comment("The sale's discount value. Only positive values are allowed.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn(app(Order::class)->getTable(), 'sale_discount', function (Blueprint $table) {
            $table->dropColumn('sale_discount');
        });

        Schema::whenTableHasColumn(app(Order::class)->getTable(), 'sale_id', function (Blueprint $table) {
            $table->dropForeign(['sale_id']);
            $table->dropColumn('sale_id');
        });
    }
};
