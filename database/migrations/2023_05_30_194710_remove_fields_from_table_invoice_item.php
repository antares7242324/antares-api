<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_item', function (Blueprint $table) {
            $table->dropColumn('unit_discounted_price');

            $table->dropColumn('refund');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_item', function (Blueprint $table) {
            $table->unsignedBigInteger('unit_discounted_price')
                ->after('unit_price')
                ->default(0)
                ->comment("The item's unit discounted price in the invoice.");

            $table->unsignedBigInteger('refund')
                ->after('subtotal')
                ->default(0)
                ->comment("The refunded amount from subtotal based on the user discount in the invoice.");
        });
    }
};
