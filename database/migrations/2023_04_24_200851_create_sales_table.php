<?php

use App\Models\Sale\Sale;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Sale::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('title')
                ->comment("The sale title.");

            $table->decimal('discount')
                ->default(0.00)
                ->comment("The extra discount that will be applied to any orders using the sale.");

            $table->date('start_at')
                ->nullable()
                ->comment("The starting date for the sale.");

            $table->date('end_at')
                ->nullable()
                ->comment("The ending date for the sale.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Sale::TABLE);
    }
};
