<?php

use App\Models\Hunt\Invoice;
use App\Models\Transaction\CreditInvoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->dropForeign(['invoice_id']);
            $table->dropColumn('invoice_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->foreignId('invoice_id')
                ->constrained(Invoice::TABLE)
                ->onDelete('cascade')
                ->comment("The invoice foreign key from ".Invoice::TABLE." table.");
        });
    }
};
