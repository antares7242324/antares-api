<?php

use App\Models\Account\User;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->foreignId('price_list_id')
                ->after('current_discount')
                ->nullable()
                ->constrained(PriceList::TABLE)
                ->onDelete('cascade')
                ->comment("The price list foreign key from ".PriceList::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->dropForeign(['price_list_id']);
            $table->dropColumn('price_list_id');
        });
    }
};
