<?php

use App\Libraries\InvoiceStatus;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Invoice::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('status')
                ->default(InvoiceStatus::PENDING)
                ->comment("The invoice current status.");

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The store foreign key from ".User::TABLE." table.");

            $table->decimal('average_discount')
                ->default(0.00)
                ->comment("The average discount based on all discounts given by the user to their client.");

            $table->decimal('amount')
                ->default(0.00)
                ->comment("The amount of value of all items' prices inside invoice.");

            $table->decimal('refund')
                ->default(0.00)
                ->comment("The refund value based on the average discount inside invoice.");

            $table->string('number')
                ->comment("The number from Brazil's invoice (NF).");

            $table->string('series')
                ->comment("The series number from Brazil's invoice (NF).");

            $table->string('cnae')
                ->comment("The Brazil's CNAE from the invoice's (NF) issuer.");

            $table->string('uf')
                ->comment("The federative unit from Brazil (UF) from recipient.");

            $table->string('municipality')
                ->comment("The municipality from recipient.");

            $table->date('issuance_at')
                ->nullable()
                ->comment("The date when the invoice was issued.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Invoice::TABLE);
    }
};
