<?php

use App\Models\Order\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->bigInteger('balance')
                ->after('current_discount')
                ->default(0)
                ->comment("The value added to the user's balance based on the refund gathered from the invoice.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->dropColumn('balance');
        });
    }
};
