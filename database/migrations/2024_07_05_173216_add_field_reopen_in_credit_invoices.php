<?php

use App\Models\Transaction\CreditInvoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn(app(CreditInvoice::class)->getTable(), 'reopened',
            function (Blueprint $table) {
                $table->boolean('reopened')
                    ->after('id')
                    ->default(false)
                    ->comment("Crédito é de uma nota fiscal reaberta em vez de upload.");
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn(app(CreditInvoice::class)->getTable(), 'reopened',
            function (Blueprint $table) {
                $table->dropColumn('reopened');
            }
        );
    }
};
