<?php

use App\Models\File\Media;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Media::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('name')
                ->comment("The uploaded file name.");

            $table->string('extension')
                ->comment("The uploaded file extension.");

            $table->string('type')
                ->comment('The uploaded file type.');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Media::TABLE);
    }
};
