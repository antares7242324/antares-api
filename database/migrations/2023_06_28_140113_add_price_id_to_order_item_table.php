<?php

use App\Models\Product\Price;
use App\Models\Order\OrderItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(OrderItem::TABLE, function (Blueprint $table) {
            $table->foreignId('price_id')
                ->after('item_id')
                ->nullable()
                ->constrained(Price::TABLE)
                ->comment("The price foreign key from ".Price::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(OrderItem::TABLE, function (Blueprint $table) {
            $table->dropConstrainedForeignId('price_id');
        });
    }
};
