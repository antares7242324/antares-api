<?php

use App\Models\Order\OrderItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(OrderItem::TABLE, function (Blueprint $table) {
            $table->unsignedBigInteger('unit_original_price')
                ->default(0)
                ->change();

            $table->unsignedBigInteger('unit_discount_price')
                ->default(0)
                ->change();

            $table->unsignedBigInteger('subtotal')
                ->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(OrderItem::TABLE, function (Blueprint $table) {
            $table->decimal('unit_original_price')
                ->default(0.00)
                ->change();

            $table->decimal('unit_discount_price')
                ->default(0.00)
                ->change();

            $table->decimal('subtotal')
                ->default(0.00)
                ->change();
        });
    }
};
