<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn('price_lists', 'last_updated_at',
            function (Blueprint $table) {
                $table->timestamp('last_updated_at')
                    ->after('active_at')
                    ->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn('price_lists', 'last_updated_at',
            function (Blueprint $table) {
                $table->dropColumn('last_updated_at');
            }
        );
    }
};
