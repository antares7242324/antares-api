<?php

use App\Models\Hunt\Invoice;
use App\Models\Transaction\CreditInvoice;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->dropForeign(['credit_invoice_id']);
            $table->dropColumn('credit_invoice_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->foreignId('credit_invoice_id')
                ->after('amount')
                ->nullable()
                ->constrained(CreditInvoice::TABLE)
                ->onDelete('cascade')
                ->comment("The credit invoice foreign key from ".CreditInvoice::TABLE." table.");
        });
    }
};
