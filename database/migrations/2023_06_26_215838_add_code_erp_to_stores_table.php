<?php

use App\Models\Account\Store;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Store::TABLE, function (Blueprint $table) {
            $table->string('code_erp')
                ->after('cnpj')
                ->nullable()
                ->comment("The ERP code for Antares.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Store::TABLE, function (Blueprint $table) {
            $table->dropColumn('code_erp');
        });
    }
};
