<?php

use App\Models\Hunt\Invoice;
use Illuminate\Support\Facades\Schema;
use App\Models\Transaction\CreditInvoice;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->foreignId('invoice_id')
                ->after('id')
                ->constrained(Invoice::TABLE)
                ->comment("The order foreign key from ".Invoice::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->dropForeign(['invoice_id']);
            $table->dropColumn('invoice_id');
        });
    }
};
