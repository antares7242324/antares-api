<?php

use App\Models\Account\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->unsignedDecimal('main_discount')
                ->after('email_verified_at')
                ->default(0.00)
                ->comment("The standard discount granted by Antares.");

            $table->unsignedDecimal('current_discount')
                ->after('main_discount')
                ->default(0.00)
                ->comment("The extra discount set by an admin based on the average discount from the user's hunts.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->dropColumn('main_discount');
            $table->dropColumn('current_discount');
        });
    }
};
