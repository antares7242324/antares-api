<?php

use App\Models\Product\Price;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Price::TABLE, function (Blueprint $table) {
            $table->date('last_update_at')
                ->after('end_at')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Price::TABLE, function (Blueprint $table) {
            $table->dropColumn('last_update_at');
        });
    }
};
