<?php

use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Transaction\DebitOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DebitOrder::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->foreignId('order_id')
                ->constrained(Order::TABLE)
                ->onDelete('cascade')
                ->comment("The order foreign key from ".Order::TABLE." table.");

            $table->decimal('amount')
                ->comment("The amount debited from user's available balance.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DebitOrder::TABLE);
    }
};
