<?php

use App\Models\Product\Price;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Price::TABLE, function (Blueprint $table) {
            $table->date('start_at')
                ->after('amount')
                ->nullable()
                ->comment("Start date of price.");

            $table->date('end_at')
                ->after('start_at')
                ->nullable()
                ->comment("End date of price.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Price::TABLE, function (Blueprint $table) {
            $table->dropColumn('start_at');
            $table->dropColumn('end_at');
        });
    }
};
