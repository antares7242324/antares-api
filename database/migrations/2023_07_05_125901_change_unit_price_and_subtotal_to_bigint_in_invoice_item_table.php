<?php

use App\Models\Hunt\InvoiceItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->unsignedBigInteger('unit_price')
                ->default(0)
                ->change();

            $table->unsignedBigInteger('subtotal')
                ->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->decimal('unit_price')
                ->default(0.00)
                ->change();

            $table->decimal('subtotal')
                ->default(0.00)
                ->change();
        });
    }
};
