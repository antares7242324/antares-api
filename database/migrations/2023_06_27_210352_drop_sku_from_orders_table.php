<?php

use App\Models\Product\Item;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Item::TABLE, function (Blueprint $table) {
            $table->dropColumn('sku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Item::TABLE, function (Blueprint $table) {
            $table->string('sku')
                ->after('name')
                ->nullable()
                ->comment("The SKU of the item.");
        });
    }
};
