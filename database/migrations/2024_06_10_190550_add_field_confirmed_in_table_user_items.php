<?php

use App\Models\Product\UserItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn(app(UserItem::class)->getTable(), 'confirmed',
            function (Blueprint $table) {
                $table->boolean('confirmed')
                    ->after('item_id')
                    ->default(false)
                    ->comment("Informa se os administradores da Antares verificaram o cadastro da equivalência do item pelo distribuidor.");
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn(app(UserItem::class)->getTable(), 'confirmed',
            function (Blueprint $table) {
                $table->dropColumn('confirmed');
            }
        );
    }
};
