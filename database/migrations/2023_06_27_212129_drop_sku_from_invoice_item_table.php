<?php

use App\Models\Hunt\InvoiceItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->dropColumn('sku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->string('sku')
                ->after('code')
                ->nullable()
                ->comment("The item's sku in the invoice.");
        });
    }
};
