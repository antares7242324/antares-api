<?php

use App\Models\Hunt\Invoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->dropColumn('refund');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->decimal('refund')
                ->default(0.00)
                ->comment("The refund value based on the average discount inside invoice.");
        });
    }
};
