<?php

use App\Models\Hunt\InvoiceItem;
use App\Models\Product\UserItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->foreignIdFor(UserItem::class, 'user_item_id')
                ->after('item_id')
                ->nullable()
                ->constrained(UserItem::TABLE)
                ->comment("The user item foreign key from ".UserItem::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->dropForeign(['user_item_id']);
            $table->dropColumn('user_item_id');
        });
    }
};
