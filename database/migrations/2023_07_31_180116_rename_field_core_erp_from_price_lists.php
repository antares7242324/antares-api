<?php

use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(PriceList::TABLE, function (Blueprint $table) {
            $table->renameColumn('core_erp', 'code_erp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(PriceList::TABLE, function (Blueprint $table) {
            $table->renameColumn('code_erp', 'core_erp');
        });
    }
};
