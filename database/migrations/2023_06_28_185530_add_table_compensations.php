<?php

use App\Models\Account\User;
use Illuminate\Support\Facades\Schema;
use App\Models\Transaction\Compensation;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Compensation::TABLE, function (Blueprint $table) {
            $table->id();

            $table->bigInteger('amount')
                ->default(0)
                ->comment("The compensation amount in cents. Positive is a credit, negative is a debit.");

            $table->bigInteger('current_balance')
                ->default(0)
                ->comment("The current balance in cents. It`s a snapshot of the user`s available_balance at the time of the compensation.");

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->comment("The user foreign key from " . User::TABLE . " table.");

            $table->foreignId('creator_id')
                ->constrained(User::TABLE)
                ->comment("The creator foreign key from " . User::TABLE . " table.");

            $table->text('reason')
                ->comment("The reason for the compensation.");

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Compensation::TABLE);
    }
};
