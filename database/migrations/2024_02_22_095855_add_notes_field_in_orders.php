<?php

use App\Models\Order\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->longText('system_notes')
                ->after('issuance_at')
                ->nullable()
                ->comment("Mensagem de notificação criada pelo sistema para o usuário.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->dropColumn('system_notes');
        });
    }
};
