<?php

use App\Models\Order\OrderItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(OrderItem::TABLE, function (Blueprint $table) {
            $table->decimal('discount')
                ->after('subtotal')
                ->default(0.00)
                ->comment("Discount value applied to the item.");

            $table->unsignedBigInteger('debit')
                ->after('discount')
                ->default(0)
                ->comment("Debit obtained from item's discount.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(OrderItem::TABLE, function (Blueprint $table) {
            $table->dropColumn('discount');

            $table->dropColumn('debit');
        });
    }
};
