<?php

use App\Models\Transaction\DebitOrder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DebitOrder::TABLE, function (Blueprint $table) {
            $table->bigInteger('current_balance')
                ->after('amount')
                ->default(0)
                ->comment("The user's available balance at the moment of this registry without the debit.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DebitOrder::TABLE, function (Blueprint $table) {
            $table->dropColumn('current_balance');
        });
    }
};
