<?php

use App\Models\Account\User;
use App\Models\Product\Item;
use App\Models\Product\UserItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserItem::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(User::class)
                ->comment('ID do usuário que cadastrou o código do item.');

            $table->foreignIdFor(Item::class)
                ->comment('ID do item no sistema.');

            $table->string('code')
                ->comment('Código do item no sistema do usuário.');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserItem::TABLE);
    }
};
