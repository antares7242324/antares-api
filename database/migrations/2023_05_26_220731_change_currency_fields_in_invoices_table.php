<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedBigInteger('average_discount')
                ->default(0)
                ->change();

            $table->unsignedBigInteger('amount')
                ->default(0)
                ->change();

            $table->unsignedBigInteger('refund')
                ->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->decimal('average_discount')
                ->default(0.00)
                ->change();

            $table->decimal('amount')
                ->default(0.00)
                ->change();

            $table->decimal('refund')
                ->default(0.00)
                ->change();
        });
    }
};
