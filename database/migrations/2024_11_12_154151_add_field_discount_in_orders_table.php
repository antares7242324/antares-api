<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn('orders', 'discount',
            function (Blueprint $table) {
                $table->decimal('discount', 10, 2)
                    ->after('debit')
                    ->default(0);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn('orders', 'discount',
            function (Blueprint $table) {
                $table->dropColumn('discount');
            }
        );
    }
};
