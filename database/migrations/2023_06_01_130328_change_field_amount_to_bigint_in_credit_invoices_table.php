<?php

use App\Models\Transaction\CreditInvoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->unsignedBigInteger('amount')
                ->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->decimal('amount')
                ->default(0.00)
                ->change();
        });
    }
};
