<?php

use App\Models\Order\Order;
use App\Models\Order\OrderItem;
use App\Models\Product\Item;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OrderItem::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignId('order_id')
                ->constrained(Order::TABLE)
                ->onDelete('cascade')
                ->comment("The order foreign key from ".Order::TABLE." table.");

            $table->foreignId('item_id')
                ->constrained(Item::TABLE)
                ->onDelete('cascade')
                ->comment("The item foreign key from ".Item::TABLE." table.");

            $table->unsignedBigInteger('quantity')
                ->default(0)
                ->comment("The quantity of items in the order.");

            $table->decimal('unit_original_price')
                ->default(0.00)
                ->comment("The original price of an item's unit in the order.");

            $table->decimal('unit_discount_price')
                ->default(0.00)
                ->comment("The discounted price of an item's unit in the order.");

            $table->decimal('subtotal')
                ->default(0.00)
                ->comment("The summed price of all units based on discounted price of the item in the order.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(OrderItem::TABLE);
    }
};
