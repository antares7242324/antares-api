<?php

use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use Illuminate\Support\Facades\Schema;
use App\Models\Transaction\CreditInvoice;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CreditInvoice::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->foreignId('invoice_id')
                ->constrained(Invoice::TABLE)
                ->onDelete('cascade')
                ->comment("The invoice foreign key from ".Invoice::TABLE." table.");

            $table->decimal('amount')
                ->comment("The amount credited to user's available balance.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CreditInvoice::TABLE);
    }
};
