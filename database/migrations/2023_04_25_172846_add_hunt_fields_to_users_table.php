<?php

use App\Models\Account\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->decimal('average_discount')
                ->after('current_discount')
                ->default(0.00)
                ->comment("The average discount based on the user's discounts from their invoices.");

            $table->decimal('available_balance')
                ->after('last_status')
                ->default(0.00)
                ->comment("The user's available balance to use in new orders.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->dropColumn('available_balance');
            $table->dropColumn('average_discount');
        });
    }
};
