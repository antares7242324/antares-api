<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Transaction\DebitCanceledInvoice;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn(
            app(DebitCanceledInvoice::class)->getTable(), 'type',
            function (Blueprint $table) {
                $table->string('type')
                    ->after('current_balance')
                    ->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn(
            app(DebitCanceledInvoice::class)->getTable(), 'type',
            function (Blueprint $table) {
                $table->dropColumn('type');
            }
        );
    }
};
