<?php

use App\Models\Order\Order;
use App\Models\Transaction\DebitOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DebitOrder::TABLE, function (Blueprint $table) {
            $table->foreignId('order_id')
                ->after('id')
                ->constrained(Order::TABLE)
                ->comment("The order foreign key from ".Order::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DebitOrder::TABLE, function (Blueprint $table) {
            $table->dropForeign(['order_id']);
            $table->dropColumn('order_id');
        });
    }
};
