<?php

use App\Models\Order\Order;
use App\Models\Account\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_canceled_orders', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->foreignId('order_id')
                ->constrained(Order::TABLE)
                ->comment("The order foreign key from ".Order::TABLE." table.");

            $table->bigInteger('amount')
                ->default(0)
                ->comment("The amount credited to user's available balance.");

            $table->bigInteger('current_balance')
                ->default(0)
                ->comment("The user's available balance at the moment of this registry without the credit.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_canceled_orders');
    }
};
