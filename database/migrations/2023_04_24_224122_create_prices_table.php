<?php

use App\Models\Product\Item;
use App\Models\Product\Price;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Price::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignId('item_id')
                ->constrained(Item::TABLE)
                ->onDelete('cascade')
                ->comment("The price list foreign key from ".Item::TABLE." table.");

            $table->foreignId('price_list_id')
                ->constrained(PriceList::TABLE)
                ->onDelete('cascade')
                ->comment("The price list foreign key from ".PriceList::TABLE." table.");

            $table->decimal('amount')
                ->default(0.00)
                ->comment("The amount in R$ of the item's price.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Price::TABLE);
    }
};
