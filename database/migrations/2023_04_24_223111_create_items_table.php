<?php

use App\Models\Product\Item;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Item::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('name')
                ->comment("The description of the item");

            $table->string('sku')
                ->comment("The SKU of the item.");

            $table->string('code')
                ->nullable()
                ->comment("The item code reffered in Brazil's invoices (NFs).");

            $table->boolean('hunt')
                ->default(true)
                ->comment("Checks if the item can be used in a hunt or not.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Item::TABLE);
    }
};
