<?php

use App\Models\File\Media;
use App\Models\Hunt\Invoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->foreignId('media_id')
                ->after('user_id')
                ->constrained(Media::TABLE)
                ->onDelete('cascade')
                ->comment("The media foreign key from ".Media::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->dropForeign(['media_id']);
            $table->dropColumn('media_id');
        });
    }
};
