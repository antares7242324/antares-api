<?php

use App\Models\Sale\Sale;
use App\Models\Account\User;
use App\Models\Sale\SaleUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SaleUser::TABLE, function (Blueprint $table) {
            $table->id();

            $table->foreignId('sale_id')
                ->constrained(Sale::TABLE)
                ->onDelete('cascade')
                ->comment("The sale foreign key from ".Sale::TABLE." table.");

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(SaleUser::TABLE);
    }
};
