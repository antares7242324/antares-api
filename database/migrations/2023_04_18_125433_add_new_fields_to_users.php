<?php

use App\Libraries\UserRole;
use App\Models\Account\User;
use App\Libraries\UserStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->string('role')
                ->after('id')
                ->default(UserRole::CLIENT)
                ->comment("The user role used to check permissions.");

            $table->string('status')
                ->after('email_verified_at')
                ->default(UserStatus::INACTIVE)
                ->comment("The user status to check for app access.");

            $table->string('last_status')
                ->after('status')
                ->nullable()
                ->comment("The last user status set whenever it's changed.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::TABLE, function (Blueprint $table) {
            $table->dropColumn('last_status');
            $table->dropColumn('status');
            $table->dropColumn('role');
        });
    }
};
