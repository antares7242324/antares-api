<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::whenTableDoesntHaveColumn('orders', 'debit',
            function (Blueprint $table) {
                $table->unsignedBigInteger('debit')
                    ->after('current_discount')
                    ->default(0);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::whenTableHasColumn('orders', 'debit',
            function (Blueprint $table) {
                $table->dropColumn('debit');
            }
        );
    }
};
