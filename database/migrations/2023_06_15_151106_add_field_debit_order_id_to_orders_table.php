<?php

use App\Models\Order\Order;
use App\Models\Transaction\DebitOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->foreignId('debit_order_id')
                ->after('current_discount')
                ->nullable()
                ->constrained(DebitOrder::TABLE)
                ->onDelete('cascade')
                ->comment("The debit order foreign key from ".DebitOrder::TABLE." table.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->dropForeign(['debit_order_id']);
            $table->dropColumn('debit_order_id');
        });
    }
};
