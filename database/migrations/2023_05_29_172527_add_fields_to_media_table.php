<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->string('storage')
                ->after('type')
                ->comment('The storage name, usually "private" for local storage, or the name of a cloudservice.');

            $table->string('path')
                ->after('storage')
                ->comment('The file path to the media.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn('storage');

            $table->dropColumn('path');
        });
    }
};
