<?php

use App\Models\Order\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->timestamp('issuance_at')
                ->after('description')
                ->nullable()
                ->comment('Data de emissão do pedido.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->dropColumn('issuance_at');
        });
    }
};
