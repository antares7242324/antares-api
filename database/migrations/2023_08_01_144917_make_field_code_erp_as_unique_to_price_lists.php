<?php

use App\Models\Product\PriceList;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(PriceList::TABLE, function (Blueprint $table) {
            $table->string('code_erp')
                ->unique()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(PriceList::TABLE, function (Blueprint $table) {
            $table->dropUnique(['code_erp']);
        });
    }
};
