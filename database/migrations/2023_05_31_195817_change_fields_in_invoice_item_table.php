<?php

use App\Models\Hunt\InvoiceItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->unsignedBigInteger('unit_cost')
                ->after('unit_price')
                ->default(0)
                ->comment("The item's cost value at the moment of invoice upload.");

            $table->unsignedBigInteger('refund')
                ->after('unit_price')
                ->default(0)
                ->comment("The refund given to the distributor based on the hunt_discount value.");

            $table->unsignedDecimal('discount')
                ->after('refund')
                ->default(0.00)
                ->comment("The discount given by invoice's distributor.");

            $table->unsignedDecimal('hunt_discount')
                ->after('discount')
                ->default(0.00)
                ->comment("The received discount from the hunting based on the discount given by the distributor.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(InvoiceItem::TABLE, function (Blueprint $table) {
            $table->dropColumn('unit_cost');
            $table->dropColumn('refund');
            $table->dropColumn('discount');
            $table->dropColumn('hunt_discount');
        });
    }
};
