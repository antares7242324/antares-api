<?php

use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(PriceList::TABLE, function (Blueprint $table) {
            $table->boolean('active')
                ->after('id')
                ->default(true)
                ->comment("Defines if price list is active or not.");

            $table->date('start_at')
                ->after('name')
                ->nullable()
                ->comment("Start date of price list.");

            $table->date('end_at')
                ->after('start_at')
                ->nullable()
                ->comment("End date of price list.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(PriceList::TABLE, function (Blueprint $table) {
            $table->dropColumn('active');
            $table->dropColumn('start_at');
            $table->dropColumn('end_at');
        });
    }
};
