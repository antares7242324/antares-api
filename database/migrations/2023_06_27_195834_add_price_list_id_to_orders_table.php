<?php

use App\Models\Order\Order;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->foreignId('price_list_id')
                ->after('store_id')
                ->nullable()
                ->constrained(PriceList::TABLE)
                ->commment("Relationship with table " . PriceList::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Order::TABLE, function (Blueprint $table) {
            $table->dropConstrainedForeignId('price_list_id');
        });
    }
};
