<?php

use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Account\Store;
use App\Libraries\OrderStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Order::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('code')
                ->comment("The code number written by the user to identify the order.");

            $table->string('status')
                ->default(OrderStatus::DRAFT)
                ->comment("The order current status.");

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->foreignId('store_id')
                ->constrained(Store::TABLE)
                ->onDelete('cascade')
                ->comment("The store foreign key from ".Store::TABLE." table.");

            $table->decimal('main_discount')
                ->default(0.00)
                ->comment("The standard discount granted by Antares when the order was made.");

            $table->decimal('current_discount')
                ->default(0.00)
                ->comment("The extra discount from the user when the order was made.");

            $table->string('payment_method')
                ->nullable()
                ->comment("The payment method of the order.");

            $table->text('description')
                ->nullable()
                ->comment("The description written by the user for the order.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Order::TABLE);
    }
};
