<?php

use App\Models\Activity;
use App\Models\Account\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Activity::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('action')
                ->comment('The action type.');

            $table->unsignedBigInteger('responsible_id')
                ->nullable()
                ->comment("The user responsible for the action if needed.");

            $table->string('restriction')
                ->nullable()
                ->comment('The restriction type. Used to determine what user roles can view the activity.');

            $table->string('loggable_type')
                ->nullable()
                ->comment("Model class name or something similar if needed.");

            $table->unsignedBigInteger('loggable_id')
                ->nullable()
                ->commend("Model id or something similar if needed.");

            $table->foreignId('user_id')
                ->nullable()
                ->constrained(User::TABLE)
                ->comment("The user who performed the action if needed.");

            $table->json('data')
                ->nullable()
                ->comment("The data at the moment when action was performed.");

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Activity::TABLE);
    }
};
