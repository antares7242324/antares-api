<?php

use App\Models\Hunt\Invoice;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->dropColumn('average_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Invoice::TABLE, function (Blueprint $table) {
            $table->decimal('average_discount')
                ->default(0.00)
                ->comment("The average discount based on all discounts given by the user to their client.");
        });
    }
};
