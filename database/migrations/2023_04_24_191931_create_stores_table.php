<?php

use App\Models\Account\User;
use App\Models\Account\Store;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Store::TABLE, function (Blueprint $table) {
            $table->id();

            $table->string('name')
                ->comment("The name of the store.");

            $table->string('cnpj')
                ->comment("The CNPJ document number.");

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Store::TABLE);
    }
};
