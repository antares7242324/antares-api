<?php

use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_canceled_invoices', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->constrained(User::TABLE)
                ->onDelete('cascade')
                ->comment("The user foreign key from ".User::TABLE." table.");

            $table->foreignId('invoice_id')
                ->constrained(Invoice::TABLE)
                ->onDelete('cascade')
                ->comment("The invoice foreign key from ".Invoice::TABLE." table.");

            $table->decimal('amount')
                ->comment("The amount debited to user's available balance.");

            $table->bigInteger('current_balance')
                ->default(0)
                ->comment("The user's available balance at the moment of this registry without the debit.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debit_canceled_invoices');
    }
};
