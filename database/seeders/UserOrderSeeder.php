<?php

namespace Database\Seeders;

use App\Libraries\OrderStatus;
use App\Libraries\PaymentMethod;
use App\Libraries\UserRole;
use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Product\Item;
use App\Libraries\UserStatus;
use App\Models\Account\Store;
use App\Models\Product\Price;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $priceList = PriceList::create([
            'active' => true,
            'name' => 'TAB.GERAL-TESTE',
            'start_at' => Carbon::now()->toDateString(),
            'end_at' => Carbon::now()->addDays(100)->toDateString(),
        ]);

        $user = User::create([
            'role' => UserRole::CLIENT,
            'status' => UserStatus::ACTIVE,
            'name' => 'Lara Croft',
            'email' => 'laracroft@tombraider.com',
            'code_erp' => '123456',
            'main_discount' => 0.45,
            'current_discount' => 0.10,
            'average_discount' => 0.12,
            'price_list_id' => $priceList->id,
            'password' => Hash::make('l4r4-cr0ft'),
        ]);

        $store = Store::create([
            'name' => 'Quitanda',
            'cnpj' => '76662577000125',
            'code_erp' => '55577789',
            'user_id' => $user->id,
        ]);

        $items = collect()
            ->push(Item::create([
                'name' => 'Acoplamento Flexível AT X9',
                'code' => '999',
                'hunt' => true,
            ]))
            ->push(Item::create([
                'name' => 'Acoplamento Flexível AT S10',
                'code' => '3336',
                'hunt' => false,
            ]));

        $prices = collect();

        $items->each(function($item) use ($priceList, $prices) {
            $prices->push(Price::create([
                'item_id' => $item->id,
                'price_list_id' => $priceList->id,
                'amount' => $item->code == '999' ? 35050 : 100000,
            ]));
        });

        $order = Order::create([
            'code' => '1098738711',
            'status' => OrderStatus::REQUESTED,
            'code_erp' => '4332354',
            'user_id' => $user->id,
            'store_id' => $store->id,
            'price_list_id' => $priceList->id,
            'main_discount' => 0.15,
            'current_discount' => 0.1,
            'payment_method' => PaymentMethod::PREPAID,
            'description' => 'LOREM IPSUM',
        ]);

        $itemsData = $prices->map(function ($price) {
            $discount = 0.2;

            $quantity = $price->item->code == '999' ? 3 : 1;

            $unitOriginalPrice = $price->amount;

            $unitDiscountPrice = $unitOriginalPrice * $discount;

            $deliveryAt = Carbon::now()->addDays(15);

            return [
                'item_id' => $price->item->id,
                'price_id' => $price->id,
                'quantity' => $quantity,
                'unit_original_price' => $unitOriginalPrice,
                'unit_discount_price' => $unitDiscountPrice,
                'subtotal' => $unitDiscountPrice * $quantity,
                'discount' => $discount,
                'debit' => 0,
                'delivery_at' => $deliveryAt->toDateString(),
            ];
        });

        $order->items()->createMany($itemsData);
    }
}
