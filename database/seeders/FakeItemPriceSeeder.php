<?php

namespace Database\Seeders;

use App\Models\Product\Item;
use App\Models\Product\PriceList;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FakeItemPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $firstItem = Item::create([
            'name' => 'Acoplamento Flexível AT 25',
        ]);

        $secondItem = Item::create([
            'name' => 'Acoplamento Flexível AT 100',
        ]);

        $thirdItem = Item::create([
            'name' => 'Acoplamento de Engrenagens LFG',
            'hunt' => false,
        ]);

        $priceList = PriceList::create([
            'name' => 'Anual 2023',
        ]);

        $priceList->items()->sync([
            $firstItem->id => ['amount' => 44.94],
            $secondItem->id => ['amount' => 446.00],
            $thirdItem->id => ['amount' => 2866.20],
        ]);
    }
}
