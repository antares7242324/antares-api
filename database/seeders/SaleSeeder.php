<?php

namespace Database\Seeders;

use App\Models\Sale\Sale;
use App\Models\Account\User;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sale = Sale::create([
            'name' => 'PROMOÇÃO DE TESTE',
            'discount' => 0.15,
            'start_at' => Carbon::now()->toDateTimeString(),
            'end_at' => Carbon::now()->addDays(100)->toDateTimeString(),
        ]);

        $users = User::all();

        $userIds = $users->pluck('id');

        $sale->users()->sync($userIds->toArray());
    }
}
