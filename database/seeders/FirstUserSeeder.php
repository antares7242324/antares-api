<?php

namespace Database\Seeders;

use App\Models\Account\User;
use Illuminate\Database\Seeder;
use App\Libraries\UserRole;
use App\Libraries\UserStatus;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FirstUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role' => UserRole::MEEG,
            'status' => UserStatus::ACTIVE,
            'name' => 'Meeg',
            'email' => 'admin@meeg.app',
            'password' => Hash::make('g0h0rse@m33g'),
        ]);
    }
}
