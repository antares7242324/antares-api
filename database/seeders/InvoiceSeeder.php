<?php

namespace Database\Seeders;

use App\Models\File\Media;
use App\Libraries\UserRole;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use App\Models\Product\Price;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use App\Libraries\InvoiceStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $media = Media::create([
            'name' => '43230201915523000120550100000626511886457755-nfe.xml',
            'extension' => 'xml',
            'type' => 'nfe',
            'storage' => 'private',
            'path' => 'users/uid2/iuIrWVGpWaJVZaDg0xrbxpVe3UZIPUJyiaGIIdrl.xml'
        ]);

        $user = User::where('role', UserRole::CLIENT)->first();

        $invoice = Invoice::create([
            'status' => InvoiceStatus::PENDING,
            'user_id' => $user->id,
            'media_id' => $media->id,
            'amount' => 146362,
            'code' => 'NFe43230201915523000120550100000626511886457755',
            'number' => '62651',
            'series' => '10',
            'cnae' => '2815102',
            'uf' => 'RS',
            'municipality' => 'CAXIAS DO SUL',
            'issuance_at' => Carbon::create(2023, 2, 10)->toDateString(),
        ]);

        $priceItem = Price::where('price_list_id', $user->price_list_id)->first();

        $unitCost = 35050 - (35050 * $user->main_discount);

        $quantity = 2;

        $invoice->items()->create([
            'item_id' => $priceItem->item->id,
            'quantity' => $quantity,
            'unit_price' => 146362,
            'refund' => 2400,
            'discount' => 0.10,
            'hunt_discount' => 0.10,
            'unit_cost' => (int) $unitCost,
            'subtotal' => $quantity * (int) $unitCost,
            'description' => "TALHA ELETRCA 1 TON ELEV. 5 M (IMOBILIZADO)",
            'code' => '005588',
        ]);
    }
}
