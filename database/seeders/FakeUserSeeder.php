<?php

namespace Database\Seeders;

use App\Libraries\UserRole;
use App\Models\Account\User;
use App\Libraries\UserStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role' => UserRole::CLIENT,
            'status' => UserStatus::ACTIVE,
            'name' => 'Jean da Silva',
            'email' => 'jean@meeg.app',
            'password' => Hash::make('JEX@m33g'),
        ]);
    }
}
