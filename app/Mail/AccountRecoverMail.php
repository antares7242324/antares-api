<?php

namespace App\Mail;

use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Laravel\Sanctum\NewAccessToken;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountRecoverMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user to send email to.
     * @var \App\Models\Account\User
     */
    protected $user;

    /**
     * The new created token for this user.
     * @var \Laravel\Sanctum\NewAccessToken
     */
    protected $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, NewAccessToken $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'Recupere a sua conta',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'mails.account-recover',
            with: [
                'user' => $this->user,
                'token' => $this->token,
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
