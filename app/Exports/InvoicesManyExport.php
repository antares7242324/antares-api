<?php

namespace App\Exports;

use App\Models\Hunt\Invoice;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvoicesManyExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    public function __construct(protected array $ids){}

    public function query()
    {
        return Invoice::query()
            ->whereIn('user_id', $this->ids);
    }

    public function headings(): array
    {
        return [
            '#',
            'Distribuidor',
            'Número',
            'Código',
            'UF',
            'Município',
            'Saldo',
            'Data de emissão',
            'Data de criação',
            'Data de atualização',
            'Status',
            'CNAE',
        ];
    }

    public function map($invoice): array
    {
        $credit = $invoice->lastCredit();

        $amount = is_null($credit) ? 0 : $credit->amount;

        $records = [
            $invoice->id,
            $invoice->user->name,
            $invoice->number,
            $invoice->code,
            $invoice->uf,
            $invoice->municipality,
            $invoice->toCurrency($amount),
            $invoice->issuance_at,
            $invoice->created_at,
            $invoice->updated_at,
            $invoice->readableStatus(),
            $invoice->cnae,
        ];

        return $records;
    }
}
