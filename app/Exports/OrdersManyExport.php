<?php

namespace App\Exports;

use App\Models\Order\Order;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrdersManyExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    public function __construct(protected array $ids){}

    public function query()
    {
        return Order::query()->whereIn('id', $this->ids);
    }

    public function headings(): array
    {
        return [
            '#',
            'Código',
            'Loja',
            'Item',
            'Quantidade',
            'Preço unitário',
            'Preço com desconto',
            'Preço total',
            'Subtotal',
            'Data de entrega',
            'Data de criação',
            'Data de atualização',
            'Status',
            'Pagamento',
            'Desconto aplicado',
            'Saldo utilizado',
        ];
    }

    public function map($order): array
    {
        $records = $order->items->map(fn($item) =>
            [
                $order->id,
                $order->code,
                $order->store->name,
                $item->item->name,
                $item->quantity,
                $item->toCurrency($item->unit_original_price),
                $item->toCurrency($item->unit_discount_price),
                $item->toCurrency($item->originalSubtotal()),
                $item->toCurrency($item->subtotal),
                $item->delivery_at,
                $order->created_at,
                $order->updated_at,
                $order->readableStatus(),
                $order->paymentMethod(),
                $item->readableDiscount(),
                $item->toCurrency($item->debit),
            ]
        );

        return $records->toArray();
    }
}
