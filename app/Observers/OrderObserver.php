<?php

namespace App\Observers;

use App\Models\Order\Order;
use Illuminate\Support\Facades\Log;
use App\Jobs\Activities\OrderStatusUpdate;
use App\Jobs\Portal\ProcessOrderRequestJob;

class OrderObserver
{
    public function updated(Order $order)
    {
        if ($order->wasChanged('status')) {
            // Adiciona a atividade no usuário que alterou os dados do pedido.
            OrderStatusUpdate::dispatch($order, auth()->user());
        }

        Log::info("Order Observer: updated", [
            'order' => $order->toArray(),
        ]);

        // // Envia a requisição do pedido/orçamento para o Antares ERP.
        // // Também deve enviar um email para o distribuidor.
        // if ($order->isRequested() && $order->isDirty('status')) {
        //     Log::info("Order Observer: enviou requisição para antares", [
        //         'order' => $order->toArray(),
        //     ]);
        //     ProcessOrderRequestJob::dispatch($order->id);
        // }
    }
}
