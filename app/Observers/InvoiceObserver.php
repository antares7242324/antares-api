<?php

namespace App\Observers;

use App\Models\Hunt\Invoice;
use App\Libraries\InvoiceStatus;
use App\Jobs\Mail\MailInvoiceRejectedJob;
use App\Jobs\Activities\InvoiceCreatedJob;
use App\Jobs\Activities\InvoiceStatusUpdate;
use App\Actions\Commands\Invoice\UpdateInvoiceCredit;

class InvoiceObserver
{
    public function created(Invoice $invoice)
    {
        // Cria uma atividade quando a nota for criada.
        InvoiceCreatedJob::dispatch($invoice, auth()->user());
    }

    public function updated(Invoice $invoice)
    {
        if ($invoice->wasChanged('status')) {
            // Cria uma atividade quando a nota mudar de status.
            InvoiceStatusUpdate::dispatch($invoice, auth()->user());

            $reopened = $invoice->isReopened();

            // Muda o status de `reopened` para 'approved' quando a nota
            // fiscal foi reaberta. Esse status é usado apenas para controle.
            if ($invoice->isReopened()) {
                Invoice::withoutEvents(function () use ($invoice) {
                    $invoice->status = InvoiceStatus::APPROVED;
                    $invoice->save();
                });
            }

            // Vai adicionar ou remover o crédito da caçada conforme o novo
            // status atribuído na nota.
            (new UpdateInvoiceCredit)->handle($invoice, $reopened, true);

            // Se o status mudou para rejeitado, manda um email.
            if ($invoice->isRejected()) {
                MailInvoiceRejectedJob::dispatch($invoice->id);
            }
        }
    }
}
