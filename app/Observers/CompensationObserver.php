<?php

namespace App\Observers;

use App\Jobs\Activities\CompensationCreate;
use App\Jobs\Activities\CompensationDelete;
use App\Jobs\Activities\CompensationUpdate;
use App\Models\Transaction\Compensation;

class CompensationObserver
{
    public function created(Compensation $compensation)
    {
        $account = auth()->user();

        CompensationCreate::dispatch($compensation, $account);
    }

    public function updated(Compensation $compensation)
    {
        $account = auth()->user();

        CompensationUpdate::dispatch($compensation, $account);
    }

    public function deleting(Compensation $compensation)
    {
        CompensationDelete::dispatch($compensation);
    }
}
