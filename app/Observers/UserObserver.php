<?php

namespace App\Observers;

use Illuminate\Foundation\Auth\User;
use App\Jobs\Activities\UserPriceListUpdate;
use App\Jobs\Activities\UserCurrentDiscountUpdate;

class UserObserver
{
    public function updating(User $user)
    {
        // Sempre salva o status antigo antes de atualizar.
        if ($user->isDirty('status')) {
            $user->last_status = $user->getOriginal('status');
        }
    }

    public function updated(User $user)
    {
        $account = auth()->user();

        if ($user->wasChanged('current_discount')) {
            UserCurrentDiscountUpdate::dispatch($user, $account);
        }

        if ($user->wasChanged('price_list_id')) {
            UserPriceListUpdate::dispatch($user, $account);
        }

        // TODO: Remover depois esse job.
        // if ($user->wasChanged('status') && $user->canUpdatePriceList()) {
        //     ProcessUserPriceListJob::dispatch($user->id);
        // }
    }
}
