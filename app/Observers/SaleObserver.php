<?php

namespace App\Observers;

use App\Actions\Queries\ListAdminUsers;
use App\Jobs\Activities\AddSaleClosedToUserJob;
use App\Models\Sale\Sale;

class SaleObserver
{
    public function deleting(Sale $sale)
    {
        // Quando desativar a promoção, vai adicionar atividades
        // do tipo encerrada apenas se ela já estava aberta.

        if ($sale->isOpen()) {
            $responsible = auth()->user();

            $admins = (new ListAdminUsers)->handle();

            if ($admins) {
                $admins->each(fn ($admin) =>
                    AddSaleClosedToUserJob::dispatch($sale->id, $admin->id, $responsible->id)
                );
            }

            $users = $sale->users;

            if ($users) {
                $users->each(fn ($user) =>
                    AddSaleClosedToUserJob::dispatch($sale->id, $user->id, $responsible->id)
                );
            }
        }
    }
}
