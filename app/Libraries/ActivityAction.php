<?php

namespace App\Libraries;

class ActivityAction
{
    const COMPENSATION_SYNCED = 'compensation.synced';

    const INVOICE_CREATED = 'invoice.created';

    const INVOICE_UPDATED_STATUS = 'invoice.updated_status';

    const INVOICE_REOPENED = 'invoice.reopened';

    const ORDER_UPDATED_STATUS = 'order.updated_status';

    const SALE_CLOSED = 'sale.closed';

    const SALE_AVAILABLE = 'sale.available';

    const USER_UPDATED_CURRENT_DISCOUNT = 'user.updated_current_discount';

    const USER_UPDATED_PRICE_LIST = 'user.updated_price_list';
}