<?php

namespace App\Libraries;

class ResourceType
{
    const ACTIVITY = 'Activity';

    const AUTH = 'Auth';

    const BIG_NUMBERS = 'BigNumbers';

    const COMPENSATION = 'Compensation';

    const CREDIT_INVOICE = 'CreditInvoice';

    const DISCOUNT = 'Discount';

    const HUNT = 'Hunt';

    const INVOICE = 'Invoice';

    const INVOICE_ITEM = 'InvoiceItem';

    const INVOICE_NFE = 'InvoiceNfe';

    const ITEM = 'Item';

    const MAILER = 'Mailer';

    const MEDIA = 'Media';

    const ORDER = 'Order';

    const ORDER_ITEM = 'OrderItem';

    const PRICE = 'Price';

    const PRICE_ITEM = 'PriceItem';

    const PRICE_LIST = 'PriceList';

    const SALE = 'Sale';

    const STATEMENT = 'Statement';

    const STORE = 'Store';

    const USER = 'User';

    const USER_ITEM = 'UserItem';
}