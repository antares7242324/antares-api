<?php

namespace App\Libraries;

class DebitCanceledType
{
    const ADMIN = 'admin';
    const STANDARD = 'standard';

    public static function all()
    {
        return [
            self::ADMIN,
            self::STANDARD,
        ];
    }
}