<?php

namespace App\Libraries;

class PaymentMethod
{
    const DAYS35 = 'days35';

    const BNDS = 'bnds';

    const PREPAID = 'prepaid';

    public static function all()
    {
        return [
            self::DAYS35,
            self::BNDS,
            self::PREPAID,
        ];
    }
}
