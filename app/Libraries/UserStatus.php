<?php

namespace App\Libraries;

class UserStatus
{
    const INACTIVE = 'inactive';

    const ACTIVE = 'active';

    const BLOCKED = 'blocked';

    /**
     * Retrive all user statuses sorted alphabetically.
     *
     * @return  array<string>  The array of user status names.
     */
    public static function all()
    {
        $statuses = [
            self::ACTIVE,
            self::BLOCKED,
            self::INACTIVE,
        ];

        sort($statuses);

        return $statuses;
    }
}