<?php

namespace App\Libraries;

class ExceptionMessage
{
    const INVOICE_STANDARD = "Houve um erro no upload da nota. Tente novamente mais tarde";
    const INVOICE_EXISTS = "Esta nota fiscal já foi adicionada anteriormente";
}