<?php

namespace App\Libraries;

class OrderStatus
{
    const DRAFT = 'draft';

    const REQUESTED = 'requested';

    const SENT = 'sent';

    const COMPLETED = 'completed';

    const CANCELLED = 'cancelled';

    const FAILED = 'failed';

    public static function all()
    {
        return [
            self::DRAFT,
            self::REQUESTED,
            self::SENT,
            self::COMPLETED,
            self::CANCELLED,
            self::FAILED,
        ];
    }
}
