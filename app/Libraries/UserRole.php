<?php

namespace App\Libraries;

class UserRole
{
    const ADMIN = 'admin';

    const CLIENT = 'client';

    const DEV = 'dev';

    const LOGISTICS = 'logistics';

    const MEEG = 'meeg';

    /**
     * Retrieve all user roles sorted alphabeticaly.
     *
     * @return  array<string>  The array of user role names.
     */
    public static function all()
    {
        $roles = [
            self::ADMIN,
            self::CLIENT,
            self::DEV,
            self::LOGISTICS,
            self::MEEG,
        ];

        sort($roles);

        return $roles;
    }

    /**
     * Recupera todos os papeis considerados 'admin'.
     *
     * @return array<string> Cada papel ordenado alfabeticamente.
     */
    public static function admin()
    {
        $roles = [
            self::ADMIN,
            self::DEV,
            self::MEEG,
        ];

        sort($roles);

        return $roles;
    }
}