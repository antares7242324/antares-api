<?php

namespace App\Libraries;

use Illuminate\Foundation\Auth\User;

class ActivityRestriction
{
    const ANY = 'any';

    const CLIENT = 'client';

    const ADMIN = 'admin';

    const LOGISTICS = 'logistics';

    public static function restrictionByUser(User $user)
    {
        if ($user->isClient())
            return self::CLIENT;

        if ($user->isAdmin() || $user->isDev() || $user->isMeeg())
            return self::ADMIN;

        return null;
    }
}