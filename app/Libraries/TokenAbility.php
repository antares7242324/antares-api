<?php

namespace App\Libraries;

class TokenAbility
{
   const ACTIVATE = 'activate';

   const ACCESS = 'access';

   const RECOVER = 'recover';
}