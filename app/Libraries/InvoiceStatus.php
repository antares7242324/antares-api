<?php

namespace App\Libraries;

class InvoiceStatus
{
    const PENDING = 'pending';

    const APPROVED = 'approved';

    const REJECTED = 'rejected';

    const REOPENED = 'reopened';

    /**
     * Recupera todos os status em ordem alfabética.
     *
     * @return  array<string>  O array com os nomes.
     */
    public static function all()
    {
        $statuses = [
            self::APPROVED,
            self::PENDING,
            self::REJECTED,
            self::REOPENED,
        ];

        sort($statuses);

        return $statuses;
    }
}