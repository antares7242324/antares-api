<?php

namespace App\Libraries;

class Shipping
{
    const CIF = 'CIF';

    const FOB = 'FOB';

    public static function all()
    {
        return [
            self::CIF,
            self::FOB,
        ];
    }
}