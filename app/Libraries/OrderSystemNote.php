<?php

namespace App\Libraries;

class OrderSystemNote
{
    const PRICE_LIST_INACTIVE = "O pedido foi cancelado porque a tabela de preços utilizada nele foi desativada.";
    const ANTARES_SEND_CONNECTION_FAILED = "O pedido ainda não foi enviado à Antares por um erro de comunicação do sistema e este erro já está sendo resolvido por nossa equipe. Aguarde.";

    const ANTARES_PENDING = "O pedido está sendo enviado para a Antares.";
    const ANTARES_PRODUCTION = "Está tudo certo com o seu pedido e ele está sendo encaminhado pelo time da Antares.";
    const ANTARES_CANCELED = "O pedido foi cancelado pela Antares.";

    public static function statusNote(string $status): string|null
    {
        if ($status == OrderStatus::SENT) {
            return OrderSystemNote::ANTARES_PENDING;
        }

        if ($status == OrderStatus::COMPLETED) {
            return OrderSystemNote::ANTARES_PRODUCTION;
        }

        if ($status == OrderStatus::CANCELLED) {
            return OrderSystemNote::ANTARES_CANCELED;
        }

        return null;
    }
}