<?php

namespace App\Support\Readables;
use App\Libraries\OrderStatus;

class OrderStatusReadable
{
    /**
     * Transforma o texto original para o formato de leitura.
     *
     * @param string $raw O texto original.
     * @return string|null Retorna o texto para leitura, ou null se o texto enviado não for encontrado.
     */
    public static function toReadable(string $raw)
    {
        if ($raw == OrderStatus::DRAFT) return 'Rascunho';

        if ($raw == OrderStatus::REQUESTED) return 'Solicitado';

        if ($raw == OrderStatus::SENT) return 'Aceito';

        if ($raw == OrderStatus::COMPLETED) return 'Concluído';

        if ($raw == OrderStatus::CANCELLED) return 'Cancelado';

        return null;
    }
}