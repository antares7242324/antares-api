<?php

namespace App\Support\Readables;

use App\Libraries\PaymentMethod;

class PaymentMethodReadable
{
    /**
     * Transforma o texto original para o formato de leitura.
     *
     * @param string $raw O texto original.
     * @return string|null Retorna o texto para leitura, ou null se o texto enviado não for encontrado.
     */
    public static function toReadable(string $raw)
    {
        if ($raw == PaymentMethod::DAYS35) return '35 Dias';

        if ($raw == PaymentMethod::BNDS) return 'BNDS';

        if ($raw == PaymentMethod::PREPAID) return 'Antecipado';

        return null;
    }
}