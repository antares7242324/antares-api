<?php

namespace App\Support\Readables;

use App\Libraries\InvoiceStatus;

class InvoiceStatusReadable
{
    /**
     * Transforma o texto original para o formato de leitura.
     *
     * @param string $raw O texto original.
     * @return string|null Retorna o texto para leitura, ou null se o texto enviado não for encontrado.
     */
    public static function toReadable(string $raw)
    {
        if ($raw == InvoiceStatus::APPROVED) return 'Aprovada';

        if ($raw == InvoiceStatus::PENDING) return 'Pendente';

        if ($raw == InvoiceStatus::REJECTED) return 'Rejeitada';

        return null;
    }
}