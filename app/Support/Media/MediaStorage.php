<?php

namespace App\Support\Media;

class MediaStorage
{
    const LOCAL = 'local';

    const PUBLIC = 'public';

    const PRIVATE = 'private';

    public static function all()
    {
        return [
            self::LOCAL,
            self::PUBLIC,
            self::PRIVATE,
        ];
    }
}