<?php

namespace App\Support\Media;

class MediaType
{
    const NFE = 'nfe';

    public static function all()
    {
        return [
            self::NFE,
        ];
    }
}