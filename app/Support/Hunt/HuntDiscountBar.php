<?php

namespace App\Support\Hunt;

use Illuminate\Foundation\Auth\User;
use App\Support\Settings\ExtraDiscount;

class HuntDiscountBar
{
   /** @var \Illuminate\Support\Collection<string> */
   protected $discounts;

   public function __construct()
   {
      $this->discounts = (new ExtraDiscount)->all();
   }

   public function values(User $user = null)
   {
      $bar = collect();

      $userDiscount = ($user) ? $user->averageDiscount() : null;

      $lastDiff = null;

      $this->discounts->each(function ($discount) use ($bar, $userDiscount, &$lastDiff) {
         $value = intval($discount * 100);

         $diff = $userDiscount - $value;

         $fill = 0;

         if ($diff >= 0) {
            $fill = 100;
            $lastDiff = $diff;
         }

         if ($diff < 0) {
            $fill = intval(($lastDiff / $value) * 100);
            $lastDiff = 0;
         }

         $data = [
            'value' => $value,
            'fill' => $fill,
         ];

         $bar->push((object) $data);
      });

      return $bar;
   }
}