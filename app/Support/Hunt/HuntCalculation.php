<?php

namespace App\Support\Hunt;
use App\Traits\Currency;

class HuntCalculation
{
    use Currency;

    /**
     * Obtém o valor alfa na caçada.
     *
     * @return float
     */
    public function huntAlpha()
    {
        return floatval(config('antares.hunt_alpha')) / 100;
    }

    /**
     * Obtém o valor de tara na caçada.
     *
     * @return float
     */
    public function huntTare()
    {
        return floatval(config('antares.hunt_tare')) / 100;
    }

    /**
     * Calcula o desconto recebido pela caçada.
     *
     * @param float $discount O desconto recebido.
     * @return float O valor calculado a partir do desconto recebido, o valor alfa, e o valor de tara.
     */
    public function receivedDiscount(float $discount)
    {
        if ($discount <= 0) return 0.00;

        $receivedDiscount = $this->huntAlpha() * ($discount - $this->huntTare());

        return $receivedDiscount > 0.25 ? 0.25 : $receivedDiscount;
    }

    /**
     * Calcula o desconto dado pelo distribuidor.
     *
     * @param int $subtotal O valor total dos itens na nota.
     * @param int $qty A quantidade de itens na nota.
     * @param int $itemPrice O preço do item na tabela de preços.
     * @return float O valor calculado a partir do desconto recebido, o valor alfa, e o valor de tara.
     */
    public function givenDiscount(int $subtotal, int $qty, int $itemPrice)
    {
        $discount = ($subtotal / ($itemPrice * $qty));

        if ($discount >= 0.61) return 0.00;

        // if ($discount >= 1) return 0.00;

        return $discount;
    }

    /**
     * Calcula a diferença entre o valor vendido pelo distribuidor
     *   e o valor na tabela de preços.
     *
     * @param int $unitPrice O valor unitário do item na tabela de preços.
     * @param int $qty A quantidade de itens na nota fiscal.
     * @param int $subtotal O valor total vendido dos itens na nota fiscal.
     * @return int A diferença calculada em centavos.
     */
    public function difference(int $unitPrice, int $qty, int $subtotal)
    {
        return ($unitPrice * $qty) - $subtotal;
    }

    /**
     * Calcula o extorno pela caçada.
     *
     * @param int $unitCost O preço de custo do item.
     * @param int $qty A quantidade de itens na nota.
     * @param float $givenDiscount O valor de desconto oferecido pelo distribuidor.
     * @return int O valor em centavos do extorno.
     */
    public function refund(int $unitCost, int $qty, float $givenDiscount)
    {
        return ($unitCost * $qty) * $givenDiscount;
    }
}