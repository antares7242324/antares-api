<?php

namespace App\Support\Settings;

class ExtraDiscount
{
    /**
     * Retorna uma coleção de todos os valores na variável de ambiente
     * `ANTARES_EXTRA_DISCOUNTS` que são separados por vírgulas.
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        $discount = explode(',', str_replace(' ', '',
            config('antares.extra_discounts')
        ));

        return collect($discount);
    }
}