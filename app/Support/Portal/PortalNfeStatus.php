<?php

namespace App\Support\Portal;

use App\Libraries\InvoiceStatus;

class PortalNfeStatus
{
   const VALID = '100';

   public static function convert(string $code)
   {
      if ($code == self::VALID)
         return InvoiceStatus::APPROVED;

      if ($code > self::VALID) return InvoiceStatus::REJECTED;

      return InvoiceStatus::PENDING;
   }
}