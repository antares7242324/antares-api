<?php

namespace App\Support\Portal;

use Illuminate\Support\Str;
use App\Traits\HttpResponses;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Portal
{
   use HttpResponses;

   private $username;

   private $password;

   private $baseUrl;

   /**
    * Constructor
    *
    * @param string $username
    *   O usuário para o login. NULL se for o padrão no env().
    * @param string $password
    *   Senha para o login. NULL se for o padrão no env().
    * @param string $baseurl
    *   URL base. NULL se for o padrão no env().
    */
   public function __construct(string $username = null, string $password = null, string $baseurl = null)
   {
      $this->username = is_null($username)
         ? config('erp.user')
         : $this->username = $username;

      $this->password = is_null($password)
         ? config('erp.password')
         : $this->password = $password;

      $this->baseUrl = is_null($baseurl)
         ? config('erp.url')
         : $this->baseUrl = $baseurl;
   }

   /**
    * Requisição GET.
    *
    * @param string $path
    *   O caminho do endpoint.
    * @param array $params
    *   O array com os parâmetros.
    * @return mixed
    */
   public function get(string $path, array $params = [])
   {
      // Monta a url com base no path.
      $url = $this->baseUrl . Str::start($path, '/');

      // Converte os parâmetros para uma collection.
      $params = collect($params);

      // Executa a requisição.
      $response = Http::withBasicAuth($this->username, $this->password)
         ->acceptJson()
         ->withOptions(array_filter(['query' => $params->toArray()]))
         ->get($url);

      Log::info('Portal GET: ' . $url, [
         'query' => $params->toArray()
      ]);

      // Retorna o JSON.
      if ($response->successful()) {
         Log::notice('Portal GET success: ' . $url, [
            'response' => $response->json()
         ]);

         return $this->success(
            $response->json(),
            Response::$statusTexts[$response->status()],
            $response->status()
         );
      }

      Log::error('Portal GET error: ' . $url, [
         'response' => $response->json()
      ]);

      // Retorna o erro.
      return $this->error(
         $response->json(),
         Response::$statusTexts[$response->status()],
         $response->status()
      );
   }

   /**
    * Requisição POST.
    *
    * @param string $path
    *   O caminho do endpoint.
    * @param array $body
    *   O array com os parâmetros.
    * @return mixed
    */
   public function post(string $path, array $body = [], array $query = [])
   {
      // Monta a url com base no path.
      $url = $this->baseUrl . Str::start($path, '/');

      // Converte o body para uma collection.
      $body = collect($body);

      // Converte a query para uma collection.
      $query = collect($query);

      if ($query->isNotEmpty()) {
         $url .= '?' . http_build_query($query->toArray());
      }

      Log::info('Portal POST: ' . $url, $body->toArray());

      // Executa a requisição.
      $response = Http::withBasicAuth($this->username, $this->password)
         ->acceptJson()
         ->timeout(60)
         ->post($url, $body->toArray());

      // Retorna o JSON.
      if ($response->successful()) {
         Log::notice('Portal POST success: ' . $url, [
            'response' => $response->json()
         ]);

         return $this->success(
            $response->json(),
            Response::$statusTexts[$response->status()],
            $response->status()
         );
      }

      Log::error('Portal POST error: ' . $url, [
         'response' => $response->json()
      ]);

      // Retorna o erro.
      return $this->error(
         $response->json(),
         Response::$statusTexts[$response->status()],
         $response->status()
      );
   }
}