<?php

namespace App\Support\Portal;

use Illuminate\Support\Carbon;

class PortalPriceListStatus
{
   const ACTIVE = 'ATIVA';

   public static function convertString(string $status): bool
   {
      return $status == self::ACTIVE;
   }

   public static function isActive(?Carbon $endAt): bool
   {
      return $endAt != null && $endAt->startOfDay() < Carbon::now()->startOfDay()
         ? false
         : true;
   }
}