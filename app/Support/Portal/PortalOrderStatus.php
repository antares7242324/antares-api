<?php

namespace App\Support\Portal;

use App\Libraries\OrderStatus;
use App\Libraries\OrderSystemNote;

class PortalOrderStatus
{
   const PENDING = 'A'; // Orçamento pendente.
   const PRODUCTION = 'B'; // Orçamento em produção.
   const CANCELED = 'C'; // Orçamento cancelado.

   public static function convert(string $code): string
   {
      if ($code == self::PENDING) return OrderStatus::SENT;

      if ($code == self::PRODUCTION) return OrderStatus::COMPLETED;

      if ($code == self::CANCELED) return OrderStatus::CANCELLED;

      return OrderStatus::REQUESTED;
   }
}