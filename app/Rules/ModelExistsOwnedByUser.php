<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ModelExistsOwnedByUser implements Rule
{
    /**
     * Nome da classe do model.
     * Exemplo: \App\Models\Order\Order::class
     */
    protected $model;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $model)
    {
        $this->model = $model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = auth()->user();

        // Verifica se todos os IDs existem e são do usuário logado.
        return $this->model::whereIn('id', $value)
            ->where('user_id', $user->id)
            ->count() === count($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'One or more IDs are invalid or do not belong to the current user.';
    }
}
