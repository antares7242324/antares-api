<?php

namespace App\Providers;

use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use App\Observers\UserObserver;
use App\Observers\OrderObserver;
use App\Observers\InvoiceObserver;
use App\Observers\CompensationObserver;
use Illuminate\Support\ServiceProvider;
use App\Models\Transaction\Compensation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Compensation::observe(CompensationObserver::class);

        Invoice::observe(InvoiceObserver::class);

        Order::observe(OrderObserver::class);

        User::observe(UserObserver::class);
    }
}
