<?php

namespace App\Actions\Commands\Portal;

use App\Models\Order\Order;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateCompletedOrder
{
    public function handle(Order $order, Collection $data)
    {
        $dataOrder = collect($data->get('pedido'));

        throw_if($dataOrder->isEmpty(), new \Exception(
            'Não foram encontrados os dados do pedido pelo Portal Antares ERP.'
        ));

        $orderItems = $order->items;

        Log::info("Update Complete Order Job: Começa a transação de update para o pedido ({$order->id})", [
            'data' => $data->toArray(),
        ]);

        DB::beginTransaction();

        $updated = false;

        try {
            foreach ($orderItems as $orderItem) {
                $item = $orderItem->item;

                $dataItems = collect($dataOrder->get('itens', []));

                Log::info("Update Complete Order Job: Verificando itens do pedido ({$order->id})", [
                    'dataOrder' => $dataOrder->toArray(),
                    'orderItem' => $orderItem->toArray(),
                    'item_code' => $item->code,
                ]);

                // O item foi removido do pedido pelo Portal, então tem que remover
                // no sistema também.
                if ($dataItems->contains('codigo', $item->code) === false) {
                    Log::info("Update Complete Order Job: Deletando item do pedido ({$order->id})", [
                        'item' => $item->toArray(),
                    ]);

                    $orderItem->delete();

                    $updated = true;
                }

                // Quando encontra o item retornado pelo Portal, tem que verificar
                // se houve alguma mudança em seus dados, tais como data e quantidade.
                // Se essa mudança ocorreu, tem que atualizar no sistema.
                if ($dataItems->contains('codigo', $item->code)) {
                    $dataItem = collect($dataItems->firstWhere('codigo', $item->code));

                    $changedQuantity = null;

                    $changedSubtotal = null;

                    Log::info("Update Complete Order Job: Achou o item do pedido ({$order->id})", [
                        'item' => $item->toArray(),
                        'dataItem' => $dataItem->toArray(),
                    ]);

                    // Verifica se houve atualização na quantidade.
                    if ($dataItem->has('quantidade')) {
                        $changedQuantity =
                            is_null($dataItem->get('quantidade')) === false
                            && $dataItem->get('quantidade') != $orderItem->quantity
                            ? $dataItem->get('quantidade')
                            : null;

                        if (is_null($changedQuantity) === false) {
                            $changedSubtotal = $item->unit_discount_price * $changedQuantity;
                        }
                    }

                    $changedDelivery = null;

                    // Verifica se houve atualização na data de entrega.
                    if ($dataItem->has('dataEntrega')) {
                        if (is_null($dataItem->get('dataEntrega')) === false) {
                            // Primeiro tenta usar data no formato 'd/m/y'.
                            $delivery = Carbon::createFromFormat('d/m/y',
                                $dataItem->get('dataEntrega')
                            );

                            // Se o formato 'd/m/y' falhar, tenta usar 'd/m/Y'.
                            if (!$delivery) {
                                $delivery = Carbon::createFromFormat('d/m/Y',
                                    $dataItem->get('dataEntrega')
                                );

                                // Se por algum motivo ainda assim der errado,
                                // tem que ocorrer uma exceção.
                                throw_if(!$delivery, new \Exception(
                                    'Data de entrega informada pelo Portal Antares ERP é inválida.'
                                ));
                            }

                            if ($orderItem->deliveryAt()->notEqualTo($delivery)) {
                                $changedDelivery = $delivery;
                            }
                        }
                    }

                    // Atualiza o item com os dados atualizados.
                    $changedData = collect()
                        ->when(is_null($changedQuantity) === false, fn ($data) =>
                            $data->put('quantity', $changedQuantity)
                                ->put('subtotal', $changedSubtotal)
                        )
                        ->when(is_null($changedDelivery) === false, fn ($data) =>
                            $data->put('delivery_at', $changedDelivery->toDateString())
                        );

                    if ($changedData->isNotEmpty()) {
                        $orderItem->update($changedData->toArray());

                        $updated = true;
                    }
                }
            }

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();

            Log::error('Erro ao atualizar o pedido pelo Portal Antares ERP.', [
                'exception' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'code_erp' => $order->code,
                'response' => $data->toArray(),
                'order' => $order->toArray(),
            ]);

            throw $e;
        }

        // Retorna se houve atualização nos itens do pedido.
        return $updated;
    }
}