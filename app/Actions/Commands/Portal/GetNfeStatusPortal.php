<?php
namespace App\Actions\Commands\Portal;

use App\Support\Portal\Portal;
use Illuminate\Support\Facades\Log;

class GetNfeStatusPortal
{
    const PATH = 'consultachavenfe';

    /** @var \App\Support\Portal\Portal */
    protected $portal;

    public function __construct()
    {
        $this->portal = new Portal;
    }

    public function handle(string $key)
    {
        $formattedKey = str_replace('nfe', '', strtolower($key));

        $params = collect(['chavenfe' => $formattedKey]);

        $response = $this->portal->get(self::PATH, $params->toArray());

        $data = collect($response->getData());

        $data->when(is_null($data->get('data')) === false, fn ($data) =>
            $data->put('data', collect($data->get('data')))
        );

        return $data;
    }
}