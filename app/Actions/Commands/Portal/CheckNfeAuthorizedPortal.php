<?php

namespace App\Actions\Commands\Portal;

use App\Libraries\ExceptionMessage;
use Illuminate\Support\Collection;

class CheckNfeAuthorizedPortal
{
    public function handle(Collection $response)
    {
        $data = $response->get('data');

        if (is_null($data)) return false;

        if (is_null($data->get('codigo'))) return false;

        if ((int)$data->get('codigo') !== 100) return false;

        return true;
    }

    public function handleWithException(Collection $response)
    {
        $success = $response->get('success');

        $data = $response->get('data');

        // Trata os erros de quando a resposta não teve sucesso.
        if (!$success) {

            $errorMessage = null;

            if ($data->has('errorMessage')) {
                $errorMessage = $data->get('errorMessage');
            }

            if ($data->has('mensagem')) {
                $errorMessage = $data->get('mensagem');
            }

            $errorCode = null;

            if ($data->has('errorCode')) {
                $errorCode = $data->get('errorCode');
            }

            if ($data->has('codigo')) {
                $errorCode = $data->get('codigo');
            }

            if ($data->get('errorCode') === 400) {
                throw new \Exception($errorMessage, $errorCode);
            }

            if (is_null($errorMessage)) {
                $errorMessage = ExceptionMessage::INVOICE_STANDARD;
            }

            throw new \Exception($errorMessage);
        }

        // Trata a resposta quando teve sucesso. Pode ser que mesmo assim
        // apresente algum erro customizado.

        // Nota fiscal está autorizada.
        if ($data->get('codigo') === 100) return true;

        throw new \Exception($data->get('mensagem'));
    }
}