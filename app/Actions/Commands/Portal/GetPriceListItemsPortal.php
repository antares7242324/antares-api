<?php

namespace App\Actions\Commands\Portal;

use App\Support\Portal\Portal;

class GetPriceListItemsPortal
{
    const PATH = 'listaprecos';

    /** @var \App\Support\Portal\Portal */
    protected $portal;

    public function __construct()
    {
        $this->portal = new Portal;
    }

    public function handle(string $codeErp, int $page = 1)
    {
        $params = collect()
            ->put('tabela', $codeErp)
            ->put('pageSize', 100)
            ->put('page', $page);

        try {
            $response = $this->portal->get(
                self::PATH, $params->toArray()
            );

            // Monta o JSON.
            $responseData = $response->getData();

            // Monta os dados da resposta.
            $data = collect($responseData->data);

            return $data;
        }
        catch (\Exception $e) {
            return null;
        };
    }
}