<?php

namespace App\Actions\Commands\Portal;

use App\Support\Portal\Portal;
use Illuminate\Support\Facades\Log;

class ShowPriceLists
{
    const PATH = 'listaTabelas';

    /** @var \App\Support\Portal\Portal */
    protected $portal;

    public function __construct()
    {
        $this->portal = new Portal;
    }

    public function handle()
    {
        try {
            $response = $this->portal->get(self::PATH);

            // Monta o JSON.
            $responseData = $response->getData();

            // Monta os dados da resposta.
            $data = collect($responseData->data->tabelas);

            return $data;
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            return collect();
        }
    }
}