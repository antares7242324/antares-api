<?php

namespace App\Actions\Commands\Portal;

use App\Support\Portal\Portal;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class GetPriceListPortal
{
    const PATH = 'listaprecos';

    /** @var \App\Support\Portal\Portal */
    protected $portal;

    public function __construct()
    {
        $this->portal = new Portal;
    }

    public function handle(Collection $params) : Collection|null
    {
        $query = collect();

        // Adiciona o código da tabela de preço.
        $query->when($params->has('price_list_code'), fn($query) =>
            $query->put('tabela', $params->get('price_list_code'))
        );

        // Adiciona o código do usuário (usuario+loja).
        $query->when($params->has('user_code'), fn($query) =>
            $query->put('codigoERP', $params->get('user_code'))
        );

        // Adiciona o tamanho da página.
        $query->when($params->has('page_size'), fn($query) =>
            $query->put('pageSize', $params->get('page_size'))
        );

        // Adiciona a página atual.
        $query->when($query->has('page'), fn($query) =>
            $query->put('page', $params->get('page'))
        );

        // Adiciona a chave local do produto.
        $query->when($params->has('search_key'), fn($query) =>
            $query->put('searchKey', $params->get('search_key'))
        );

        // Adiciona a data de alteração.
        $query->when($params->has('date'), fn($query) =>
            $query->put('dataAlteracao', $params->get('date'))
        );

        Log::info('GetPriceListPortal: Price List ' . $params->get('price_list_code'));

        try {
            $response = $this->portal->get(
                self::PATH, $query->toArray()
            );

            // Monta o JSON.
            $responseData = $response->getData();

            // Monta os dados da resposta.
            $data = collect($responseData->data);

            Log::info('GetPriceListPortal: Resposta Portal para Price List' . $params->get('price_list_code'), [
                'data' =>  $data->toArray(),
            ]);

            return $data->only(
                'codigo',
                'descricao',
                'status',
                'inicio_vigencia',
                'fim_vigencia'
            );
        }
        catch (\Exception $e) {
            return null;
        };
    }
}