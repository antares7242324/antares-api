<?php

namespace App\Actions\Commands\Faker;

use App\Libraries\UserRole;
use App\Models\Account\User;
use App\Libraries\UserStatus;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Hash;

class MakeFakeUser
{
   /**
    * Cria um novo usuário com dados fakes sem salvar no banco de dados.
    *
    * @param array $data
    * Os dados do usuário. Se quiser usar os valores padrões, deixe vazio ou envie NULL.
    * @return \App\Models\Account\User
    */
   public function handle(array $data = [], PriceList $priceList = null)
   {
      if (is_null($priceList)) {
         $priceList = PriceList::first();
      }

      $userData = collect($data);

      $userData
         ->when(! $userData->has('role'), fn($data) => $data->put('role', UserRole::CLIENT))
         ->when(! $userData->has('name'), fn($data) => $data->put('name', 'Lara Croft'))
         ->when(! $userData->has('email'), fn($data) => $data->put('email', 'laracroft@tombraider.com'))
         ->when(! $userData->has('code_erp'), fn($data) => $data->put('code_erp', '123456'))
         ->when(! $userData->has('main_discount'), fn($data) => $data->put('main_discount', 0.45))
         ->when(! $userData->has('current_discount'), fn($data) => $data->put('current_discount', 0.10))
         ->when(! $userData->has('average_discount'), fn($data) => $data->put('average_discount', 0.12))
         ->when(! $userData->has('price_list_id'), fn($data) => $data->put('price_list_id', $priceList->id))
         ->when(! $userData->has('status'), fn($data) => $data->put('status', UserStatus::ACTIVE))
         ->when(! $userData->has('last_status'), fn($data) => $data->put('last_status', UserStatus::INACTIVE))
         ->when(! $userData->has('available_balance'), fn($data) => $data->put('available_balance', 150000))
         ->when(! $userData->has('password'), fn($data) => $data->put('password', Hash::make('l4r4-cr0ft')));

      $user = User::make($userData->toArray());

      return $user;
   }
}