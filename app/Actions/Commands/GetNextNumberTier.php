<?php

namespace App\Actions\Commands;

class GetNextNumberTier
{
    /**
     * Pega o próximo nível na faixa de valores caso se saiba o nível atual.
     *
     * @param array $numberTier Lista com a faixa de valores
     * @param int $tier A chave da lista para a faixa atual. Considera-se que se for null, é porque não achou o nível.
     * @return int O nível da próxima faixa. Pega o primeiro nível se não achou o nível. Pega o último nível se o nível informado for maior ou igual ao último.
     */
    public function handle(array $numberTier, int|null $tier)
    {
        $collection = collect($numberTier);

        if (is_null($tier)) return $collection->keys()->first();

        $lastKey = $collection->keys()->last();

        if ($tier >= $lastKey) return $lastKey;

        return $tier + 1;
    }
}