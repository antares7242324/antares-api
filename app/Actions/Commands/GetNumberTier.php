<?php

namespace App\Actions\Commands;

class GetNumberTier
{
    /**
     * Recupera o número da faixa de valores definidas.
     *
     * Exemplo: Se $tierNumbers for [10, 14, 18] e $number for 16, então será retornado 1, que é a chave do segundo elemento de $tierNumbers.
     *
     * @param array $tierNumbers Lista de números que representa uma faixa de valores.
     * @param int|float $number O número para descobrir de qual faixa pertence.
     * @return mixed A chave do elemento dentro da faixa de valores.
     */
    public function handle(array $tierNumbers, int|float $number)
    {
        $collection = collect($tierNumbers);

        $tier = $collection->keys()->first(fn($tier) => $number >= $collection[$tier]);

        return $tier;
    }
}