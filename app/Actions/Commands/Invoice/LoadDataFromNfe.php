<?php

namespace App\Actions\Commands\Invoice;

use App\Models\Product\UserItem;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Auth\User;
use App\Actions\Commands\Hunt\CalculateItemHunt;

class LoadDataFromNfe
{
    /**
     * @var \App\Actions\Commands\Hunt\CalculateItemHunt
     */
    protected $calculateHunts;

    /**
     * Constrói o objeto.
     */
    public function __construct()
    {
        $this->calculateHunts = new CalculateItemHunt();
    }

    /**
     * Pega os dados da NFe e calcula os possíveis extratos.
     * Retorna os itens com seus dados calculados da caçada.
     *
     * @param Collection $data
     *   Os dados da nota fiscal.
     * @param User $user
     *   A entidade do usuário.
     * @return \Illuminate\Support\Collection
     */
    public function handle(Collection $data, User $user)
    {
        $this->calculateHunts->setUser($user);

        $items = $data->get('items')->map(function ($element) use ($user) {
            $item = collect($element)
                ->put('userItem', null)
                ->put('hunt', null);

            if (is_null($item->get('code')) === false) {
                $userItem = UserItem::query()
                    ->where('user_id', $user->id)
                    ->where('code', $item->get('code'))
                    ->first();

                $item->put('userItem', $userItem);

                if (is_null($userItem)) {
                    return $item;
                }

                // Cálculo da caçada para o item carregado.
                $itemAntares = $userItem->item;

                // Calcula apenas se o item estiver disponível na caçada.
                if ($itemAntares->hunt) {
                    $hunt = $this->calculateHunts->handle(
                        $itemAntares,
                        $element->get('quantity', 0),
                        $element->get('subtotal', 0)
                    );

                    $item->put('hunt', $hunt);
                }
            }

            return $item;
        });

        return $items;
    }
}