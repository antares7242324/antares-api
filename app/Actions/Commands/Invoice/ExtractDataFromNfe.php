<?php

namespace App\Actions\Commands\Invoice;

use App\Traits\Currency;
use Carbon\Carbon;

class ExtractDataFromNfe
{
    use Currency;

    /**
     * Extrai os dados do conteúdo do arquivo da nota fiscal.
     *
     * @param string $file O coneúdo do arquivo.
     * @return \Illuminate\Support\Collection
     */
    public function handle($file)
    {
        $xml = new \SimpleXMLElement($file);

        // Get product items and collecting them.
        $infNfe = collect((array) $xml->NFe->infNFe->children());

        // Pega os itens dentro da nota.
        $nfeItems = $infNfe->get('det', []);

        // A lógica para array de itens é diferente de quando
        // existe apenas um, então temos que tratar as duas
        // situações de forma diferente.
        if (is_array($nfeItems)) {
            $items = collect($nfeItems)
                ->map(function ($item) {
                    return collect([
                        'code' => (string) $item->prod->cProd,
                        'quantity' => (int) $item->prod->qCom,
                        'unit_price' => $this->toCents((string) $item->prod->vUnCom),
                        'subtotal' => $this->toCents((string) $item->prod->vProd),
                        'description' => (string) $item->prod->xProd,
                    ]);
                });
        }
        else {
            $items = collect([collect([
                'code' => (string) $nfeItems->prod->cProd,
                'quantity' => (int) $nfeItems->prod->qCom,
                'unit_price' => $this->toCents((string) $nfeItems->prod->vUnCom),
                'subtotal' => $this->toCents((string) $nfeItems->prod->vProd),
                'description' => (string) $nfeItems->prod->xProd,
            ])]);
        }

        // Monta a data para o campo issuance_at.
        $issuanceAt = Carbon::createFromFormat(
            'Y-m-d\TH:i:sP',
            (string) $xml->NFe->infNFe->ide->dhEmi
        );

        // Criando a coleção para retornar os valores do xml.
        $collection = collect()
            ->put('code', (string) $xml->NFe->infNFe->attributes()->Id)
            ->put('number', (string) $xml->NFe->infNFe->ide->nNF)
            ->put('series', (string) $xml->NFe->infNFe->ide->serie)
            ->put('issuance_at', $issuanceAt->toDateString())
            ->put('uf', strtoupper((string) $xml->NFe->infNFe->dest->enderDest->UF))
            ->put('municipality', strtoupper((string) $xml->NFe->infNFe->dest->enderDest->xMun))
            ->put('amount', (int) str_replace('.', '', (string) $xml->NFe->infNFe->total->ICMSTot->vProd))
            ->put('cnae', (string) ($xml->NFe->infNFe->emit->CNAE ?? ''))
            ->put('items', $items);

        return $collection;
    }
}