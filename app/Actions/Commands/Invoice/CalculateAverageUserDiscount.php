<?php

namespace App\Actions\Commands\Invoice;

use App\Models\Account\User;

class CalculateAverageUserDiscount
{
    public function handle(User $user)
    {
        $totalDiscount = 0;

        $totalItems = 0;

        $averageDiscount = 0;

        $invoiceItems = $user->invoices
            ->flatMap(function ($invoice) {
                if ($invoice->isNotRejected()) {
                    return $invoice->items;
                }
                return [];
            });

        if ($invoiceItems->isNotEmpty()) {
            $totalDiscount = $invoiceItems->pluck('discount')->sum();
            $totalItems = $invoiceItems->count();
        }

        if ($totalItems > 0) {
            $averageDiscount = $totalDiscount / $totalItems;

            $user->update(['average_discount' => $averageDiscount]);
        }
    }
}