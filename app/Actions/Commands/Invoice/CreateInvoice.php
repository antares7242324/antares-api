<?php

namespace App\Actions\Commands\Invoice;

use App\Models\Hunt\Invoice;
use App\Libraries\InvoiceStatus;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CreateInvoice
{
    public function handle(Collection $data)
    {
        return DB::transaction(function () use ($data) {
            // Monta os dados para criar o invoice.
            $invoiceData = $data->only(
                'media_id',
                'user_id',
                'amount',
                'code',
                'number',
                'series',
                'cnae',
                'uf',
                'municipality',
                'issuance_at',
                'status',
            );

            // Se não foi informado o status da nota, coloca como PENDING.
            // Isso é um resquício do código legado que será mantido para
            // evitar criar problemas inexperados por enquanto.
            $invoiceData->when($data->has('status') === false, fn ($data) =>
                $data->put('status', InvoiceStatus::PENDING)
            );

            // Monta os dados para sincronizar os itens no invoice.
            $itemsData = collect($data->get('items'));

            // Cria o objeto invoice com a NFe.
            $invoice = Invoice::create($invoiceData->toArray());

            // Adiciona os itens da nota.
            $items = $itemsData->map(function ($item) {
                $item = collect($item);

                $discount = $item->get('discount', 0);

                $huntDiscount = $item->get('hunt_discount', 0);

                $data = collect($item->only(
                    'quantity',
                    'unit_price',
                    'refund',
                    'unit_cost',
                    'subtotal',
                    'code',
                    'user_item_id',
                    'description',
                ))
                ->put('item_id', $item['id'])
                ->put('discount', $discount > 0 ? $discount / 100 : 0)
                ->put('hunt_discount', $huntDiscount > 0 ? $huntDiscount / 100 : 0);

                return $data->toArray();
            });

            $invoice->items()->createMany($items->toArray());

            $invoice->refresh();

            return $invoice;
        });
    }
}