<?php

namespace App\Actions\Commands\Invoice;

use App\Models\Hunt\Invoice;

class UpdateInvoice
{
   /**
    * Atualiza uma nota fiscal.
    *
    * @param \App\Models\Hunt\Invoice $invoice
    *   Objeto Invoice da nota fiscal.
    * @param array $validated
    *   O array com os campos validados.
    * @return \App\Models\Hunt\Invoice
    *   Objeto Invoice atualizado.
    */
   public function handle(Invoice $invoice, array $validated)
   {
      $data = collect($validated);

      $invoice->update($data->toArray());

      $invoice->refresh();

      return $invoice;
   }
}