<?php

namespace App\Actions\Commands\Invoice;

use App\Actions\Commands\Hunt\CancelCreditFromInvoice;
use App\Actions\Commands\Hunt\CreditFromInvoice;
use App\Models\Hunt\Invoice;

class UpdateInvoiceCredit
{
    /** @var \App\Actions\Commands\Hunt\CreditFromInvoice */
    protected $creditInvoice;

    /** @var \App\Actions\Commands\Hunt\CancelCreditFromInvoice */
    protected $cancelCreditInvoice;

    public function __construct()
    {
        $this->creditInvoice = new CreditFromInvoice;
        $this->cancelCreditInvoice = new CancelCreditFromInvoice;
    }

    /**
     * Atualiza o crédito da caçada do usuário com base na nota fiscal
     * - Se a nota for aprovada, adiciona o crédito.
     * - Se a nota for rejeitada, cancela o crédito.
     *
     * @param \App\Models\Hunt\Invoice $invoice
     *   O model de nota fiscal.
     * @param bool $reopened
     *   Indica se a nota fiscal foi reaberta.
     * @return bool
     *   TRUE se o crédito foi adicionado ou removido, FALSE se não.
     */
    public function handle(Invoice $invoice, bool $reopened = false, bool $byAdmin = false)
    {
        // Independente do status da nota, tem que atualizar a média de
        // de desconto gerada pelo usuário em suas vendas.
        (new CalculateAverageUserDiscount)->handle($invoice->user);

        // Se a nota for aprovada, adiciona o crédito.
        // Também informa se a nota foi reaberta.
        if ($invoice->isApproved()) {
            return $this->creditInvoice->handle($invoice, $reopened);
        }

        // Se a nota for rejeitada, adiciona o débito.
        if ($invoice->isRejected()) {
            return $this->cancelCreditInvoice->handle($invoice, $byAdmin);
        }

        return false;
    }
}