<?php

namespace App\Actions\Commands\Invoice;

use App\Models\Account\User;
use App\Models\Product\UserItem;
use Illuminate\Support\Collection;

class CheckUserItemsFromNfe
{
    public function handle(Collection $data, User $user)
    {
        $items = $data->get('items')->map(function ($element) use ($user) {
            $item = collect($element)
                ->put('userItem', null);

            if ($item->isNotEmpty()) {
                $userItem = UserItem::query()
                    ->where('user_id', $user->id)
                    ->where('code', $item->get('code'))
                    ->first();

                $item->put('userItem', $userItem);
            }

            return $item;
        });

        return $items;
    }
}