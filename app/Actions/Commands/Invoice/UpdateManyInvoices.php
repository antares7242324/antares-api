<?php

namespace App\Actions\Commands\Invoice;

use App\Models\Hunt\Invoice;

class UpdateManyInvoices
{
    /** @var \App\Actions\Commands\Invoice\UpdateInvoice */
    protected $updateInvoice;

    public function __construct()
    {
        $this->updateInvoice = new UpdateInvoice;
    }

    public function handle(array $validated)
    {
        $data = collect($validated);

        $ids = $data->pull('ids');

        $invoices = Invoice::whereIn('id', $ids)->get();

        if ($invoices->isNotEmpty()) {
            $invoices->transform(fn ($invoice) =>
                $this->updateInvoice->handle($invoice, $data->toArray())
            );
        }

        return $invoices;
    }
}