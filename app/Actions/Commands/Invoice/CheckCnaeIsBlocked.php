<?php

namespace App\Actions\Commands\Invoice;

class CheckCnaeIsBlocked
{
    public function handle(string $cnae)
    {
        $blockedCnae = config('antares.blocked_cnae');

        return in_array($cnae, $blockedCnae);
    }
}