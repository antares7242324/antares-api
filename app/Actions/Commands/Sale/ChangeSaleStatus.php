<?php

namespace App\Actions\Commands\Sale;
use App\Models\Sale\Sale;

class ChangeSaleStatus
{
   /**
    * Restore or delete a Sale.
    * 
    * @param  \App\Models\Sale\Sale  $sale
    *   The model to restore or delete.
    * @param  bool  $active
    *   Restore when $active is true and Sale is trashed, otherwise delete if sale is not trashed.
    * @return  \App\Models\Sale\Sale
    *   Returns the Sale even if it was not restored or deleted.
    */
   public function handle(Sale $sale, bool $active) : Sale
   {
      if ($active === true && $sale->trashed()) {
         $sale->restore();
      }

      if ($active === false && ! $sale->trashed()) {
         $sale->delete();
      }

      return $sale;
   }
}