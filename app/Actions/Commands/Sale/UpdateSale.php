<?php

namespace App\Actions\Commands\Sale;

use App\Models\Sale\Sale;
use App\Models\Account\User;
use Illuminate\Support\Carbon;
use App\Actions\Commands\Sale\ProcessSaleUsers;
use App\Actions\Commands\Sale\ProcessSaleAdmins;
use App\Actions\Commands\Sale\ProcessSaleRemovedUsers;

class UpdateSale
{
    public function handle(Sale $sale, array $validated)
    {
        $data = collect($validated);

        $dataUpdate = $data->only(
            'name',
            'discount',
            'start_at',
            'end_at'
        );

        $dataUpdate
            ->when($dataUpdate->has('start_at'), function ($data) {
                $startAt = $data->get('start_at');
                if ($startAt !== null) {
                    $data->put('start_at', Carbon::parse($startAt));
                }
            })
            ->when($dataUpdate->has('end_at'), function ($data) {
                $endAt = $data->get('end_at');
                if ($endAt !== null) {
                    $data->put('end_at', Carbon::parse($endAt));
                }
            });

        $sale->update($dataUpdate->toArray());

        // Sincroniza o campo users caso ele seja enviado.
        // Se estiver vazio, vai desvincular todos os users.
        if ($data->has('users')) {
            $userIds = $data->get('users', []);

            // Prepara os dados a serem processados adiante.
            $originalUsers = $sale->users()->get();
            $updatedUsers = User::find($userIds);

            // Atualiza os users dentro da promoção.
            $sale->users()->sync($userIds);

            // Processa os usuários que foram removidos.
            (new ProcessSaleRemovedUsers)
                ->handle($sale, $originalUsers, $updatedUsers);
        }

        // Processa as atividades e envio de emails
        // dos usuários atualizados na promoção.
        (new ProcessSaleUsers)->handle($sale);

        // Processa as atividades dos admins.
        (new ProcessSaleAdmins)->handle($sale);

        return $sale;
    }
}
