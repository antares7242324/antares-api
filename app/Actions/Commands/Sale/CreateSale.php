<?php

namespace App\Actions\Commands\Sale;

use App\Models\Sale\Sale;
use App\Actions\Commands\Sale\ProcessSaleUsers;
use App\Actions\Commands\Sale\ProcessSaleAdmins;
use Illuminate\Support\Carbon;

class CreateSale
{
    public function handle(array $validated)
    {
        $data = collect($validated);

        $dataCreate = $data->only(
            'name',
            'discount',
            'start_at',
            'end_at'
        );

        $dataCreate
            ->when($dataCreate->has('start_at'), function ($data) {
                $startAt = $data->get('start_at');
                if ($startAt !== null) {
                    $data->put('start_at', Carbon::parse($startAt));
                }
            })
            ->when($dataCreate->has('end_at'), function ($data) {
                $endAt = $data->get('end_at');
                if ($endAt !== null) {
                    $data->put('end_at', Carbon::parse($endAt));
                }
            });

        $sale = Sale::create($dataCreate->toArray());

        // Adiciona users se for enviado o campo no form.
        // Sincroniza APENAS os users que estiverem dentro do array.
        $userIds = $data->only('users');

        if ($userIds['users'] ?? [])
            $sale->users()->sync($userIds['users']);

        // Processa as atividades e envio de emails
        // dos usuários adicionados na promoção.
        (new ProcessSaleUsers)->handle($sale);

        // Processa as atividades dos admins.
        (new ProcessSaleAdmins)->handle($sale);

        return Sale::find($sale->id);
    }
}
