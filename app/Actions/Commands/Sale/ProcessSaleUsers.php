<?php

namespace App\Actions\Commands\Sale;

use App\Models\Sale\Sale;
use App\Jobs\Activities\AddSaleClosedToUserJob;
use App\Jobs\Activities\AddSaleAvailableToUserAndMailJob;

class ProcessSaleUsers
{
   public function handle(Sale $sale)
   {
      // Monta o user admin para os processos.
      $responsible = auth()->user();

      $users = $sale->users;

      // Faz as validações para adicionar as atividades
      // caso algum admin resolva modificar as datas de
      // início e término da promoção. Do contrário, essa
      // verificação é feita periodicamente no servidor.

      if ($sale->isOpen()) {
         $users->each(function ($user) use ($sale, $responsible) {
            // Adiciona atividade de promoção disponível
            // e envia o email se for possível.
            AddSaleAvailableToUserAndMailJob::dispatch(
               $sale->id,
               $user->id,
               $responsible->id
            );
         });
      }

      if ($sale->isClosed()) {
         $users->each(fn($user) =>
            // Adiciona atividade de promoção encerrada.
            AddSaleClosedToUserJob::dispatch($sale->id, $user->id, $responsible->id)
         );
      }
   }
}