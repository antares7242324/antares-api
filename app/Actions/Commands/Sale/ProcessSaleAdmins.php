<?php

namespace App\Actions\Commands\Sale;

use App\Models\Sale\Sale;
use App\Actions\Queries\ListAdminUsers;
use App\Jobs\Activities\AddSaleClosedToUserJob;
use App\Jobs\Activities\AddSaleAvailableToUserJob;

class ProcessSaleAdmins
{
   public function handle(Sale $sale)
   {
      $responsible = auth()->user();

      $admins = (new ListAdminUsers)->handle();

      // Faz as validações para adicionar as atividades
      // caso algum admin resolva modificar as datas de
      // início e término da promoção. Do contrário, essa
      // verificação é feita periodicamente no servidor.

      if ($sale->isOpen()) {
         // Adiciona atividade de promoção disponível.
         $admins->each(fn($admin) =>
            AddSaleAvailableToUserJob::dispatch($sale->id, $admin->id, $responsible->id)
         );
      }

      if ($sale->isClosed()) {
         // Adiciona atividade de promoção encerrada.
         $admins->each(fn($admin) =>
            AddSaleClosedToUserJob::dispatch($sale->id, $admin->id, $responsible->id)
         );
      }
   }
}