<?php

namespace App\Actions\Commands\Sale;

use App\Jobs\Activities\DeleteSaleFromUserJob;
use App\Models\Sale\Sale;
use Illuminate\Support\Collection;

class ProcessSaleRemovedUsers
{
   public function handle(Sale $sale, Collection $originalUsers, Collection $updatedUsers)
   {
      // Cria nova collection com apenas os users removidos.
      $removedUsers = $originalUsers->diff($updatedUsers);

      if ($removedUsers) {
         $removedUsers->each(fn($user) =>
            DeleteSaleFromUserJob::dispatch($sale->id, $user->id)
         );
      }
   }
}