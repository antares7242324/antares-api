<?php

namespace App\Actions\Commands\Mailer;

use App\Mail\InvoiceConfirmationMail;
use App\Models\Hunt\Invoice;
use Illuminate\Support\Facades\Mail;

class MailInvoiceConfirmation
{
    /**
     * Envia o email de confirmação de nota fiscal se ela for aprovada.
     *
     * @param \App\Models\Hunt\Invoice $invoice
     *   O model de nota fiscal.
     * @return bool
     *   True se o email foi enviado com sucesso. False caso contrário.
     */
    public function handle(Invoice $invoice)
    {
        if ($invoice->isApproved() === false) return false;

        $email = $invoice->user->email;

        $sentMessage = Mail::to($email)->send(new InvoiceConfirmationMail($invoice));

        if ($sentMessage) return true;

        return false;
    }
}