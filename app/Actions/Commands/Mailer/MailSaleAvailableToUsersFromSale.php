<?php

namespace App\Actions\Commands\Mailer;

use App\Mail\SaleAvailableMail;
use App\Models\Sale\Sale;
use Illuminate\Support\Facades\Mail;

class MailSaleAvailableToUsersFromSale
{
    public function handle(Sale $sale)
    {
        $users = $sale->users;

        if ($users->isEmpty()) return false;

        // Criado o contador para verificar se pelo
        // menos um email foi enviado com sucesso.
        $sentCounter = 0;

        $users->each(function ($user) use ($sale, $sentCounter) {
            $sentMessage = Mail::to($user->email)
                ->send(new SaleAvailableMail($sale, $user));

            if ($sentMessage) $sentCounter++;
        });

        return $sentCounter > 0 ? true : false;
    }
}