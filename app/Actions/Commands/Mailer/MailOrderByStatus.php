<?php

namespace App\Actions\Commands\Mailer;

use App\Mail\OrderSentMail;
use App\Models\Order\Order;
use App\Mail\OrderCancelledMail;
use App\Mail\OrderCompletedMail;
use App\Mail\OrderRequestedMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderRequestedDistributorMail;

class MailOrderByStatus
{
    public function handle(Order $order, string $status = null)
    {
        $email = $order->user->email;

        $sentMessage = $this->send($order, $email, $status);

        if ($sentMessage) return true;

        return false;
    }

    /**
     * Envia o email de acordo com o status do pedido
     *
     * @param \App\Models\Order\Order $order
     *   Objeto do pedido.
     * @param string $email
     *   Email do usuário.
     * @return \Illuminate\Mail\SentMessage|null
     */
    private function send(Order $order, string $email, string $status = null)
    {
        if (is_null($status) === false) {
            $mail = $order->mailByStatus($status);

            if (is_null($mail)) return null;

            return Mail::to($email)->send($mail);
        }

        if ($order->isRequested()) {
            if (config('antares.email') !== null) {
                Mail::to(config('antares.email'))
                    ->send(new OrderRequestedDistributorMail($order));
            }

            return Mail::to($email)->send(new OrderRequestedMail($order));
        }

        if ($order->isSent()) {
            return Mail::to($email)->send(new OrderSentMail($order));
        }

        if ($order->isCompleted()) {
            return Mail::to($email)->send(new OrderCompletedMail($order));
        }

        if ($order->isCancelled()) {
            return Mail::to($email)->send(new OrderCancelledMail($order));
        }

        return null;
    }
}