<?php

namespace App\Actions\Commands\PriceList;

use App\Models\Product\PriceList;
use Illuminate\Support\Collection;

class UpdateCreatePriceList
{
    public function handle(Collection $data)
    {
        $priceList = PriceList::updateOrCreate(
            ['code_erp' => $data->get('code_erp')],
            $data->toArray()
        );

        return $priceList;
    }
}