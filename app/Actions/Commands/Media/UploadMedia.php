<?php

namespace App\Actions\Commands\Media;

use App\Support\Media\MediaStorage;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Auth\User;

class UploadMedia
{
    public function handle(UploadedFile $file, User|null $user = null, $storage = MediaStorage::PRIVATE)
    {
        $storagePath = 'uploads';

        if ($user) {
            $storagePath = 'users/' . 'uid' . $user->id;
        }

        return $file->store($storagePath, $storage);
    }
}