<?php

namespace App\Actions\Commands\Media;

use App\Models\File\Media;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class OpenMediaContent
{
    public function handle(Media $media)
    {
        // TODO: Desacoplar essa parte do código e montar um serviço
        //   que verificará se o arquivo está salvo no storage ou
        //   em algum serviço da nuvem. Então recupera o arquivo.
        $filePath = Storage::disk($media->storage)->path($media->path);

        if (! File::exists($filePath)) {
            abort(404, 'File not found');
        }

        return File::get($filePath);
    }
}