<?php

namespace App\Actions\Commands\Media;

use App\Models\File\Media;
use App\Support\Media\MediaStorage;
use Illuminate\Foundation\Auth\User;
use App\Actions\Commands\Media\UploadMedia;

class CreateMedia
{
    public function handle(array $validated, User|null $user = null)
    {
        $data = collect($validated);

        $file = $data->get('file');

        // Upload the file.
        $uploadMedia = new UploadMedia;

        $path = $uploadMedia->handle($file, $user);

        // Edit the data array.
        $data->put('path', $path);

        $data->forget('file');

        if (! $data->get('name'))
            $data->put('name', $file->getClientOriginalName());

        if (! $data->get('extension'))
            $data->put('extension', $file->getClientOriginalExtension());

        if (! $data->get('type'))
            $data->put('type', $file->getClientMimeType());

        if (! $data->get('storage'))
            $data->put('storage', MediaStorage::PRIVATE);

        // Create the Media model.
        return Media::create($data->toArray());
    }
}