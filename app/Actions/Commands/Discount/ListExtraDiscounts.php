<?php

namespace App\Actions\Commands\Discount;

class ListExtraDiscounts
{
   public function handle()
   {
      $discounts = explode(',', str_replace(' ', '',
         config('antares.extra_discounts')
      ));

      return $discounts;
   }

   public function toReadable()
   {
      $discounts = collect($this->handle());

      $discounts = $discounts->map(fn($discount) => (int) ($discount * 100));

      return $discounts->toArray();
   }
}