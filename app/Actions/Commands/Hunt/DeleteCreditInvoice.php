<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Transaction\CreditInvoice;
use App\Actions\Commands\Hunt\UpdateUserBalance;

class DeleteCreditInvoice
{
    /** @var \App\Actions\Commands\Hunt\UpdateUserBalance */
    protected $updateBalance;

    public function __construct()
    {
        $this->updateBalance = new UpdateUserBalance;
    }

    /**
     * Remove o registro de CreditInvoice informado e atualiza o saldo do usuário.
     * Usado para debitar o valor creditado quando a nota fiscal for rejeitada.
     *
     * @param \App\Models\Transaction\CreditInvoice $creditInvoice
     *   O registro de CreditInvoice.
     * @return bool
     *   Retorna true se o registro de CreditInvoice foi deletado
     *   e o saldo foi atualizado, false caso contrário.
     */
    public function handle(CreditInvoice $creditInvoice)
    {
        // Busca o usuário antes de deletar.
        $user = $creditInvoice->user;

        // Deleta se encontrou, false se não conseguiu deletar.
        if (! $creditInvoice->delete()) return false;

        // Atualiza o saldo do usuário.
        $this->updateBalance->handle($user);

        return true;
    }
}