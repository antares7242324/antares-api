<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Hunt\Invoice;
use App\Actions\Commands\Hunt\AddDebitFromCanceledInvoice;
use App\Libraries\DebitCanceledType;
use App\Models\Transaction\CreditInvoice;

class CancelCreditFromInvoice
{
    /** @var \App\Actions\Commands\Hunt\AddDebitCanceledInvoice */
    protected $addDebitCanceledInvoice;

    public function __construct()
    {
        $this->addDebitCanceledInvoice = new AddDebitFromCanceledInvoice;
    }

    public function handle(Invoice $invoice, bool $byAdmin = false): bool
    {
        // Não pode remover o crédito se não houver valor para tal.
        if (is_null($invoice->lastCredit())) return false;

        // Pega o valor do crédito da nota fiscal e usa para debitar.
        $creditInvoice = CreditInvoice::query()
            ->where('invoice_id', $invoice->id)
            ->first();

        $debit = $creditInvoice->amount;

        // Cria o model DebitCancelInvoice e atualiza o saldo do usuário.
        $debitCanceledInvoice = $this->addDebitCanceledInvoice->handle(
            $invoice,
            $debit,
            $invoice->user->available_balance,
            $byAdmin ? DebitCanceledType::ADMIN :DebitCanceledType::STANDARD,
        );

        if (is_null($debitCanceledInvoice)) return false;

        return true;
    }
}