<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Hunt\Invoice;
use App\Actions\Commands\Hunt\UpdateUserBalance;

class AddCreditFromInvoice
{
    /** @var \App\Actions\Commands\Hunt\UpdateUserBalance */
    protected $updateBalance;

    public function __construct()
    {
        $this->updateBalance = new UpdateUserBalance;
    }

    /**
     * Aplica o crédito da nota fiscal e atualiza o saldo do usuário.
     *
     * @param \App\Models\Hunt\Invoice $invoice
     *   O objeto nota fiscal.
     * @param int $credit
     *   O valor do crédito.
     * @param int $balance
     *   O saldo atual.
     * @param bool $reopened
     *   Indica se a nota fiscal foi reaberta.
     * @return \App\Models\Transaction\CreditInvoice
     *   O objeto CreditInvoice.
     */
    public function handle(Invoice $invoice, int $credit, int $balance, bool $reopened = false)
    {
        $user = $invoice->user;

        $creditInvoice = $invoice->credits()->create([
            'user_id' => $user->id,
            'amount' => abs($credit),
            'current_balance' => $balance,
            'reopened' => $reopened,
        ]);

        // Atualiza o saldo do usuário.
        $this->updateBalance->handle($user);

        return $creditInvoice;
    }
}