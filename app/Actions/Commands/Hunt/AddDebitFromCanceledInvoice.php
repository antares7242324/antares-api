<?php

namespace App\Actions\Commands\Hunt;

use App\Libraries\DebitCanceledType;
use App\Models\Hunt\Invoice;
use App\Models\Transaction\DebitCanceledInvoice;

class AddDebitFromCanceledInvoice
{
    /** @var \App\Actions\Commands\Hunt\UpdateUserBalance */
    protected $updateBalance;

    public function __construct()
    {
        $this->updateBalance = new UpdateUserBalance;
    }

    public function handle(Invoice $invoice, int $debit, int $balance, ?string $type = null): DebitCanceledInvoice|null
    {
        $user = $invoice->user;

        $debitCanceledInvoice = $invoice->debits()->create([
            'user_id' => $user->id,
            'amount' => abs($debit),
            'current_balance' => $balance,
            'type' => $type ?? DebitCanceledType::STANDARD,
        ]);

        // Atualiza o saldo do usuário.
        $this->updateBalance->handle($user);

        return $debitCanceledInvoice;
    }
}