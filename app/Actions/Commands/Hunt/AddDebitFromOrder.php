<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Order\Order;
use App\Actions\Commands\Hunt\UpdateUserBalance;

class AddDebitFromOrder
{
   /**
    * @var \App\Actions\Commands\Hunt\UpdateUserBalance
    */
   protected $updateBalance;

   public function __construct()
   {
      $this->updateBalance = new UpdateUserBalance;
   }

   /**
    * Aplica a dedução no saldo no usuário.
    *
    * @param \App\Models\Order\Order $order
    *   O pedido feito.
    * @param int $debit
    *   O valor a ser deduzido do saldo.
    * @param int $balance
    *   O valor atual do saldo ainda sem o débito.
    * @return \App\Models\Transaction\DebitOrder
    *   O objeto DebitOrder.
    */
   public function handle(Order $order, int $debit, int $balance)
   {
      $user = $order->user;

      $debitOrder = $order->debitTransaction()->create([
         'user_id' => $user->id,
         'amount' => abs($debit),
         'current_balance' => $balance,
      ]);

      // Atualiza o saldo do usuário.
      $this->updateBalance->handle($user);

      return $debitOrder;
   }
}