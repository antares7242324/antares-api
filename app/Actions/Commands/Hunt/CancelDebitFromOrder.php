<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Order\Order;
use App\Models\Transaction\DebitOrder;
use App\Models\Transaction\CreditCanceledOrder;
use App\Actions\Commands\Hunt\AddCreditFromCanceledOrder;

class CancelDebitFromOrder
{
    /**
     * @var \App\Actions\Commands\Hunt\AddCreditFromCanceledOrder
     */
    protected $AddCreditCanceledOrder;

    public function __construct()
    {
        $this->AddCreditCanceledOrder = new AddCreditFromCanceledOrder;
    }

    public function handle(Order $order): bool
    {
        // Apenas pode reembolsar se o pedido for cancelado.
        if ($order->isCancelled() === false) return false;

        // Não pode creditar se já existe algum crédito para este pedido.
        $creditCanceledOrder = CreditCanceledOrder::query()
            ->where('order_id', $order->id);

        if ($creditCanceledOrder->count() > 0) return false;

        // Não pode remover o débito se não houver valor para tal.
        if (! optional($order->debitTransaction)->exists()) return false;

        // Pega o valor do débito do pedido e usa para creditar.
        $debitOrder = DebitOrder::query()
            ->where('order_id', $order->id)
            ->first();

        $credit = abs($debitOrder->amount);

        // Cria o model CreditCanceledOrder e atualiza o saldo do usuário.
        $creditCanceledOrder = $this->AddCreditCanceledOrder->handle(
            $order, $credit, $order->user->available_balance
        );

        if (is_null($creditCanceledOrder)) return false;

        return true;
    }
}