<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Product\Item;
use App\Support\Hunt\HuntCalculation;
use App\Traits\Currency;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;

class CalculateItemHunt
{
    use Currency;

    protected $user;

    protected $huntCalc;

    public function __construct(User $user = null)
    {
        $this->user = $user;
        $this->huntCalc = new HuntCalculation;
    }

    /**
     * Adiciona os dados do usuário para o cálculo da caçada.
     *
     * @param \Illuminate\Foundation\Auth\User $user A entidade do usuário.
     * @return \App\Actions\Commands\Hunt\CalculateItemHunt A própria classe.
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Calcula a caçada no item informado.
     *
     * @param \App\Models\Product\Item $item A entidade do item.
     * @param int $soldQty A quantidade de itens na nota fiscal.
     * @param int $soldSubtotal O valor total dos itens na nota fiscal.
     */
    public function handle(Item $item, int $soldQty, int $soldSubtotal): Collection
    {
        // Objeto tabela de preços.
        $priceItem = $item->priceFromList($this->user->price_list_id);

        // Valor original do produto em centavos.
        $originalPrice = $priceItem->original_amount;
        $originalSubtotal = $originalPrice * $soldQty;

        // Valor de tabela do produto em centavos.
        $listPrice = $priceItem->amount;
        $listSubtotal = $listPrice * $soldQty;

        // Percentual de desconto aplicado pelo distribuidor.
        $sellerPercent = $originalSubtotal > 0
            ? 1 - ($soldSubtotal / $originalSubtotal)
            : 0;

        // Valor percentual da tara.
        $huntTarePercent = floatval(config('antares.hunt_tare'));

        // Valor do alfa.
        $huntAlpha = floatval(config('antares.hunt_alpha'));

        // Percentual da caçada. Se a tara for maior que o percentual do
        // desconto dado ao consumir, então o percentual da caçada é zero.
        $huntPercent = $huntTarePercent < $sellerPercent
            ? $huntAlpha * ($sellerPercent - $huntTarePercent)
            : 0;

        // Se o percentual da caçada for maior que 61%, então o valor
        // final do desconto tem que ser 61%.
        if ($huntPercent > 0.61) $huntPercent = 0.61;

        // Valor do desconto da caçada em centavos.
        $amountDiscount = $listSubtotal - ($listSubtotal * $huntPercent);

        // Valor do cashback.
        $cashback = intval(round($listSubtotal - $amountDiscount));

        return collect([
            'unit_list' => $originalPrice,
            'subtotal_list' => $originalSubtotal,
            'unit_cost' => $listPrice,
            'subtotal_cost' => $listSubtotal,
            'seller_percent' => $sellerPercent,
            'hunt_percent' => $huntPercent,
            'amount_discount' => $amountDiscount,
            'cashback' => $cashback,
        ]);
    }
}