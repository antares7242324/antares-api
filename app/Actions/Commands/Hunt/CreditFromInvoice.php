<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Hunt\Invoice;
use App\Models\Transaction\CreditInvoice;

class CreditFromInvoice
{
   /** @var \App\Actions\Commands\Hunt\AddCreditFromInvoice */
   protected $addCreditInvoice;

   public function __construct()
   {
      $this->addCreditInvoice = new AddCreditFromInvoice;
   }

   public function handle(Invoice $invoice, bool $reopened = false)
   {
      // Não pode creditar se não existem itens vinculados na nota fiscal.
      if ($invoice->items->isEmpty()) return false;

      // Soma o refund de todos os itens para calcular o crédito.
      $credit = $invoice->items()->sum('refund');

      // O valor a ser reembolsado tem que ser positivo.
      // Caso o valor seja zero ainda assim adiciona como crédito
      // para funciona como um extrato da nota fiscal.
      if ($credit < 0) return false;

      // Cria o model CreditInvoice e atualiza o saldo do usuário.
      $creditInvoice = $this->addCreditInvoice->handle(
         $invoice, $credit, $invoice->user->available_balance, $reopened
      );

      if (! $creditInvoice) return false;

      return true;
   }
}