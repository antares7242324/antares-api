<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Order\Order;
use App\Models\Transaction\CreditCanceledOrder;

class AddCreditFromCanceledOrder
{
    /** @var \App\Actions\Commands\Hunt\UpdateUserBalance */
    protected $updateBalance;

    public function __construct()
    {
        $this->updateBalance = new UpdateUserBalance;
    }

    public function handle(Order $order, int $credit, int $balance): CreditCanceledOrder|null
    {
        $user = $order->user;

        $creditCanceledOrder = $order->creditTransaction()->create([
            'user_id' => $user->id,
            'amount' => abs($credit),
            'current_balance' => $balance,
        ]);

        // Atualiza o saldo do usuário.
        $this->updateBalance->handle($user);

        return $creditCanceledOrder;
    }
}