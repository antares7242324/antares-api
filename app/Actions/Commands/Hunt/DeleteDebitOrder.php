<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Transaction\DebitOrder;

class DeleteDebitOrder
{
    /**
    * @var \App\Actions\Commands\Hunt\UpdateUserBalance
    */
    protected $updateBalance;

    public function __construct()
    {
        $this->updateBalance = new UpdateUserBalance;
    }

    /**
     * Remove o registro de DebitOrder informado e atualiza o saldo do usuário.
     * Usado para reembolsar o valor debitado quando o pedido for cancelado.
     *
     * @param \App\Models\Transaction\DebitOrder $debitOrder
     *   O registro de DebitOrder.
     * @return bool
     *   Retorna true se o registro de DebitOrder foi deletado
     *   e o saldo foi atualizado, false caso contrário.
     */
    public function handle(DebitOrder $debitOrder)
    {
        // Busca o usuário antes de deletar.
        $user = $debitOrder->user;

        // Deleta se encontrou, false se não conseguiu deletar.
        if (! $debitOrder->delete()) return false;

        // Atualiza o saldo do usuário.
        $this->updateBalance->handle($user);

        return true;
    }
}