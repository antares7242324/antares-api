<?php

namespace App\Actions\Commands\Hunt;

use Illuminate\Foundation\Auth\User;

class UpdateUserBalance
{
    public function handle(User $user)
    {
        $credits = $user->sumCredits();

        $debits = $user->sumDebits();

        $user->available_balance = $credits - $debits;

        $user->save();

        return $user->available_balance;
    }
}