<?php

namespace App\Actions\Commands\Hunt;

use App\Models\Order\Order;
use Illuminate\Support\Facades\Log;
use App\Models\Transaction\DebitOrder;

class DebitFromOrder
{
    /**
     * @var \App\Actions\Commands\Hunt\AddDebitFromOrder
     */
    protected $addDebitOrder;

    public function __construct()
    {
        $this->addDebitOrder = new AddDebitFromOrder;
    }

    public function handle(Order $order, int $debit, int $balance)
    {
        // Apenas pode debitar se o pedido for solicitado.
        if (! $order->isRequested()) return false;

        // Não pode debitar se não houver valor para tal.
        if ($debit >= 0) return false;

        // Não pode debitar se já existe algum débito para este pedido.
        if (DebitOrder::where('order_id', $order->id)->count() > 0) {
            return false;
        }

        // Cria o model DebitOrder e atualiza o saldo do usuário.
        $debitOrder = $this->addDebitOrder->handle($order, $debit, $balance);

        if (! $debitOrder) return false;

        return true;
    }
}