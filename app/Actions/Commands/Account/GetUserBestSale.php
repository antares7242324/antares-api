<?php

namespace App\Actions\Commands\Account;

use App\Models\Sale\Sale;
use App\Models\Account\User;

class GetUserBestSale
{
    /**
     * Handle the user and return a sale if available.
     *
     * @param User $user The user object
     * @return Sale|null The sale object or null
     */
    public function handle(User $user): ?Sale
    {
        $sale = $user->sales()
            ->whereNull('sales.deleted_at')
            ->orderBy('discount', 'desc')
            ->first();

        return $sale;
    }
}