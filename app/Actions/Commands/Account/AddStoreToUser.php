<?php

namespace App\Actions\Commands\Account;
use App\Models\Account\User;
use App\Models\Account\Store;
use App\Http\Requests\Account\CreateStoreRequest;

class AddStoreToUser
{
    public function handle(User $user, CreateStoreRequest $request) : Store
    {
        $data = $request->validated();

        $data['user_id'] = $user->id;

        $store = Store::create($data);

        return Store::find($store->id);
    }
}