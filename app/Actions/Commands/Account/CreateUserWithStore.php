<?php

namespace App\Actions\Commands\Account;

use Illuminate\Support\Collection;

class CreateUserWithStore
{
    protected CreateUser $createUser;

    public function __construct()
    {
        $this->createUser = new CreateUser();
    }

    public function handle(Collection $data)
    {
        $store = $data->pull('store');

        $user = $this->createUser->handle($data);

        $user->stores()->create($store);

        $user->refresh();

        return $user;
    }
}