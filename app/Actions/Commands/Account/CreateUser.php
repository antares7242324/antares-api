<?php

namespace App\Actions\Commands\Account;

use App\Models\Account\User;
use Illuminate\Support\Collection;

class CreateUser
{
    public function handle(Collection $data)
    {
        $user = User::create($data->toArray());

        $user->refresh();

        return $user;
    }
}

