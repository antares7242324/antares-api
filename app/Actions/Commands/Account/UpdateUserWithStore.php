<?php

namespace App\Actions\Commands\Account;

use App\Models\Account\User;
use Illuminate\Support\Collection;

class UpdateUserWithStore
{
    protected UpdateUser $updateUser;
    protected UpdateUserStore $updateUserStore;

    public function __construct()
    {
        $this->updateUser = new UpdateUser();

        $this->updateUserStore = new UpdateUserStore();
    }

    public function handle(User $user, Collection $data)
    {
        $store = collect($data->pull('store'));

        if ($store->isNotEmpty()) {
            $this->updateUserStore->handle($user, $store);
        }

        $user = $this->updateUser->handle($user, $data);

        $user->refresh();

        return $user;
    }
}