<?php

namespace App\Actions\Commands\Account;

use App\Models\Account\User;
use Illuminate\Support\Collection;

// Ação criada por enquanto que um user pode ter apenas 1 loja.
class UpdateUserStore
{
   public function handle(User $user, Collection $validated)
   {
      $activeStore = $user->activeStore();

      if ($activeStore->cnpj != $validated->get('cnpj')) {
         $user->stores->each(fn ($store) => $store->delete());

         $user->stores()->create($validated->toArray());
      }
      else {
         $activeStore->update($validated->toArray());
      }

      return $user->activeStore();
   }
}