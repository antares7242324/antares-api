<?php

namespace App\Actions\Commands\Account;

use App\Models\Account\User;
use Illuminate\Support\Collection;

class UpdateUser
{
    public function handle(User $user, Collection $data)
    {
        $user->update($data->toArray());

        $user->refresh();

        return $user;
    }
}
