<?php

namespace App\Actions\Commands\Transaction;
use App\Actions\Commands\Hunt\UpdateUserBalance;
use App\Models\Transaction\Compensation;

class UpdateCompensation
{
   public function handle(Compensation $compensation, array $validated)
   {
      $data = collect($validated);

      $compensation->update($data->toArray());

      (new UpdateUserBalance)->handle($compensation->user);

      return Compensation::find($compensation->id);
   }
}