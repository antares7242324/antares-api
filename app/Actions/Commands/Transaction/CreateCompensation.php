<?php

namespace App\Actions\Commands\Transaction;

use Illuminate\Foundation\Auth\User;
use App\Models\Transaction\Compensation;
use App\Actions\Commands\Hunt\UpdateUserBalance;

class CreateCompensation
{
    /**
     * Cria a compensação.
     *
     * @param array $validated
     *   Os campos a serem inseridos.
     * @param User $creator
     *   O usuário que realizou a operação.
     * @return \App\Models\Transaction\Compensation
     */
    public function handle(array $validated, User $creator)
    {
        $data = collect($validated)
            ->put('creator_id', $creator->id);

        $data->put('user_id', $data->get('user'))
            ->forget('user');

        $compensation = Compensation::create($data->toArray());

        (new UpdateUserBalance)->handle($compensation->user);

        return $compensation;
    }
}