<?php

namespace App\Actions\Commands\Transaction;

use Illuminate\Foundation\Auth\User;
use App\Models\Transaction\Compensation;
use App\Actions\Commands\Hunt\UpdateUserBalance;

class CreateManyCompensation
{
   /**
    * Cria objetos Compensation baseados nos usuários informados.
    *
    * @param array $validated
    *   Os campos a serem inseridos.
    * @param User $creator
    *   O usuário que realizou a operação.
    * @return \Illuminate\Database\Eloquent\Collection<\App\Models\Transaction\Compensation>
    */
   public function handle(array $validated, User $creator)
   {
      $validated = collect($validated)
         ->put('creator_id', $creator->id);

      $users = User::whereIn('id', $validated->get('users'))->get();

      $data = $users->map(function ($user) use ($validated) {
         $data = clone $validated;

         return $data->forget('users')
            ->put('current_balance', $user->available_balance)
            ->put('user_id', $user->id)
            ->toArray();
      });

      $compensations = $data->map(
         fn($compensation) => Compensation::create($compensation)
      );

      $compensations->each(
         fn($compensation) => (new UpdateUserBalance)->handle($compensation->user)
      );

      return $compensations;
   }
}