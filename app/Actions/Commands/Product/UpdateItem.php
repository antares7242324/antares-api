<?php

namespace App\Actions\Commands\Product;

use App\Http\Requests\Product\UpdateItemRequest;
use App\Models\Product\Item;

class UpdateItem
{
    public function handle(Item $item, UpdateItemRequest $request)
    {
        $data = $request->validated();

        $item->update($data);

        return $item;
    }
}
