<?php

namespace App\Actions\Commands\Product;

use App\Models\Account\User;
use Illuminate\Support\Collection;

class CreateUserItem
{
   public function handle(Collection $validated)
   {
      $user = User::find($validated->get('user_id'));

      $validated->forget('user_id');

      return $user->userItems()->create($validated->toArray());
   }
}