<?php

namespace App\Actions\Commands\Product;

use App\Http\Requests\Product\CreateItemRequest;
use App\Models\Product\Item;

class CreateItem
{
    public function handle(CreateItemRequest $request)
    {
        $data = $request->validated();

        $item = Item::create($data);

        return Item::find($item->id);
    }
}
