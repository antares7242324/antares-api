<?php

namespace App\Actions\Commands\Product;

use App\Models\Account\User;
use Illuminate\Support\Collection;

class UpdateUserItem
{
   public function handle(User $user, Collection $validated)
   {
      return $user->userItems()->create($validated->toArray());
   }
}