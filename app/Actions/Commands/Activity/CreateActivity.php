<?php

namespace App\Actions\Commands\Activity;

use App\Models\Activity;

class CreateActivity
{
    public function handle(array $validated)
    {
        $data = collect($validated);

        $activity = Activity::create($data->toArray());

        return $activity;
    }
}