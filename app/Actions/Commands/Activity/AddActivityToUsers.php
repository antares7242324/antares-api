<?php

namespace App\Actions\Commands\Activity;

use App\Models\Activity;
use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Model;
use App\Actions\Commands\Activity\CreateActivity;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class AddActivityToUsers
{
   /** @var \App\Actions\Commands\Activity\CreateActivity */
   protected $create;

   public function __construct()
   {
      $this->create = new CreateActivity;
   }

   /**
    * Adicionar atividade para os usuários informados.
    *
    * @param EloquentCollection|SupportCollection $users
    *   A coleção de usuários, seja Illuminate\Database\Eloquent\Collection ou Illuminate\Support\Collection.
    * @param string $action
    *   A ação da atividade baseada em \App\Libraries\ActivityAction.
    * @param string $loggableType
    *   O tipo de loggable, que é o namespace do model. Exemplo: \App\Models\Sale\Sale::class
    * @param Model $loggableModel
    *   O model do loggable.
    */
   public function handle(EloquentCollection|SupportCollection $users, string $action, string $loggableType, Model $loggableModel, string $restriction, User $responsible = null)
   {
      $activities = collect();

      // Adiciona a atividade para cada usuário novo vinculado.
      $users->each(function ($user) use ($activities, $action, $loggableType, $loggableModel, $restriction, $responsible) {
         $activity = Activity::where('action', $action)
            ->where('user_id', $user->id)
            ->where('loggable_type', $loggableType)
            ->where('loggable_id', $loggableModel->id)
            ->get();

         $data = collect([
            'sale' => $loggableModel->getAttributes(),
            'user' => $user->getAttributes(),
         ]);

         $activityData = collect()
            ->put('action', $action)
            ->when(
               is_null($responsible) === false,
               fn($collection) => $collection->put('responsible_id', $responsible->id)
            )
            ->put('restriction', $restriction)
            ->put('loggable_type', $loggableType)
            ->put('loggable_id', $loggableModel->id)
            ->put('user_id', $user->id)
            ->put('data', json_encode($data->toArray()));

         $activity = $this->create->handle($activityData->toArray());

         $activities->add($activity);
      });

      return $activities;
   }
}