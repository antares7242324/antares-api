<?php

namespace App\Actions\Commands\Activity;

use App\Models\Activity;
use App\Models\Sale\Sale;
use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use App\Models\Transaction\Compensation;
use App\Http\Resources\Activity\LoggableSaleResource;
use App\Http\Resources\Activity\LoggableUserResource;
use App\Http\Resources\Activity\LoggableOrderResource;
use App\Http\Resources\Activity\LoggableInvoiceResource;
use App\Http\Resources\Activity\LoggableCompensationResource;

class BuildActivityResource
{
    public function handle(Activity $activity)
    {
        if ($activity->loggable_type == Order::class)
            return LoggableOrderResource::make($activity);

        if ($activity->loggable_type == Invoice::class)
            return LoggableInvoiceResource::make($activity);

        if ($activity->loggable_type == Compensation::class)
            return LoggableCompensationResource::make($activity);

        if ($activity->loggable_type == User::class)
            return LoggableUserResource::make($activity);

        if ($activity->loggable_type == Sale::class)
            return LoggableSaleResource::make($activity);

        return [];
    }
}