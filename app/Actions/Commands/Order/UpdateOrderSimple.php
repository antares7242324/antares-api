<?php

namespace App\Actions\Commands\Order;

use App\Models\Order\Order;

class UpdateOrderSimple
{
   public function handle(array $validated, Order $order)
   {
      $data = collect($validated);

      $order->update($data->toArray());

      $order->refresh();

      return $order;
   }
}