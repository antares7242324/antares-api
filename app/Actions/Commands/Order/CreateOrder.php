<?php

namespace App\Actions\Commands\Order;

use App\Libraries\Shipping;
use App\Models\Order\Order;
use App\Libraries\OrderStatus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\User;
use App\Actions\Commands\Hunt\DebitFromOrder;
use App\Actions\Commands\Hunt\CancelDebitFromOrder;

class CreateOrder
{
    /**
     * @var \App\Actions\Commands\Hunt\DebitFromOrder
     */
    protected $debitOrder;

    /**
     * @var \App\Actions\Commands\Hunt\CancelDebitFromOrder
     */
    protected $cancelDebitOrder;

    public function __construct()
    {
        $this->debitOrder = new DebitFromOrder;
        $this->cancelDebitOrder = new CancelDebitFromOrder;
    }

    public function handle(Collection $params, User $user)
    {
        $status = $params->get('status');

        $data = collect($params)
            ->put('user_id', $user->id)
            // Coloca o desconto atual mais atualizado.
            ->put('current_discount', $user->current_discount)
            // Coloca a tabela de preços mais atualizada.
            ->put('price_list_id', $user->price_list_id)
            // Adiciona o tipo de frete padrão (CIF) se não foi informado.
            ->when($params->has('shipping') === false, fn ($data) =>
                $data->put('shipping', Shipping::CIF)
            )
            // Adiciona a data de emissão do pedido se ele for solicitado.
            ->when($status == OrderStatus::REQUESTED, fn ($data) =>
                $data->put('issuance_at', Carbon::now())
            )
            // Corrige o valor do desconto da promoção quando for nulo.
            ->when($params->has('sale_discount'), function ($data) {
                $saleDiscount = $data->get('sale_discount', 0) / 100;
                $data->put('sale_discount', $saleDiscount);
            });

        // Cria o pedido com todos os dados informados.
        $order = Order::create($data->toArray());

        // Verifica se precisa debitar o valor do saldo do usuário.
        // Debita se for necessário.
        $this->debitOrder->handle(
            $order,
            (int)$data->get('used_balance'),
            (int)$data->get('previous_balance')
        );

        // Verifica se o pedido foi cancelado o estorna o valor
        // do saldo do usuário caso ele tenha sido debitado.
        $this->cancelDebitOrder->handle($order);

        $order->refresh();

        return $order;
    }
}

