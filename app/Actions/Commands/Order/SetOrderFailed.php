<?php

namespace App\Actions\Commands\Order;

use App\Models\Order\Order;
use App\Libraries\OrderStatus;
use App\Libraries\OrderSystemNote;

class SetOrderFailed
{
    public function handle(Order $order)
    {
        $order->update([
            'status' => OrderStatus::FAILED,
            'system_notes' => OrderSystemNote::ANTARES_SEND_CONNECTION_FAILED,
        ]);

        $order->refresh();

        return $order;
    }
}