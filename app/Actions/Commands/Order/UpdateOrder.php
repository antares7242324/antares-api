<?php

namespace App\Actions\Commands\Order;

use App\Libraries\Shipping;
use App\Models\Order\Order;
use App\Libraries\OrderStatus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Jobs\Portal\ProcessOrderRequestJob;
use App\Actions\Commands\Hunt\DebitFromOrder;
use App\Actions\Commands\Hunt\CancelDebitFromOrder;

class UpdateOrder
{
    /**
     * @var \App\Actions\Commands\Hunt\DebitFromOrder
     */
    protected $debitOrder;

    /**
     * @var \App\Actions\Commands\Hunt\CancelDebitFromOrder
     */
    protected $cancelDebitOrder;

    public function __construct()
    {
        $this->debitOrder = new DebitFromOrder;
        $this->cancelDebitOrder = new CancelDebitFromOrder;
    }

    public function handle(Collection $params, Order $order)
    {
        $status = $params->get('status');

        $data = collect($params)
            // Coloca o desconto atual mais atualizado.
            ->put('current_discount', $order->user->current_discount)
            // Adiciona o tipo de frete padrão (CIF) se não foi informado.
            ->when($params->has('shipping') === false, fn ($data) =>
                $data->put('shipping', Shipping::CIF)
            )
            // Atualiza a tabela de preços caso esteja desativada.
            ->when($order->priceList->active === false, fn ($data) =>
                $data->put('price_list_id', $order->user->price_list_id)
            )
            // Corrige o valor do desconto da promoção quando for nulo.
            ->when($params->has('sale_discount'), function ($data) {
                $saleDiscount = $data->get('sale_discount', 0) / 100;
                $data->put('sale_discount', $saleDiscount);
            });

        // Se o pedido não estava solicitado e for solicitado agora,
        // adiciona a data de emissão do pedido.
        if ($order->isNotRequested() && $status == OrderStatus::REQUESTED) {
            $data->put('issuance_at', Carbon::now());
        }

        // Atualiza o pedido.
        $order->update($data->toArray());

        // Verifica se precisa debitar o valor do saldo do usuário.
        // Debita se for necessário.
        $this->debitOrder->handle(
            $order,
            (int)$data->get('used_balance'),
            (int)$data->get('previous_balance')
        );

        // Verifica se o pedido foi cancelado o estorna o valor
        // do saldo do usuário caso ele tenha sido debitado.
        $this->cancelDebitOrder->handle($order);

        $order->refresh();

        // Se o pedido não estava solicitado e for solicitado agora,
        // processa o envio para o portal antares.
        if ($order->isNotRequested() && $status == OrderStatus::REQUESTED) {
            Log::info("Order Observer: enviou requisição para antares", [
                'order' => $order->toArray(),
            ]);
            ProcessOrderRequestJob::dispatch($order->id);
        }

        return $order;
    }
}
