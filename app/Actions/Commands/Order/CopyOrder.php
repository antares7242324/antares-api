<?php

namespace App\Actions\Commands\Order;

use App\Libraries\OrderStatus;
use App\Models\Order\Order;

class CopyOrder
{
    protected $syncItems;

    public function __construct()
    {
        $this->syncItems = new SyncItemsToOrder;
    }

    public function handle(Order $order)
    {
        // Prepara para copiar os atributos do pedido orginal.
        $attributes = collect($order->attributesToArray());

        $attributes
            ->forget('id')
            ->forget('created_at')
            ->forget('updated_at')
            ->put('status', OrderStatus::DRAFT)
            // Atualiza o price_list_id caso esteja desativado.
            ->when(! $order->priceList->active, function ($attributes) use ($order) {
                $attributes->put('price_list_id', $order->user->price_list_id);
            });

        $orderCopy = Order::create($attributes->toArray());

        // Verifica se tem itens vinculados no pedido.
        $items = $order->items;

        // Se não tiver itens, não precisa copiar.
        if (! $items) return $orderCopy;

        // Prepara para copiar os itens dentro do pedido original.
        $itemsData = [
            'items' => $items->map(fn ($orderItem) => [
                'id' => $orderItem->item->id,
                'quantity' => $orderItem->quantity,
                'unit_original_price' => $orderItem->unit_original_price,
                'unit_discount_price' => $orderItem->unit_discount_price,
                'subtotal' => $orderItem->subtotal,
                'delivery_at' => $orderItem->delivery_at,
            ])->toArray(),
        ];

        return $this->syncItems->handle(collect($itemsData), $orderCopy);
    }
}

