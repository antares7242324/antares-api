<?php

namespace App\Actions\Commands\Order;

use App\Models\Order\Order;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SyncItemsToOrder
{
    public function handle(Collection $validated, Order $order)
    {
        // Remove todos os itens vinculados para poder adicionar ou atualizar
        // todos os itens no pedido. Se o pedido não possui itens ou eles
        // foram removidos, essa função garante isso.
        $order->items()->delete();

        if ($validated->has('items')) {
            $items = collect($validated->get('items', []));

            $order = DB::transaction(function () use ($items, $order) {
                $itemsData = $items->map(function ($item) {
                    $discount = $item['discount'] ?? 0.00;

                    $debit = $item['debit'] ?? 0;

                    $deliveryAt = Carbon::createFromFormat('Y-m-d', $item['delivery_at'])
                        ->toDateString();

                    return [
                        'item_id' => $item['id'],
                        'price_id' => $item['price_id'],
                        'quantity' => $item['quantity'],
                        'unit_original_price' => $item['unit_original_price'],
                        'unit_discount_price' => $item['unit_discount_price'],
                        'subtotal' => $item['subtotal'],
                        'discount' => $discount,
                        'debit' => $debit,
                        'delivery_at' => $deliveryAt,
                    ];
                });

                // Adiciona os itens agora atualizados no pedido.
                // Mesmo que algum dos itens não mudado nada, ele é adicionado.
                $order->items()->createMany($itemsData);

                return $order;
            });
        }

        $order->refresh();

        return $order;
    }
}