<?php

namespace App\Actions\Commands\Order;

use App\Models\Order\Order;

class UpdateManyOrders
{
    public function handle(array $validated)
    {
        $validated = collect($validated);

        $ids = $validated->pull('ids');

        $orders = Order::whereIn('id', $ids)->get();

        $orders->each(fn ($order) => $order->update($validated->toArray()));

        // Diferente de CreateOrder e UpdateOrder, quando atualizar várias
        // ao mesmo tempo, não se atualiza os campos used_balance e previous_balance, então não é
        // necessário executar o comando de atualizar o saldo do usuário.

        return $orders;
    }
}
