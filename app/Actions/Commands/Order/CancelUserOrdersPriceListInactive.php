<?php

namespace App\Actions\Commands\Order;

use App\Libraries\OrderStatus;
use App\Libraries\OrderSystemNote;
use App\Models\Account\User;

class CancelUserOrdersPriceListInactive
{
    public function handle(User $user, string $status = OrderStatus::DRAFT)
    {
        $orders = $user->orders()
            ->where('status', $status)
            ->get();

        $orders->each(function ($order) {
            $priceList = $order->priceList;

            if ($priceList->isNotActive() || $priceList->hasEnded()) {
                $order->update([
                    'status' => OrderStatus::FAILED,
                    'system_notes' => OrderSystemNote::PRICE_LIST_INACTIVE
                ]);
            }
        });
    }
}