<?php

namespace App\Actions\Queries;

use App\Models\Product\PriceList;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class ListPrices
{
   public function handle()
   {
      return QueryBuilder::for(PriceList::class)
         ->allowedFilters([
            'name',
            'active',
            AllowedFilter::exact('code', 'code_erp'),
            AllowedFilter::exact('start-at', 'start_at'),
            AllowedFilter::exact('end-at', 'end_at'),
         ])
         ->allowedSorts([
            'name',
            'active',
            AllowedSort::field('code', 'code_erp'),
            AllowedSort::field('start-at', 'start_at'),
            AllowedSort::field('end-at', 'end_at'),
         ])
         ->allowedIncludes([
            'users',
            'items',
            'items.prices',
         ])
         ->jsonPaginate();
   }
}