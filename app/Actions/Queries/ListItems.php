<?php

namespace App\Actions\Queries;

use App\Models\Product\Item;
use Spatie\QueryBuilder\QueryBuilder;

class ListItems
{
    public function handle()
    {
        return QueryBuilder::for(Item::class)
            ->allowedFilters([
                'id',
                'name',
                'code',
                'hunt'
            ])
            ->allowedSorts([
                'id',
                'name',
                'code',
                'hunt'
            ])
            ->jsonPaginate();
    }
}