<?php

namespace App\Actions\Queries;

use App\Libraries\UserRole;
use App\Models\Account\User;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Models\Transaction\Compensation;
use App\Models\Transaction\CreditCanceledOrder;
use App\Models\Transaction\CreditInvoice;
use App\Models\Transaction\DebitCanceledInvoice;
use App\Models\Transaction\DebitOrder;

class ListUserHunts
{
    public function handle()
    {
        $userTable = User::TABLE;

        $compensationQuery = Compensation::query()
            ->selectRaw("user_id, SUM(amount) as amount, max(updated_at) as updated_at")
            ->groupBy('user_id');

        $creditInvoiceQuery = CreditInvoice::query()
            ->selectRaw("user_id, SUM(amount) as amount, max(updated_at) as updated_at")
            ->groupBy('user_id');

        $debitCanceledInvoicesQuery = DebitCanceledInvoice::query()
            ->selectRaw("user_id, SUM(amount) as amount, max(updated_at) as updated_at")
            ->groupBy('user_id');

        $debitOrderQuery = DebitOrder::query()
            ->selectRaw("user_id, SUM(amount) as amount, max(updated_at) as updated_at")
            ->groupBy('user_id');

        $creditCanceledOrderQuery = CreditCanceledOrder::query()
            ->selectRaw("user_id, SUM(amount) as amount, max(updated_at) as updated_at")
            ->groupBy('user_id');

        $result = QueryBuilder::for(User::class)
            ->leftJoinSub($compensationQuery, 'compensations', fn ($join) =>
                $join->on("{$userTable}.id", '=', 'compensations.user_id')
            )
            ->leftJoinSub($creditInvoiceQuery, 'creditInvoices', fn ($join) =>
                $join->on("{$userTable}.id", '=', 'creditInvoices.user_id')
            )
            ->leftJoinSub($debitCanceledInvoicesQuery, 'debitCanceledInvoices', fn ($join) =>
                $join->on("{$userTable}.id", '=', 'debitCanceledInvoices.user_id')
            )
            ->leftJoinSub($debitOrderQuery, 'debitOrders', fn ($join) =>
                $join->on("{$userTable}.id", '=', 'debitOrders.user_id')
            )
            ->leftJoinSub($creditCanceledOrderQuery, 'creditCanceledOrders', fn ($join) =>
                $join->on("{$userTable}.id", '=', 'creditCanceledOrders.user_id')
            )
            ->select("{$userTable}.*")
            ->selectRaw("coalesce(compensations.amount,0) +
                coalesce(creditInvoices.amount,0) +
                coalesce(creditCanceledOrders.amount,0) -
                coalesce(debitCanceledInvoices.amount,0) -
                coalesce(debitOrders.amount,0)
                as balance")
            ->selectRaw("greatest(
                coalesce(compensations.updated_at, '0000-00-00 00:00:00'),
                coalesce(creditInvoices.updated_at, '0000-00-00 00:00:00'),
                coalesce(creditCanceledOrders.updated_at, '0000-00-00 00:00:00'),
                coalesce(debitOrders.updated_at, '0000-00-00 00:00:00'),
                coalesce(debitCanceledInvoices.updated_at, '0000-00-00 00:00:00')
                ) as last_updated")
            ->where('role', UserRole::CLIENT)
            ->groupBy("{$userTable}.id")
            ->allowedFilters(
                'id',
                'name',
                'created',
                AllowedFilter::exact('status', 'status'),
                AllowedFilter::partial('balance', 'balance'),
                AllowedFilter::partial('last-updated', 'last_updated'),
            )
            ->allowedSorts(
                'id',
                'name',
                'created',
                'status',
                AllowedSort::field('balance', 'balance'),
                AllowedSort::field('last-updated', 'last_updated'),
            )
            ->jsonPaginate();

        return $result;
    }
}
