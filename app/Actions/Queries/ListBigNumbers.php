<?php

namespace App\Actions\Queries;
use App\Models\Hunt\Invoice;
use App\Libraries\InvoiceStatus;

class ListBigNumbers
{
   public function handle()
   {
      $approvedInvoices = Invoice::where('status', InvoiceStatus::APPROVED)
         ->count();

      $rejectedInvoices = Invoice::where('status', InvoiceStatus::REJECTED)
         ->count();

      $pendingInvoices = Invoice::where('status', InvoiceStatus::PENDING)
         ->count();

      $bignumbers = collect([
         'invoices' => [
            'approved' => $approvedInvoices,
            'rejected' => $rejectedInvoices,
            'pending' => $pendingInvoices,
         ],
      ]);

      return $bignumbers;
   }
}