<?php

namespace App\Actions\Queries;

use App\QueryBuilder\Filters\FilterPriceItemIdList;
use App\Models\Product\Item;
use App\Models\Product\Price;
use App\Models\Product\PriceList;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;

class ListPriceItems
{
    public function handle(PriceList $priceList)
    {
        $query = QueryBuilder::for(Price::class)
            ->select([
                Price::TABLE . '.*',
            ])
            ->join(
                Item::TABLE,
                Item::TABLE . '.id', '=', Price::TABLE . '.item_id'
            )
            ->join(
                PriceList::TABLE,
                PriceList::TABLE . '.id', '=', Price::TABLE . '.price_list_id'
            )
            ->where('price_list_id', $priceList->id)
            ->allowedFilters([
                AllowedFilter::exact('id', 'id'),

                AllowedFilter::partial('start-at', 'start_at'),

                AllowedFilter::partial('end-at', 'end_at'),

                AllowedFilter::custom('active', new FilterPriceItemIdList),

                AllowedFilter::partial('price-start-at', 'priceList.start_at'),

                AllowedFilter::partial('price-end-at', 'priceList.end_at'),

                AllowedFilter::partial('item-name', 'item.name'),

                AllowedFilter::partial('item-code', 'item.code'),

                AllowedFilter::exact('item-hunt', 'item.hunt'),
            ])
            ->allowedIncludes([
                AllowedInclude::relationship('list', 'priceList'),
                'item',
            ])
            ->jsonPaginate();

        return $query;
    }
}