<?php

namespace App\Actions\Queries;

use App\Models\Hunt\Invoice;
use Illuminate\Foundation\Auth\User;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Models\Account\User as Account;
use App\Models\Transaction\CreditInvoice;

class ListInvoices
{
    public function handle(User|null $user = null)
    {
        return QueryBuilder::for(Invoice::class)
            ->allowedFilters([
                'id',
                'status',
                'number',
                'series',
                'cnae',
                'uf',
                'municipality',
                AllowedFilter::partial('created-at', 'created_at'),
                AllowedFilter::partial('updated-at', 'updated_at'),
                AllowedFilter::partial('user-name', 'user.name'),
                AllowedFilter::exact('credit', 'credit.amount'),
            ])
            ->allowedSorts([
                'id',
                'number',
                'series',
                'cnae',
                'uf',
                'municipality',
                AllowedSort::field('created-at', 'created_at'),
                AllowedSort::field('updated-at', 'updated_at'),
                AllowedSort::field('user-name', 'users.name'),
                AllowedSort::field('credit', 'credit_invoices.amount'),
            ])
            ->allowedIncludes([
                'user',
                'items',
                'credit',
            ])
            ->when($user, fn($query) =>
                $query->where(Invoice::TABLE . '.user_id', $user->id)
            )
            ->when($this->hasSort('credit'), fn($query) =>
                $query->leftJoin(CreditInvoice::TABLE, Invoice::TABLE.'.id', '=', CreditInvoice::TABLE . '.invoice_id')
            )
            ->when($this->hasSort('user-name'), fn($query) =>
                $query->leftJoin(Account::TABLE, Invoice::TABLE.'.user_id', '=', Account::TABLE . '.id')
            )
            ->jsonPaginate();
    }

    private function hasSort(string $name)
    {
        $sorts = explode(',', request('sort'));

        if ($sorts === false) return false;

        $sorts = collect($sorts)->transform(function ($sort) {
            $trimmed = rtrim(ltrim($sort));
            return ltrim($trimmed, '-');
        });

        return $sorts->contains($name);
    }
}