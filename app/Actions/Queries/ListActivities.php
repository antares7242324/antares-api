<?php

namespace App\Actions\Queries;

use App\Models\Activity;
use Illuminate\Foundation\Auth\User;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class ListActivities
{
    public function handle(User|null $user = null)
    {
        return QueryBuilder::for(Activity::class)
            ->allowedFilters(
                AllowedFilter::exact('id'),
                'action',
                AllowedFilter::exact('responsible-id', 'responsible_id')->nullable(),
                AllowedFilter::exact('restriction'),
                AllowedFilter::callback('restriction:not', function ($query, $value) {
                    $restrictions = is_array($value) ? $value : [$value];
                    $query->whereNotIn('restriction', $restrictions);
                }),
                AllowedFilter::exact('user-id', 'user_id')->nullable(),
                AllowedFilter::partial('created', 'created_at'),
                AllowedFilter::partial('updated', 'updated_at'),
                AllowedFilter::trashed(),
            )
            ->allowedSorts(
                'id',
                'action',
                'restriction',
                AllowedSort::field('created', 'created_at'),
                AllowedSort::field('updated', 'updated_at')
            )
            ->allowedIncludes(
                'user',
                'responsible',
            )
            ->when($user, fn ($query) => $query->where('user_id', $user->id))
            ->jsonPaginate();
    }
}