<?php

namespace App\Actions\Queries;

use App\Models\Order\Order;
use App\Models\Hunt\Invoice;
use App\Models\Account\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\Transaction\DebitOrder;
use App\Models\Transaction\Compensation;
use App\Models\Transaction\CreditCanceledOrder;
use App\Models\Transaction\CreditInvoice;
use App\Models\Transaction\DebitCanceledInvoice;
use App\Traits\RequestParamsTrait;
use Spatie\QueryBuilder\Exceptions\InvalidSortQuery;

class ListUserStatements
{
    use RequestParamsTrait;

    protected $allowedSorts;

    public function __construct()
    {
        $this->allowedSorts = collect([
            'created' => 'created_at',
            'updated' => 'updated_at',
            'code' => 'code',
            'amount' => 'amount',
        ]);
    }

    /**
     * Lista os extratos do usuário.
     *
     * @param \Illuminate\Support\Collection|null $params
     *   Os parâmetros da consulta.
     * @return \Illuminate\Pagination\LengthAwarePaginator
     *   Objeto de coleção do paginador.
     */

    public function handle(?Collection $params = null, ?User $user = null)
    {
        $tableOrder = app(Order::class)->getTable();
        $tableDebitOrder = app(DebitOrder::class)->getTable();
        $tableCreditCanceledOrder = app(CreditCanceledOrder::class)->getTable();
        $tableInvoice = app(Invoice::class)->getTable();
        $tableCreditInvoice = app(CreditInvoice::class)->getTable();
        $tableDebitCanceledInvoice = app(DebitCanceledInvoice::class)->getTable();
        $tableCompensation = app(Compensation::class)->getTable();

        // Debit Order query.
        $orderQuery = QueryBuilder::for(Order::class)
            ->select(
                "{$tableOrder}.id",
                DB::raw("{$tableOrder}.code as value"),
                "{$tableDebitOrder}.created_at",
                "{$tableDebitOrder}.updated_at",
                DB::raw("-1 * {$tableDebitOrder}.amount as amount"),
                DB::raw("'debit' as type"),
                DB::raw("'{$tableOrder}' as model"),
                "{$tableOrder}.user_id",
                DB::raw("'{$tableDebitOrder}' as transaction_model"),
                DB::raw("{$tableDebitOrder}.id as transaction_id"),
            )
            // \App\Models\Transaction\DebitOrder
            ->join(
                $tableDebitOrder,
                "{$tableOrder}.id", "=", "{$tableDebitOrder}.order_id"
            )
            // Adiciona o filtro de usuário se informado.
            ->when(is_null($user) === false, fn ($query) =>
                $query->where("{$tableOrder}.user_id", $user->id)
            )
            ->when($this->requestHasFilter('user'), fn ($query) =>
                $query->whereIn("{$tableOrder}.user_id", $this->requestFilterArray('user'))
            )
            ->getQuery();

        // Credit Canceled Order query.
        $canceledOrderQuery = QueryBuilder::for(Order::class)
            ->select(
                "{$tableOrder}.id",
                DB::raw("{$tableOrder}.code as value"),
                "{$tableCreditCanceledOrder}.created_at",
                "{$tableCreditCanceledOrder}.updated_at",
                "{$tableCreditCanceledOrder}.amount as amount",
                DB::raw("'credit' as type"),
                DB::raw("'{$tableOrder}' as model"),
                "{$tableOrder}.user_id",
                DB::raw("'{$tableCreditCanceledOrder}' as transaction_model"),
                DB::raw("{$tableCreditCanceledOrder}.id as transaction_id"),
            )
            // \App\Models\Transaction\CreditCanceledOrder
            ->join(
                $tableCreditCanceledOrder,
                "{$tableOrder}.id", "=", "{$tableCreditCanceledOrder}.order_id"
            )
            // Adiciona o filtro de usuário se informado.
            ->when(is_null($user) === false, fn ($query) =>
                $query->where("{$tableOrder}.user_id", $user->id)
            )
            ->when($this->requestHasFilter('user'), fn ($query) =>
                $query->whereIn("{$tableOrder}.user_id", $this->requestFilterArray('user'))
            )
            ->getQuery();

        // Credit Invoice query.
        $invoiceQuery = QueryBuilder::for(Invoice::class)
            ->select(
                "{$tableInvoice}.id",
                DB::raw("CONCAT({$tableInvoice}.number, '/', {$tableInvoice}.series) as value"),
                "{$tableCreditInvoice}.created_at",
                "{$tableCreditInvoice}.updated_at",
                "{$tableCreditInvoice}.amount",
                DB::raw("'credit' as type"),
                DB::raw("'{$tableInvoice}' as model"),
                "{$tableInvoice}.user_id",
                DB::raw("'{$tableCreditInvoice}' as transaction_model"),
                DB::raw("{$tableCreditInvoice}.id as transaction_id"),
            )
            // \App\Models\Transaction\CreditInvoice
            ->join(
                $tableCreditInvoice,
                "{$tableInvoice}.id", "=", "{$tableCreditInvoice}.invoice_id"
            )
            // Adiciona o filtro de usuário se informado.
            ->when(is_null($user) === false, fn ($query) =>
                $query->where("{$tableInvoice}.user_id", $user->id)
            )
            ->when($this->requestHasFilter('user'), fn ($query) =>
                $query->whereIn("{$tableInvoice}.user_id", $this->requestFilterArray('user'))
            )
            ->getQuery();

        // Debit Canceled Invoice query.
        $canceledInvoiceQuery = QueryBuilder::for(Invoice::class)
            ->select(
                "{$tableInvoice}.id",
                DB::raw("CONCAT({$tableInvoice}.number, '/', {$tableInvoice}.series) as value"),
                "{$tableDebitCanceledInvoice}.created_at",
                "{$tableDebitCanceledInvoice}.updated_at",
                "{$tableDebitCanceledInvoice}.amount",
                DB::raw("'debit' as type"),
                DB::raw("'{$tableInvoice}' as model"),
                "{$tableInvoice}.user_id",
                DB::raw("'{$tableDebitCanceledInvoice}' as transaction_model"),
                DB::raw("{$tableDebitCanceledInvoice}.id as transaction_id"),
            )
            // \App\Models\Transaction\DebitCanceledInvoice
            ->join(
                $tableDebitCanceledInvoice,
                "{$tableInvoice}.id", "=", "{$tableDebitCanceledInvoice}.invoice_id"
            )
            // Adiciona o filtro de usuário se informado.
            ->when(is_null($user) === false, fn ($query) =>
                $query->where("{$tableInvoice}.user_id", $user->id)
            )
            ->when($this->requestHasFilter('user'), fn ($query) =>
                $query->whereIn("{$tableInvoice}.user_id", $this->requestFilterArray('user'))
            )
            ->getQuery();

        // Compensation query.
        $compensationQuery = QueryBuilder::for(Compensation::class)
            ->select(
                "{$tableCompensation}.id",
                DB::raw("{$tableCompensation}.reason as value"),
                "{$tableCompensation}.created_at",
                "{$tableCompensation}.updated_at",
                "{$tableCompensation}.amount",
                DB::raw("IF(amount > 0, 'credit', IF(amount < 0, 'debit', '')) as type"),
                DB::raw("'{$tableCompensation}' as model"),
                "{$tableCompensation}.user_id",
                DB::raw("null as transaction_model"),
                DB::raw("null as transaction_id"),
            )
            // Adiciona o filtro de usuário se informado.
            ->when(is_null($user) === false, fn ($query) =>
                $query->where("{$tableCompensation}.user_id", $user->id)
            )
            ->when($this->requestHasFilter('user'), fn ($query) =>
                $query->whereIn("{$tableCompensation}.user_id", $this->requestFilterArray('user'))
            )
            ->getQuery();

        // Union all queries.
        $unionQuery = DB::query()->fromSub($orderQuery, 'q')
            ->unionAll(DB::query()->fromSub($canceledOrderQuery, 'q'))
            ->unionAll(DB::query()->fromSub($invoiceQuery, 'q'))
            ->unionAll(DB::query()->fromSub($canceledInvoiceQuery, 'q'))
            ->unionAll(DB::query()->fromSub($compensationQuery, 'q'));

        // Sorting.
        $unionQuery->when($params->has('sort'), fn ($query) =>
            $this->applySorts($query, $params->get('sort', ''))
        );

        // Get query results.
        $statements = $unionQuery->jsonPaginate();

        return $statements;
    }

    private function applySorts($query, string $sort)
    {
        $sorts = collect(explode(',', $sort));

        $requestedSortNames = $sorts->map(function (string $sort) {
            return ltrim($sort, '-');
        });

        $allowedSortNames = $this->allowedSorts->keys();

        $unknownSorts = $requestedSortNames->diff($allowedSortNames);

        if ($unknownSorts->isNotEmpty()) {
            throw InvalidSortQuery::sortsNotAllowed($unknownSorts, $allowedSortNames);
        }

        $sorts->each(function ($sort) use ($query) {
            $name = ltrim($sort, '-');

            $direction = strpos($sort, '-') === 0 ? 'DESC' : 'ASC';

            $query->orderBy($this->allowedSorts->get($name), $direction);
        });

        return $query;
    }
}