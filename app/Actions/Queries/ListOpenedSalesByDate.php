<?php

namespace App\Actions\Queries;

use App\Models\Sale\Sale;
use Illuminate\Support\Carbon;

class ListOpenedSalesByDate
{
   public function handle(Carbon $date = null)
   {
      if (is_null($date)) $date = Carbon::now();

      $sales = Sale::query()
         ->where('start_at', '<=', $date)
         ->where('end_at', '>=', $date)
         ->get();

      return $sales;
   }
}