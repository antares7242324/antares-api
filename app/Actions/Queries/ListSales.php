<?php

namespace App\Actions\Queries;

use App\Models\Sale\Sale;
use App\QueryBuilder\Filters\FilterByTrashed;
use App\QueryBuilder\Sorts\SortByStatus;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class ListSales
{
    public function handle()
    {
        return QueryBuilder::for(Sale::class)
            ->withTrashed()
            ->allowedFilters(
                'name',
                'discount',
                AllowedFilter::partial('start-at', 'start_at'),
                AllowedFilter::partial('end-at', 'end_at'),
                AllowedFilter::trashed()
            )
            ->allowedSorts(
                'name',
                'discount',
                AllowedSort::field('start-at', 'start_at'),
                AllowedSort::field('end-at', 'end_at'),
                AllowedSort::custom(SortByStatus::NAME, new SortByStatus)
            )
            ->jsonPaginate();
    }
}