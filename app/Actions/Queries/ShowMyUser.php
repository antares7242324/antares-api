<?php

namespace App\Actions\Queries;
use App\Models\Account\User;

class ShowMyUser
{
    public function handle() : User
    {
        return User::find(auth()->user()->id);
    }
}