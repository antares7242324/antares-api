<?php

namespace App\Actions\Queries;

use App\Models\Account\User;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Models\Transaction\Compensation;

class ListCompensations
{
    public function handle(User|null $user = null)
    {
        $request = request();

        $query = QueryBuilder::for(Compensation::class)
            ->allowedFilters(
                'id',
                'amount',
                AllowedFilter::exact('user.id', 'user_id'),
                AllowedFilter::exact('creator.id', 'creator_id'),
                'reason',
                AllowedFilter::partial('created', 'created_at'),
                AllowedFilter::partial('updated', 'updated_at'),
                AllowedFilter::partial('creator.name', 'creator.name'),
                AllowedFilter::partial('user.name', 'user.name'),
            )
            ->allowedSorts(
                'id',
                'amount',
                'reason',
                AllowedSort::field('created', 'created_at'),
                AllowedSort::field('updated', 'updated_at'),
                AllowedSort::field('creator.name', 'creator.name'),
                AllowedSort::field('user.name', 'user.name'),
            )
            ->allowedIncludes(
                'creator',
                'user',
            )
            ->when($request->has('include'), function ($query) use ($request) {
                $include = explode(',', $request->get('include'));

                $addSelect = false;

                if (in_array('user', $include)) {
                    $query->join(
                        User::TABLE.' as user',
                        'user.id', '=', Compensation::TABLE.'.user_id'
                    );

                    $addSelect = true;
                }

                if (in_array('creator', $include)) {
                    $query->join(
                        User::TABLE.' as creator',
                        'creator.id', '=', Compensation::TABLE.'.user_id'
                    );

                    $addSelect = true;
                }

                if ($addSelect) {
                    $query->select(Compensation::TABLE.'.*');
                }
            })
            ->when($user, fn($query) => $query->where(
                Compensation::TABLE . '.user_id', $user->id
            ))
            ->jsonPaginate();

        return $query;
    }
}