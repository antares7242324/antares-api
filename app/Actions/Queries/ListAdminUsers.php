<?php

namespace App\Actions\Queries;

use App\Libraries\UserRole;
use App\Models\Account\User;

class ListAdminUsers
{
   public function handle()
   {
      $users = User::whereIn('role', UserRole::admin())
         ->get();

      return $users;
   }
}