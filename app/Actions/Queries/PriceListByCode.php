<?php

namespace App\Actions\Queries;

use App\Models\Product\PriceList;

class PriceListByCode
{
    public function handle(string $code, bool $fail = false)
    {
        $query = PriceList::where('code_erp', $code);

        if ($fail) {
            return $query->firstOrFail();
        }

        return  $query->first();
    }
}