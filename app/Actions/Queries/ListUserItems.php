<?php

namespace App\Actions\Queries;

use App\Models\Product\UserItem;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\QueryBuilder\Sorts\SortByItemName;

class ListUserItems
{
   public function handle()
   {
      return QueryBuilder::for(UserItem::class)
         ->select(UserItem::TABLE.'.*')
         ->allowedFilters([
            AllowedFilter::exact('id'),
            'code',
            'confirmed',
            AllowedFilter::exact('code-exact', 'code'),
            AllowedFilter::partial('name', 'name'),
            AllowedFilter::exact('name-exact', 'name'),
            AllowedFilter::exact('item', 'item_id'),
            AllowedFilter::exact('user', 'user_id'),
            AllowedFilter::partial('item.name', 'item.name'),
            AllowedFilter::exact('item.name-exact', 'item.name'),
            AllowedFilter::trashed()
         ])
         ->allowedSorts([
            'id',
            'code',
            'name',
            'confirmed',
            AllowedSort::field('created', 'created_at'),
            AllowedSort::custom('item.name', new SortByItemName(), 'name'),
         ])
         ->allowedIncludes([
            'invoices',
         ])
         ->jsonPaginate();
   }
}