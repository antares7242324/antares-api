<?php

namespace App\Actions\Queries;

use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Account\Store;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class ListOrders
{
    /**
     * Lista todos os pedidos
     *
     * @param \App\Models\Account\User|null $user
     *   Objeto User para listar os seus pedidos. Null para listar todos os pedidos.
     */
    public function handle(User|null $user = null)
    {
        return QueryBuilder::for(Order::class)
            ->select(Order::TABLE . '.*')
            ->join(Store::TABLE, Store::TABLE . '.id', '=', Order::TABLE . '.store_id')
            ->join(User::TABLE, User::TABLE . '.id', '=', Order::TABLE . '.user_id')
            ->allowedFilters([
                'id',
                'description',
                AllowedFilter::exact('description-exact', 'description'),
                'status',
                AllowedFilter::exact('code-erp', 'code_erp'),
                AllowedFilter::exact('store-id', 'store_id'),
                AllowedFilter::exact('user-id', 'user_id'),
                AllowedFilter::exact('payment-method', 'payment_method'),
                'shipping',
                AllowedFilter::partial('created-at', 'created_at'),
                AllowedFilter::partial('updated-at', 'updated_at'),
                AllowedFilter::partial('store-name', Store::TABLE . '.name'),
                AllowedFilter::partial('user-name', User::TABLE . '.name'),
                AllowedFilter::partial('user-email', User::TABLE . '.email'),
                'debit',
            ])
            ->allowedSorts([
                'id',
                'description',
                'status',
                AllowedSort::field('code-erp', 'code_erp'),
                AllowedSort::field('payment-method', 'payment_method'),
                'shipping',
                AllowedSort::field('created-at', 'created_at'),
                AllowedSort::field('updated-at', 'updated_at'),
                AllowedSort::field('store-name', Store::TABLE . '.name'),
                AllowedSort::field('user-name', User::TABLE . '.name'),
                AllowedSort::field('user-email', User::TABLE . '.email'),
                'debit',
            ])
            ->allowedIncludes([
                'user',
                'store',
                'sale',
                'items',
                'debit',
            ])
            ->when($user,
                fn($query) => $query->where(Order::TABLE . '.user_id', $user->id)
            )
            ->jsonPaginate();
    }
}