<?php

namespace App\Actions\Queries;

use App\Models\Sale\Sale;
use App\Models\Activity;

class ListUserSaleActivities
{
   public function handle(int $userId, string $action = null, int $saleId = null)
   {
      $activities = Activity::where('user_id', $userId)
         ->where('loggable_type', Sale::class)
         ->when(is_null($action) === false, fn($query) =>
            $query->where('action', $action)
         )
         ->when(is_null($saleId) === false, fn($query) =>
            $query->where('loggable_id', $saleId)
         )
         ->get();

      return $activities;
   }
}