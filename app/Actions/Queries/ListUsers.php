<?php

namespace App\Actions\Queries;

use App\Models\Account\User;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use App\QueryBuilder\Filters\FiltersExcludeIds;

class ListUsers
{
    public function handle()
    {
        return QueryBuilder::for(User::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::custom('exclude', new FiltersExcludeIds),
                'name',
                'email',
                AllowedFilter::exact('status'),
                AllowedFilter::exact('role'),
                AllowedFilter::partial('last-login-at', 'last_login_at'),
                AllowedFilter::partial('code-erp', 'code_erp'),
                AllowedFilter::exact('price-list-id', 'price_list_id'),
            ])
            ->allowedSorts([
                'id',
                'name',
                'email',
                'status',
                'role',
                AllowedSort::field('last-login-at', 'last_login_at'),
                AllowedSort::field('code-erp', 'code_erp'),
                AllowedSort::field('price-list-id', 'price_list_id'),
            ])
            ->allowedIncludes([
                'stores',
                AllowedInclude::relationship('price', 'priceList'),
                AllowedInclude::count('itemCode', 'userItemsCount'),

                // OBS: Esse relacionamento não está definido da melhor
                // maneira. O correto seria o bignumbers ser literalmente
                // um relacionamento, mas na verdade ele é um resource apenas.
                // Foi colocado dessa maneira porquê quando tenta adicionar
                // `bignumbers` no `include` ele acaba bloqueando se não tiver
                // esse relacionamento, mas tecnicamente, ele não existe.
                // Por enquanto, esta é uma gambiarra para funcionar por causa da
                // maneira como o package Spatie Laravel QueryBuilder foi implementado.
                AllowedInclude::relationship('bigNumbers', 'priceList'),
            ])
            ->jsonPaginate();
    }
}