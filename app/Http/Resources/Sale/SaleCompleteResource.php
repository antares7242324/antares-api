<?php

namespace App\Http\Resources\Sale;

use App\Http\Resources\Sale\SaleResource;

class SaleCompleteResource extends SaleResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect(parent::toArray($request));

        $resource->put('relationships',
            SaleUserResource::collection($this->users)
        );

        return $resource->toArray();
    }
}
