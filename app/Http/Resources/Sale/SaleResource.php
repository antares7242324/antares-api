<?php

namespace App\Http\Resources\Sale;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::SALE,
            'id' => (string) $this->id,
            'attributes' => [
                'name' => $this->name,
                'discount' => $this->discount,
                'startAt' => $this->start_at,
                'endAt' => $this->end_at,
                'trashed' => $this->trashed(),
            ],
            'relationships' => [],
            'links' => [],
        ];
    }
}
