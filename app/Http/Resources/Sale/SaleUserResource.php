<?php

namespace App\Http\Resources\Sale;

use App\Http\Resources\Account\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SaleUserResource extends UserResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['links'] = [
            // Sempre vai mostrar o link para o User pois este resource sempre é uma lista.
            'show' => route('users.show', ['user' => $this->id]),
        ];

        // Removendo excesso de atributos do User.
        // Atualmente os permitidos são:
        //   - name
        //   - firstName
        $collection = collect($resource);

        $attributes = collect($collection->get('attributes'))
            ->forget(['status', 'role', 'lastLoginAt']);

        $collection->put('attributes', $attributes->toArray());

        $resource = $collection->toArray();

        return $resource;
    }
}
