<?php

namespace App\Http\Resources\Information;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class BigNumbersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resources = [
            'type' => ResourceType::BIG_NUMBERS,
            'attributes' => [
                'invoices' => $this->get('invoices'),
            ],
        ];

        return $resources;
    }
}
