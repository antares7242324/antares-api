<?php

namespace App\Http\Resources;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = $this->get('user');

        return [
            'type' => ResourceType::AUTH,
            'access' => $user->accessName(),
            'attributes' => [
                'token' => $this->get('token')->plainTextToken,
                'name' => $this->get('token')->accessToken->name,
                'abilities' => $this->get('token')->accessToken->abilities,
                'expiresAt' => $this->get('token')->accessToken->expires_at,
                'lastLoginAt' => $user->last_login_at,
            ],
        ];
    }
}
