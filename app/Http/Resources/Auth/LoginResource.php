<?php

namespace App\Http\Resources\Auth;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * This resource expects to receive a collection with specific indexes:
 *   - 'user' object \App\Models\Account\User
 *   - 'token' object \Laravel\Sanctum\NewAccessToken
 */
class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::AUTH,
            'access' => $this->get('user')->accessName(),
            'attributes' => [
                'token' => $this->get('token')->plainTextToken,
            ],
        ];
    }
}
