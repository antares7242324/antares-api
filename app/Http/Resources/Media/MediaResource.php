<?php

namespace App\Http\Resources\Media;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::MEDIA,
            'id' => (string) $this->id,
            'attributes' => [
                'name' => $this->name,
                'extension' => $this->extension,
                'type' => $this->type,
                'storage' => $this->storage,
                'path' => $this->path(),
            ],
            'relationships' => [],
            'links' => [],
        ];
    }
}
