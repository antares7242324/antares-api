<?php

namespace App\Http\Resources\Hunt;

use App\Libraries\ResourceType;
use App\Http\Resources\Product\UserItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceNfeHuntResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $items = $this->get('items');

        $resource = collect()
            ->put('type', ResourceType::INVOICE_NFE)
            ->put('attributes', collect());

        $attributes = collect()
            ->put('code', $this->get('code'))
            ->put('number', $this->get('number'))
            ->put('series', $this->get('series'))
            ->put('issuanceAt', $this->get('issuance_at'))
            ->put('uf', $this->get('uf'))
            ->put('municipality', $this->get('municipality'))
            ->put('cnae', $this->get('cnae'))
            ->put('amount', $this->get('amount'));

        $items = $items->transform(function ($item) {
            $originalHunt = $item->get('hunt');

            if (is_null($originalHunt) === false) {
                $sellerPercent = (int) (100 * ($originalHunt->get('seller_percent', "0.00")));

                $huntPercent = (int) (100 * ($originalHunt->get('hunt_percent', "0.00")));

                $resourceHunt = collect()
                    ->put('unitList', $originalHunt->get('unit_list', 0))
                    ->put('subtotalList', $originalHunt->get('subtotal_list', 0))
                    ->put('unitCost', $originalHunt->get('unit_cost', 0))
                    ->put('subtotalCost', $originalHunt->get('subtotal_cost', 0))
                    ->put('sellerPercent', $sellerPercent)
                    ->put('huntPercent', $huntPercent)
                    ->put('amountDiscount', $originalHunt->get('amount_discount', 0))
                    ->put('cashback', $originalHunt->get('cashback', 0));
            }

            $userItem = UserItemResource::make($item->get('userItem'));

            $element = collect()
                ->put('code', $item->get('code'))
                ->put('quantity', $item->get('quantity', 0))
                ->put('unitPrice', $item->get('unit_price', 0))
                ->put('subtotal', $item->get('subtotal', 0))
                ->put('description', $item->get('description'))
                ->put('hunt', $resourceHunt ?? null)
                ->put('relationships', [
                    'userItem' => $userItem
                ]);

            return $element;
        });

        $cashback = $items->sum(function ($item) {
            $hunt = $item->get('hunt');

            return is_null($hunt) ? 0 : $hunt->get('cashback', 0);
        });

        $attributes->put('cashback', $cashback);

        $attributes->put('items', $items->toArray());

        $resource->put('attributes', $attributes)
            ->put('relationships', [])
            ->put('links', []);

        return $resource->toArray();
    }
}
