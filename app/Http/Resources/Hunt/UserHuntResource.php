<?php

namespace App\Http\Resources\Hunt;

use Illuminate\Support\Carbon;
use App\Libraries\ResourceType;
use App\Libraries\InvoiceStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class UserHuntResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $invoiceCounts = $this->invoices->countBy('status');

        $invoiceIds = $this->invoices->pluck('id');

        $lastUpdated = is_null($this->last_updated)
            ? null
            : Carbon::parse($this->last_updated);

        $resource = [
            'type' => ResourceType::HUNT,
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'balance' => intval($this->balance),
                'createdAt' => $this->created_at,
                'updatedAt' => $lastUpdated,
                'counts' => [
                    InvoiceStatus::PENDING => $invoiceCounts->get(InvoiceStatus::PENDING, 0),
                    InvoiceStatus::APPROVED => $invoiceCounts->get(InvoiceStatus::APPROVED, 0),
                    InvoiceStatus::REJECTED => $invoiceCounts->get(InvoiceStatus::REJECTED, 0),
                ],
            ],
            'relationships' => [
                'users' => [
                    'data' => [
                        'type' => ResourceType::USER,
                        'id' => $this->id,
                        'attributes' => [],
                        'relationships' => [],
                        'links' => [
                            'self' => route('users.show', ['user' => $this->id]),
                        ],
                    ],
                ],
                'invoices' => [
                    'data' => $invoiceIds->map(fn($id) => [
                        'type' => ResourceType::INVOICE,
                        'id' => $id,
                        'attributes' => [],
                        'relationships' => [],
                        'links' => [
                            'self' => route('invoices.show', ['invoice' => $id]),
                        ],
                    ])->toArray(),
                    'links' => [
                        'list' => route('invoices.index'),
                    ],
                ],
            ],
            'links' => [],
        ];

        return $resource;

    }
}
