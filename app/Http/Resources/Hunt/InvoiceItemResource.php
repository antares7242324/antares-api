<?php

namespace App\Http\Resources\Hunt;

use App\Http\Resources\Product\UserItemResource;
use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $userItem = $this->userItem()
            ->withTrashed()
            ->orderBy('created_at', 'desc')
            ->first();

        $resources = collect([
            'type' => ResourceType::INVOICE_ITEM,
            'id' => (string) $this->id,
            'attributes' => [
                'code' => $this->code,
                'unitPrice' => (int) $this->unit_price,
                'unitCost' => (int) $this->unit_cost,
                'quantity' => (int) $this->quantity,
                'subtotal' => (int) $this->subtotal,
                'refund' => (int) $this->refund,
                'discount' => floatval($this->discount),
                'huntDiscount' => floatval($this->hunt_discount),
                'description' => $this->description,
            ],
            'relationships' => [
                'userItem' => UserItemResource::make($userItem),
            ],
            'links' => [],
        ]);

        return $resources->toArray();
    }
}
