<?php

namespace App\Http\Resources\Hunt;

use App\Libraries\ResourceType;
use App\Http\Resources\Product\UserItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceNfeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $items = $this->get('items');

        $resource = collect()
            ->put('type', ResourceType::INVOICE_NFE)
            ->put('attributes', collect());

        $attributes = collect()
            ->put('code', $this->get('code'))
            ->put('number', $this->get('number'))
            ->put('series', $this->get('series'))
            ->put('issuanceAt', $this->get('issuance_at'))
            ->put('uf', $this->get('uf'))
            ->put('municipality', $this->get('municipality'))
            ->put('cnae', $this->get('cnae'))
            ->put('amount', $this->get('amount'));

        $items = $items->transform(function ($item) {
            $userItem = UserItemResource::make($item->get('userItem'));

            $element = collect()
                ->put('code', $item->get('code'))
                ->put('quantity', $item->get('quantity', 0))
                ->put('unitPrice', $item->get('unit_price', 0))
                ->put('subtotal', $item->get('subtotal', 0))
                ->put('description', $item->get('description'))
                ->put('relationships', [
                    'userItem' => $userItem
                ]);

            return $element;
        });

        $attributes->put('items', $items->toArray());

        $resource->put('attributes', $attributes)
            ->put('relationships', [])
            ->put('links', []);

        return $resource->toArray();
    }
}
