<?php

namespace App\Http\Resources\Hunt;

use App\Http\Resources\Hunt\InvoiceResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceDetailResource extends InvoiceResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect(parent::toArray($request));

        $relationships = collect($resource->get('relationships'))
            ->put('items', InvoiceItemCollection::make($this->items));

        $resource->put('relationships', $relationships->toArray());

        return $resource->toArray();
    }
}
