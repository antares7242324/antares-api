<?php

namespace App\Http\Resources\Hunt;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class CreditInvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::CREDIT_INVOICE,
            'id' => (string) $this->id,
            'attributes' => [
                'amount' => (int) $this->amount,
                'currentBalance' => (int) $this->current_balance,
                'finalBalance' => $this->finalBalance(),
            ],
            'relationships' => [],
            'links' => [],
        ];
    }
}
