<?php

namespace App\Http\Resources\Hunt;

use App\Libraries\ResourceType;
use App\Http\Resources\Account\UserResource;
use Spatie\QueryBuilder\QueryBuilderRequest;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Hunt\InvoiceItemCollection;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        $items = $this->items;

        $attributes = collect([
            'status' => $this->status,
            'code' => $this->code,
            'number' => $this->number,
            'series' => $this->series,
            'cnae' => $this->cnae,
            'amount' => $this->amount,
            'cashback' => $items->sum('refund'),
            'uf' => $this->uf,
            'municipality' => $this->municipality,
            'issuanceAt' => $this->issuance_at,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
        ]);

        $relationships = collect([
            'user' => $this->when(
                $includes->contains('user'),
                UserResource::make($this->user)
            ),
            'items' => $this->when(
                $includes->contains('items'),
                InvoiceItemCollection::make($items)
            ),
            'credit' => $this->when(
                $includes->contains('credit'),
                CreditInvoiceResource::make($this->lastCredit()),
            ),
        ]);

        $links = collect([]);

        $resources = collect([
            'type' => ResourceType::INVOICE,
            'id' => (string) $this->id,
            'attributes' => $attributes->toArray(),
            'relationships' => $relationships->toArray(),
            'links' => $links->toArray(),
        ]);

        return $resources->toArray();
    }
}
