<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\Account\UserResource;
use Spatie\QueryBuilder\QueryBuilderRequest;

class CompensationListResource extends CompensationResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        $resource = collect(parent::toArray($request));

        $relationships = collect($resource->get('relationships'));

        $relationships->when(
            $includes->contains('creator'),
            fn($relationships) => $relationships->put('creator', UserResource::make($this->creator))
        );

        $relationships->when(
            $includes->contains('user'),
            fn($relationships) => $relationships->put('user', UserResource::make($this->user))
        );

        $resource->put('relationships', $relationships);

        return $resource->toArray();
    }
}
