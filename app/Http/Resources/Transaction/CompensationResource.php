<?php

namespace App\Http\Resources\Transaction;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class CompensationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::COMPENSATION,
            'id' => (string) $this->id,
            'attributes' => [
                'amount' => (int) $this->amount,
                'currentBalance' => (int) $this->current_balance,
                'reason' => $this->reason,
                'createdAt' => $this->created_at,
            ],
            'relationships' => [],
            'links' => [
                'self' => route('compensations.show', [
                    'compensation' => $this->id
                ]),
            ],
        ];
    }
}
