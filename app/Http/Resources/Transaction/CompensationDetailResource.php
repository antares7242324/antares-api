<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\Account\UserReducedResource;
use App\Http\Resources\Account\UserResource;

class CompensationDetailResource extends CompensationResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect(parent::toArray($request));

        $relationships = collect($resource->get('relationships'));

        $relationships
            ->put('creator', UserReducedResource::make($this->creator))
            ->put('user', UserReducedResource::make($this->user));

        $resource->put('relationships', $relationships);

        return $resource->toArray();
    }
}
