<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Sale\SaleResource;
use App\Http\Resources\Account\UserResource;
use App\Http\Resources\Account\StoreResource;
use App\Http\Resources\Order\OrderDebitResource;

class OrderDetailResource extends OrderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect(parent::toArray($request));

        $resource->put('relationships', [
            'user' => UserResource::make($this->user),
            'store' => StoreResource::make($this->store()->withTrashed()->first()),
            'sale' => SaleResource::make($this->sale()->withTrashed()->first()),
            'items' => OrderItemCollection::make($this->items),
            'debit' => OrderDebitResource::make($this->debitTransaction),
        ]);

        return $resource->toArray();
    }
}
