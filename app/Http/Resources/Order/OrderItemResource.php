<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Product\PriceItemResource;
use App\Libraries\ResourceType;
use App\Http\Resources\Product\ItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = [
            'type' => ResourceType::ORDER_ITEM,
            'id' => (string)$this->id,
            'attributes' => [
                'quantity' => $this->quantity,
                'unitOriginalPrice' => $this->unit_original_price,
                'unitDiscountPrice' => $this->unit_discount_price,
                'subtotal' => $this->subtotal,
                'discount' => $this->discount,
                'debit' => $this->debit,
                'deliveryAt' => $this->delivery_at,
            ],
            'relationships' => [
                'item' => ItemResource::make($this->item),
                'price' => PriceItemResource::make($this->price),
            ],
            'links' => [
                'price' => route('my.items.show', [
                    'price' => $this->price->id,
                ])
            ],
        ];

        return $resource;
    }
}
