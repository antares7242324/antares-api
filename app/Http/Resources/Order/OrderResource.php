<?php

namespace App\Http\Resources\Order;

use App\Libraries\ResourceType;
use App\Http\Resources\Sale\SaleResource;
use App\Http\Resources\Account\UserResource;
use Spatie\QueryBuilder\QueryBuilderRequest;
use App\Http\Resources\Account\StoreResource;
use App\Http\Resources\Order\OrderDebitResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        return [
            'type' => ResourceType::ORDER,
            'id' => (string) $this->id,
            'attributes' => [
                'status' => $this->status,
                'code' => $this->code,
                'codeErp' => $this->code_erp,
                'description' => $this->description,
                'currentDiscount' => $this->current_discount,
                'debit' => $this->debit,
                'discount' => $this->discount,
                'saleDiscount' => $this->sale_discount,
                'paymentMethod' => $this->payment_method,
                'shipping' => $this->shipping,
                'systemNotes' => $this->system_notes,
                'issuanceAt' => $this->issuance_at,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ],
            'relationships' => [
                'user' => $this->when(
                    $includes->contains('user'),
                    UserResource::make($this->user)
                ),
                'store' => $this->when(
                    $includes->contains('store'),
                    StoreResource::make($this->store()->withTrashed()->first())
                ),
                'sale' => $this->when(
                    $includes->contains('sale'),
                    SaleResource::make($this->sale()->withTrashed()->first())
                ),
                'items' => $this->when(
                    $includes->contains('items'),
                    OrderItemCollection::make($this->items)
                ),
                'debit' => $this->when(
                    $includes->contains('debit'),
                    OrderDebitResource::make($this->debitTransaction)
                ),
            ],
            'links' => [],
        ];
    }
}
