<?php

namespace App\Http\Resources\Order;

// use Illuminate\Http\Resources\Json\JsonResource;

class OrderSyncItemResource extends OrderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resources = collect(parent::toArray($request));

        $relationships = [
            'items' => OrderItemCollection::make($this->items),
        ];

        $resources->put('relationships', $relationships);

        return $resources;
    }
}
