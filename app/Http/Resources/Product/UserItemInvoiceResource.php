<?php

namespace App\Http\Resources\Product;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserItemInvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect([
            'type' => ResourceType::INVOICE,
            'id' => $this->id,
            'attributes' => [
                'status' => $this->status,
                'issuance_at' => $this->issuance_at,
            ],
            'links' => [
                'self' => route('invoices.show', $this->id),
                'self.my' => route('my.invoices.show', $this->id),
            ],
        ]);

        return $resource->toArray();
    }
}
