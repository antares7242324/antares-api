<?php

namespace App\Http\Resources\Product;

use App\Libraries\ResourceType;
use App\Traits\RequestQueryBuilderTrait;
use App\Http\Resources\Account\UserResource;
use App\Http\Resources\Product\ItemResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product\UserItemInvoiceCollection;

class UserItemResource extends JsonResource
{
    use RequestQueryBuilderTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $includes = $this->queryBuilderIncludes($request);

        return [
            'type' => ResourceType::USER_ITEM,
            'id' => (string) $this->id,
            'attributes' => [
                'confirmed' => $this->confirmed,
                'code' => $this->code,
                'name' => $this->name,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ],
            'relationships' => [
                'user' => UserResource::make($this->user),
                'item' => ItemResource::make($this->item),
                'invoices' => $this->when(
                    $includes->contains('invoices'),
                    UserItemInvoiceCollection::make($this->invoices)
                ),
            ],
            'links' => [],
        ];
    }
}
