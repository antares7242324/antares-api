<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Account\UserCollection;
use App\Libraries\ResourceType;
use Spatie\QueryBuilder\QueryBuilderRequest;
use Illuminate\Http\Resources\Json\JsonResource;

class PriceListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        return [
            'type' => ResourceType::PRICE_LIST,
            'id' => (string) $this->id,
            'attributes' => [
                'active' => (bool) $this->active,
                'name' => $this->name,
                'code' => $this->code_erp,
                'startAt' => $this->start_at,
                'endAt' => $this->end_at,
                'activeAt' => $this->active_at,
            ],
            'relationships' => [
                'users' => $this->when(
                    $includes->contains('users'),
                    UserCollection::make($this->users)
                ),
                'items' => $this->when(
                    $includes->contains('items'),
                    ItemCollection::make($this->items)
                )
            ],
            'links' => [],
        ];
    }
}
