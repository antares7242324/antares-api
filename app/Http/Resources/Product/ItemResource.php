<?php

namespace App\Http\Resources\Product;

use App\Libraries\ResourceType;
use Spatie\QueryBuilder\QueryBuilderRequest;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        return [
            'type' => ResourceType::ITEM,
            'id' => (string)$this->id,
            'attributes' => [
                'name' => $this->name,
                'code' => $this->code,
                'hunt' => (bool)$this->hunt,
            ],
            'relationships' => [
                'price' => $this->when(
                    $includes->contains('items.prices'),
                    PriceItemResource::make($this->pivot)
                )
            ],
            'links' => [],
        ];
    }
}
