<?php

namespace App\Http\Resources\Product;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class PriceItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::PRICE,
            'id' => (string)$this->id,
            'attributes' => [
                'amount' => $this->amount,
                'originalAmount' => $this->original_amount,
                'startAt' => $this->start_at,
                'endAt' => $this->end_at,
                'lastUpdateAt' => $this->last_update_at,
            ],
            'relationships' => [],
            'links' => [],
        ];
    }
}
