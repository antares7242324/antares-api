<?php

namespace App\Http\Resources\Product;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserItemReducedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::USER_ITEM,
            'id' => (string) $this->id,
            'attributes' => [
                'code' => $this->code,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ],
            'relationships' => [],
            'links' => [],
        ];
    }
}
