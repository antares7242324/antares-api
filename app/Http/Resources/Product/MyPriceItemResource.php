<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Product\PriceItemResource;

class MyPriceItemResource extends PriceItemResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect(parent::toArray($request));

        $relationships = collect($resource->get('relationships'))
            ->put('list', PriceListResource::make($this->priceList))
            ->put('item', ItemResource::make($this->item));

        $resource->put('relationships', $relationships);

        $links = collect($resource->get('links'))
            ->put('self', route('my.items.show', ['price' => $this->id]));

        $resource->put('links', $links);

        return $resource->toArray();
    }
}
