<?php

namespace App\Http\Resources;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * This resource expects to receive a Collection with specific indexes:
 *   - 'sentMessage' \Illuminate\Mail\SentMessage
 *   - 'user' \App\Models\Account\User
 *   - 'token' \Laravel\Sanctum\NewAccessToken (optional)
 */
class MailerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $authResource = collect([
            'user' => $this->get('user'),
            'token' => $this->get('token'),
        ]);

        return [
            'type' => ResourceType::MAILER,
            'sent' => $this->get('sentMessage') ? true : false,
            'email' => $this->get('user')->email,
            'auth' => $this->when($this->get('token'), AuthResource::make($authResource)),
        ];
    }
}
