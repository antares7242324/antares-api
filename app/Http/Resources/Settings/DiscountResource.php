<?php

namespace App\Http\Resources\Settings;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $extraDiscounts = $this->get('extra_discounts', []);

        return [
            'type' => ResourceType::DISCOUNT,
            'attributes' => [
                'extra' => $extraDiscounts,
            ],
        ];
    }
}
