<?php

namespace App\Http\Resources\Activity;

use App\Models\Account\User;
use App\Libraries\ActivityAction;
use Illuminate\Http\Resources\Json\JsonResource;

class LoggableInvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = json_decode($this->data);

        return [
            'invoiceId' => isset($data->invoice)
                ? $data->invoice->id
                : null,
            'code' => isset($data->invoice)
                ? "{$data->invoice->number}/{$data->invoice->series}"
                : null,
            'status' => isset($data->invoice)
                ? $data->invoice->status
                : null,
            'userName' => isset($data->user)
                ? $data->user->name
                : null,
        ];
    }
}
