<?php

namespace App\Http\Resources\Activity;

use App\Models\Account\User;
use App\Libraries\ActivityAction;
use Illuminate\Http\Resources\Json\JsonResource;

class LoggableCompensationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = json_decode($this->data);

        return [
            'compensationId' => isset($data->compensation)
                ? $data->compensation->id
                : null,
            'amount' => isset($data->compensation)
                ? $data->compensation->amount
                : null,
            'reason' => isset($data->compensation)
                ? $data->compensation->reason
                : null,
            'userName' => isset($data->user)
                ? $data->user->name
                : null,
        ];
    }
}
