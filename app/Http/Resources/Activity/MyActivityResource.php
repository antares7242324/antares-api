<?php

namespace App\Http\Resources\Activity;

use App\Http\Resources\Activity\ActivityResource;

class MyActivityResource extends ActivityResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = collect(parent::toArray($request));

        $relationships = collect($resource->get('relationships'));

        // Remove o user porque já é uma atividade do usuário.
        $relationships->when(
            $relationships->get('user'),
            fn($collection) => $collection->forget('user')
        );

        $resource->put('relationships', $relationships);

        return $resource->toArray();
    }
}
