<?php

namespace App\Http\Resources\Activity;

use App\Libraries\ResourceType;
use App\Http\Resources\Account\UserResource;
use Spatie\QueryBuilder\QueryBuilderRequest;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        $user = $this->user;

        $responsible = $this->responsible;

        $resource = [
            'type' => ResourceType::ACTIVITY,
            'id' => (string) $this->id,
            'attributes' => [
                'action' => $this->action,
                'restriction' => $this->restriction,
                'log' => $this->log(),
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
                'deletedAt' => $this->deleted_at,
            ],
            'relationships' => [
                'user' => [],
                'responsible' => [],
            ],
            'links' => [],
        ];

        if ($user) {
            $resource['relationships']['user'] = $this->when(
                $includes->contains('user'),
                UserResource::make($user),
                ['data' => [
                    'id' => (string) $user->id,
                    'attributes' => [],
                    'links' => [],
                ]],
            );
        }

        if ($responsible) {
            $resource['relationships']['responsible'] = $this->when(
                $includes->contains('responsible'),
                UserResource::make($responsible),
                ['data' => [
                    'id' => (string) $responsible->id,
                    'attributes' => [],
                    'links' => [],
                ]],
            );
        }

        return $resource;
    }
}
