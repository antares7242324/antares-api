<?php

namespace App\Http\Resources\Activity;

use App\Models\Account\User;
use App\Libraries\ActivityAction;
use App\Support\Readables\OrderStatusReadable;
use Illuminate\Http\Resources\Json\JsonResource;

class LoggableOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = json_decode($this->data);

        return [
            'orderId' => isset($data->order)
                ? $data->order->id
                : null,
            'code' => isset($data->order)
                ? $data->order->code
                : null,
            'status' => isset($data->order)
                ? $data->order->status
                : null,
            'userName' => isset($data->user)
                ? $data->user->name
                : null,
        ];
    }
}
