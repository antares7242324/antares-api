<?php

namespace App\Http\Resources\Activity;

use App\Models\Product\PriceList;
use Illuminate\Http\Resources\Json\JsonResource;

class LoggableUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = json_decode($this->data);

        return [
            'userId' => isset($data->user)
                ? $data->user->id
                : null,
            'currentDiscount' => isset($data->user)
                ? $data->user->current_discount
                : null,
            'userName' => isset($data->user)
                ? $data->user->name
                : null,
            'priceListName' => isset($data->priceList)
                ? $data->priceList->name
                : null,
        ];
    }
}
