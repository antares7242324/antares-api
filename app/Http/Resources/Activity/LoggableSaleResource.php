<?php

namespace App\Http\Resources\Activity;

use App\Models\Account\User;
use App\Libraries\ActivityAction;
use Illuminate\Http\Resources\Json\JsonResource;

class LoggableSaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = json_decode($this->data);

        if ($this->action == ActivityAction::SALE_AVAILABLE) {
            return [
                'saleId' => isset($data->sale)
                    ? $data->sale->id
                    : null,
                'name' => isset($data->sale)
                    ? $data->sale->name
                    : null,
                'discount' => isset($data->sale)
                    ? $data->sale->discount
                    : null,
                'userName' => isset($data->user)
                    ? $data->user->name
                    : null,
            ];
        }

        return [];
    }
}
