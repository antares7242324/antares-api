<?php

namespace App\Http\Resources\Account;

use App\Libraries\ResourceType;
use App\Support\Settings\ExtraDiscount;
use Illuminate\Http\Resources\Json\JsonResource;

class UserBigNumbersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $extraDiscount = (new ExtraDiscount)->all();

        return [
            'type' => ResourceType::BIG_NUMBERS,
            'attributes' => [
                'discountsTrack' => $extraDiscount->toArray(),
                'averageDiscount' => $this->average_discount,
                'currentDiscount' => $this->current_discount,
                'availableBalance' => $this->available_balance,
                'creditsQty' => $this->countInvoiceCredits(),
                'savedAmount' => $this->savedAmount(),
                'debitsQty' => $this->countOrderDebits(),
            ],
        ];
    }
}
