<?php

namespace App\Http\Resources\Account;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserReducedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::USER,
            'id' => (string) $this->id,
            'attributes' => [
                'role' => $this->role,
                'email' => $this->email,
                'name' => $this->name,
                'firstName' => $this->firstName(),
            ],
            'relationships' => [],
            'links' => [
                'self' => route('users.show', ['user' => $this->id]),
            ],
        ];
    }
}
