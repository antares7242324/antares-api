<?php

namespace App\Http\Resources\Account;

use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use App\Libraries\ResourceType;
use App\Traits\RequestParamsTrait;
use App\Models\Transaction\Compensation;
use App\Http\Resources\Account\UserResource;
use App\Models\Transaction\DebitCanceledInvoice;
use Illuminate\Http\Resources\Json\JsonResource;

class UserStatementsResource extends JsonResource
{
    use RequestParamsTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $reopened = false;

        $modelLinks = collect();

        $modelAttributes = collect();

        if ($this->model == Order::TABLE) {
            $modelLinks->put('self', route('my.orders.show', ['order' => $this->id]))
                ->put('list', route('my.orders.index'));
        }

        if ($this->model == Invoice::TABLE) {
            if ($this->type == 'credit') {
                $invoice = Invoice::find($this->id);

                $invoiceCredit = $invoice->lastCredit();

                $reopened = $invoiceCredit->reopened;
            }

            $modelLinks->put('self', route('my.invoices.show', ['invoice' => $this->id]))
                ->put('list', route('my.invoices.index'));

            $modelAttributes->put('id', (string) $this->id)
                ->put('reopened', $reopened);
        }

        if ($this->model == Compensation::TABLE) {
            $modelLinks->put('self', route('compensations.show', ['compensation' => $this->id]))
                ->put('list', route('compensations.index'));
        }

        if ($modelAttributes->isEmpty()) {
            $modelAttributes->put('id', (string) $this->id)
                ->put('reopened', false);
        }

        if (isset($modelResource) === false) {
            $modelResource = collect([
                'attributes' => $modelAttributes,
                'links' => $modelLinks,
            ]);
        }

        $user = User::find($this->user_id);

        $statementType = $this->type;

        if ($this->transaction_model === app(DebitCanceledInvoice::class)->getTable()) {
            $transaction = DebitCanceledInvoice::find($this->transaction_id);

            $statementType .= $transaction->byAdmin() ? '_admin' : '';
        }

        $resource = [
            'type' => ResourceType::STATEMENT,
            'attributes' => [
                'type' => $statementType,
                'amount' => $this->amount,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
                'model' => $this->model,
                'value' => $this->value,
            ],
            'relationships' => [
                $this->model => $modelResource,
                'user' => $this->when($this->requestHasInclude('user'),
                    function () use ($user) {
                        return UserResource::make($user);
                    }
                ),
            ],
        ];

        return $resource;
    }
}
