<?php

namespace App\Http\Resources\Account;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = [
            'type' => ResourceType::STORE,
            'id' => (string)$this->id,
            'attributes' => [
                'name' => $this->name,
                'cnpj' => $this->cnpj,
                'codeErp' => $this->code_erp,
            ],
            'relationships' => [],
            'links' => [],
        ];

        return $resource;
    }
}
