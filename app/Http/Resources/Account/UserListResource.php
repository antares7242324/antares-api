<?php

namespace App\Http\Resources\Account;

use Spatie\QueryBuilder\QueryBuilderRequest;
use App\Http\Resources\Product\PriceListResource;
use App\Http\Resources\Account\UserBigNumbersResource;
use App\Http\Resources\Product\UserItemReducedResource;

class UserListResource extends UserResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        // Separa todas as partes do resource.

        $resource = collect(parent::toArray($request));

        $attributes = collect($resource->get('attributes'));

        $relationships = collect($resource->get('relationships'))
            // Adiciona a lista de lojas do parceiro.
            ->when($includes->contains('stores'), function ($relationships) {
                $stores = null;
                $activeStore = $this->activeStore();
                if ($this->isClient() && $activeStore) {
                    // OBS: Comentado enquanto o user pode ter apenas 1 loja.
                    // $stores = StoreCollection::make($this->stores);
                    $stores = StoreCollection::make(collect([$activeStore]));
                }
                $relationships->put('stores', $stores);
            })
            // Adiciona a lista de preços de produtos do parceiro.
            ->when($includes->contains('price'), fn ($relationships) =>
                $relationships->put(
                    'price', PriceListResource::make($this->priceList)
                )
            )
            // Adiciona informações sobre os códigos vinculados aos itens
            // pelo parceiro. Está adicionando um contador e o último código.
            ->when($includes->contains('itemCode'), function ($relationships) {
                $userItem = collect();

                if ($this->user_items_count > 0) {
                    $last = $this->latestUpdatedUserItems->first();

                    $userItem = UserItemReducedResource::make($last);
                }

                $relationships
                    ->put('itemCodeCount', $this->user_items_count)
                    ->put('itemCode', $userItem);
            })
            // Adiciona os dados do BigNumbers do parceiro.
            ->when($includes->contains('bigNumbers'), fn ($relationships) =>
                $relationships->put(
                    'bigNumbers', UserBigNumbersResource::make($this)
                )
            );

        $links = collect()
            ->put('show', route('users.show', ['user' => $this->id]));

        // Conecta novamente todas as partes separadas do resource.
        $resource->put('attributes', $attributes)
            ->put('relationships', $relationships)
            ->put('links', $links);

        return $resource->toArray();
    }
}
