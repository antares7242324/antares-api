<?php

namespace App\Http\Resources\Account;

use App\Actions\Commands\Account\GetUserBestSale;
use Spatie\QueryBuilder\QueryBuilderRequest;
use App\Http\Resources\Product\PriceListResource;
use App\Http\Resources\Account\UserBigNumbersResource;
use App\Http\Resources\Sale\SaleResource;

class UserSingleResource extends UserResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        // Busca a loja ativa.
        $activeStore = $this->activeStore();

        // Monta os dados para o resource.
        $resource = collect(parent::toArray($request));

        // Monta os dados dos attributes.
        $attributes = collect($resource->get('attributes'))
            ->put('availableBalance', $this->available_balance)
            ->put('discounts', [
                'current' => $this->current_discount,
                'average' => $this->average_discount,
            ]);

        // Monta os dados dos relationships.
        $relationships = collect()
            // Loja.
            ->when($this->isClient() && $activeStore, fn($relationships) =>
                $relationships->put(
                    'stores',
                    // StoreCollection::make($this->stores),
                    StoreCollection::make(collect([$activeStore]))
                ),
            )
            // Tabela de preços.
            ->put('price', PriceListResource::make($this->priceList))
            // Best Sale.
            ->when($includes->contains('bestSale'), function ($collection) {
                $bestSale = (new GetUserBestSale)->handle($this->resource);
                $collection->put('bestSale', SaleResource::make($bestSale));
            })
            // Big Numbers.
            ->when($includes->contains('bigNumbers'), function ($collection) {
                $collection->put('bigNumbers',
                    UserBigNumbersResource::make($this)
                );
            });
            // TODO: Será adicionado mais tarde.
            // Statements.
            // ->when($includes->contains('statements'), function ($collection) {
            // });

        $resource
            ->put('attributes', $attributes)
            ->put('relationships', $relationships);

        return $resource;
    }
}
