<?php

namespace App\Http\Resources\Account;

use App\Libraries\ResourceType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => ResourceType::USER,
            'id' => (string) $this->id,
            'attributes' => [
                'status' => $this->status,
                'role' => $this->role,
                'lastLoginAt' => $this->last_login_at,
                'email' => $this->email,
                'codeErp' => $this->code_erp,
                'name' => $this->name,
                'firstName' => $this->firstName(),
            ],
            'relationships' => [],
            'links' => [],
        ];
    }
}
