<?php

namespace App\Http\Controllers\MyUser;

use App\Traits\AccountTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Actions\Queries\ListUserStatements;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Account\UserStatementsCollection;

/**
 * @group Meu Cadastro
 * @subgroup Usuário
 */
class ListMyStatementsController extends Controller
{
    use AccountTrait;

    /**
     * Statements
     *
     * Mostra os dados dos statements do usuário autenticado.
     *
     * ### Ordenamentos permitidos (sort)
     * - created
     * - updated
     * - code
     * - amount
     *
     * Quando não for informado o campo `sort`, ordena por registros
     * atualizados descrescentes (`-updated`).
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListUserStatements $listUserStatements
     * @return \Illuminate\Http\Response
     */
    #[ResponseFromFile('storage/responses/user/statements.json')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'code, -amount')]
    public function __invoke(Request $request, ListUserStatements $listUserStatements)
    {
        $user = $this->loadUserAccount();

        $params = collect($request->all());

        $params->put('user', $user->id);

        if ($params->has('sort') === false) {
            $params->put('sort', '-updated');
        }

        $statements = $listUserStatements->handle($params, $user);

        $success = ! $statements->isEmpty();

        return UserStatementsCollection::make($statements)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ]
            ]);
    }
}
