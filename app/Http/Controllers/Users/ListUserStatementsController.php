<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Actions\Queries\ListUserStatements;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Account\UserStatementsCollection;
use App\Traits\RequestParamsTrait;

/**
 * @group Admin
 * @subgroup Usuários
 */
class ListUserStatementsController extends Controller
{
    use RequestParamsTrait;

    /**
     * Listar Extratos
     *
     * Mostra os dados dos extratos de todos os usuários.
     *
     * ### Ordenamentos permitidos (sort)
     * - created
     * - updated
     * - code
     * - amount
     *
     * ### Inclusões permitidas (include)
     * - user
     *
     * Quando não for informado o campo `sort`, ordena por registros criados
     * crescentes (`created`).
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListUserStatements $listUserStatements
     * @return \Illuminate\Http\Response
     */
    #[QueryParam('filter[user]', 'integer', "Filtra pelo ID dos pedidos.<br>")]
    #[ResponseFromFile('storage/responses/user/statements.json')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'code, -amount')]
    public function __invoke(Request $request, ListUserStatements $listUserStatements)
    {
        $params = collect($request->all());

        if ($params->has('sort') === false) {
            $params->put('sort', 'created');
        }

        $statements = $listUserStatements->handle($params);

        $success = ! $statements->isEmpty();

        // Adiciona o include para o relação `user`
        $include = $request->input('include', '');

        if ($this->requestHasInclude('user', $request) === false) {
            $include = array_merge($this->requestIncludeArray($request), ['user']);

            $request->merge(['include' => implode(',', $include)]);
        }

        return UserStatementsCollection::make($statements)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ]
            ]);
    }
}
