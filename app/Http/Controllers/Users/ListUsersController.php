<?php

namespace App\Http\Controllers\Users;

use App\Libraries\UserRole;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Actions\Queries\ListUsers;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Account\UserListCollection;

/**
 * @group Admin
 * @subgroup Usuários
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('name', 'string', 'Nome do usuário.')]
#[ResponseField('firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
#[ResponseField('discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
#[ResponseField('discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
#[ResponseField('stores.name', 'string', 'Nome da loja.')]
#[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
#[ResponseField('price.name', 'string', 'Nome da tabela de preços.')]
#[ResponseField('bigNumbers.discountsTrack', 'array', 'Lista com todos os descontos do Portal Antares.')]
#[ResponseField('bigNumbers.averageDiscount', 'string', 'Desconto médio ao cliente captado pelas notas fiscais adicionadas no sistema.')]
#[ResponseField('bigNumbers.currentDiscount', 'string', 'Desconto atual concedido ao distribuidor pelo Portal Antares.')]
#[ResponseField('bigNumbers.availableBalance', 'integer', 'Saldo disponível para uso na caçada.')]
#[ResponseField('bigNumbers.creditsQty', 'integer', 'Quantidade de créditos.')]
#[ResponseField('bigNumbers.savedAmount', 'integer', 'Valor economizado através das caçadas.')]
#[ResponseField('bigNumbers.debitsQty', 'integer', 'Quantidade de débitos.')]
class ListUsersController extends Controller
{
    /**
     * Listar
     *
     * Mostra todos os usuários com paginação.
     *
     * ### Inclusões permitidas (include)
     * - **stores:** Adiciona uma lista de lojas do usuário. Atualmente apenas uma.
     * - **price:** Adiciona a lista de preços de produtos vinculada ao usuário.
     * - **itemCode:** Adiciona dados relacionados aos códigos dos itens do usuário
     *   vinculados aos produtos Antares.
     * - **bigNumbers:** Adiciona os dados de big numbers do usuário.
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - name
     * - email
     * - code-erp
     * - status
     * - role
     * - last-login-at
     * - price-list-id
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListUsers $listuser
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/user/users.get.admin.json')]
    #[QueryParam('filter[id]', 'string', "Filtra pelo ID do usuário.<br>", example: 10)]
    #[QueryParam('filter[exclude]', 'string', "Exclui uma lista de IDs, separados por vírgula.<br>", example: "1,4,10")]
    #[QueryParam('filter[role]', 'string', "Filtra pelo papel do usuário.<br>", example: UserRole::CLIENT)]
    #[QueryParam('filter[name]', 'string', "Filtra pelo nome do usuário.<br>", example: "Lara Croft")]
    #[QueryParam('filter[email]', 'string', "Filtra pelo e-mail do usuário.<br>", example: "laracroft@tombraider.com")]
    #[QueryParam('filter[code-erp]', 'string', "Filtra pelo código ERP da Antares.<br>", example: "000556")]
    #[QueryParam('filter[status]', 'string', "Filtra pelo status do usuário.<br>", example: UserStatus::ACTIVE)]
    #[QueryParam('filter[last-login-at]', 'string', "Filtra pelo data de último login na plataforma.<br>", example: "2023-06-07 19:49:13")]
    #[QueryParam('filter[price-list-id]', 'integer', "Filtra pelo ID da tabela de preços vinculada ao usuário.<br>", example: 12)]
    #[QueryParam('include', 'string', 'Adiciona os dados dos recursos informados.<br>', example: 'stores')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: "last_login_at,role")]
    public function __invoke(Request $request, ListUsers $listUsers)
    {
        $users = $listUsers->handle();

        $success = $users->isEmpty() ? false : true;

        return UserListCollection::make($users)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
