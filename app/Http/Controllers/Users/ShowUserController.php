<?php

namespace App\Http\Controllers\Users;

use App\Libraries\UserRole;
use App\Models\Account\User;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Account\UserSingleResource;
use Knuckles\Scribe\Attributes\QueryParam;

/**
 * @group Admin
 *
 * @subgroup Usuários
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('name', 'string', 'Nome do usuário.')]
#[ResponseField('firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
#[ResponseField('discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
#[ResponseField('discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
#[ResponseField('stores.name', 'string', 'Nome da loja.')]
#[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
#[ResponseField('price.name', 'string', 'Nome da tabela de preços.')]
#[ResponseField('bestSale.name', 'string', 'Nome da promoção.')]
#[ResponseField('bestSale.discount', 'string', 'Desconto da promoção.')]
#[ResponseField('bestSale.startAt', 'timestamp', 'Data de início da promoção.')]
#[ResponseField('bestSale.endAt', 'timestamp', 'Data de conclusão da promoção.')]
#[ResponseField('bestSale.trashed', 'timestamp', 'Data de remoção da promoção.')]
#[ResponseField('bigNumbers.discountsTrack', 'array<string>', 'Array com os descontos do sistema para os distribuidores.')]
#[ResponseField('bigNumbers.averageDiscount', 'integer', 'Desconto médio obtido pelas caçadas.')]
#[ResponseField('bigNumbers.currentDiscount', 'integer', 'Desconto atual definido pela administração do Portal Antares.')]
#[ResponseField('bigNumbers.availableBalance', 'integer', 'Saldo disponível.')]
#[ResponseField('bigNumbers.creditsQty', 'integer', 'Quantidade de créditos.')]
#[ResponseField('bigNumbers.savedAmount', 'integer', 'Valor economizado através das caçadas.')]
#[ResponseField('bigNumbers.debitsQty', 'integer', 'Quantidade de débitos.')]
class ShowUserController extends Controller
{
    /**
     * Detalhar
     *
     * Mostra mais informações sobre o usuário informado.
     *
     * ### Inclusões permitidas (include)
     *
     * - **`bestSale`:** Adiciona os dados da melhor promoção para o usuário.
     * - **`bigNumbers`:** Adiciona os dados de big numbers do usuário.
     *
     * @param \App\Models\Account\User $user
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[UrlParam('id', 'string', "ID do usuário.", required: true, example: "1")]
    #[QueryParam('include', 'string', "Inclui os campos informados. Pode ser uma lista de campos separados por vírgula.<br>", example: "bigNumbers,bestSale")]
    #[ResponseFromFile('storage/responses/user/user.get.admin.json')]
    public function __invoke(User $user, Request $request)
    {
        return UserSingleResource::make($user)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
