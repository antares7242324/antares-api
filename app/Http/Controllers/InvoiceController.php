<?php

namespace App\Http\Controllers;

use App\Models\Hunt\Invoice;
use App\Traits\HttpResponses;
use Illuminate\Http\Request;
use App\Libraries\InvoiceStatus;
use App\Exports\InvoicesManyExport;
use App\Actions\Queries\ListInvoices;
use App\Jobs\Mail\MailInvoicesExportJob;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Http\Resources\Hunt\InvoiceResource;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Http\Resources\Hunt\InvoiceCollection;
use App\Actions\Commands\Invoice\UpdateInvoice;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Invoice\ExportInvoiceRequest;
use App\Http\Requests\Invoice\UpdateInvoiceRequest;
use App\Actions\Commands\Invoice\UpdateManyInvoices;
use App\Http\Requests\Invoice\UpdateManyInvoiceRequest;

/**
 * @group Admin
 *
 * @subgroup Notas Fiscais
 * @subgroupDescription
 * Endpoints para visualizar as notas
 * fiscais dos cashbacks dos usuários.
 *
 * ### Tipos de Situação (status)
 * - pending (pendente)
 * - approved (aprovada)
 * - rejected (rejeitada)
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual.')]
#[ResponseField('code', 'string', 'Código chave.')]
#[ResponseField('number', 'string', 'Código que compõe o número de série.')]
#[ResponseField('series', 'string', 'Digito do número de série.')]
#[ResponseField('cnae', 'string', 'Código CNAE do distribuidor que gerou a nota fiscal.')]
#[ResponseField('amount', 'integer', 'Valor da nota fiscal.')]
#[ResponseField('uf', 'string', 'Sigla da unidade federativa.')]
#[ResponseField('municipality', 'string', 'Nome do município.')]
#[ResponseField('issuanceAt', 'string', 'Data de emissão no formato *y-m-d h:m:s*.')]
#[ResponseField('createdAt', 'string', 'Data de criação no formato *y-m-d h:m:s.')]
#[ResponseField('updatedAt', 'string', 'Data de atualização no formato *y-m-d h:m:s.')]
#[ResponseField('items.code', 'string', 'Código do item na ANTARES.')]
#[ResponseField('items.unitPrice', 'integer', 'Valor unitário do item em centavos.')]
#[ResponseField('items.unitCost', 'integer', 'Valor de custo do item em centavos.')]
#[ResponseField('items.quantity', 'integer', 'Quantidade do item na nota fiscal.')]
#[ResponseField('items.subtotal', 'integer', 'Valor total do item na nota fiscal.')]
#[ResponseField('items.refund', 'integer', 'Valor de crédito gerado para o usuário no saldo de cashback.')]
#[ResponseField('items.discount', 'string', 'Valor do desconto que o usuário aplicou no item no formato decimal.')]
#[ResponseField('items.huntDiscount', 'string', 'Valor de desconto obtido pelo uso de cashback no formato decimal')]
#[ResponseField('items.description', 'integer', 'Descrição do item pela nota fiscal.')]
#[ResponseField('user.status', 'integer', 'Status do usuário.')]
#[ResponseField('user.role', 'string', 'Papel do usuário no sistema.')]
#[ResponseField('user.lastLoginAt', 'string', 'Data do último login efetuado pelo usuário.')]
#[ResponseField('user.name', 'string', 'Nome completo do usuário.')]
#[ResponseField('user.firstName', 'string', 'Primeira parte do nome do usuário.')]
class InvoiceController extends Controller
{
    use HttpResponses;

    /**
     * Listar
     *
     * Listar as notas fiscais.
     *
     * ### Inclusões permitidas (include)
     * - users
     * - items
     * - credit
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - number
     * - series
     * - cnae
     * - uf
     * - municipality
     * - created-at
     * - updated-at
     * - user-name
     * - credit
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListInvoices $listInvoices
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoices.json')]
    #[QueryParam('filter[id]', 'string', "Filtra pelo ID da nota fiscal. Pode enviar uma lista de IDs separados por vírgula.<br>", example: '12,5')]
    #[QueryParam('filter[status]', 'string', "Filtra pelo nome do status.<br>", example: InvoiceStatus::PENDING)]
    #[QueryParam('filter[number]', 'string', "Filtra pelo número da nota fiscal.<br>", example: "44567")]
    #[QueryParam('filter[series]', 'string', 'Filtra pelo número de série da nota fiscal.<br>', example: "11")]
    #[QueryParam('filter[cnae]', 'string', "Filtra pelo código CNAE.<br>", "6677899")]
    #[QueryParam('filter[uf]', 'string', "Filtra pela sigla UF.<br>", example: "RS")]
    #[QueryParam('filter[municipality]', 'string', "Filtra pelo nome do município.<br>", example: 'Caxias do Sul')]
    #[QueryParam('filter[created-at]', 'string', "Filtra pela data de emissão.<br>", example: '2023-05-01 14:00:00')]
    #[QueryParam('filter[updated-at]', 'string', "Filtra pela data de atualização.<br>", example: '2023-05-01 14:00:00')]
    #[QueryParam('filter[user-name]', 'string', "Filtra pelo nome do distribuidor.<br>", example: 'Lara Croft')]
    #[QueryParam('filter[credit]', 'integer', "Filtra pelo valor de crédito em centavos obtido no uso do cashback.<br>", example: '10000')]
    #[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: 'items,user')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'cnae,-uf')]
    public function index(Request $request, ListInvoices $listInvoices)
    {
        $invoices = $listInvoices->handle();

        return InvoiceCollection::make($invoices)
            ->additional([
                'success' => ! $invoices->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Detalhar a nota fiscal informada.
     *
     * @param \App\Models\Hunt\Invoice $invoice
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoices.json')]
    #[UrlParam('id', 'integer', "ID da nota fiscal.")]
    public function show(Invoice $invoice, Request $request)
    {
        return InvoiceResource::make($invoice)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Atualizar
     *
     * Atualizar a nota fiscal informada.
     *
     * @param \App\Models\Hunt\Invoice $invoice
     * @param \App\Http\Requests\Invoice\UpdateInvoiceRequest $request
     * @param \App\Actions\Commands\Invoice\UpdateInvoice $updateInvoice
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoice.json')]
    #[UrlParam('id', 'integer', "ID da nota fiscal.")]
    public function update(Invoice $invoice, UpdateInvoiceRequest $request, UpdateInvoice $updateInvoice)
    {
        $validated = $request->validated();

        $invoice = $updateInvoice->handle($invoice, $validated);

        return InvoiceResource::make($invoice)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route(
                        'invoices.show',
                        ['invoice' => $invoice->id]
                    ),
                ],
            ]);
    }

    /**
     * Atualizar Várias
     *
     * Atualizar várias notas fiscais.
     *
     * @param \App\Http\Requests\Invoice\UpdateManyInvoiceRequest $request
     * @param \App\Actions\Commands\Invoice\UpdateManyInvoices $updateManyInvoices
     */
    #[ResponseFromFile('storage/responses/invoice/invoices.json')]
    public function updateMany(UpdateManyInvoiceRequest $request, UpdateManyInvoices $updateManyInvoices)
    {
        $validated = $request->validated();

        $invoices = $updateManyInvoices->handle($validated);

        // Recupera os ids para montar o link de lista.
        $filterIds = $invoices->pluck('id');

        return InvoiceCollection::make($invoices)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'list' => route(
                        'invoices.index',
                        ['filter[id]' => $filterIds->toArray()]
                    ),
                ],
            ]);
    }

    /**
     * Exportar
     *
     * Exportar as cashbacks dos usuários informados.
     *
     * @param \App\Http\Requests\Invoice\ExportInvoiceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    #[ResponseFromFile('storage/responses/invoice/invoice-export-success.json')]
    public function export(ExportInvoiceRequest $request)
    {
        $ids = $request->get('users');

        $filename = 'cashbacks_' . uniqid() . '.csv';

        (new InvoicesManyExport($ids))->queue($filename, 'public')
            ->chain([
                new MailInvoicesExportJob(auth()->user()->id, $filename),
            ]);

        return $this->success(null, 'Cashbacks exportados para o seu email.');
    }
}
