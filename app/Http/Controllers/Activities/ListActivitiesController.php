<?php

namespace App\Http\Controllers\Activities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Actions\Queries\ListActivities;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Activity\ActivityCollection;

/**
 * @group Admin
 *
 * @subgroup Atividades
 * @subgroupDescription
 * Todos os endpoints relacionados às atividades da plataforma.
 *
 * ### Tipos de restrições (restriction)
 * Informa quais grupos de usuários podem ver determinada restrição.
 * - any (qualquer)
 * - client (cliente)
 * - admin (administrador)
 */
class ListActivitiesController extends Controller
{
    /**
     * Listar
     *
     * Listar as atividades.
     *
     * ### Inclusões permitidas (include)
     * - users
     * - responsible
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - action
     * - restriction
     * - created
     * - updated
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListActivities $listActivities
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/activity/activities.json')]
    #[QueryParam('filter[id]', 'string', "Filtra por um conjunto de IDs das atividades.<br>", example: '1,2,3,4,5')]
    #[QueryParam('filter[action]', 'string', "Filtra pelo nome interno de ação da atividade.<br>", example: 'invoice.updated_status')]
    #[QueryParam('filter[responsible-id]', 'string', "Filtra pelo ID do responsável pela atividade.<br>", example: '57')]
    #[QueryParam('filter[restriction]', 'string', "Filtra pelo tipo de restrição da atividade.<br>", example: 'client')]
    #[QueryParam('filter[user-id]', 'string', "Filtra pelo ID do usuário alvo da atividade.<br>", example: '11')]
    #[QueryParam('filter[created]', 'string', "Filtra pela data de criação da atividade.<br>", example: '2023-01-01 15:00:00')]
    #[QueryParam('filter[updated]', 'string', "Filtra pela data de atualização da atividade.<br>", example: '2023-01-01 15:00:00')]
    #[QueryParam('filter[trashed]', 'string', "Adiciona as datas que estão removidas ou desativadas.<br>", example: 'with')]
    #[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: 'user, responsible')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'action, -created')]
    public function __invoke(Request $request, ListActivities $listActivities)
    {
        $activities = $listActivities->handle();

        return ActivityCollection::make($activities)
            ->additional([
                'success' => ! $activities->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ]
            ]);
    }
}
