<?php

namespace App\Http\Controllers;

use App\Libraries\UserRole;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Traits\HttpResponses;
use App\Actions\Queries\ListPrices;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Jobs\Portal\UpdateCreatePriceListJob;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Product\PriceListCollection;
use Knuckles\Scribe\Attributes\UrlParam;

/**
 * @group Admin
 *
 * @subgroup Preços
 * @subgroupDescription
 * Endpoints com ações que acessam os preços dos itens e tabelas de preços.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('active', 'boolean', 'Demonstra se a tabela de preços está ativa ou não.')]
#[ResponseField('name', 'string', 'Nome da tabela de preços.')]
#[ResponseField('startAt', 'string', 'Data de vigência inicial da tabela de preços no formato *y-m-d*.')]
#[ResponseField('endAt', 'string', 'Data de vigência final da tabela de preços no formato *y-m-d*.')]
#[ResponseField('users.status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('users.role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('users.lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('users.email', 'boolean', 'E-mail do usuário.')]
#[ResponseField('users.codeErp', 'string', 'Código ERP do usuário.')]
#[ResponseField('users.name', 'string', 'Nome do usuário.')]
#[ResponseField('users.firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('items.name', 'string', 'Nome do item.')]
#[ResponseField('items.code', 'string', 'Código ANTARES do item.')]
#[ResponseField('items.hunt', 'boolean', 'Demonstra se o item pertence às caçadas.')]
#[ResponseField('price.amount', 'integer', 'Preço do item em centavos baseado nessa tabela de preços.')]
#[ResponseField('price.originalAmount', 'integer', 'Preço original de custo do item em centavos.')]
#[ResponseField('price.startAt', 'string', 'Data de vigência inicial desse preço vinculado no formato *y-m-d*.')]
#[ResponseField('price.endAt', 'string', 'Data de vigência final desse preço vinculado no formato *y-m-d*.')]
class PriceController extends Controller
{
    use HttpResponses;

    /**
     * Listar tabelas de preços
     *
     * Mostra todas as tabelas de preços com paginação.
     *
     * ### Inclusões permitidas (include)
     * - users
     * - items
     * - items.prices (mostra o preço do item vinculado)
     *
     * ### Ordenamentos permitidos (sort)
     * - name
     * - active
     * - start-at
     * - end-at
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListPrices $listPrices
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/price-lists.json')]
    #[QueryParam("filter[name]", "string", "Filtra pelo nome da tabela de preços, pode ser completo ou parcial.<br>", example: "TAB. GERAL")]
    #[QueryParam("filter[active]", "boolean", "Filtra se a tabela de preços está ativa ou não.<br>", example: "true")]
    #[QueryParam("filter[start-at]", "string", "Filtra pela data de vigência inicial da tabela de preços.<br>", example: "2023-06-07 19:49:13")]
    #[QueryParam("filter[end-at]", "string", "Filtra pela data de vigência final da tabela de preços.<br>", example: "2023-10-07 19:49:13")]
    #[QueryParam("include", "string", "Adiciona os dados dos recursos informados.<br>", example: "users")]
    #[QueryParam("sort", "string", "Ordena pelos recursos informados.<br>", example: "name,-active")]
    public function index(Request $request, ListPrices $listPrices)
    {
        $prices = $listPrices->handle();

        $success = $prices->isEmpty() ? false : true;

        return PriceListCollection::make($prices)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Atualizar uma tabela de preços
     *
     * @param string $codeErp
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/price-lists.update.json')]
    #[UrlParam('code_erp', "string", "Código ERP da tabela de preços.", required: true, example: "180")]
    public function update($codeErp)
    {
        UpdateCreatePriceListJob::dispatch($codeErp);

        return $this->success(
            null,
            "Tabela de preços {$codeErp} está sendo atualizada. Aguarde um momento."
        );
    }
}
