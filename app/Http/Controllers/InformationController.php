<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\Queries\ListBigNumbers;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Information\BigNumbersResource;

/**
 * @group Admin
 *
 * @subgroup Informação
 * @subgroupDescription
 * Todos os endpoints que trazem alguma informação desagrupada sobre a plataforma.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class InformationController extends Controller
{
    /**
     * Big Numbers
     *
     * Mostra os big numbers da plataforma.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListBigNumbers $listBigNumbers
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/information/bignumbers.admin.json')]
    #[ResponseField('invoices', 'array[]', 'Quantidade de notas.')]
    #[ResponseField('invoices.approved', 'integer', 'Quantidade de notas aprovadas.')]
    #[ResponseField('invoices.rejected', 'integer', 'Quantidade de notas rejeitadas.')]
    #[ResponseField('invoices.pending', 'integer', 'Quantidade de notas pendentes.')]
    public function bignumbers(Request $request, ListBigNumbers $listBigNumbers)
    {
        $bigNumbers = $listBigNumbers->handle();

        return BigNumbersResource::make($bigNumbers)
            ->additional([
                'success' => ! $bigNumbers->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
