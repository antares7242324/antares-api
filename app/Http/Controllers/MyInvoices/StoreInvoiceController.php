<?php

namespace App\Http\Controllers\MyInvoices;

use App\Models\Hunt\Invoice;
use App\Traits\HttpResponses;
use App\Libraries\InvoiceStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Actions\Commands\Invoice\CreateInvoice;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Hunt\InvoiceDetailResource;
use App\Actions\Commands\Portal\GetNfeStatusPortal;
use App\Http\Requests\Invoice\CreateInvoiceRequest;
use App\Actions\Commands\Invoice\UpdateInvoiceCredit;
use App\Actions\Commands\Portal\CheckNfeAuthorizedPortal;
use App\Libraries\ExceptionMessage;

/**
 * @group Meu Cadastro
 *
 * @subgroup Notas Fiscais
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual.')]
#[ResponseField('code', 'string', 'Código chave.')]
#[ResponseField('number', 'string', 'Código que compõe o número de série.')]
#[ResponseField('series', 'string', 'Digito do número de série.')]
#[ResponseField('cnae', 'string', 'Código CNAE do distribuidor que gerou a nota fiscal.')]
#[ResponseField('amount', 'integer', 'Valor da nota fiscal.')]
#[ResponseField('uf', 'string', 'Sigla da unidade federativa.')]
#[ResponseField('municipality', 'string', 'Nome do município.')]
#[ResponseField('issuanceAt', 'string', 'Data de emissão no formato *y-m-d h:m:s*.')]
#[ResponseField('createdAt', 'string', 'Data de criação no formato *y-m-d h:m:s*.')]
#[ResponseField('updatedAt', 'string', 'Data de atualização no formato *y-m-d h:m:s*.')]
#[ResponseField('items.code', 'string', 'Código do item na ANTARES.')]
#[ResponseField('items.unitPrice', 'integer', 'Valor unitário do item em centavos.')]
#[ResponseField('items.unitCost', 'integer', 'Valor de custo do item em centavos.')]
#[ResponseField('items.quantity', 'integer', 'Quantidade do item na nota fiscal.')]
#[ResponseField('items.subtotal', 'integer', 'Valor total do item na nota fiscal.')]
#[ResponseField('items.refund', 'integer', 'Valor de crédito gerado para o usuário na caçada.')]
#[ResponseField('items.discount', 'string', 'Valor do desconto que o usuário aplicou no item no formato decimal.')]
#[ResponseField('items.huntDiscount', 'string', 'Valor de desconto obtido pela caçada no formato decimal')]
#[ResponseField('items.description', 'integer', 'Descrição do item pela nota fiscal.')]
#[ResponseField('user.status', 'integer', 'Status do usuário.')]
#[ResponseField('user.role', 'string', 'Papel do usuário no sistema.')]
#[ResponseField('user.lastLoginAt', 'string', 'Data do último login efetuado pelo usuário.')]
#[ResponseField('user.name', 'string', 'Nome completo do usuário.')]
#[ResponseField('user.firstName', 'string', 'Primeira parte do nome do usuário.')]
class StoreInvoiceController extends Controller
{
    use HttpResponses;

    /**
     * Adicionar
     *
     * Adicionar uma nova nota fiscal.
     *
     * @param \App\Http\Requests\Invoice\CreateInvoiceRequest $request
     * @param \App\Actions\Commands\Invoice\CreateInvoice $createInvoice
     * @param \App\Actions\Commands\Portal\CheckNfeAuthorizedPortal $checkNfeAuthorizedPortal
     * @param \App\Actions\Commands\Portal\GetNfeStatusPortal $getNfeStatusPortal
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoices.json')]
    public function __invoke(
        CreateInvoiceRequest $request,
        CreateInvoice $createInvoice,
        CheckNfeAuthorizedPortal $checkNfeAuthorizedPortal,
        GetNfeStatusPortal $getNfeStatusPortal
    )
    {
        $validated = collect($request->validated());

        // Usuário logado.
        $user = auth()->user();

        // Reescreve o user_id para impedir que um usuário possa adicionar
        // uma nota para outro usuário. Este endpoint é para adicionar
        // as próprias notas ficais.
        $validated->put('user_id', $user->id);

        DB::beginTransaction();

        try {
            $nfeCode = $validated->get('code');

            // Busca o status da nota fiscal no Portal ERP se não estiver
            // bloqueado nas configurações do sistema.
            if (config('erp.active.invoice_checking')) {
                $nfeStatusResponse = $getNfeStatusPortal->handle($nfeCode);

                throw_if($nfeStatusResponse->isEmpty(),
                    new \Exception(ExceptionMessage::INVOICE_STANDARD)
                );

                // Verifica a resposta do Portal ERP para saber se a nota
                // está autorizada ou se existe algum erro.
                $checkNfeAuthorizedPortal
                    ->handleWithException($nfeStatusResponse);
            }

            // Verifica se esta nota já foi adicionada anteriormente.
            // Não pode adicionar a mesma nota mais de uma vez.
            throw_if(
                Invoice::where('code', $nfeCode)->exists(),
                new \Exception(ExceptionMessage::INVOICE_EXISTS)
            );

            // Adiciona a nota fiscal agora que está autorizada.
            $validated->put('status', InvoiceStatus::APPROVED);

            $invoice = $createInvoice->handle($validated);

            throw_if(!$invoice,
                new \Exception(ExceptionMessage::INVOICE_STANDARD)
            );

            // Adiciona ou remove o crédito da caçada conforme o status.
            (new UpdateInvoiceCredit)->handle($invoice);
        }
        catch (\Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage(), [
                'exception' => $e,
                'request' => $request->all(),
            ]);

            $errorData = collect()
                ->put('url', $request->url())
                ->put('request', $request->all());

            // Retorna o erro junto com o payload enviado.
            return $this->error(
                $errorData->toArray(),
                $e->getMessage(),
                $e->getCode() < 1 ? 500 : $e->getCode(),
            );
        }

        DB::commit();

        return InvoiceDetailResource::make($invoice)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route(
                        'my.invoices.show', ['invoice' => $invoice->id]
                    ),
                ],
            ]);
    }
}
