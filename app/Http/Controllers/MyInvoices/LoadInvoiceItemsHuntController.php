<?php

namespace App\Http\Controllers\MyInvoices;

use App\Models\File\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Actions\Commands\Media\OpenMediaContent;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Actions\Commands\Invoice\LoadDataFromNfe;
use App\Http\Resources\Hunt\InvoiceNfeHuntResource;
use App\Actions\Commands\Invoice\ExtractDataFromNfe;

/**
 * @group Meu Cadastro
 *
 * @subgroup Notas Fiscais
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class LoadInvoiceItemsHuntController extends Controller
{
    /**
     * Carregar dados da caçada
     *
     * Carrega os dados do arquivo de nota fiscal informado e calcula a caçada.
     * Retornará todos os dados necessários para apresentar o resultado da
     * caçada para o usuário.
     *
     * <aside>
     * Será necessário fazer um post desses dados calculados para confirmar
     * a caçada e o upload da nota fiscal. Esse passo é feito no endpoint de
     * ADICIONAR dentro de Notas Fiscais.
     * </aside>
     *
     * @param \App\Models\File\Media $media
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoice-nfe-hunt.json')]
    #[UrlParam('media_id', 'integer', "ID do arquivo de nota fiscal.")]
    #[ResponseField('code', 'string', "Código chave da nota fiscal.")]
    #[ResponseField('number', 'string', 'Número da nota fiscal.')]
    #[ResponseField('series', 'string', 'Dígito do número de série.')]
    #[ResponseField('issuanceAt', 'string', 'Data de emissão no formato *y-m-d h:m:s*.')]
    #[ResponseField('uf', 'string', 'Siga da unidade federativa.')]
    #[ResponseField('municipality', 'string', 'Município da nota fiscal.')]
    #[ResponseField('cnae', 'string', 'Código CNAE do distribuidor.')]
    #[ResponseField('amount', 'integer', 'Valor total da nota fiscal em centavos.')]
    #[ResponseField('cashback', 'integer', 'Valor total do bashback em centavos.')]
    #[ResponseField('items.code', 'string', 'Código do item na ANTARES.')]
    #[ResponseField('items.quantity', 'integer', 'Quantidade do item na nota fiscal.')]
    #[ResponseField('items.unitPrice', 'integer', 'Valor unitário do item em centavos.')]
    #[ResponseField('items.subtotal', 'integer', 'Valor total do item na nota fiscal.')]
    #[ResponseField('items.description', 'integer', 'Descrição do item pela nota fiscal.')]
    #[ResponseField('items.hunt.unitList', 'integer', 'Valor de original do item em centavos.')]
    #[ResponseField('items.hunt.subtotalList', 'integer', 'Valor total original do produto em centavos.')]
    #[ResponseField('items.hunt.unitCost', 'integer', 'Valor de custo do item em centavos.')]
    #[ResponseField('items.hunt.subtotalCost', 'integer', 'Valor total do custo do produto em centavos.')]
    #[ResponseField('items.hunt.sellerPercent', 'integer', 'Valor do desconto que o vendedor aplicou no item no formato decimal.')]
    #[ResponseField('items.hunt.huntPercent', 'integer', 'Valor de desconto obtido pela caçada no formato decimal')]
    #[ResponseField('items.hunt.amountDiscount', 'integer', 'Valor do desconto com base no percentual da caçada em centavos.')]
    #[ResponseField('items.hunt.cashback', 'integer', 'Valor de crédito gerado para o usuário na caçada em centavos.')]
    #[ResponseField('items.relationships.userItem.type', 'string', 'Tipo de recurso do código de item pelo usuário.')]
    #[ResponseField('items.relationships.userItem.id', 'string', 'ID do código de item pelo usuário.')]
    #[ResponseField('items.relationships.userItem.attributes.code', 'string', 'Código vinculado ao item pelo usuário.')]
    #[ResponseField('items.relationships.userItem.attributes.createdAt', 'datetime', 'Data de criação do vínculo ao item pelo usuário.')]
    #[ResponseField('items.relationships.userItem.attributes.updatedAt', 'datetime', 'Data de atualização do vínculo ao item pelo usuário.')]
    #[ResponseField('items.relationships.userItem.relationships.user', 'object', 'Dados do usuário do item vinculado.')]
    #[ResponseField('items.relationships.userItem.relationships.item', 'object', 'Dados do item no sistema Antares.')]
    public function __invoke(Media $media, Request $request, LoadDataFromNfe $command)
    {
        $user = auth()->user();

        $fileContent = (new OpenMediaContent)->handle($media);

        $data = (new ExtractDataFromNfe)->handle($fileContent);

        $items = $command->handle($data, $user);

        $data->put('items', $items);

        return InvoiceNfeHuntResource::make($data)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'store' => route('my.invoices.store'),
                ],
            ]);
    }
}
