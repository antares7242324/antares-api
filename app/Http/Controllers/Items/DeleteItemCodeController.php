<?php

namespace App\Http\Controllers\Items;

use App\Models\Product\UserItem;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Itens
 * @subgroup Códigos dos Produtos
 */
class DeleteItemCodeController extends Controller
{
    /**
     * Remover
     *
     * Deleta um código de produto vinculado por um distribuidor de um
     * produto equivalente na Antares.
     *
     * @param \App\Models\Account\User $user
     * @param \App\Models\Product\UserItem $userItem
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/user-item-success.json')]
    #[UrlParam('userItem_id', 'string', "O ID do código do produto.", example: "4")]
    public function __invoke(UserItem $userItem)
    {
        $userItem->delete();

        return $this->success(null, 'User Item removed');
    }
}
