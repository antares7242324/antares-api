<?php

namespace App\Http\Controllers\Items;

use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use App\Models\Product\UserItem;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Product\UserItemCollection;

/**
 * @group Itens
 * @subgroup Códigos dos Produtos
 */
class ToggleConfirmationItemCodesController extends Controller
{
    use HttpResponses;

    /**
     * Confirmação
     *
     * Ativa ou desativa a confirmação de uma lista de equivalência de itens
     * cadastradas pelos distribuidores. Deverá ser enviado um array com todos os IDs.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    #[ResponseFromFile('storage/responses/item/user-items.json')]
    public function __invoke(Request $request)
    {
        $validated = $request->validate([
            'ids' => ['array', 'required'],
            // Array de IDs `UserItem` das equivalências de itens cadastradas pelos distribuidores.
            'ids.*' => ['integer', 'required', 'exists:'.UserItem::TABLE.',id'],
            // Informa se o administrador da Antares verificou o cadastro da equivalência do item pelo distribuidor.
            'confirmed' => ['boolean', 'required'],
        ]);

        $userItems = UserItem::find($validated['ids']);

        try {
            $userItems->each(fn ($item) =>
                $item->update(['confirmed' => $validated['confirmed']])
            );

            $userItems = $userItems->fresh();

            return UserItemCollection::make($userItems)
                ->additional([
                    'success' => $userItems->isEmpty() ? false : true,
                    'links' => [
                        'self' => $request->url(),
                    ],
                ]);
        }
        catch (\Exception $ex) {
            $data = collect(['payload' => $validated]);

            return $this->error($data, $ex->getMessage(), 500);
        }
    }
}
