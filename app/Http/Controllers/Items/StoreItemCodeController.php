<?php

namespace App\Http\Controllers\Items;

use App\Http\Controllers\Controller;
use App\Actions\Commands\Product\CreateUserItem;
use App\Http\Resources\Product\UserItemResource;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Product\CreateUserItemRequest;

/**
 * @group Itens
 * @subgroup Códigos dos Produtos
 */
class StoreItemCodeController extends Controller
{
    /**
     * Adicionar
     *
     * Adiciona um código de produto vinculado por um distribuidor a
     * um item equivalente na Antares.
     *
     * @param \App\Http\Requests\Product\CreateUserItemRequest $request
     * @param \App\Actions\Commands\Product\CreateUserItem $command
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/user-items.json')]
    public function __invoke(CreateUserItemRequest $request, CreateUserItem $command)
    {
        $validated = collect($request->validated());

        $userItem = $command->handle($validated);

        return UserItemResource::make($userItem)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
