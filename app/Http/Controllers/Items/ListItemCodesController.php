<?php

namespace App\Http\Controllers\Items;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Actions\Queries\ListUserItems;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Product\UserItemCollection;

/**
 * @group Itens
 * @subgroup Códigos dos Produtos
 * @subgroupDescription
 * Endpoints para acessar os dados dos códigos dos
 * produtos vinculados a um distribuidor.
 */
class ListItemCodesController extends Controller
{
    /**
     * Listar
     *
     * Mostra todos os códigos dos itens vinculados pelos distribuidores
     * aos produtos da Antares.
     *
     * ### Includes permitidos (include)
     * - invoices (Notas fiscais)
     *
     * ### Ordenamentos permitidos (sort)
     * - id (código ID do UserItem)
     * - code
     * - name (nome do produto no sistema do usuário)
     * - item.name (nome do produto na Antares)
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListUserItems $command
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/user-items.json')]
    #[QueryParam('filter[id]', 'string', "Filtra pelo ID do código do produto. Pode ser uma lista de IDs separados por vírgula.<br>", example: "1,2,3")]
    #[QueryParam('filter[code]', 'string', "Filtra pelo código do produto informado pelo usuário. Busca qualquer combinação do que foi escrito.<br>", example: "56")]
    #[QueryParam('filter[code-exact]', 'string', "Filtra pelo código do produto informado pelo usuário. Busca exatamente o que foi escrito.<br>", example: "00567")]
    #[QueryParam('filter[name]', 'string', "Filtra pelo nome do item informado pelo usuário. Pode ser um texto parcial.<br>", example: "Ultra")]
    #[QueryParam('filter[name-exact]', 'string', "Filtra pelo nome do item informado pelo usuário. Deve ser o nome exato.<br>", example: "Acoplamento Flexível Ultra Plus")]
    #[QueryParam('filter[item]', 'string', "Filtra pelo ID do item. Pode ser uma lista de IDs separados por vírgula.<br>", example: "1, 2, 3")]
    #[QueryParam('filter[user]', 'string', "Filtra pelo ID do usuário. Pode ser uma lista de IDs separados por vírgula.<br>", example: "1, 2, 3")]
    #[QueryParam('filter[item.name]', 'string', "Filtra pelo nome do item. Pode ser um texto parcial.<br>", example: "Acoplamento")]
    #[QueryParam('filter[item.name-exact]', 'string', "Filtra pelo nome do item. Deve ser o nome exato.<br>", example: "Acoplamento Flexível AT S10")]
    #[QueryParam('filter[trashed]', 'string', "Permite mostrar os registros excluídos. 'with' inclui ambos, 'only' apenas os excluídos.<br>", example: "with")]
    #[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: 'invoices')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'id,-nome')]
    public function __invoke(Request $request, ListUserItems $command)
    {
        $userItems = $command->handle();

        return UserItemCollection::make($userItems)
            ->additional([
                'success' => $userItems->isEmpty() ? false : true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
