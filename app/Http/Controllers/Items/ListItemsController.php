<?php

namespace App\Http\Controllers\Items;

use Illuminate\Http\Request;
use App\Actions\Queries\ListItems;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Http\Resources\Product\ItemCollection;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Itens
 *
 * Todos os endpoints relacionados aos itens da plataforma.
 * Os itens são os dados dos produtos sem estarem vinculados com a tabela de preços.
 * Quando um item é vinculado a uma tabela de preços, ele passa a ser chamado de produto.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class ListItemsController extends Controller
{
    /**
     * Listar
     *
     * Lista os items.
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - name
     * - code
     * - hunt
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListItems $query
     * @return \Illuminate\Http\Response
     */
    #[ResponseFromFile('storage/responses/item/item.index.json')]
    #[QueryParam('filter[id]', 'string', "Filtra pelo ID do item. Pode enviar uma lista de IDs separados por vírgula.<br>", example: '12,5')]
    #[QueryParam('filter[name]', 'string', "Filtra pelo nome do item.<br>", example: "Acoplamento Flexível AT 25")]
    #[QueryParam('filter[code]', 'string', "Filtra pelo código do item.<br>", example: "44567")]
    #[QueryParam('filter[hunt]', 'boolean', 'Filtra se o item pertence à caçada ou não.<br>', example: true)]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'id,-name,code,hunt')]
    public function __invoke(Request $request, ListItems $query)
    {
        $items = $query->handle();

        $success = $items->isEmpty() ? false : true;

        return ItemCollection::make($items)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
