<?php

namespace App\Http\Controllers\Items;

use Illuminate\Http\Request;
use App\Models\Product\UserItem;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Http\Resources\Product\UserItemResource;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Itens
 * @subgroup Códigos dos Produtos
 */
class ShowItemCodeController extends Controller
{
    /**
     * Detalhar
     *
     * Detalha um código de produto vinculado por um distribuidor
     * equivalente a um produto da Antares.
     *
     * @param \App\Models\Product\UserItem $userItem
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/user-item.json')]
    #[UrlParam('userItem_id', 'string', "O ID do código do produto.", example: "4")]
    public function __invoke(UserItem $userItem, Request $request)
    {
        return UserItemResource::make($userItem)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'delete' => route('items.codes.delete', [
                        'userItem' => $userItem->id
                    ]),
                ],
            ]);
    }
}
