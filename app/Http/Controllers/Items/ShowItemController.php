<?php

namespace App\Http\Controllers\Items;

use App\Models\Product\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Http\Resources\Product\ItemResource;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Itens
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class ShowItemController extends Controller
{
    /**
     * Detalhar
     *
     * Detalhar o item informado.
     *
     * @param \App\Models\Product\Item $item
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    #[ResponseFromFile('storage/responses/item/item.index.json')]
    #[UrlParam('id', 'string', "ID do item.", required: true, example: "1")]
    public function __invoke(Item $item, Request $request)
    {
        return ItemResource::make($item)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
