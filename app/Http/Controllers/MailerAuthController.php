<?php

namespace App\Http\Controllers;

use App\Models\Account\User;
use Illuminate\Support\Carbon;
use App\Libraries\TokenAbility;
use App\Mail\AccountInviteMail;
use App\Mail\AccountRecoverMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\MailerResource;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\Unauthenticated;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Http\Requests\Account\EmailRequest;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group E-mail
 *
 * Endpoints para envio de e-mail.
 *
 * Todos os e-mails estão sendo enviados através do serviço do mailgun, que
 * é configurado no arquivo env.
 *
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('sent', 'boolean', 'Informa se o e-mail foi enviado.')]
#[ResponseField('email', 'string', 'O e-mail do usuário.')]
#[ResponseField('access', 'string', 'O tipo de acesso desse usuário. Poder ser client ou admin.')]
#[ResponseField('token', 'string', 'O token gerado para o usuário.')]
#[ResponseField('name', 'string', 'Informa o tipo do token conforme a sua função.')]
#[ResponseField('abilities', 'array', 'Um array com as permissões e habilidades do token.')]
#[ResponseField('expiresAt', 'string', 'Informa a data de expiração do token. Será null se ele não expira.')]
#[ResponseField('lastLoginAt', 'string', 'Informa a data do último login do usuário. Será null se ele não logou ainda.')]
class MailerAuthController extends Controller
{
    /**
     * Convidar novo usuário
     *
     * Envia o e-mail de convite para o usuário informado no parâmetro usando o seu ID. Sempre
     * que esse endpoint for chamado, um novo token de ativação é criado para o usuário.
     * Este token segue a mesma lógica dos nomes de tokens informados na autenticação.
     *
     * @param \App\Models\Account\User $user
     * @return \Illuminate\Http\Resources\Json\JsonResource|\Illuminate\Http\JsonResponse
     */
    #[UrlParam('user_id', 'integer', 'O ID do usuário.')]
    #[ResponseFromFile('storage/responses/mailer/user-invite.json')]
    public function invite(User $user)
    {
        $token = $user->createToken('activation', [TokenAbility::ACTIVATE], Carbon::now()->addHours(2));

        $sentMessage = Mail::to($user->email)->send(new AccountInviteMail($user, $token));

        $resource = collect([
            'user' => $user,
            'token' => $token,
            'sentMessage' => $sentMessage,
        ]);

        return MailerResource::make($resource)->additional(['success' => true]);
    }

    /**
     * Recuperar conta do usuário
     *
     * Envia o e-mail de recuperação para o usuário com o e-mail informado no corpo da requisição.
     *
     * @param \App\Http\Requests\Account\EmailRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource|\Illuminate\Http\JsonResponse`
     */
    #[Unauthenticated]
    #[BodyParam('email', 'string', 'O email do usuário.')]
    #[ResponseFromFile('storage/responses/mailer/user-recover.json')]
    public function recover(EmailRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        $token = $user->createToken('recovering', [TokenAbility::RECOVER], Carbon::now()->addHours(2));

        $sentMessage = Mail::to($user->email)->send(new AccountRecoverMail($user, $token));

        $resource = collect([
            'user' => $user,
            'token' => $token,
            'sentMessage' => $sentMessage,
        ]);

        return MailerResource::make($resource)->additional(['success' => true]);
    }
}
