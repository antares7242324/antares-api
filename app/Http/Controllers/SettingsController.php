<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Settings\DiscountResource;
use App\Actions\Commands\Discount\ListExtraDiscounts;

/**
 * @group Admin
 *
 * @subgroup Configuração
 * @subgroupDescription
 * Todos os endpoints relacionados as configurações da plataforma.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class SettingsController extends Controller
{
    /**
     * Descontos
     *
     * Mostra os descontos configurados na plataforma.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Commands\Discount\ListExtraDiscounts $lisExtraDiscounts
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/settings/discounts.admin.json')]
    #[ResponseField('main', 'array[string]', "Lista de descontos da plataforma ANTARES.")]
    #[ResponseField('extra', 'array[string]', "Lista de descontos adicionais.")]
    public function discounts(Request $request, ListExtraDiscounts $lisExtraDiscounts)
    {
        $extraDiscounts = $lisExtraDiscounts->handle();

        $success = ! $extraDiscounts ? false : true;

        $resource = [
            'extra_discounts' => $extraDiscounts,
        ];

        return DiscountResource::make(collect($resource))
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
