<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use App\Models\Transaction\Compensation;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Actions\Queries\ListCompensations;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Actions\Commands\Hunt\UpdateUserBalance;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Actions\Commands\Transaction\CreateCompensation;
use App\Actions\Commands\Transaction\UpdateCompensation;
use App\Actions\Commands\Transaction\CreateManyCompensation;
use App\Http\Requests\Transaction\UpdateCompensationRequest;
use App\Http\Resources\Transaction\CompensationDetailResource;
use App\Http\Resources\Transaction\CompensationListCollection;
use App\Http\Requests\Transaction\CreateManyCompensationRequest;
use App\Http\Requests\Transaction\CreateSingleCompensationRequest;

/**
 * @group Admin
 *
 * @subgroup Compensações
 * @subgroupDescription
 * Ações para manipular as compensações aos usuários na plataforma.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('amount', 'integer', 'Valor da compensação em centavos.')]
#[ResponseField('currentBalance', 'integer', 'Saldo do usuário em centavos no momento da compensação.')]
#[ResponseField('createdAt', 'string', 'Data de criação da compensação no formato *y-m-d H:i:s*.')]
class CompensationController extends Controller
{
    use HttpResponses;

    /**
     * Listar
     *
     * Lista as compensações.
     *
     * ### Inclusões permitidas (include)
     * - creator
     * - user
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - amount
     * - reason
     * - created
     * - updated
     * - creator.name (precisa do include 'creator')
     * - user.name (precisa do include 'user')
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListCompensations $listCompensations
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensations.get.json')]
    #[QueryParam('filter[id]', 'integer', "Filtra pelo ID da compensação.<br>", example: 11)]
    #[QueryParam('filter[amount]', 'integer', "Filtra pelo valor da compensação, positivo ou negativo e em centavos.<br>", example: 10000)]
    #[QueryParam('filter[user.id]', 'integer', 'Filtra pelo ID do usuário.<br>', example: 4)]
    #[QueryParam('filter[creator.id]', 'integer', "Filtra pelo ID do criador.<br>", 6)]
    #[QueryParam('filter[reason]', 'string', "Filtra pelo texto de razão.<br>", "Teste")]
    #[QueryParam('filter[created]', 'string', "Filtra pela data de criação.<br>", "2023-07-15 13:00:00")]
    #[QueryParam('filter[updated]', 'string', "Filtra pela data de atualização.<br>", "2023-07-15 13:00:00")]
    #[QueryParam('filter[creator.name]', 'string', "Filtra pelo nome do criador da compensação.<br>", "Meeg")]
    #[QueryParam('filter[user.name]', 'string', "Filtra pelo nome do usuário.<br>", "Merlin")]
    #[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: 'user,creator')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'user-name,-amount')]
    #[ResponseField('creator.role', 'string', 'Papel do usuário criador.')]
    #[ResponseField('creator.email', 'string', 'Email do usuário criador.')]
    #[ResponseField('creator.name', 'integer', 'Nome do usuário criador.')]
    #[ResponseField('creator.firstName', 'string', 'Nome sem sobrenome do usuário criador.')]
    #[ResponseField('user.role', 'string', 'Papel do usuário que recebeu a compensação.')]
    #[ResponseField('user.email', 'string', 'Email do usuário que recebeu a compensação.')]
    #[ResponseField('user.name', 'integer', 'Nome do usuário que recebeu a compensação.')]
    #[ResponseField('user.firstName', 'string', 'Nome sem sobrenome do usuário que recebeu a compensação.')]
    public function index(Request $request, ListCompensations $listCompensations)
    {
        $compensations = $listCompensations->handle();

        return CompensationListCollection::make($compensations)
            ->additional([
                'success' => ! $compensations->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Adicionar
     *
     * Adiciona uma nova compensação para um único usuário.
     *
     * @param \App\Http\Requests\Transaction\CreateSingleCompensationRequest $request
     * @param \App\Actions\Commands\Transaction\CreateCompensation $createCompensation
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensation.json')]
    public function store(CreateSingleCompensationRequest $request, CreateCompensation $createCompensation)
    {
        $validated = $request->validated();

        $compensation = $createCompensation->handle($validated, auth()->user());

        return CompensationDetailResource::make($compensation)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Adicionar muitas
     *
     * Adiciona uma nova compensação para uma lista de usuários.
     *
     * @param \App\Http\Requests\Transaction\CreateManyCompensationRequest $request
     * @param \App\Actions\Commands\Transaction\CreateManyCompensation $createManyCompensation
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensations.post.json')]
    public function storeMany(CreateManyCompensationRequest $request, CreateManyCompensation $createManyCompensation)
    {
        $validated = $request->validated();

        $compensations = $createManyCompensation->handle($validated, auth()->user());

        return CompensationListCollection::make($compensations)
            ->additional([
                'success' => true,
                'links' => [
                   'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Detalha uma compensação.
     *
     * @param \App\Models\Transaction\Compensation $compensation
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensation.json')]
    #[UrlParam('id', 'string', "ID da compensação.", required: true, example: "1")]
    #[ResponseField('creator.role', 'string', 'Papel do usuário criador.')]
    #[ResponseField('creator.email', 'string', 'Email do usuário criador.')]
    #[ResponseField('creator.name', 'integer', 'Nome do usuário criador.')]
    #[ResponseField('creator.firstName', 'string', 'Nome sem sobrenome do usuário criador.')]
    #[ResponseField('user.role', 'string', 'Papel do usuário que recebeu a compensação.')]
    #[ResponseField('user.email', 'string', 'Email do usuário que recebeu a compensação.')]
    #[ResponseField('user.name', 'integer', 'Nome do usuário que recebeu a compensação.')]
    #[ResponseField('user.firstName', 'string', 'Nome sem sobrenome do usuário que recebeu a compensação.')]
    public function show(Compensation $compensation, Request $request)
    {
        return CompensationDetailResource::make($compensation)
            ->additional([
                'success' => true,
            ]);
    }

    /**
     * Atualizar
     *
     * Atualizar uma compensação.
     *
     * @param \App\Models\Transaction\Compensation $compensation
     * @param \App\Http\Requests\Transaction\UpdateCompensationRequest $request
     * @param \App\Actions\Commands\Transaction\UpdateCompensation $updateCompensation
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensation.json')]
    #[UrlParam('id', 'string', "ID da compensação.", required: true, example: "1")]
    public function update(Compensation $compensation, UpdateCompensationRequest $request, UpdateCompensation $updateCompensation)
    {
        $validated = $request->validated();

        $compensation = $updateCompensation->handle($compensation, $validated);

        return CompensationDetailResource::make($compensation)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('compensations.show', ['compensation' => $compensation->id]),
                ],
            ]);
    }

    /**
     * Deletar
     *
     * Remove a compensação informada. Vai preencher o campo `deleted_at` e impedir que a compensação seja exibida novamente.
     *
     * @param \App\Models\Transaction\Compensation $compensation
     * @param \App\Actions\Commands\Hunt\UpdateUserBalance $updateUserBalance
     * @return \Illuminate\Http\JsonResponse
     */
    #[UrlParam('id', 'string', "ID da compensação.", required: true, example: "1")]
    public function destroy(Compensation $compensation, UpdateUserBalance $updateUserBalance)
    {
        $user = $compensation->user;

        $compensation->delete();

        $updateUserBalance->handle($user);

        return $this->success(null, 'Compensation removed.');
    }
}
