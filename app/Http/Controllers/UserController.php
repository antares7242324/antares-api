<?php

namespace App\Http\Controllers;

use App\Libraries\UserRole;
use App\Models\Account\User;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use Illuminate\Support\Facades\DB;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\BodyParam;
use App\Actions\Commands\Account\CreateUser;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Http\Requests\Account\CreateUserRequest;
use App\Http\Requests\Account\UpdateUserRequest;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Account\UserSingleResource;
use App\Actions\Commands\Account\CreateUserWithStore;
use App\Actions\Commands\Account\UpdateUser;
use App\Actions\Commands\Account\UpdateUserWithStore;
use App\Http\Requests\Account\CreateUserClientRequest;

/**
 * @group Admin
 *
 * Endpoints acessíveis apenas para administradores e desenvolvedores.
 *
 * @subgroup Usuários
 * @subgroupDescription
 * Endpoints com ações que lidam com os usuários no sistema,
 * sejam distribuidores ou administradores.
 *
 * ### Situação (status)
 * - active (ativo)
 * - inactive (inativo)
 * - blocked (bloqueado)
 *
 * ### Papel (role)
 * - client (cliente, distribuidor)
 * - admin (administrador)
 * - meeg (meeg, desenvolvedores)
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('name', 'string', 'Nome do usuário.')]
#[ResponseField('firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
#[ResponseField('discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
#[ResponseField('discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
#[ResponseField('stores.name', 'string', 'Nome da loja.')]
#[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
#[ResponseField('price.name', 'string', 'Nome da tabela de preços.')]
class UserController extends Controller
{
    /**
     * Adicionar
     *
     * Adiciona um novo usuário na plataforma.
     *
     * @param  \App\Http\Requests\Account\CreateUserRequest  $request
     * @param  \App\Actions\Commands\Account\CreateUser  $createUser
     * @return  \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/user/user.get.admin.json')]
    #[BodyParam('current_discount', 'float', "Desconto atual do distribuidor. *Obrigatório se o papel for ".UserRole::CLIENT."*.", required: false, example: 0.10)]
    #[BodyParam('price_list_id', 'string', "ID da tabela de preços (ver ***Listar tabelas de preços***). *Obrigatório se o papel for ".UserRole::CLIENT."*.", required: false, example: 0.10)]
    #[BodyParam('code_erp', 'string', "Código ERP do Portal Antares para o distribuidor. *Obrigatório se o papel for ".UserRole::CLIENT."*.", required: false, example: "556")]
    #[BodyParam('store.name', 'string', "Nome da loja. *Obrigatório se o papel for ".UserRole::CLIENT."*.", required: false, example: "Marcenaria Du Bem")]
    #[BodyParam('store.cnpj', 'string', "CNPJ da loja. *Obrigatório se o papel for ".UserRole::CLIENT."*.", required: false, example: "27641266000170")]
    #[BodyParam('store.code_erp', 'string', "Código ERP do Portal Antares para a loja. *Obrigatório se o papel for ".UserRole::CLIENT."*.", required: false, example: "11")]
    public function store(CreateUserRequest $request, CreateUserWithStore $createUserWithStore, CreateUser $createUser)
    {
        $validated = collect($request->validated());

        $role = $validated->get('role');

        // Adiciona a validação específica de
        // quando o usuario for um distribuidor.
        if ($role == UserRole::CLIENT) {
            $clientValidated = $this->validate(
                $request, (new CreateUserClientRequest())->rules()
            );

            $validated = $validated->merge($clientValidated);
        }

        DB::beginTransaction();

        try {
            $user = $role == UserRole::CLIENT
                ? $createUserWithStore->handle($validated)
                : $createUser->handle($validated);

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();

            return $this->error(
                ['errors' => $e->getTraceAsString()],
                $e->getMessage(),
                500
            );
        }

        return UserSingleResource::make($user)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('users.show', ['user' => $user->id]),
                ],
            ]);
    }

    /**
     * Atualizar
     *
     * Atualiza um usuário na plataforma.
     *
     * @param \App\Models\Account\User $user
     * @param \App\Http\Requests\Account\UpdateUserRequest $request
     * @param \App\Actions\Commands\Account\UpdateUser $updateUser
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[UrlParam('id', 'string', "ID do usuário.", required: true, example: "1")]
    #[ResponseFromFile('storage/responses/user/user.get.admin.json')]
    #[BodyParam('store.name', 'string', "Nome da loja. *Obrigatório se resolver editar a loja.*", required: false, example: "Marcenaria Du Bem")]
    #[BodyParam('store.cnpj', 'string', "CNPJ da loja. *Obrigatório se resolver editar a loja.*", required: false, example: "27641266000170")]
    #[BodyParam('store.code_erp', 'string', "Código ERP do Portal Antares para a loja. *Obrigatório se resolver editar a loja.*", required: false, example: "11")]
    public function update(User $user, UpdateUserRequest $request, UpdateUserWithStore $updateUserWithStore, UpdateUser $updateUser)
    {
        $validated = collect($request->validated());

        DB::beginTransaction();

        try {

            $user = $user->isClient()
                ? $updateUserWithStore->handle($user, $validated)
                : $updateUser->handle($user, $validated);

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();

            return $this->error(
                ['errors' => $e->getTraceAsString()],
                $e->getMessage(),
                500
            );
        }

        return UserSingleResource::make($user)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('users.show', ['user' => $user->id]),
                ],
            ]);
    }
}
