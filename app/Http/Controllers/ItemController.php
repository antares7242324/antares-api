<?php

namespace App\Http\Controllers;

use App\Models\Product\Item;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use App\Actions\Queries\ListItems;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Actions\Commands\Product\CreateItem;
use App\Actions\Commands\Product\UpdateItem;
use App\Http\Resources\Product\ItemResource;
use App\Http\Resources\Product\ItemCollection;
use App\Http\Requests\Product\CreateItemRequest;
use App\Http\Requests\Product\UpdateItemRequest;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Admin
 *
 * @subgroup Produtos
 * @subgroupDescription
 * Todos os endpoints relacionados aos produtos da plataforma.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class ItemController extends Controller
{
    use HttpResponses;

    /**
     * Listar
     *
     * Listar os itens.
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - name
     * - code
     * - hunt
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actions\Queries\ListItems  $listItems
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[QueryParam('filter[id]', 'string', "Filtra pelo ID da nota fiscal. Pode enviar uma lista de IDs separados por vírgula.<br>", example: '12,5')]
    #[QueryParam('filter[name]', 'string', "Filtra pelo nome do item.<br>", example: "Acoplamento Flexível AT 25")]
    #[QueryParam('filter[code]', 'string', "Filtra pelo código do item.<br>", example: "44567")]
    #[QueryParam('filter[hunt]', 'boolean', 'Filtra se o item pertence à caçada ou não.<br>', example: true)]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'id,-name')]
    public function index(Request $request, ListItems $listItems)
    {
        $items = $listItems->handle();

        $success = $items->isEmpty() ? false : true;

        return ItemCollection::make($items)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Criar
     *
     * Cria um item.
     *
     * @param \App\Http\Requests\Product\CreateItemRequest $request
     * @param \App\Actions\Commands\Product\CreateItem $createItem
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/item.json')]
    #[UrlParam('item_id', 'string', "ID do produto.", required: true, example: "1")]
    public function store(CreateItemRequest $request, CreateItem $createItem)
    {
        $item = $createItem->handle($request);

        return ItemResource::make($item)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('products.items.show', ['item' => $item->id]),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Detalhar o item.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product\Item  $item
     * @return  \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/item.json')]
    #[UrlParam('item_id', 'string', "ID do produto.", required: true, example: "1")]
    public function show(Request $request, Item $item)
    {
        return ItemResource::make($item)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Atualizar
     *
     * Atualiza o item.
     *
     * @param \App\Http\Requests\Product\UpdateItemRequest $request
     * @param \App\Models\Product\Item $item
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/item.json')]
    #[UrlParam('item_id', 'string', "ID do produto.", required: true, example: "1")]
    public function update(UpdateItemRequest $request, Item $item, UpdateItem $updateItem)
    {
        $item = $updateItem->handle($item, $request);

        return ItemResource::make($item)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('products.items.show', ['item' => $item->id]),
                ],
            ]);
    }

    /**
     * Remover
     *
     * Remove o item.
     *
     * @param \App\Models\Product\Item $item
     * @return \Illuminate\Http\JsonResponse
     */
    #[ResponseFromFile('storage/responses/item/item.delete.json')]
    #[UrlParam('item_id', 'string', "ID do produto.", required: true, example: "1")]
    public function destroy(Item $item)
    {
        $item->delete();

        return $this->success(null, 'Item removed.');
    }
}
