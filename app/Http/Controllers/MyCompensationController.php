<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction\Compensation;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Actions\Queries\ListCompensations;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\Transaction\CompensationDetailResource;
use App\Http\Resources\Transaction\CompensationListCollection;

/**
 * @group Meu Cadastro
 *
 * @subgroup Compensações
 * @subgroupDescription
 * Endpoints que acessam os dados das compensações.
 */
class MyCompensationController extends Controller
{
    /**
     * Listar
     *
     * Lista as compensações.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListCompensations $listCompensations
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensations.get.json')]
    #[ResponseField('creator.role', 'string', 'Papel do usuário criador.')]
    #[ResponseField('creator.email', 'string', 'Email do usuário criador.')]
    #[ResponseField('creator.name', 'integer', 'Nome do usuário criador.')]
    #[ResponseField('creator.firstName', 'string', 'Nome sem sobrenome do usuário criador.')]
    #[ResponseField('user.role', 'string', 'Papel do usuário que recebeu a compensação.')]
    #[ResponseField('user.email', 'string', 'Email do usuário que recebeu a compensação.')]
    #[ResponseField('user.name', 'integer', 'Nome do usuário que recebeu a compensação.')]
    #[ResponseField('user.firstName', 'string', 'Nome sem sobrenome do usuário que recebeu a compensação.')]
    public function index(Request $request, ListCompensations $listCompensations)
    {
        // Adiciona o usuário para evitar que ele acesse os pedidos
        // dos outros usuários na plataforma.
        $user = auth()->user();

        $compensations = $listCompensations->handle($user);

        return CompensationListCollection::make($compensations)
            ->additional([
                'success' => ! $compensations->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Detalha uma compensação.
     *
     * @param \App\Models\Transaction\Compensation $compensation
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/transaction/compensation.json')]
    #[UrlParam('id', 'string', "ID da compensação.", required: true, example: "1")]
    #[ResponseField('creator.role', 'string', 'Papel do usuário criador.')]
    #[ResponseField('creator.email', 'string', 'Email do usuário criador.')]
    #[ResponseField('creator.name', 'integer', 'Nome do usuário criador.')]
    #[ResponseField('creator.firstName', 'string', 'Nome sem sobrenome do usuário criador.')]
    #[ResponseField('user.role', 'string', 'Papel do usuário que recebeu a compensação.')]
    #[ResponseField('user.email', 'string', 'Email do usuário que recebeu a compensação.')]
    #[ResponseField('user.name', 'integer', 'Nome do usuário que recebeu a compensação.')]
    #[ResponseField('user.firstName', 'string', 'Nome sem sobrenome do usuário que recebeu a compensação.')]
    public function show(Compensation $compensation, Request $request)
    {
        // Adiciona o usuário para evitar que ele acesse os pedidos
        // dos outros usuários na plataforma.
        $user = auth()->user();

        if ($compensation->user_id == $user->id) new ModelNotFoundException;

        return CompensationDetailResource::make($compensation)
            ->additional([
                'success' => true,
            ]);
    }
}
