<?php

namespace App\Http\Controllers;

use App\Models\Account\User;
use App\Models\Account\Store;
use App\Traits\HttpResponses;
use Knuckles\Scribe\Attributes\ResponseFromApiResource;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Http\Resources\Account\StoreResource;
use App\Actions\Commands\Account\AddStoreToUser;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Account\CreateStoreRequest;

/**
 * @group Admin
 *
 * @subgroup Lojas
 * @subgroupDescription Ações para administrar as lojas dos usuários.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class UserStoreController extends Controller
{
    use HttpResponses;

    /**
     * Adicionar loja a um usuário
     *
     * Adiciona a loja ao usuário informado pelo ID.
     * Cada usuário pode possuir mais de uma loja.
     *
     * @param  \App\Models\Account\User  $user
     * @param  \App\Http\Requests\Account\CreateStoreRequest  $request
     * @param  \App\Actions\Commands\Account\AddStoreToUser $addStoreToUser
     * @return  \Illuminate\Http\Resources\Json\JsonResource
     */
    #[UrlParam('user_id', 'string', "ID do usuário.", required: true, example: "1")]
    #[ResponseFromApiResource(StoreResource::class, Store::class)]
    public function store(User $user, CreateStoreRequest $request, AddStoreToUser $addStoreToUser)
    {
        $store = $addStoreToUser->handle($user, $request);

        return StoreResource::make($store)
            ->additional([
                'success' => true,
            ]);
    }

    /**
     * Remover a loja de um usuário
     *
     * Deve ser informado o ID do usuário para garantir a exclusão corretamente.
     *
     * @param  \App\Models\Account\User  $user
     * @param  \App\Models\Account\Store  $store
     * @return  \Illuminate\Http\JsonResponse
     */
    #[UrlParam('user_id', 'string', "ID do usuário.", required: true, example: "1")]
    #[UrlParam('store_id', 'string', "ID da loja.", required: true, example: "6")]
    #[ResponseFromFile('storage/responses/store/store.delete.admin.json', 401)]
    public function destroy(User $user, Store $store)
    {
        $store->delete();

        return $this->success(null, 'Store removed.');
    }
}
