<?php

namespace App\Http\Controllers;

use App\Models\Order\Order;
use App\Models\Account\User;
use Illuminate\Http\Request;
use App\Libraries\OrderStatus;
use App\Actions\Queries\ListOrders;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Actions\Commands\Order\CopyOrder;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Actions\Commands\Order\CreateOrder;
use App\Actions\Commands\Order\UpdateOrder;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Order\OrderCollection;
use App\Http\Requests\Order\CreateOrderRequest;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Actions\Commands\Order\UpdateManyOrders;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Order\OrderDetailResource;
use App\Http\Requests\Order\UpdateManyOrderRequest;

/**
 * @group Admin
 *
 * @subgroup Pedidos
 * @subgroupDescription
 * Endpoints para visualizar e manipular os pedidos dos usuários.
 *
 * ### Tipos de Situação (status)
 * - draft (rascunho)
 * - requested (requerido)
 * - sent (enviado)
 * - completed (concluído)
 * - cancelled (cancelado)
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class OrderController extends Controller
{
    /**
     * Listar
     *
     * Lista todos os pedidos no sistema.
     *
     * ### Includes permitidos (include)
     * - user
     * - store
     * - sale
     * - items
     *
     * ### Ordenamentos permitidos (sort)
     * - id,
     * - code
     * - description
     * - status
     * - code-erp
     * - payment-method
     * - shipping
     * - created-at
     * - updated-at
     * - store-name
     * - user-name
     * - user-email
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListOrders $listOrders
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/orders.json')]
    #[QueryParam('filter[id]', 'integer', "Filtra pelo ID dos pedidos.<br>")]
    #[QueryParam('filter[code]', 'string', "Filtra pelo código do item na ANTARES.<br>", example: "1335")]
    #[QueryParam('filter[code-erp]', 'string', "Filtra pelo código ERP da Antares.<br>", example: "006543")]
    #[QueryParam('filter[status]', 'string', "Filtra pelo status dos pedidos.<br>", example: OrderStatus::COMPLETED)]
    #[QueryParam('filter[store-id]', 'integer', "Filtra pelo ID da loja vinculada ao pedido.<br>")]
    #[QueryParam('filter[user-id]', 'integer', "Filtra pelo ID do usuário vinculado ao pedido.<br>")]
    #[QueryParam('filter[payment-method]', 'string', "Filtra pelo método de pagamento.<br>")]
    #[QueryParam('filter[shipping]', 'string', "Filtra pelo tipo de frete.<br>")]
    #[QueryParam('filter[created-at]', 'string', "Filtra pela data de criação dos pedidos.<br>", example: "2023-06-21 12:30:10")]
    #[QueryParam('filter[updated-at]', 'string', "Filtra pela data de atualização dos pedidos.<br>", example: "2023-06-21 12:30:10")]
    #[QueryParam('filter[store-name]', 'string', "Filtra pelo nome da loja vinculada ao pedido.<br>", example: "Grande Loja")]
    #[QueryParam('filter[user-name]', 'string', "Filtra pelo nome do usuário vinculado ao pedido.<br>", example: "Juteloncio")]
    #[QueryParam('filter[user-email]', 'string', "Filtra pelo email do usuário vinculado ao pedido.<br>", example: "juteloncio@gmail.com")]
    #[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: "items,store")]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: "code,status")]
    public function index(Request $request, ListOrders $listOrders)
    {
        $orders = $listOrders->handle();

        $success = $orders->isEmpty() ? false : true;

        return OrderCollection::make($orders)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Adicionar
     *
     * Adiciona um novo pedido no sistema.
     *
     * @param \App\Http\Requests\Order\CreateOrderRequest $request
     * @param \App\Actions\Commands\Order\CreateOrder $createOrder
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    public function store(CreateOrderRequest $request, CreateOrder $createOrder)
    {
        $user = User::find($request->input('user_id'));

        $validated = $request->validated();

        $order = $createOrder->handle($validated, $user);

        return OrderResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('orders.show', ['order' => $order->id]),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Detalha um pedido no sistema.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Order\Order $order
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    #[UrlParam('id', 'string', "ID do pedido.", required: true, example: "1")]
    public function show(Request $request, Order $order)
    {
        return OrderDetailResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Atualizar
     *
     * Atualiza um pedido no sistema.
     *
     * @param \App\Http\Requests\Order\UpdateOrderRequest $request
     * @param \App\Models\Order\Order $order
     * @param \App\Actions\Commands\Order\UpdateOrder $updateOrder
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    #[UrlParam('id', 'string', "ID do pedido.", required: true, example: "1")]
    public function update(UpdateOrderRequest $request, Order $order, UpdateOrder $updateOrder)
    {
        $validated = $request->validated();

        $order = $updateOrder->handle($validated, $order);

        return OrderResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('orders.show', ['order' => $order->id]),
                ],
            ]);
    }

    /**
     * Copiar
     *
     * Copia um pedido no sistema.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Order\Order $order
     * @param \App\Actions\Commands\Order\CopyOrder $copyOrder
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    #[UrlParam('id', 'string', "ID do pedido.", required: true, example: "1")]
    public function copy(Request $request, Order $order, CopyOrder $copyOrder)
    {
        $orderCopy = $copyOrder->handle($order);

        return OrderResource::make($orderCopy)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('my.orders.show', ['order' => $orderCopy->id]),
                ],
            ]);
    }

    /**
     * Atualizar vários
     *
     * Atualiza vários pedidos no sistema.
     *
     * @param \App\Http\Requests\Order\UpdateManyOrderRequest $request
     * @param \App\Actions\Commands\Order\UpdateManyOrders $updateManyOrders
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/orders.json')]
    public function updateMany(UpdateManyOrderRequest $request, UpdateManyOrders $updateManyOrders)
    {
        $validated = $request->validated();

        $orders = $updateManyOrders->handle($validated);

        // Recupera os ids para montar o link de lista.
        $filterIds = $orders->pluck('id');

        return OrderCollection::make($orders)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'list' => route('orders.index', ['filter[id]' => $filterIds->toArray()]),
                ],
            ]);
    }
}
