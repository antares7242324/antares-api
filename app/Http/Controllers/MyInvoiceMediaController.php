<?php

namespace App\Http\Controllers;

use App\Traits\HttpResponses;
use Illuminate\Support\Carbon;
use App\Support\Media\MediaType;
use App\Actions\Commands\Media\CreateMedia;
use App\Http\Resources\Media\MediaResource;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Http\Requests\Media\CreateMediaRequest;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Meu Cadastro
 *
 * @subgroup Notas Fiscais
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class MyInvoiceMediaController extends Controller
{
    use HttpResponses;

    /**
     * Upload
     *
     * Faz o upload da nota fiscal para o sistema e retorna os dados do arquivo.
     *
     * Será necessário carregar os dados da nota fiscal depois do upload para calcular a caçada.
     *
     * ### Tipos de arquivo (type)
     * - nfe (nota fiscal)
     *
     * ### Tipos de visualização (storage)
     * - local
     * - public
     * - private
     *
     * @param \App\Http\Requests\Media\CreateMediaRequest $request
     * @param \App\Actions\Commands\Media\CreateMedia $createMedia
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoice-media.json')]
    #[ResponseField('name', 'string', "Nome do arquivo.")]
    #[ResponseField('extension', 'string', 'Extensão do arquivo.')]
    #[ResponseField('type', 'string', "Tipo do arquivo. Se for uma nota fiscal, o tipo é 'nfe', por exemplo.")]
    #[ResponseField('storage', 'string', "Tipo de local onde o arquivo é guardado. Se for público, será 'public', por exemplo.")]
    #[ResponseField('path', 'string', 'Path do local onde o arquivo está guardado.')]
    public function upload(CreateMediaRequest $request, CreateMedia $createMedia)
    {
        $validated = collect($request->validated());

        $validated->put('type', MediaType::NFE);

        $fileContents = file_get_contents($validated['file']->getRealPath());

        $xml = new \SimpleXMLElement($fileContents);

        $date = $xml->protNFe->infProt->dhRecbto ?? null;

        if ($date !== null) {
            $date = Carbon::parse((string) $date);

            if ($date->diffInDays(now()) > 90) {
                $errorData = collect()
                    ->put('url', $request->url())
                    ->put('request', $validated);

                // Retorna o erro junto com o payload enviado.
                return $this->error(
                    $errorData->toArray(),
                    "A data de recebimento da NFe (". $date->format('d/m/Y'). ") não pode ter sido emitida a mais de 90 dias.",
                    422,
                );
            }
        }

        $user = auth()->user();

        $media = $createMedia->handle($validated->toArray(), $user);

        $success = $media ? true : false;

        return MediaResource::make($media)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);

    }
}
