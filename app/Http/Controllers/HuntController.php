<?php

namespace App\Http\Controllers;

use App\Libraries\InvoiceStatus;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Actions\Queries\ListUserHunts;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Http\Resources\Hunt\UserHuntCollection;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Admin
 *
 * @subgroup Caçadas
 * @subgroupDescription
 * Endpoints para acessar e manipular os dados das caçadas.
 *
 * ### Status do usuário
 * - Ativo (active)
 * - Bloqueado (blocked)
 * - Inativo (inactive)
 *
 * ### Status da caçada
 * - Aprovada (approved)
 * - Pendente (pending)
 * - Rejeitada (rejected)
 */
#[ResponseFromFile('storage/responses/transaction/compensations.get.json')]
class HuntController extends Controller
{
    /**
     * Listar
     *
     * Lista as caçadas.
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - name
     * - status
     * - created
     * - last-updated
     * - balance
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListUserHunts $listUserHunts
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/hunt/hunts.json')]
    #[QueryParam('filter[id]', 'integer', "Filtra pelo ID do usuário.<br>", example: 2)]
    #[QueryParam('filter[name]', 'string', "Filtra pelo nome do usuário.<br>", example: 'Indiana')]
    #[QueryParam('filter[status]', 'string', 'Filtra pelo status do usuário.<br>', example: UserStatus::ACTIVE)]
    #[QueryParam('filter[created]', 'string', "Filtra pela data inicial no formato *y-m-d H:i:s*.<br>", "2023-01-01 00:00:00")]
    #[QueryParam('filter[last-updated]', 'string', "Filtra pela data de atualização no formato *y-m-d H:i:s*.<br>", "2023-01-01 00:00:00")]
    #[QueryParam('filter[balance]', 'integer', "Filtra pelo saldo da caçada em centavos.<br>", 100000)]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'name,-invoice-updated')]
    public function index(Request $request, ListUserHunts $listUserHunts)
    {
        $hunts = $listUserHunts->handle();

        return UserHuntCollection::make($hunts)
            ->additional([
                'success' => ! $hunts->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
