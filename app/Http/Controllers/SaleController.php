<?php

namespace App\Http\Controllers;

use App\Models\Sale\Sale;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use App\Actions\Queries\ListSales;
use Knuckles\Scribe\Attributes\Group;
use Knuckles\Scribe\Attributes\Subgroup;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Actions\Commands\Sale\CreateSale;
use App\Actions\Commands\Sale\UpdateSale;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Http\Resources\Sale\SaleCollection;
use App\Http\Requests\Sale\CreateSaleRequest;
use App\Http\Requests\Sale\UpdateSaleRequest;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Sale\SaleCompleteResource;

#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('name', 'string', 'Nome da promoção.')]
#[ResponseField('discount', 'string', 'Desconto no formato decimal.')]
#[ResponseField('startAt', 'string', 'Data de início da promoção. Exemplo: 2023-06-07 15:00:00')]
#[ResponseField('endAt', 'string', 'Data de término da promoção. Exemplo: 2023-06-07 15:00:00')]
#[ResponseField('trashed', 'string', 'Informa se a promoção foi removida.')]
#[ResponseField('users.name', 'string', 'Nome completo do usuário.')]
#[ResponseField('users.firstName', 'string', 'Nome do usuário.')]
class SaleController extends Controller
{
    use HttpResponses;

    /**
     * Listar
     *
     * Mostra todas as promoções com paginação.
     *
     * ### Ordenamentos permitidos (sort)
     * - name
     * - discount
     * - start-at
     * - end-at
     * - status (separa em ativas e removidas)
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListSales $listSales
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[Group("Promoções", "Endpoints das promoções.")]
    #[QueryParam("filter[name]", "string", "Filtra pelo nome da promoção.<br>", example: "TAB. GERAL")]
    #[QueryParam("filter[discount]", "string", "Filtra pelo valor do desconto em decimal.<br>", example: "0.15")]
    #[QueryParam("filter[start-at]", "string", "Filtra pela data de início da promoção.<br>", example: "2023-06-07 15:00:00")]
    #[QueryParam("filter[end-at]", "string", "Filtra pela data de conclusão da promoção.<br>", example: "2023-12-07 15:00:00")]
    #[QueryParam("filter[trashed]", "boolean", "Filtra se a promoção foi removida.<br>", example: '1')]
    #[QueryParam("sort", "string", "Ordena pelos campos informados.<br>", example: 'name,-discount')]
    #[ResponseFromFile('storage/responses/sale/sales.get.json')]
    public function index(Request $request, ListSales $listSales)
    {
        $sales = $listSales->handle();

        $success = $sales->isEmpty() ? false : true;

        return SaleCollection::make($sales)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Adicionar
     *
     * Adiciona uma nova promoção.
     *
     * @param  \App\Http\Requests\Sale\CreateSaleRequest  $request
     * @param  \App\Actions\Commands\Sale\CreateSale  $createSale
     * @return  \Illuminate\Http\Resources\Json\JsonResource
     */
    #[Group("Admin")]
    #[Subgroup("Promoções", "Endpoints das promoções.")]
    #[ResponseFromFile('storage/responses/sale/sales.get.json')]
    public function store(CreateSaleRequest $request, CreateSale $createSale)
    {
        $validated = $request->validated();

        $sale = $createSale->handle($validated);

        return SaleCompleteResource::make($sale)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('sales.show', ['sale' => $sale->id]),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Mostra informações sobre a promoção informada.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Sale\Sale $sale
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[Group("Promoções")]
    #[UrlParam('id', 'string', "ID da promoção.", required: true, example: "10")]
    #[ResponseFromFile('storage/responses/sale/sale.get.json')]
    public function show(Request $request, Sale $sale)
    {
        return SaleCompleteResource::make($sale)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Atualizar
     *
     * Atualiza a promoção informada.
     *
     * @param  \App\Http\Requests\Sale\UpdateSaleRequest  $request
     * @param  \App\Models\Sale\Sale  $sale
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[Group("Admin")]
    #[Subgroup("Promoções")]
    #[UrlParam('id', 'string', "ID da promoção.", required: true, example: "10")]
    #[ResponseFromFile('storage/responses/sale/sale.get.json')]
    public function update(UpdateSaleRequest $request, Sale $sale, UpdateSale $updateSale)
    {
        $validated = $request->validated();

        $sale = $updateSale->handle($sale, $validated);

        return SaleCompleteResource::make($sale)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('sales.show', ['sale' => $sale->id]),
                ],
            ]);
    }

    /**
     * Deletar
     *
     * Remove a promoção informada. Serão adicionadas atividades que informam
     * se a promoção foi encerrada caso ela já estivesse aberta.
     *
     * @param  \App\Models\Sale\Sale  $sale
     * @return \Illuminate\Http\JsonResponse
     */
    #[Group("Admin")]
    #[Subgroup("Promoções")]
    #[UrlParam('id', 'string', "ID da promoção.", required: true, example: "10")]
    public function destroy(Sale $sale)
    {
        $sale->delete();

        // Existe um observer que está processando as atividades
        // para o delete. @see App\Observers\SaleObserver

        return $this->success(null, 'Sale removed.');
    }
}
