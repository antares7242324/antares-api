<?php

namespace App\Http\Controllers\Antares;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Jobs\Portal\CheckEveryOrderStatusJob;
use App\Traits\HttpResponses;

class RetryUpdateOrderStatusController extends Controller
{
    use HttpResponses;

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        CheckEveryOrderStatusJob::dispatch();

        $message = "Despachado o job para atualizar o status de todos os orçamentos.";

        Log::channels('antares_erp')->info($message);

        return $this->success([], 'Tentando atualizar todos os pedidos');
    }
}
