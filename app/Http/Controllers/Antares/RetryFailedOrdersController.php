<?php

namespace App\Http\Controllers\Antares;

use App\Models\Order\Order;
use App\Traits\HttpResponses;
use App\Libraries\OrderStatus;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Jobs\Portal\ProcessOrderRequestJob;

class RetryFailedOrdersController extends Controller
{
    use HttpResponses;

    /**
     * Tentar reenviar todos os orçamentos com erro para o Antares ERP.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $orders = Order::where('status', OrderStatus::FAILED)->get();

        $retries = 0;

        $orders->each(function ($order) use (&$retries) {
            $order->update([
                'status' => OrderStatus::REQUESTED,
                'system_notes' => null,
            ]);

            $message = "Retry Failed Orders: Tentando fazer o reenvio do pedido ($order->id)";

            Log::channel('antares_erp')->info($message, [
                'order' => $order->toArray()
            ]);

            ProcessOrderRequestJob::dispatch($order->id);

            $retries += 1;
        });

        $response = collect()
            ->put('orders', $orders->count())
            ->put('retries', $retries);

        return $this->success($response->toArray(), 'Tentativa de pedidos reenviados');
    }
}
