<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product\Price;
use App\Actions\Queries\ListPriceItems;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Product\MyPriceItemResource;
use App\Http\Resources\Product\MyPriceItemCollection;
use Knuckles\Scribe\Attributes\UrlParam;

/**
 * @group Meu Cadastro
 *
 * @subgroup Produtos
 * @subgroupDescription
 * Endpoints para acessar os dados dos produtos
 * vinculados à lista de pedidos do usuário logado.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('amount', 'integer', 'Preço do item em centavos baseado na tabela de preços.')]
#[ResponseField('originalAmount', 'integer', 'Preço original de custo do item em centavos.')]
#[ResponseField('startAt', 'string', 'Data de vigência inicial do item na tabela de preços no formato *y-m-d*.')]
#[ResponseField('endAt', 'string', 'Data de vigência final do item na tabela de preços no formato *y-m-d*.')]
#[ResponseField('item.quantity', 'integer', 'Quantidade do mesmo item no pedido.')]
#[ResponseField('item.unitOriginalPrice', 'integer', 'Valor do item sem o desconto de caçada no formato em centavos.')]
#[ResponseField('item.unitDiscountPrice', 'integer', 'Valor do item com o desconto de caçada no formato em centavos.')]
#[ResponseField('item.subtotal', 'integer', 'Valor total do item no formato em centavos.')]
#[ResponseField('item.deliveryAt', 'integer', 'Data de entrega do item no formato *y-m-d h:m:s*.')]
class MyItemController extends Controller
{
    /**
     * Listar
     *
     * Lista todos os produtos vinculados ao usuário logado.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListPriceItems $listPriceItems
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/price-items.json')]
    #[QueryParam('filter[id]', 'string', "Filtra pelo ID dos produtos. Pode ser uma lista de IDs separados por vírgula.<br>", example: "1,2,3")]
    #[QueryParam('filter[start-at]', 'string', "Filtra pela data de vigência inicial do preço do item.<br>", example: "2023-02-10")]
    #[QueryParam('filter[end-at]', 'string', "Filtra pela data de vigência final do preço do item.<br>", example: "2023-12-10")]
    #[QueryParam('filter[active]', 'boolean', "Filtra pelos itens que estão ativos na tabela de preços.<br>", example: "1")]
    #[QueryParam('filter[price-start-at]', 'string', "Filtra pela data de vigência inicial da tabela de preços.<br>", example: "2023-12-10")]
    #[QueryParam('filter[price-end-at]', 'string', "Filtra pela data de vigência final da tabela de preços.<br>", example: "2023-12-10")]
    #[QueryParam('filter[item-name]', 'string', "Filtra pelo nome do item, pode buscar por uma parte.<br>", example: "Acoplamento Flexível")]
    #[QueryParam('filter[item-code]', 'string', "Filtra pelo código do item na ANTARES.<br>", example: "111333")]
    #[QueryParam('filter[item-hunt]', 'boolean', 'Filtra se o item pertence às caçadas.<br>', example: '1')]
    public function index(Request $request, ListPriceItems $listPriceItems)
    {
        $user = auth()->user();

        $priceItems = $listPriceItems->handle($user->priceList);

        $success = $priceItems->isEmpty() ? true : false;

        return MyPriceItemCollection::make($priceItems)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ]
            ]);
    }

    /**
     * Detalhar
     *
     * Detalha um produto vinculado ao usuário logado.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product\Price $price
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/item/price-item.json')]
    #[UrlParam('price_id', 'integer', "ID do item na tabela de preços.")]
    public function show(Request $request, Price $price)
    {
        return MyPriceItemResource::make($price)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}