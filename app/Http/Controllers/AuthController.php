<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Traits\HttpResponses;
use App\Libraries\TokenAbility;
use App\Http\Requests\Account\LoginRequest;
use App\Models\PersonalAccessToken;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthResource;
use App\Http\Requests\Account\PasswordRequest;
use App\Http\Middleware\PreventBlockedUser;
use App\Http\Middleware\PreventInactiveUser;
use Illuminate\Support\Carbon;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use Knuckles\Scribe\Attributes\Unauthenticated;

/**
 * @group Autenticação
 *
 * Endpoints para autenticação.
 *
 * Todos os endpoints que criarem algum token sempre adicionam um name,
 * que é usado para informar qual será o seu uso.
 *
 * - access: Usado para acessar a plataforma.
 * - activation: Usado para ativar o usuário.
 * - recovering: Usado para recuperar a conta do usuário.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class AuthController extends Controller
{
    use HttpResponses;

    /**
     * Deslogar um usuário
     *
     * Desloga um usuário da plataforma. Deleta todos os seus tokens existentes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    #[ResponseFromFile('storage/responses/auth/logout.json')]
    public function logout()
    {
        // Remove all tokens from user.
        auth()->user()->tokens()->delete();

        return $this->success(null, 'User logged out');
    }

    /**
     * Ativar um usuário
     *
     * Ativa um usuário enquanto ele não tenha logado na plataforma.
     * Espera receber um token com o name activation para ativá-lo e
     * permitir que ele possa logar posteriormente.
     *
     * @param \App\Http\Requests\Account\PasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    #[Unauthenticated]
    #[ResponseFromFile('storage/responses/auth/activate.json')]
    #[ResponseFromFile('storage/responses/auth/activate.forbidden.json', 403)]
    public function activate(PasswordRequest $request)
    {
        $token = PersonalAccessToken::findToken($request->token);

        if (! $token || ! $token->activation()) {
            return $this->error(null, 'Unauthorized access', 401);
        }

        $user = $token->tokenable;

        if (! $user || $user->status != UserStatus::INACTIVE) {
            return $this->error(null, 'The account must be inactive to be activated.', 403);
        }

        // Update User attributes.
        $user->setStatus(UserStatus::ACTIVE)
            ->setPassword($request->password)
            ->save();

        // Remove all tokens from user.
        $user->tokens()->delete();

        return $this->success(null, 'Account activated');
    }

    /**
     * Bloquear um usuário
     *
     * Bloqueia um usuário enquanto ele ainda não tenha logado na plataforma.
     * Espera receber um token com o name activation para bloqueá-lo e
     * impedir que tente logar na plataforma.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    #[Unauthenticated]
    #[BodyParam('token', 'string', 'O token enviado ao usuário para bloquear a conta.', true, "AO3eRF6rF1qDOYQ9Decd4bpY76W3iDuE8gieFkbL")]
    #[ResponseFromFile('storage/responses/auth/block.json')]
    #[ResponseFromFile('storage/responses/auth/block.forbidden.json', 403)]
    public function block(Request $request)
    {
        $token = PersonalAccessToken::findToken($request->token);

        if (! $token || ! $token->activation()) {
            return $this->error(null, 'Unauthorized access', 401);
        }

        $user = $token->tokenable;

        if (! $user || $user->status != UserStatus::INACTIVE) {
            return $this->error(null, 'The account must be inactive to be blocked.', 403);
        }

        // Update User attributes.
        $user->setStatus(UserStatus::BLOCKED)->save();

        // Remove all tokens from user.
        $user->tokens()->delete();

        return $this->success(null, 'Account blocked');
    }

    /**
     * Recuperar a conta de um usuário
     *
     * Recupera a conta de um usuário depois de logar na plataforma.
     * Espera receber um token com o name recovering para atualizar
     * a conta com a nova senha.
     *
     * @param \App\Http\Requests\Account\PasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    #[Unauthenticated]
    #[ResponseFromFile('storage/responses/auth/recover.json')]
    #[ResponseFromFile('storage/responses/auth/recover.forbidden.json', 403)]
    public function recover(PasswordRequest $request)
    {
        $token = PersonalAccessToken::findToken($request->token);

        if (! $token || ! $token->recovering()) {
            return $this->error(null, 'Unauthorized access', 401);
        }

        $user = $token->tokenable;

        if (! $user || $user->status != UserStatus::ACTIVE) {
            return $this->error(null, 'The account must be active to be recovered.', 403);
        }

        // Update User attributes.
        $user->setPassword($request->password)->save();

        // Remove all tokens from user.
        $user->tokens()->delete();

        return $this->success(null, 'Account recovered');
    }
}
