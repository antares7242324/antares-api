<?php

namespace App\Http\Controllers;

use App\Libraries\UserRole;
use App\Models\Order\Order;
use App\Libraries\UserStatus;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Actions\Commands\Order\SyncItemsToOrder;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Order\SyncMyOrderItemRequest;
use App\Http\Resources\Order\OrderSyncItemResource;

// /**
//  * @group Meu Cadastro
//  *
//  * @subGroup Pedidos
//  */
// #[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
// #[ResponseField('status', 'string', 'Situação atual do pedido baseado nos valores informados nesta seção.')]
// #[ResponseField('code', 'string', 'Código informado pelo usuário ao criar o pedido.')]
// #[ResponseField('description', 'string', 'Descrição do pedido.')]
// #[ResponseField('paymentMethod', 'string', 'Método de pagamento baseado nos valores informados nesta seção.')]
// #[ResponseField('user.status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
// #[ResponseField('user.role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
// #[ResponseField('user.lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
// #[ResponseField('user.name', 'string', 'Nome do usuário.')]
// #[ResponseField('user.firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
// #[ResponseField('user.availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
// #[ResponseField('user.discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
// #[ResponseField('user.discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
// #[ResponseField('user.discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
// #[ResponseField('stores.name', 'string', 'Nome da loja.')]
// #[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
// #[ResponseField('items.quantity', 'integer', 'Quantidade do mesmo item no pedido.')]
// #[ResponseField('items.unitOriginalPrice', 'integer', 'Valor do item sem o desconto de caçada no formato em centavos.')]
// #[ResponseField('items.unitDiscountPrice', 'integer', 'Valor do item com o desconto de caçada no formato em centavos.')]
// #[ResponseField('items.subtotal', 'integer', 'Valor total do item no formato em centavos.')]
// #[ResponseField('items.deliveryAt', 'integer', 'Data de entrega do item no formato *y-m-d h:m:s*.')]
class MyOrderItemController extends Controller
{
    // /**
    //  * Sincronizar produtos
    //  *
    //  * Atualiza todos os itens dentro do pedido. Precisa sempre enviar todos os itens, mesmo que não tenha nenhuma alteração em algum item específico.
    //  * Pode enviar itens com o mesmo ID para caso seus valores ou descontos sejam diferentes.
    //  *
    //  * @param \App\Http\Requests\Order\SyncMyOrderItemRequest $request
    //  * @param \App\Models\Order\Order $order
    //  * @param \App\Actions\Commands\Order\SyncItemsToOrder $syncItemsToOrder
    //  * @return \Illuminate\Http\Resources\Json\JsonResource
    //  */
    // #[ResponseFromFile('storage/responses/order/order.json')]
    // #[UrlParam('order_id', 'integer', "ID do pedido.")]
    // public function sync(SyncMyOrderItemRequest $request, Order $order, SyncItemsToOrder $syncItemsToOrder)
    // {
    //     $validated = $request->validated();

    //     $order = $syncItemsToOrder->handle($validated, $order);

    //     return OrderSyncItemResource::make($order)
    //         ->additional([
    //             'success' => true,
    //             'links' => [
    //                 'self' => $request->url(),
    //                 'show' => route('my.orders.show', ['order' => $order->id]),
    //             ],
    //         ]);
    // }
}
