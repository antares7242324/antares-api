<?php

namespace App\Http\Controllers\Tests;

use App\Actions\Commands\Hunt\CancelDebitFromOrder;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\OrderStatus;

class CancelOrderTestController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $orderId = $request->input('order_id');

        $order = Order::find($orderId);

        $order->update(['status' => OrderStatus::CANCELLED]);

        $result = (new CancelDebitFromOrder)->handle($order);

        dd($result);
    }
}
