<?php

namespace App\Http\Controllers\Tests;

use App\Libraries\Shipping;
use App\Models\Order\Order;
use App\Models\Account\User;
use Illuminate\Http\Request;
use App\Models\Product\Price;
use App\Libraries\OrderStatus;
use Illuminate\Support\Carbon;
use App\Libraries\PaymentMethod;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CreateOrderTestController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = User::find(3);

        $store = $user->activeStore();

        $priceList = $user->priceList;

        $discount = 0.1;

        $itemPrice = Price::query()
            ->where('item_id', 1)
            ->where('price_list_id', $priceList->id)
            ->first();

        $itemDiscount = $itemPrice->amount * $discount;

        $itemDiscountedAmount = $itemPrice->amount - $itemDiscount;

        $itemSubtotal = $itemDiscountedAmount * 1;

        $balance = $user->available_balance;

        $params = collect([
            'user_id' => $user->id,
            'store_id' => $store->id,
            'price_list_id' => $priceList->id,
            'description' => 'Teste de criação de pedido',
            'status' => OrderStatus::SENT,
            'payment_method' => PaymentMethod::DAYS35,
            'used_balance' => $itemSubtotal,
            'previous_balance' => $balance,
            'current_discount' => $discount,
            'shipping' => Shipping::CIF,
            'issuance_at' => Carbon::now(),
        ]);

        DB::beginTransaction();

        try {
            $order = Order::create($params->toArray());

            $orderItem = $order->items()->create([
                'item_id' => $itemPrice->item_id,
                'price_id' => $itemPrice->id,
                'quantity' => 1,
                'unit_original_price' => $itemPrice->amount,
                'unit_discount_price' => $itemDiscountedAmount,
                'subtotal' => $itemSubtotal,
                'discount' => $discount,
                'debit' => 0,
                'delivery_at' => Carbon::now()->addDays(30),
            ]);

            $debit = $order->debitTransaction()->create([
                'user_id' => $user->id,
                'amount' => $itemSubtotal,
                'current_balance' => $balance,
            ]);

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();

            dd($e->getMessage());
        }

        dd($order->id, $orderItem->id, $debit->id);
    }
}
