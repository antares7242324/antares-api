<?php

namespace App\Http\Controllers\Tests;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Jobs\Portal\CheckEveryOrderStatusJob;

class CheckEveryOrderStatusController extends Controller
{
    public function __invoke()
    {
        Log::info("Inicia a verificação do status do pedido");

        CheckEveryOrderStatusJob::dispatch();

        return response()->json([
            'message' => 'Operação realizada com sucesso.'
        ]);
    }
}