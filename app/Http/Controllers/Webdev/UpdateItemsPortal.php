<?php

namespace App\Http\Controllers\Webdev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\Portal\CheckUpdateEveryItemJob;

class UpdateItemsPortal extends Controller
{
    public function __invoke(Request $request)
    {
        $updatedAt = $request->get('updated');

        $searchkey = $request->get('searchkey');

        $pageSize = $request->get('pagesize', 300);

        CheckUpdateEveryItemJob::dispatch($updatedAt, $searchkey, $pageSize);

        dd("despachado");
    }
}