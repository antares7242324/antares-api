<?php

namespace App\Http\Controllers\Webdev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowConfigController extends Controller
{
    public function __invoke(Request $request)
    {
        $erpConnection = collect([
            'url' => config('erp.url'),
            'username' => config('erp.user'),
            'password' => config('erp.password'),
        ]);

        return response()->json([
            'erp' => $erpConnection,
        ]);
    }
}
