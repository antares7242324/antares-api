<?php

namespace App\Http\Controllers\Webdev;

use App\Models\Sale\Sale;
use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use Illuminate\Http\Request;
use App\Libraries\OrderStatus;
use Illuminate\Support\Carbon;
use App\Libraries\TokenAbility;
use App\Libraries\InvoiceStatus;
use App\Http\Controllers\Controller;
use App\Actions\Commands\Faker\MakeFakeUser;
use App\Actions\Commands\Discount\ListExtraDiscounts;
use App\Support\Hunt\HuntDiscountBar;

class MailController extends Controller
{
    /**
     * Visualiza o email dos pedidos de um usuário.
     *
     * @param string $id O id do usuário. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @param MakeFakeUser $makeFakeUser Uma instância da classe MakeFakeUser.
     * @return \Illuminate\View\View A visualização do email.
     */

    public function myorders(string $id = null, Request $request, MakeFakeUser $makeFakeUser)
    {
        $user = is_null($id)
            ? $makeFakeUser->handle()
            : User::find($id);

        return view('mails.order-export', compact('user'));
    }

    /**
     * Visualiza o email com o pedido requisitado.
     *
     * @param string $id O id do pedido. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function orderRequested(Request $request, string $id = null)
    {
        $order = is_null($id)
            ? Order::first()
            : Order::find($id);

        $order->status = OrderStatus::REQUESTED;

        $user = $order->user;

        $items = $order->items;

        $discounts = new HuntDiscountBar;

        $order->store = $order->store()->withTrashed()->first();

        return view(
            'mails.order-requested',
            compact('order', 'user', 'items', 'discounts')
        );
    }

    /**
     * Visualiza o email com o pedido encaminhado.
     *
     * @param string $id O id do pedido. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function orderSent(string $id = null, Request $request)
    {
        $order = is_null($id)
            ? Order::first()
            : Order::find($id);

        $order->status = OrderStatus::SENT;

        $user = $order->user;

        $items = $order->items;

        $discounts = new HuntDiscountBar;

        return view(
            'mails.order-sent',
            compact('order', 'user', 'items', 'discounts')
        );
    }

    /**
     * Visualiza o email com o pedido completado.
     *
     * @param string $id O id do pedido. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function orderCompleted(Request $request, $id = null)
    {
        $order = is_null($id)
            ? Order::first()
            : Order::find($id);

        $order->status = OrderStatus::COMPLETED;

        $user = $order->user;

        $items = $order->items;

        $discounts = new HuntDiscountBar;

        return view(
            'mails.order-completed',
            compact('order', 'user', 'items', 'discounts')
        );
    }

    /**
     * Visualiza o email com o pedido cancelado.
     *
     * @param string $id O id do pedido. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function orderCancelled(string $id = null, Request $request)
    {
        $order = is_null($id)
            ? Order::first()
            : Order::find($id);

        $order->status = OrderStatus::CANCELLED;

        $user = $order->user;

        $items = $order->items;

        $discounts = new HuntDiscountBar;

        return view(
            'mails.order-cancelled',
            compact('order', 'user', 'items', 'discounts')
        );
    }

    /**
     * Visualiza o email de convite para um usuário.
     *
     * @param string $id O id do usuário. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function accountInvite(string $id = null, Request $request)
    {
        $user = is_null($id)
            ? User::where('role', 'client')->first()
            : User::find($id);

        $token = $user->createToken(
            'activation',
            [TokenAbility::ACTIVATE],
            Carbon::now()->addHours(2)
        );

        return view('mails.account-invite', compact('user', 'token'));
    }

    /**
     * Visualiza o email de recuperação de senha para um usuário.
     *
     * @param string $id O id do usuário. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function accountRecover(string $id = null, Request $request)
    {
        $user = is_null($id)
            ? User::where('role', 'client')->first()
            : User::find($id);

        $token = $user->createToken(
            'recovering',
            [TokenAbility::RECOVER],
            Carbon::now()->addHours(2)
        );

        return view('mails.account-recover', compact('user', 'token'));
    }

    /**
     * Visualiza o email de confirmação de promoção.
     *
     * @param string $id O id do usuário. NULL se não for fornecido.
     * @param Request $request O objeto de requisição.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function sale(string $id = null, Request $request)
    {
        $sale = is_null($id)
            ? Sale::first()
            : Sale::findOrFail($id);

        $user = $sale->users->first();

        $discountList = new ListExtraDiscounts;

        $discounts = new HuntDiscountBar;

        return view(
            'mails.sale-confirmation',
            compact('user', 'sale', 'discountList', 'discounts')
        );
    }

    /**
     * Visualiza o email das caçadas no sistema.
     *
     * @param string $id O id do usuário. NULL se não for fornecido.
     * @param Request $request O objeto de solicitação.
     * @param MakeFakeUser $makeFakeUser O objeto MakeFakeUser.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function invoices(string $id = null, Request $request, MakeFakeUser $makeFakeUser)
    {
        $user = is_null($id)
            ? $makeFakeUser->handle()
            : Invoice::find($id)->user;

        return view('mails.invoice-export', compact('user'));
    }

    /**
     * Visualiza o email das notas fiscais confirmadas.
     *
     * @param string $id O id da nota fiscal. NULL se não for fornecido.
     * @param Request $request O objeto de solicitação.
     * @return \Illuminate\View\View A visualização do email.
     */
    public function invoiceConfirmation(string $id = null, Request $request)
    {
        $invoice = is_null($id)
            ? Invoice::where('status', OrderStatus::COMPLETED)->firstOrFail()
            : Invoice::findOrFail($id);

        $invoice->status = InvoiceStatus::APPROVED;

        $items = $invoice->items;

        $user = $invoice->user;

        $discounts = new HuntDiscountBar;

        return view(
            'mails.invoice-confirmation',
            compact('invoice', 'user', 'items', 'discounts')
        );
    }

    public function invoiceRejected(string $id = null, Request $request)
    {
        $invoice = is_null($id)
            ? Invoice::where('status', InvoiceStatus::REJECTED)->firstOrFail()
            : Invoice::findOrFail($id);

        return view(
            'mails.invoice-rejected',
            compact('invoice')
        );
    }
}
