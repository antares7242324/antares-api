<?php

namespace App\Http\Controllers\Webdev;

use App\Models\Order\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function test(Request $request)
    {
        $order = Order::find(73);

        $items = $order->items;

        foreach ($items as $item) {
            dd($item->item->code);
        }
    }
}
