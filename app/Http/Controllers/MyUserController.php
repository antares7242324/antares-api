<?php

namespace App\Http\Controllers;

use App\Libraries\UserRole;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Actions\Queries\ShowMyUser;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Account\UserSingleResource;
use App\Http\Resources\Account\UserBigNumbersResource;

/**
 * @group Meu Cadastro
 *
 * Endpoints vinculados com o usuário atualmente logado. Todos esses
 * endpoints sempre serão executados a partir de alguma ação ativada
 * pelo usuário enquanto navega pela plataforma.
 *
 * @subgroup Usuário
 * @subgroupDescription Endpoints relacionados aos dados do usuário.
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class MyUserController extends Controller
{
    /**
     * Detalhar o próprio cadastro
     *
     * Mostra os dados do usuário autenticado.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ShowMyUser $showMyUser
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/user/user.get.json')]
    #[ResponseField('status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
    #[ResponseField('role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
    #[ResponseField('lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
    #[ResponseField('name', 'string', 'Nome do usuário.')]
    #[ResponseField('firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
    #[ResponseField('availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
    #[ResponseField('discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
    #[ResponseField('discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
    #[ResponseField('discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
    #[ResponseField('stores.name', 'string', 'Nome da loja.')]
    #[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
    public function show(Request $request, ShowMyUser $showMyUser)
    {
        $user = $showMyUser->handle();

        $success = $user ? true : false;

        return UserSingleResource::make($user)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ]
            ]);
    }

    /**
     * Big Numbers
     *
     * Mostra os dados de big numbers do usuário autenticado.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ShowMyUser $showMyUser
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/user/bignumbers.json')]
    public function bigNumbers(Request $request, ShowMyUser $showMyUser)
    {
        $user = $showMyUser->handle();

        $success = $user ? true : false;

        return UserBigNumbersResource::make($user)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ]
            ]);
    }
}
