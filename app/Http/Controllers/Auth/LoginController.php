<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Carbon;
use App\Libraries\TokenAbility;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthResource;
use App\Http\Middleware\PreventBlockedUser;
use App\Http\Requests\Account\LoginRequest;
use App\Http\Middleware\PreventInactiveUser;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\Unauthenticated;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Autenticação
 *
 * Endpoints para autenticação.
 *
 * Todos os endpoints que criarem algum token sempre adicionam um name,
 * que é usado para informar qual será o seu uso.
 *
 * - **access** (Usado para acessar a plataforma)
 * - **activation** (Usado para ativar o usuário)
 * - **recovering** (Usado para recuperar a conta do usuário)
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
class LoginController extends Controller
{
    /**
     * Logar um usuário
     *
     * Cria um token usado para login do usuário. Um novo token é criado toda
     * a vez que usar este endpoint. Por padrão esse tipo de token nunca expira.
     *
     * @param \App\Http\Requests\Account\LoginRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource|\Illuminate\Http\JsonResponse
     */
    #[Unauthenticated]
    #[ResponseFromFile('storage/responses/auth/login.json')]
    #[ResponseField('access', 'string', 'O tipo de acesso desse login. Poder ser client ou admin.')]
    #[ResponseField('name', 'string', 'Informa o tipo do token conforme a sua função.')]
    #[ResponseField('abilities', 'array', 'Um array com as permissões e habilidades do token.')]
    #[ResponseField('expiresAt', 'string', 'Informa a data de expiração do token. Será null se ele não expira.')]
    public function __invoke(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (auth()->attempt($credentials) === false) {
            return $this->error(null, 'Invalid Credentials', 401);
        }

        $this->middleware(PreventBlockedUser::class);

        $this->middleware(PreventInactiveUser::class);

        $user = auth()->user();

        // Cria o token de acesso.
        $token = $user->createToken('access', [TokenAbility::ACCESS]);

        // Coloca a data do último login.
        $user->forceFill([
            'last_login_at' => Carbon::now(),
        ])->save();

        // Monta a resposta.
        $resource = collect([
            'user' => $user,
            'token' => $token
        ]);

        return AuthResource::make($resource)->additional(['success' => true]);
    }
}
