<?php

namespace App\Http\Controllers;

use App\Jobs\Mail\MailOrdersExportToUserJob;
use App\Libraries\Shipping;
use App\Libraries\UserRole;
use App\Models\Order\Order;
use App\Traits\HttpResponses;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Libraries\PaymentMethod;
use App\Exports\OrdersManyExport;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Actions\Commands\Order\CopyOrder;
use App\Http\Resources\Order\OrderCollection;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Actions\Commands\Order\UpdateManyOrders;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Order\ExportMyOrderRequest;
use App\Http\Resources\Order\OrderDetailResource;
use App\Http\Requests\Order\UpdateMyManyOrderRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * @group Meu Cadastro
 *
 * @subgroup Pedidos
 * @subgroupDescription
 * Endpoints para acessar e manipular dados relacionados aos pedidos do usuário.
 *
 * ### Situação (status)
 * - draft (rascunho)
 * - requested (solicitado)
 * - sent (enviado)
 * - completed (concluído)
 * - cancelled (cancelado)
 *
 * ### Método de pagamento (payment_method)
 * - days35 (35 dias)
 * - bnds (BNDS)
 * - prepaid (Antecipado)
 *
 * ### Tipos de frete (shipping)
 * - CIF
 * - FOB
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual do pedido baseado nos valores informados nesta seção.')]
#[ResponseField('code', 'string', 'Código informado pelo usuário ao criar o pedido.')]
#[ResponseField('codeErp', 'string', 'Código do pedido no ERP da Antares.')]
#[ResponseField('description', 'string', 'Descrição do pedido.')]
#[ResponseField('paymentMethod', 'string', 'Método de pagamento baseado nos valores informados nesta seção. Poder ser *'.PaymentMethod::DAYS35.'*, *'.PaymentMethod::BNDS.'* ou *'.PaymentMethod::PREPAID.'*')]
#[ResponseField('shipping', 'string', 'Tipo de frete. Pode ser *'.Shipping::CIF.'* ou *'.Shipping::FOB.'*.')]
#[ResponseField('user.status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('user.role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('user.lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('user.name', 'string', 'Nome do usuário.')]
#[ResponseField('user.firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('user.availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
#[ResponseField('user.discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
#[ResponseField('user.discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('user.discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
#[ResponseField('stores.name', 'string', 'Nome da loja.')]
#[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
#[ResponseField('items.quantity', 'integer', 'Quantidade do mesmo item no pedido.')]
#[ResponseField('items.unitOriginalPrice', 'integer', 'Valor do item sem o desconto de caçada no formato em centavos.')]
#[ResponseField('items.unitDiscountPrice', 'integer', 'Valor do item com o desconto de caçada no formato em centavos.')]
#[ResponseField('items.subtotal', 'integer', 'Valor total do item no formato em centavos.')]
#[ResponseField('items.deliveryAt', 'string', 'Data de entrega do item no formato *y-m-d*.')]
#[ResponseField('items.item.name', 'string', 'Nome do item.')]
#[ResponseField('items.item.code', 'string', 'Cõdigo do item pela ANTARES.')]
#[ResponseField('items.item.hunt', 'boolean', 'Informa se o item pertence às caçadas.')]
#[ResponseField('items.price.amount', 'integer', 'Preço do item em centavos baseado na tabela de preços.')]
#[ResponseField('items.price.originalAmount', 'integer', 'Preço original de custo do item em centavos.')]
#[ResponseField('items.price.startAt', 'string', 'Data de vigência inicial do item no formato *y-m-d*.')]
#[ResponseField('items.price.endAt', 'string', 'Data de vigência final do item no formato *y-m-d*.')]
class MyOrderController extends Controller
{
    use HttpResponses;


    /**
     * Detalhar
     *
     * Detalha um pedido informado do usuário logado.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Order\Order $order
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    #[UrlParam('id', 'integer', "ID do pedido.")]
    public function show(Request $request, Order $order)
    {
        // Adiciona o usuário para evitar que ele acesse os pedidos
        // dos outros usuários na plataforma.
        $user = auth()->user();

        if ($order->user_id == $user->id) new ModelNotFoundException;

        return OrderDetailResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Copiar
     *
     * Copia um pedido informado do usuário logado.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Order\Order $order
     * @param \App\Actions\Commands\Order\CopyOrder $copyOrder
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    #[UrlParam('order_id', 'integer', "ID do pedido.")]
    public function copy(Request $request, Order $order, CopyOrder $copyOrder)
    {
        $orderCopy = $copyOrder->handle($order);

        return OrderDetailResource::make($orderCopy)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('my.orders.show', ['order' => $orderCopy->id]),
                ],
            ]);
    }

    /**
     * Atualizar Vários
     *
     * Atualiza os pedidos informados do usuário logado.
     *
     * @param \App\Http\Requests\Order\UpdateMyManyOrderRequest $request
     * @param \App\Actions\Commands\Order\UpdateManyOrders $updateManyOrders
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/orders.json')]
    public function updateMany(UpdateMyManyOrderRequest $request, UpdateManyOrders $updateManyOrders)
    {
        $validated = $request->validated();

        $orders = $updateManyOrders->handle($validated);

        // Recupera os ids para montar o link de lista.
        $filterIds = $orders->pluck('id');

        return OrderCollection::make($orders)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'list' => route('my.orders.index', ['filter[id]' => $filterIds->toArray()]),
                ],
            ]);
    }

    /**
     * Exportar Vários
     *
     * Exporta os pedidos informados.
     *
     * @param \App\Http\Requests\Order\ExportMyOrderRequest $request
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order-export-success.json')]
    public function exportMany(ExportMyOrderRequest $request)
    {
        $ids = $request->get('orders');

        $filename = 'pedidos_' . uniqid() . '.csv';

        (new OrdersManyExport($ids))->queue($filename, 'public')
            ->chain([
                new MailOrdersExportToUserJob(auth()->user()->id, $filename),
            ]);

        return $this->success(null, 'Pedidos exportados para o email.');
    }
}
