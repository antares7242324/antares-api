<?php

namespace App\Http\Controllers\MyOrders;

use App\Libraries\Shipping;
use App\Libraries\UserRole;
use Illuminate\Http\Request;
use App\Libraries\UserStatus;
use App\Traits\HttpResponses;
use App\Libraries\OrderStatus;
use App\Libraries\PaymentMethod;
use App\Actions\Queries\ListOrders;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\QueryParam;
use App\Http\Resources\Order\OrderCollection;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @group Meu Cadastro
 *
 * @subgroup Pedidos
 * @subgroupDescription
 * Endpoints para acessar e manipular dados relacionados aos pedidos do usuário.
 *
 * ### Situação (status)
 * - draft (rascunho)
 * - requested (solicitado)
 * - sent (enviado)
 * - completed (concluído)
 * - cancelled (cancelado)
 *
 * ### Método de pagamento (payment_method)
 * - days35 (35 dias)
 * - bnds (BNDS)
 * - prepaid (Antecipado)
 *
 * ### Tipos de frete (shipping)
 * - CIF
 * - FOB
 */
#[ResponseFromFile('storage/responses/order/orders.json')]
#[QueryParam('filter[id]', 'integer', "Filtra pelo ID dos pedidos.<br>")]
#[QueryParam('filter[code]', 'string', "Filtra pelo código do item na ANTARES.<br>", example: "1335")]
#[QueryParam('filter[code-erp]', 'string', "Filtra pelo código ERP da Antares.<br>", example: "006543")]
#[QueryParam('filter[status]', 'string', "Filtra pelo status dos pedidos.<br>", example: OrderStatus::COMPLETED)]
#[QueryParam('filter[store-id]', 'integer', "Filtra pelo ID da loja vinculada ao pedido.<br>")]
#[QueryParam('filter[user-id]', 'integer', "Filtra pelo ID do usuário vinculado ao pedido.<br>")]
#[QueryParam('filter[payment-method]', 'string', "Filtra pelo método de pagamento.<br>", example: "prepaid")]
#[QueryParam('filter[shipping]', 'string', "Filtra pelo tipo de frete.<br>", example: "FOB")]
#[QueryParam('filter[created-at]', 'string', "Filtra pela data de criação dos pedidos.<br>", example: "2023-06-21 12:30:10")]
#[QueryParam('filter[updated-at]', 'string', "Filtra pela data de atualização dos pedidos.<br>", example: "2023-06-21 12:30:10")]
#[QueryParam('filter[store-name]', 'string', "Filtra pelo nome da loja vinculada ao pedido.<br>", example: "Grande Loja")]
#[QueryParam('filter[user-name]', 'string', "Filtra pelo nome do usuário vinculado ao pedido.<br>", example: "Juteloncio")]
#[QueryParam('filter[user-email]', 'string', "Filtra pelo email do usuário vinculado ao pedido.<br>", example: "juteloncio@gmail.com")]
#[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: "items,store")]
#[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: "code,status")]

#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual do pedido baseado nos valores informados nesta seção.')]
#[ResponseField('code', 'string', 'Código informado pelo usuário ao criar o pedido.')]
#[ResponseField('codeErp', 'string', 'Código do pedido no ERP da Antares.')]
#[ResponseField('description', 'string', 'Descrição do pedido.')]
#[ResponseField('currentDiscount', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('paymentMethod', 'string', 'Método de pagamento baseado nos valores informados nesta seção. Poder ser *'.PaymentMethod::DAYS35.'*, *'.PaymentMethod::BNDS.'* ou *'.PaymentMethod::PREPAID.'*')]
#[ResponseField('shipping', 'string', 'Tipo de frete. Pode ser *'.Shipping::CIF.'* ou *'.Shipping::FOB.'*.')]
#[ResponseField('systemNotes', 'string', 'Notificação do sistema.')]
#[ResponseField('issuanceAt', 'datetime', 'Data de emissão do pedido.')]
#[ResponseField('createdAt', 'datetime', 'Data de criação do pedido.')]
#[ResponseField('updatedAt', 'datetime', 'Data de atualização do pedido.')]
#[ResponseField('user.status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('user.role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('user.lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('user.name', 'string', 'Nome do usuário.')]
#[ResponseField('user.firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('user.availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
#[ResponseField('user.discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
#[ResponseField('user.discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('user.discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
#[ResponseField('stores.name', 'string', 'Nome da loja.')]
#[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
#[ResponseField('items.quantity', 'integer', 'Quantidade do mesmo item no pedido.')]
#[ResponseField('items.unitOriginalPrice', 'integer', 'Valor do item sem o desconto de caçada no formato em centavos.')]
#[ResponseField('items.unitDiscountPrice', 'integer', 'Valor do item com o desconto de caçada no formato em centavos.')]
#[ResponseField('items.subtotal', 'integer', 'Valor total do item no formato em centavos.')]
#[ResponseField('items.deliveryAt', 'string', 'Data de entrega do item no formato *y-m-d*.')]
#[ResponseField('items.item.name', 'string', 'Nome do item.')]
#[ResponseField('items.item.code', 'string', 'Cõdigo do item pela ANTARES.')]
#[ResponseField('items.item.hunt', 'boolean', 'Informa se o item pertence às caçadas.')]
#[ResponseField('items.price.amount', 'integer', 'Preço do item em centavos baseado na tabela de preços.')]
#[ResponseField('items.price.originalAmount', 'integer', 'Preço original de custo do item em centavos.')]
#[ResponseField('items.price.startAt', 'string', 'Data de vigência inicial do item no formato *y-m-d*.')]
#[ResponseField('items.price.endAt', 'string', 'Data de vigência final do item no formato *y-m-d*.')]
class ListMyOrdersController extends Controller
{
    use HttpResponses;

    /**
     * Listar
     *
     * Lista todos os pedidos do usuário logado.
     *
     * ### Inclusões permitidas (include)
     * - user
     * - store
     * - items
     * - debit
     *
     * ### Ordenamentos permitidos (sort)
     * - id,
     * - code
     * - code-erp
     * - status
     * - payment-method
     * - shipping
     * - created-at
     * - updated-at
     * - store-name
     * - user-name
     * - user-email
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListOrders $listOrders
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function __invoke(Request $request, ListOrders $listOrders)
    {
        // Adiciona o usuário para evitar que ele acesse os pedidos
        // dos outros usuários na plataforma.
        $user = auth()->user();

        $orders = $listOrders->handle($user);

        return OrderCollection::make($orders)
            ->additional([
                'success' => ! $orders->isEmpty(),
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
