<?php

namespace App\Http\Controllers\MyOrders;

use App\Libraries\UserRole;
use App\Models\Order\Order;
use App\Libraries\UserStatus;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Actions\Commands\Order\SyncItemsToOrder;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Order\SyncMyOrderItemRequest;
use App\Http\Resources\Order\OrderSyncItemResource;

/**
 * @group Meu Cadastro
 *
 * @subGroup Pedidos
 */
#[UrlParam('order_id', 'integer', "ID do pedido.")]
#[ResponseFromFile('storage/responses/order/order.json')]
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual do pedido baseado nos valores informados nesta seção.')]
#[ResponseField('code', 'string', 'Código informado pelo usuário ao criar o pedido.')]
#[ResponseField('description', 'string', 'Descrição do pedido.')]
#[ResponseField('paymentMethod', 'string', 'Método de pagamento baseado nos valores informados nesta seção.')]
#[ResponseField('user.status', 'string', 'Situação atual do usuário. Pode ser *'.UserStatus::ACTIVE.'*, *'.UserStatus::INACTIVE.'* ou *'.UserStatus::BLOCKED.'*.')]
#[ResponseField('user.role', 'string', 'Tipo do papel do usuário. Pode ser *'.UserRole::ADMIN.'*, *'.UserRole::CLIENT.'* ou *'.UserRole::MEEG.'*.')]
#[ResponseField('user.lastLoginAt', 'string', 'Data do último login feito pelo usuário no formato *y-m-d h:m:s*.')]
#[ResponseField('user.name', 'string', 'Nome do usuário.')]
#[ResponseField('user.firstName', 'string', 'Primeiro nome do usuário. Mostra apenas a primeira palavra do campo *name*.')]
#[ResponseField('user.availableBalance', 'integer', 'Saldo disponível do usuário no formato em centavos. Exemplo: 30000 é R$ 300,00')]
#[ResponseField('user.discounts.main', 'string', 'Desconto principal definido pela plataforma Antares no formato decimal.')]
#[ResponseField('user.discounts.current', 'string', 'Desconto de caçada configurado pelo administrador para o usuário no formato decimal.')]
#[ResponseField('user.discounts.average', 'string', 'Desconto médio da caçada no formato decimal.')]
#[ResponseField('stores.name', 'string', 'Nome da loja.')]
#[ResponseField('stores.cnpj', 'string', 'Número do CNPJ da loja sem formatação.')]
#[ResponseField('items.quantity', 'integer', 'Quantidade do mesmo item no pedido.')]
#[ResponseField('items.unitOriginalPrice', 'integer', 'Valor do item sem o desconto de caçada no formato em centavos.')]
#[ResponseField('items.unitDiscountPrice', 'integer', 'Valor do item com o desconto de caçada no formato em centavos.')]
#[ResponseField('items.subtotal', 'integer', 'Valor total do item no formato em centavos.')]
#[ResponseField('items.deliveryAt', 'integer', 'Data de entrega do item no formato *y-m-d h:m:s*.')]
class SyncMyOrderItemsController extends Controller
{
    /**
     * Sincronizar produtos
     *
     * Atualiza todos os itens dentro do pedido. Precisa sempre enviar todos os itens, mesmo que não tenha nenhuma alteração em algum item específico.
     * Pode enviar itens com o mesmo ID para caso seus valores ou descontos sejam diferentes.
     *
     * @param \App\Models\Order\Order $order
     * @param \App\Http\Requests\Order\SyncMyOrderItemRequest $request
     * @param \App\Actions\Commands\Order\SyncItemsToOrder $syncItemsToOrder
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function __invoke(Order $order, SyncMyOrderItemRequest $request, SyncItemsToOrder $syncItemsToOrder)
    {
        $validated = collect($request->validated());

        // if (!$validated->has('no_items') && !$validated->has('items')) {
        //     return response()->json([
        //         'message' => 'At least either items field or no_items field are required.',
        //         'errors' => [
        //             'items' => [
        //                 'The items field is required.',
        //             ],
        //             'no_items' => [
        //                 'The no_items field is required.',
        //             ]
        //         ],
        //     ], 400);
        // }

        $order = $syncItemsToOrder->handle($validated, $order);

        Log::info("Sync My Order Items Controller", [
            'order' => $order->toArray(),
            'items' => collect($validated->get('items', []))->toArray(),
        ]);

        return OrderSyncItemResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('my.orders.show', ['order' => $order->id]),
                ],
            ]);
    }
}
