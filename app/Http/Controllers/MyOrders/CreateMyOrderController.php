<?php

namespace App\Http\Controllers\MyOrders;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Actions\Commands\Order\CreateOrder;
use App\Http\Resources\Order\OrderResource;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Order\CreateMyOrderRequest;

/**
 * @group Meu Cadastro
 * @subgroup Pedidos
 */
class CreateMyOrderController extends Controller
{
    /**
     * Adicionar
     *
     * Adiciona um novo pedido para o usuário logado.
     *
     * @param \App\Http\Requests\Order\CreateMyOrderRequest $request
     * @param \App\Actions\Commands\Order\CreateOrder $action
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    public function __invoke(CreateMyOrderRequest $request, CreateOrder $action)
    {
        // Sempre tem que criar o pedido para o usuário que estiver logado.
        $user = auth()->user();

        $validated = collect($request->validated());

        $order = $action->handle($validated, $user);

        return OrderResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('my.orders.show', [
                        'order' => $order->id
                    ]),
                ],
            ]);
    }
}
