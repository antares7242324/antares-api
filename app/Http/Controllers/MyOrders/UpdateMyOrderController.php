<?php

namespace App\Http\Controllers\MyOrders;

use App\Models\Order\Order;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Knuckles\Scribe\Attributes\UrlParam;
use App\Actions\Commands\Order\UpdateOrder;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Order\UpdateMyOrderRequest;
use App\Http\Resources\Order\OrderDetailResource;

class UpdateMyOrderController extends Controller
{
    /**
     * Atualizar.
     *
     * Atualiza um pedido informado do usuário logado.
     *
     * @param \App\Http\Requests\Order\UpdateMyOrderRequest $request
     * @param \App\Models\Order\Order $order
     * @param \App\Actions\Commands\Order\UpdateOrder $action
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/order/order.json')]
    #[UrlParam('id', 'integer', "ID do pedido.")]
    public function __invoke(UpdateMyOrderRequest $request, Order $order, UpdateOrder $action)
    {
        $validated = collect($request->validated());

        $order = $action->handle($validated, $order);

        return OrderDetailResource::make($order)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                    'show' => route('my.orders.show', [
                        'order' => $order->id
                    ]),
                ],
            ]);
    }
}
