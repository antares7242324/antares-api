<?php

namespace App\Http\Controllers;

use App\Libraries\InvoiceStatus;
use App\Models\Hunt\Invoice;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Actions\Queries\ListInvoices;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseField;
use App\Http\Resources\Hunt\InvoiceCollection;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Resources\Hunt\InvoiceDetailResource;
use Knuckles\Scribe\Attributes\UrlParam;

/**
 * @group Meu Cadastro
 *
 * @subgroup Notas Fiscais
 * @subgroupDescription
 * Endpoints que acessam e manipulam os dados das
 * notas ficais e entidades relacionadas.
 *
 * ### Tipos de Situação (status)
 * - pending (pendente)
 * - approved (aprovada)
 * - rejected (rejeitada)
 */
#[ResponseFromFile('storage/responses/standard/unauthorized.json', 401)]
#[ResponseField('status', 'string', 'Situação atual.')]
#[ResponseField('code', 'string', 'Código chave.')]
#[ResponseField('number', 'string', 'Código que compõe o número de série.')]
#[ResponseField('series', 'string', 'Digito do número de série.')]
#[ResponseField('cnae', 'string', 'Código CNAE do distribuidor que gerou a nota fiscal.')]
#[ResponseField('amount', 'integer', 'Valor da nota fiscal.')]
#[ResponseField('uf', 'string', 'Sigla da unidade federativa.')]
#[ResponseField('municipality', 'string', 'Nome do município.')]
#[ResponseField('issuanceAt', 'string', 'Data de emissão no formato *y-m-d h:m:s*.')]
#[ResponseField('createdAt', 'string', 'Data de criação no formato *y-m-d h:m:s*.')]
#[ResponseField('updatedAt', 'string', 'Data de atualização no formato *y-m-d h:m:s*.')]
#[ResponseField('items.code', 'string', 'Código do item na ANTARES.')]
#[ResponseField('items.unitPrice', 'integer', 'Valor unitário do item em centavos.')]
#[ResponseField('items.unitCost', 'integer', 'Valor de custo do item em centavos.')]
#[ResponseField('items.quantity', 'integer', 'Quantidade do item na nota fiscal.')]
#[ResponseField('items.subtotal', 'integer', 'Valor total do item na nota fiscal.')]
#[ResponseField('items.refund', 'integer', 'Valor de crédito gerado para o usuário na caçada.')]
#[ResponseField('items.discount', 'string', 'Valor do desconto que o usuário aplicou no item no formato decimal.')]
#[ResponseField('items.huntDiscount', 'string', 'Valor de desconto obtido pela caçada no formato decimal')]
#[ResponseField('items.description', 'integer', 'Descrição do item pela nota fiscal.')]
#[ResponseField('user.status', 'integer', 'Status do usuário.')]
#[ResponseField('user.role', 'string', 'Papel do usuário no sistema.')]
#[ResponseField('user.lastLoginAt', 'string', 'Data do último login efetuado pelo usuário.')]
#[ResponseField('user.name', 'string', 'Nome completo do usuário.')]
#[ResponseField('user.firstName', 'string', 'Primeira parte do nome do usuário.')]
class MyInvoiceController extends Controller
{
    /**
     * Listar
     *
     * Listar as notas fiscais.
     *
     * ### Inclusões permitidas (include)
     * - users
     * - items
     *
     * ### Ordenamentos permitidos (sort)
     * - id
     * - number
     * - series
     * - cnae
     * - uf
     * - municipality
     * - created-at
     * - updated-at
     * - user-name
     * - credit
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Actions\Queries\ListInvoices $listInvoices
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoices.json')]
    #[QueryParam('filter[status]', 'string', "Filtra pelo nome do status.<br>", example: InvoiceStatus::PENDING)]
    #[QueryParam('filter[number]', 'string', "Filtra pelo número da nota fiscal.<br>", example: "44567")]
    #[QueryParam('filter[series]', 'string', 'Filtra pelo número de série da nota fiscal.<br>', example: "11")]
    #[QueryParam('filter[cnae]', 'string', "Filtra pelo código CNAE.<br>", "6677899")]
    #[QueryParam('filter[uf]', 'string', "Filtra pela sigla UF.<br>", example: "RS")]
    #[QueryParam('filter[municipality]', 'string', "Filtra pelo nome do município.<br>", example: 'Caxias do Sul')]
    #[QueryParam('filter[created-at]', 'string', "Filtra pela data de emissão.<br>", example: '2023-05-01 14:00:00')]
    #[QueryParam('filter[updated-at]', 'string', "Filtra pela data de atualização.<br>", example: '2023-05-01 14:00:00')]
    #[QueryParam('filter[user-name]', 'string', "Filtra pelo nome do distribuidor.<br>", example: 'Lara Croft')]
    #[QueryParam('filter[credit]', 'integer', "Filtra pelo valor de crédito em centavos obtido na caçada.<br>", example: '10000')]
    #[QueryParam('include', 'string', "Adiciona os dados dos recursos informados.<br>", example: 'items,user')]
    #[QueryParam('sort', 'string', "Ordena pelos recursos informados.<br>", example: 'cnae,-uf')]
    public function index(Request $request, ListInvoices $listInvoices)
    {
        $user = auth()->user();

        $invoices = $listInvoices->handle($user);

        $success = $invoices->isEmpty() ? false : true;

        return InvoiceCollection::make($invoices)
            ->additional([
                'success' => $success,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }

    /**
     * Detalhar
     *
     * Detalhar a nota fiscal informada.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Hunt\Invoice $invoice
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    #[ResponseFromFile('storage/responses/invoice/invoice.json')]
    #[UrlParam('id', 'integer', "ID da nota fiscal.")]
    #[ResponseField('items.relationships.userItem', 'object', 'Os dados do código vinculado ao item pelo usuário.')]
    public function show(Request $request, Invoice $invoice)
    {
        // Adiciona o usuário para evitar que ele acesse os pedidos
        // dos outros usuários na plataforma.
        $user = auth()->user();

        if ($invoice->user_id == $user->id) new ModelNotFoundException;

        return InvoiceDetailResource::make($invoice)
            ->additional([
                'success' => true,
                'links' => [
                    'self' => $request->url(),
                ],
            ]);
    }
}
