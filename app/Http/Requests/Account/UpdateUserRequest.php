<?php

namespace App\Http\Requests\Account;

use App\Models\Account\User;
use App\Libraries\UserStatus;
use App\Models\Account\Store;
use Illuminate\Validation\Rule;
use App\Models\Product\PriceList;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $userId = $this->user->id ?? null;

        $storeRules = collect([
            'store' => 'required|array',
            'store.name' => 'required|string|max:255',
            'store.cnpj' => [
                'sometimes', 'required', 'string', 'max:255',
                Rule::unique(Store::TABLE, 'cnpj')->where(fn ($query) =>
                    $query->whereNull('deleted_at')
                        ->where('user_id', '!=', $userId)
                ),
            ],
            'store.code_erp' => 'required|string',
        ]);

        $rules = collect([
            'name' => 'sometimes|required|string|max:255',
            'email' => [
                'sometimes', 'required', 'string', 'email', 'max:255',
                Rule::when($userId, Rule::unique(User::TABLE, 'email')
                    ->ignore($userId)),
            ],
            // OBS: Por enquanto não vai ser permitido trocar de status.
            // 'role' => [
            //     'sometimes', 'required', Rule::in(UserRole::all())
            // ],
            'status' => [
                'sometimes', 'required', Rule::in(UserStatus::all())
            ],
            'last_status' => [
                'sometimes', Rule::in(UserStatus::all())
            ],
            'current_discount' => 'sometimes|required|decimal:2',
            'price_list_id' => [
                'sometimes', 'required', Rule::exists(PriceList::TABLE, 'id')
            ],
            'code_erp' => 'sometimes|required|string',
        ]);

        if ($this->has('store')) {
            $rules = $rules->merge($storeRules);
        }

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'Nome do usuário.',
                'example' => 'Lara Croft',
            ],
            'email' => [
                'description' => 'E-mail do usuário.',
                'example' => 'laracroft@tombraider.com',
            ],
            // 'role' => [
            //     'description' => 'Papel da conta.',
            //     'example' => UserRole::CLIENT,
            // ],
            'status' => [
                'description' => 'Status da conta.',
                'example' => UserStatus::ACTIVE,
            ],
            'current_discount' => [
                'description' => 'O desconto atual informado pelo painel do administrador.',
                'example' => 0.10,
            ],
            'price_list_id' => [
                'description' => 'ID da tabela de preços vinculada ao usuário (ver ***Listar tabelas de preços***).',
                'example' => '11',
            ],
            'code_erp' => [
                'description' => 'Código ERP da Antares do distribuidor.',
                'example' => '000556',
            ],
            'store.name' => [
                'description' => 'Nome da loja.',
                'example' => 'Loja Metalúrgica',
            ],
            'store.cnpj' => [
                'description' => 'CNPJ da loja.',
                'example' => '01964522000176',
            ],
            'store.code_erp' => [
                'description' => 'Código ERP da Antares da loja.',
                'example' => '91',
            ],
        ];
    }
}
