<?php

namespace App\Http\Requests\Account;

use App\Models\Account\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', Rule::exists(User::TABLE)],
            'password' => ['required', 'string'],
        ];
    }

    public function bodyParameters()
    {
        return [
            'email' => [
                'description' => 'E-mail do usuário.',
                'example' => 'spock@kirk.com',
            ],
            'password' => [
                'description' => 'Senha do usuário.',
            ],
        ];
    }
}
