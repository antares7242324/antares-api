<?php

namespace App\Http\Requests\Account;

use App\Models\Account\Store;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'cnpj' => [
                'required',
                'string',
                'max:255',
                Rule::unique(Store::TABLE)
                    ->where(fn ($query) => $query->whereNull('deleted_at')),
            ],
            'code_erp' => ['required', 'string'],
        ];
    }

    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'Nome da loja.',
                'example' => 'Metalúrgica S.A.',
            ],
            'cnpj' => [
                'description' => 'CNPJ da loja.',
                'example' => '59841297000160',
            ],
            'code_erp' => [
                'description' => 'Código ERP da Antares.',
                'example' => '002387',
            ],
        ];
    }
}
