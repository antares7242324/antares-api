<?php

namespace App\Http\Requests\Account;

use App\Libraries\UserRole;
use App\Models\Account\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::TABLE)
            ],
            'role' => ['required', Rule::in(UserRole::all())],
        ];
    }

    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'Nome do usuário.',
                'example' => 'Lara Croft',
            ],
            'email' => [
                'description' => 'E-mail do usuário.',
                'example' => 'laracroft@tombraider.com',
            ],
            'role' => [
                'description' => 'Papel da conta.',
                'example' => UserRole::CLIENT,
            ],
        ];
    }
}
