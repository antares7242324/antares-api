<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'token' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }

    public function bodyParameters()
    {
        return [
            'token' => [
                'description' => 'Token enviado ao usuário para ativar ou recuperar a conta.',
                'example' => 'AO3eRF6rF1qDOYQ9Decd4bpY76W3iDuE8gieFkbL',
            ],
            'password' => [
                'description' => 'Senha do usuário.',
            ],
        ];
    }
}
