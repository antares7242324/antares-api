<?php

namespace App\Http\Requests\Account;

use App\Models\Account\Store;
use Illuminate\Validation\Rule;
use App\Models\Product\PriceList;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'current_discount' => 'required|decimal:2',
            'price_list_id' => [
                'required', Rule::exists(PriceList::TABLE, 'id')
            ],
            'code_erp' => 'required|string',
            'store' => 'required|array',
            'store.name' => 'required|string|max:255',
            'store.cnpj' => [
                'required',
                'string',
                'max:255',
                Rule::unique(Store::TABLE, 'cnpj')->where(
                    fn ($query) => $query->whereNull('deleted_at')
                ),
            ],
            'store.code_erp' => 'required|string',
        ];
    }

    public function bodyParameters()
    {
        return [
            'current_discount' => [
                'description' => 'Desconto atual do distribuidor.',
                'example' => 0.10,
            ],
            'price_list_id' => [
                'description' => 'ID da tabela de preços (price_lists).',
                'example' => 1,
            ],
            'code_erp' => [
                'description' => 'Código ERP do Portal Antares para o distribuidor.',
                'example' => '111',
            ],
            'store.name' => [
                'description' => 'Nome da loja.',
                'example' => 'Marcenaria Du Bem',
            ],
            'store.cnpj' => [
                'description' => 'CNPJ da loja.',
                'example' => '27641266000170',
            ],
            'store.code_erp' => [
                'description' => 'Código ERP do Portal Antares para a loja.',
                'example' => '85',
            ],
        ];
    }
}
