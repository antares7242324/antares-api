<?php

namespace App\Http\Requests\Media;

use App\Support\Media\MediaStorage;
use App\Support\Media\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'file' => ['required', 'file'],
            'name' => ['sometimes', 'required', 'string'],
            'type' => ['sometimes', 'required', Rule::in(MediaType::all())],
            'service' => ['sometimes', 'required', Rule::in(MediaStorage::all())],
        ];
    }

    public function bodyParameters()
    {
        return [
            'file' => [
                'description' => "Arquivo da nota fiscal",
            ],
            'name' => [
                'description' => "Nome do arquivo",
            ],
            'type' => [
                'description' => "Tipo do arquivo",
            ],
            'service' => [
                'description' => "Serviço para onde será usado. Se for um arquivo público, deve ser o serviço 'public'",
            ],
        ];
    }
}
