<?php

namespace App\Http\Requests\Product;

use App\Models\Account\User;
use App\Models\Product\Item;
use App\Models\Product\UserItem;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => [
                'required',
                Rule::exists(User::class, 'id'),
            ],
            'item_id' => [
                'required',
                Rule::exists(Item::class, 'id'),
                Rule::unique(UserItem::TABLE, 'item_id')->where(fn ($query) =>
                    $query->whereNull('deleted_at')
                        ->where('user_id', $this->user_id)
                ),
            ],
            'code' => [
                'required',
                'string',
                Rule::unique(UserItem::TABLE, 'code')->where(fn ($query) =>
                    $query->whereNull('deleted_at')
                        ->where('user_id', $this->user_id)
                ),
            ],
            'name' => [
                'string',
                'nullable',
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'user_id' => [
                'description' => 'ID do usuário distribuidor que será vinculado à equivalência de item.',
                'example' => '6',
            ],
            'item_id' => [
                'description' => 'ID do item que será vinculado ao código.',
                'example' => '1',
            ],
            'code' => [
                'description' => 'Código do item no sistema do usuário.',
                'example' => '18820',
            ],
            'name' => [
                'description' => 'Nome do item no sistema do usuário.',
                'example' => 'Acoplamento Flexível Plus Ultra',
            ],
        ];
    }
}
