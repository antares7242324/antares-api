<?php

namespace App\Http\Requests\Product;

use App\Models\Product\Item;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $itemId = $this->item->id ?? null;

        return [
            'name' => ['sometimes', 'string', 'max:255'],
            'code' => [
                'sometimes',
                'nullable',
                'string',
                Rule::when($itemId, Rule::unique(Item::TABLE)
                    ->ignore($itemId)
                    ->where(fn ($query) => $query->whereNull('deleted_at'))
                ),
            ],
            'hunt' => ['sometimes', 'boolean'],
        ];
    }
}
