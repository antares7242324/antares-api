<?php

namespace App\Http\Requests\Transaction;

use App\Http\Requests\CreateCompensationRequest;
use App\Models\Account\User;
use Illuminate\Validation\Rule;

class CreateManyCompensationRequest extends CreateCompensationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = collect(parent::rules());

        $rules->put('users', ['required', 'array'])
            ->put('users.*', [
                'required',
                'exists:' . User::TABLE . ',id',
                Rule::notIn([auth()->id()]),
            ]);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $parameters = collect(parent::bodyParameters());

        $parameters->put('users.*', [
            'description' => "Lista de usuários.",
            'example' => "1,2,3",
        ]);

        return $parameters->toArray();
    }
}
