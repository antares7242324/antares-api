<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompensationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $creator = $this->user();

        $compensation = $this->route('compensation') ?? null;

        if (!$compensation) return false;

        if ($creator->isLogistics() && $creator->id != $compensation->creator_id) return false;

        $amount = $this->has('amount') ? $this->input('amount') : null;

        if ($amount && $creator->isLogistics() && $amount < 0) return false;

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'amount' => [
                'sometimes',
                'integer',
                'not_in:0',
            ],
            'reason' => [
                'sometimes',
                'string',
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'amount' => [
                'description' => "Valor da compensação em centavos. Pode ser positivo ou negativo, mas não zero.",
                'example' => 10000,
            ],
            'reason' => [
                'description' => "Razão da compensação.",
                'example' => "Desconto de frete padrão.",
            ],
        ];
    }
}
