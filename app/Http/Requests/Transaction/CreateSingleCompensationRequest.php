<?php

namespace App\Http\Requests\Transaction;

use App\Models\Account\User;
use Illuminate\Validation\Rule;
use App\Http\Requests\CreateCompensationRequest;

class CreateSingleCompensationRequest extends CreateCompensationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = collect(parent::rules());

        $rules->put('user', [
            'required',
            'exists:' . User::TABLE . ',id',
            Rule::notIn([auth()->id()]),
        ]);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $parameters = collect(parent::bodyParameters());

        $parameters->put('user', [
            'description' => "ID do usuário que recebeu a compensação.",
            'example' => '44',
        ]);

        return $parameters->toArray();
    }
}
