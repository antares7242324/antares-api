<?php

namespace App\Http\Requests\Order;

use App\Models\Sale\Sale;
use App\Libraries\Shipping;
use App\Models\Account\User;
use App\Models\Account\Store;
use App\Libraries\OrderStatus;
use Illuminate\Validation\Rule;
use App\Libraries\PaymentMethod;
use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'code' => [
                'sometimes',
                'nullable',
                'string',
            ],
            'code_erp' => [
                'sometimes',
                'string',
            ],
            'user_id' => [
                'sometimes',
                'required',
                'exists:' . User::TABLE . ',id',
            ],
            'store_id' => [
                'sometimes',
                'required',
                'exists:' . Store::TABLE . ',id',
            ],
            'description' => [
                'sometimes',
                'nullable',
                'string',
            ],
            'status' => [
                'sometimes',
                'required',
                Rule::in(OrderStatus::all()),
                'string',
            ],
            'payment_method' => [
                'sometimes',
                'required',
                Rule::in(PaymentMethod::all()),
                'string',
            ],
            'shipping' => [
                'sometimes',
                'required',
                Rule::in(Shipping::all()),
                'string',
            ],
            'used_balance' => [
                'sometimes',
                'integer',
                'lte:0',
            ],
            'previous_balance' => [
                'sometimes',
                'integer',
            ],
            'sale_id' => [
                'sometimes',
                'nullable',
                'exists:' . app(Sale::class)->getTable() . ',id',
            ],
            'sale_discount' => [
                'sometimes',
                'nullable',
                'integer',
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'code' => [
                'description' => "Código do pedido criado pelo usuário.",
                'example' => '12345',
            ],
            'code_erp' => [
                'description' => 'Código ERP da Antares relacionado ao pedido.',
                'example' => '12345',
            ],
            'user_id' => [
                'description' => "ID do usuário que fez o pedido.",
                'example' => '55',
            ],
            'store_id' => [
                'description' => "ID da loja do pedido.",
                'example' => '12',
            ],
            'description' => [
                'description' => "Descricao do pedido.",
                'example' => 'Pedido de metais.',
            ],
            'status' => [
                'description' => "Status do pedido.",
                'example' => OrderStatus::REQUESTED,
            ],
            'payment_method' => [
                'description' => "Método de pegamento.",
                'example' => PaymentMethod::PREPAID,
            ],
            'shipping' => [
                'description' => "Tipo de frete.",
                'example' => Shipping::CIF,
            ],
            'used_balance' => [
                'description' => "Valor em centavos do saldo usado",
                "example" => 10000,
            ],
            'previous_balance' => [
                'description' => "Valor em centavos do saldo antes do pedido.",
                'example' => 3000000,
            ],
            'sale_id' => [
                'description' => "ID da promoção",
                'example' => 1,
            ],
            'sale_discount' => [
                'description' => "Desconto da promoção.",
                'example' => 10,
            ],
        ];
    }
}
