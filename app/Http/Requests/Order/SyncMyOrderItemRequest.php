<?php

namespace App\Http\Requests\Order;

use App\Models\Product\Item;
use App\Models\Product\Price;
use Illuminate\Foundation\Http\FormRequest;

class SyncMyOrderItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'items' => ['sometimes', 'array'],
            'items.*.id' => [
                'required',
                'integer',
                'exists:' . Item::TABLE . ',id',
            ],
            'items.*.price_id' => [
                'required',
                'integer',
                'exists:' . Price::TABLE . ',id',
            ],
            'items.*.quantity' => [
                'required',
                'integer',
                'min:1',
            ],
            'items.*.unit_original_price' => [
                'required',
                'integer',
                'min:0',
            ],
            'items.*.unit_discount_price' => [
                'required',
                'integer',
                'min:0',
            ],
            'items.*.subtotal' => [
                'required',
                'integer',
                'min:0',
            ],
            'items.*.delivery_at' => [
                'required',
                'date_format:Y-m-d',
            ],
            'items.*.discount' => [
                'sometimes',
                'numeric',
                'between:0,1',
                'decimal:0,2',
            ],
            'items.*.debit' => [
                'sometimes',
                'integer',
                'min:0',
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'items' => [
                'description' => "Array com os itens do pedido.",
            ],
            'items.*.id' => [
                'description' => "ID do item.",
            ],
            'items.*.price_id' => [
                'description' => "ID do preço vinculado ao item.",
            ],
            'items.*.quantity' => [
                'description' => "Quantidade dos itens.",
                'example' => 3,
            ],
            'items.*.unit_original_price' => [
                'description' => "Valor em centavos do preço unitário do item.",
                'example' => 10000,
            ],
            'items.*.unit_discount_price' => [
                'description' => "Valor em centavos do preço unitário do item com desconto de caçada.",
                'example' => 5000,
            ],
            'items.*.subtotal' => [
                'description' => "Valor final em centavos somando todos os itens.",
                'example' => 30000,
            ],
            'items.*.delivery_at' => [
                'description' => "Data de entrega prevista para o item.",
            ],
            'items.*.discount' => [
                'description' => "Valor do desconto sobre o item.",
                'example' => 0.27,
            ],
            'items.*.debit' => [
                'description' => "Valor do débito no saldo do usuário baseado no desconto do item.",
                'example' => 1500,
            ],
        ];
    }
}
