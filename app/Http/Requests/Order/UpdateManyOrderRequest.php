<?php

namespace App\Http\Requests\Order;

use App\Models\Order\Order;
use Illuminate\Foundation\Http\FormRequest;

class UpdateManyOrderRequest extends UpdateOrderRequest
{
    /**
     * Monta as regras de validação baseadas em UpdateOrderRequest
     * @see \App\Http\Requests\Order\UpdateOrderRequest
     * As regras são:
     * - 'code'
     * - 'user_id'
     * - 'store_id'
     * - 'description'
     * - 'status'
     * - 'payment_method'
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = collect(parent::rules());

        $rules->put('ids', [
            'required',
            'array',
            'exists:' . Order::TABLE . ',id'
        ]);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $bodyParameters = collect(parent::bodyParameters());

        $bodyParameters->put('ids', [
            'description' => "IDs dos pedidos que serão atualizados.",
            'example' => "1, 2, 3",
        ]);

        return $bodyParameters->toArray();
    }
}
