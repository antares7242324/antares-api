<?php

namespace App\Http\Requests\Order;

use App\Models\Order\Order;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ExportMyOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'orders' => [
                'required',
                'array',
                Rule::exists(Order::TABLE, 'id')->where(fn($query) =>
                    $query->where('user_id', $this->user()->id)
                ),
            ],
        ];
    }

    public function messages()
    {
        return [
            'orders.*.exists' => 'One or more orders does not exist.',
        ];
    }
}
