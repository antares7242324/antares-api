<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMyOrderRequest extends UpdateOrderRequest
{
    /**
     * Monta as regras de validação baseadas em UpdateOrderRequest
     * @see \App\Http\Requests\Order\UpdateOrderRequest
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $parent = collect(parent::rules());

        $rules = $parent->only(
            'code',
            'code_erp',
            'store_id',
            'description',
            'status',
            'payment_method',
            'shipping',
            'used_balance',
            'previous_balance',
            'sale_id',
            'sale_discount',
        );

        // Deixa required os campos used_balance e previous_balance.
        $rules->put('used_balance', ['required', 'integer', 'lte:0'])
            ->put('previous_balance', ['required', 'integer'])
            ->put('debit', ['sometimes', 'integer'])
            ->put('discount', ['sometimes', 'decimal:2', 'between:0.00,1.00']);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $parent = collect(parent::bodyParameters());

        $bodyParams = $parent->only(
            'code',
            'code_erp',
            'store_id',
            'description',
            'status',
            'payment_method',
            'shipping',
            'used_balance',
            'previous_balance',
            'sale_id',
            'sale_discount',
        );

        return $bodyParams->toArray();
    }
}
