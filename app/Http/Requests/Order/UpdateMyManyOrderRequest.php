<?php

namespace App\Http\Requests\Order;

use App\Models\Order\Order;
use App\Rules\ModelExistsOwnedByUser;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMyManyOrderRequest extends UpdateMyOrderRequest
{
    /**
     * Monta as regras de validação baseadas em UpdateMyOrderRequest
     * @see \App\Http\Requests\Order\UpdateMyOrderRequest
     * As regras são:
     * - 'code'
     * - 'store_id'
     * - 'description'
     * - 'status'
     * - 'payment_method'
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = collect(parent::rules());

        $rules->put('ids', ['required', 'array', new ModelExistsOwnedByUser(Order::class)]);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $bodyParameters = collect(parent::bodyParameters());

        $bodyParameters->put('ids', [
            'description' => "Coleção de IDs de pedidos que serão atualizados.",
        ]);

        return $bodyParameters->toArray();
    }
}
