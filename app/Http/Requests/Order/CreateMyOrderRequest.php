<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\Order\CreateOrderRequest;

/**
 * Extende CreateOrderRequest por possuir os mesmos campos e mesmas regras.
 * A diferença é que não tem que informar um usuário, pois é vinculado com o próprio usuário que acessou o endpoint.
 */
class CreateMyOrderRequest extends CreateOrderRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $parent = collect(parent::rules());

        $rules = $parent->only(
            'store_id',
            'description',
            'status',
            'payment_method',
            'shipping',
            'used_balance',
            'previous_balance',
            'sale_id',
            'sale_discount',
        );

        // Deixa required os campos used_balance e previous_balance.
        $rules->put('used_balance', ['required', 'integer', 'lte:0'])
            ->put('previous_balance', ['required', 'integer'])
            ->put('debit', ['sometimes', 'integer'])
            ->put('discount', ['sometimes', 'decimal:2', 'between:0.00,1.00']);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $parent = collect(parent::bodyParameters());

        $bodyParams = $parent->only(
            'store_id',
            'description',
            'status',
            'payment_method',
            'shipping',
            'used_balance',
            'previous_balance',
            'sale_id',
            'sale_discount',
        );

        return $bodyParams->toArray();
    }
}
