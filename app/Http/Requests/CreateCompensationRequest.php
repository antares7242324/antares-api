<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompensationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $creator = $this->user();

        $amount = $this->input('amount', 0);

        if ($creator->isLogistics() && $amount >= 0) return false;

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'amount' => [
                'required',
                'integer',
                'not_in:0',
            ],
            'reason' => [
                'required',
                'string',
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'amount' => [
                'description' => "Valor da compensação em centavos. Pode ser positivo ou negativo, mas não zero.",
                'example' => 10000,
            ],
            'reason' => [
                'description' => "Razão da compensação.",
                'example' => "Desconto de frete padrão.",
            ],
        ];
    }
}
