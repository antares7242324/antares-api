<?php

namespace App\Http\Requests\Invoice;

use App\Models\Hunt\Invoice;
use App\Http\Requests\Invoice\UpdateInvoiceRequest;

class UpdateManyInvoiceRequest extends UpdateInvoiceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = collect(parent::rules());

        $rules->put('ids', [
            'required',
            'array',
            'exists:'.Invoice::TABLE.',id'
        ]);

        return $rules->toArray();
    }

    public function bodyParameters()
    {
        $parameters = collect(parent::bodyParameters());

        $parameters->put('ids', [
            'description' => 'IDs das notas fiscais a serem alteradas.',
            'examples' => "1, 2, 3,",
        ]);

        return $parameters->toArray();
    }
}
