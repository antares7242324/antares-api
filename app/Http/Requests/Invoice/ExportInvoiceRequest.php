<?php

namespace App\Http\Requests\Invoice;

use App\Models\Hunt\Invoice;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ExportInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'users' => [
                'required',
                'array',
                Rule::exists(Invoice::TABLE, 'user_id'),
            ],
        ];
    }

    public function messages()
    {
        return [
            'invoices.*.exists' => "One or more users doesn't have invoices.",
        ];
    }
}
