<?php

namespace App\Http\Requests\Invoice;

use App\Models\File\Media;
use App\Models\Account\User;
use App\Models\Product\Item;
use App\Models\Product\UserItem;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'media_id' => [
                'required',
                Rule::exists(Media::TABLE, 'id'),
            ],
            'user_id' => [
                'sometimes',
                'required',
                Rule::exists(User::TABLE, 'id'),
            ],
            'amount' => [
                'required',
                'integer',
            ],
            'code' => [
                'required',
                'string',
            ],
            'number' => [
                'required',
                'string',
            ],
            'series' => [
                'required',
                'string',
            ],
            'cnae' => [
                'sometimes',
                'nullable',
                'string',
            ],
            'uf' => [
                'required',
                'string',
            ],
            'municipality' => [
                'required',
                'string',
            ],
            'issuance_at' => [
                'required',
                'date_format:Y-m-d',
            ],
            'items' => [
                'required',
                'array',
            ],
            'items.*.id' => [
                'required',
                Rule::exists(Item::TABLE, 'id'),
            ],
            'items.*.quantity' => [
                'required',
                'integer',
            ],
            'items.*.unit_price' => [
                'required',
                'integer',
            ],
            'items.*.refund' => [
                'sometimes',
                'integer',
            ],
            'items.*.discount' => [
                'sometimes',
                'integer',
            ],
            'items.*.hunt_discount' => [
                'sometimes',
                'integer',
            ],
            'items.*.unit_cost' => [
                'sometimes',
                'integer',
            ],
            'items.*.subtotal' => [
                'required',
                'integer',
            ],
            'items.*.description' => [
                'required',
                'string',
            ],
            'items.*.code' => [
                'required',
                'string',
            ],
            'items.*.user_item_id' => [
                'sometimes',
                'nullable',
            ],
        ];
    }

    public function bodyParameters()
    {
        return [
            'media_id' => [
                'description' => "ID do arquivo da nota fiscal subido anteriormente.",
            ],
            'user_id' => [
                'description' => "ID do usuário que subiu a nota fiscal.",
            ],
            'amount' => [
                'description' => "Valor total da nota fiscal em centavos.",
                'example' => 523000
            ],
            'code' => [
                'description' => "Código da chave da nota fiscal.",
                'example' => "NFe43760201913323000120330100000626511996457777"
            ],
            'number' => [
                'description' => "Número da nota fiscal sem pontuação.",
                'example' => "11882",
            ],
            'series' => [
                'description' => "Número de série da nota fiscal.",
                'example' => "11",
            ],
            'cnae' => [
                'description' => "O código CNAE do distribuidor.",
                "example" => "7785",
            ],
            'uf' => [
                'description' => "Sigla da unidade federativa do Brasil na nota fiscal.",
                'example' => "RS",
            ],
            'municipality' => [
                'description' => "Nome do município da nota fiscal.",
                'example' => "Porto Alegre",
            ],
            'issuance_at' => [
                'description' => "Data de emissão da nota fiscal.",
                'example' => "2023-02-22",
            ],
            'items' => [
                'description' => "Array com os itens da nota fiscal",
            ],
            'items.*.id' => [
                'description' => "ID do item encontrado na API.",
            ],
            'items.*.quantity' => [
                'description' => "Quantidade de itens.",
            ],
            'items.*.unit_price' => [
                'description' => "Valor do preço unitário do item em centavos",
                'example' => 12000,
            ],
            'items.*.refund' => [
                'description' => "Valor em centavos do quanto será creditado a caçada para o usuário.",
                'example' => 5000,
            ],
            'items.*.discount' => [
                'description' => "Valor do desconto do item no formato decimal.",
                'example' => '0.15',
            ],
            'items.*.hunt_discount' => [
                'description' => "Valor do desconto da caçada no item no formato decimal.",
                'example' => '0.20',
            ],
            'items.*.unit_cost' => [
                'description' => "Valor em centavos do custo unitário do item.",
                'example' => 5000,
            ],
            'items.*.subtotal' => [
                'description' => "Valor total dos itens na nota fiscal.",
                'example' => 150000,
            ],
            'items.*.description' => [
                'description' => "Descrição do item na nota fiscal.",
            ],
            'items.*.code' => [
                'description' => "Código do item conforme a ANTARES.",
            ],
            'items.*.user_item_id' => [
                'description' => "ID do código do item vinculado ao distribuidor.",
            ],
        ];
    }
}
