<?php

namespace App\Http\Middleware;

use App\Libraries\TokenAbility;
use Closure;
use Illuminate\Http\Request;

class AccessApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (! auth()->user() || ! auth()->user()->currentAccessToken())
            abort(403, "Forbidden access");

        if (! auth()->user()->tokenCan(TokenAbility::ACCESS))
            abort(403, "Forbidden access");

        return $next($request);
    }
}
