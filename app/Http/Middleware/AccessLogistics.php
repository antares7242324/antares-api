<?php

namespace App\Http\Middleware;

use App\Libraries\UserRole;
use Closure;
use Illuminate\Http\Request;

class AccessLogistics
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // $roles = [UserRole::MEEG, UserRole::ADMIN, UserRole::LOGISTICS];

        // $user = auth()->user();

        // if (!$user || !in_array($user->role, $roles)) {
        //     abort(403, 'Forbidden access');
        // }

        return $next($request);
    }
}
