<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Esse middleware aceita um nome de $object e verifica se o campo user_id existe no objeto.
 * Então, verifica se o user_id é o mesmo id do usuário logado. Impede o acesso se não for.
 */
class VerifyOwnership
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, string $object)
    {
        $user = auth()->user();

        if (isset($request->$object->user_id) && $request->$object->user_id != $user->id)
            abort(403, 'Forbidden access');

        return $next($request);
    }
}
