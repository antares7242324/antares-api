<?php

namespace App\Http\Middleware;

use App\Models\Product\PriceList;
use Closure;
use Illuminate\Http\Request;

class UserHasPriceList
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        if (! $user->priceList) {
            return abort(404, "Price List not found.");
        }

        return $next($request);
    }
}
