<?php

namespace App\Jobs\Orders;

use App\Models\Order\Order;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ProcessCancelOrdersPriceListInactiveJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected array $ids){}

    public function uniqueId(): string
    {
        return "process-cancel-orders-price-list-inactive-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Order::whereIn('id', $this->ids)->update([
                'status' => OrderStatus::CANCELLED,
                'updated_at' => Carbon::now(),
            ]);
        }
        catch (\Exception $e) {
            Log::error('ProcessCancelOrderPriceListInactiveJob: Erro ao cancelar os pedidos', [
                'order_ids' => $this->ids,
                'error' => $e->getMessage(),
            ]);
        }
    }
}
