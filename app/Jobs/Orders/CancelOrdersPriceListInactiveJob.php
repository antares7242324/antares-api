<?php

namespace App\Jobs\Orders;

use App\Models\Order\Order;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Orders\ProcessCancelOrdersPriceListInactiveJob;

class CancelOrdersPriceListInactiveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected int $chunk = 100)
    {
        $this->chunk = max(1, $chunk);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = 1;

        do {
            $orders = Order::query()
                ->where('status', OrderStatus::DRAFT)
                ->whereHas('priceList', fn ($query) =>
                    $query->where('active', false)
                )
                ->orderBy('orders.id')
                ->limit($this->chunk)
                ->offset(($page - 1) * $this->chunk)
                ->get();
                // ->paginate($this->chunk, ['*'], 'page', $page);

            $orderIds = $orders->pluck('id')->toArray();

            if (empty($orderIds) === false) {
                ProcessCancelOrdersPriceListInactiveJob::dispatch($orderIds);
            }

            $page++;
        }
        // while ($orders->hasMorePages());
        while ($orders->count() === $this->chunk);
    }
}
