<?php

namespace App\Jobs\PriceLists;

use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use App\Models\Product\PriceList;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CancelOrdersPriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?PriceList $priceList;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($priceListId, protected ?string $status)
    {
        $this->priceList = PriceList::findOrFail($priceListId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orders = $this->priceList->orders()
            ->when(is_null($this->status) === false, fn ($query) =>
                $query->where('status', $this->status)
            )
            ->get();

        $orders->each(function ($order) {
            $order->update(['status' => OrderStatus::CANCELLED]);
        });
    }
}
