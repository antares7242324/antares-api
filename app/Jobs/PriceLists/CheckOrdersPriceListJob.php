<?php

namespace App\Jobs\PriceLists;

use Illuminate\Bus\Queueable;
use App\Models\Product\PriceList;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Libraries\OrderStatus;

class CheckOrdersPriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected int $chunk = 100)
    {
        if ($chunk < 1) $this->chunk = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = 1;

        do {
            $priceLists = PriceList::query()
                ->orderBy('id')
                ->paginate($this->chunk, ['*'], 'page', $page);

            $priceLists->each(function ($priceList) {
                if ($priceList->isNotActive() || $priceList->hasEnded()) {
                    $hasOrders = $priceList->orders()
                        ->where('status', OrderStatus::DRAFT)
                        ->exists();

                    if ($hasOrders) {
                        CancelOrdersPriceListJob::dispatch(
                            $priceList->id, OrderStatus::DRAFT
                        );
                    }
                }
            });

            $page++;
        }
        while ($priceLists->hasMorePages());
    }
}
