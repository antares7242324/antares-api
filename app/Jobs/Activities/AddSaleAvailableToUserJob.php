<?php

namespace App\Jobs\Activities;

use App\Actions\Queries\ListUserSaleActivities;
use App\Models\Sale\Sale;
use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Libraries\ActivityAction;
use App\Libraries\ActivityRestriction;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Activity\CreateActivity;

class AddSaleAvailableToUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?Sale $sale;
    protected ?User $user;
    protected ?int $responsibleId;

    /** @var \Illuminate\Support\Collection<\App\Models\Activity> */
    protected $activities;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $saleId, int $userId, $responsibleId = null)
    {
        $this->sale = Sale::findOrFail($saleId);

        $this->user = User::findOrFail($userId);

        $this->responsibleId = $responsibleId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity, ListUserSaleActivities $listActivities)
    {
        $this->activities = $listActivities->handle(
            $this->user->id,
            ActivityAction::SALE_AVAILABLE,
            $this->sale->id
        );

        if ($this->activities->count() < 1) {
            $data = collect([
                'sale' => $this->sale->getAttributes(),
                'user' => $this->user->getAttributes(),
            ]);

            $activity = collect()
                ->put('action', ActivityAction::SALE_AVAILABLE)
                ->when(
                    is_null($this->responsibleId) === false,
                    fn($collection) =>
                        $collection->put('responsible_id', $this->responsibleId)
                )
                ->put('restriction', ActivityRestriction::restrictionByUser($this->user))
                ->put('loggable_type', Sale::class)
                ->put('loggable_id', $this->sale->id)
                ->put('user_id', $this->user->id)
                ->put('data', json_encode($data->toArray()));

            $createActivity->handle($activity->toArray());
        }
    }
}
