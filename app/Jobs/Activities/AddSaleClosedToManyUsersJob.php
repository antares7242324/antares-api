<?php

namespace App\Jobs\Activities;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Jobs\Activities\AddSaleClosedToUserJob;

class AddSaleClosedToManyUsersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $userIds, protected int $saleId)
    {
        // Não precisa adicionar $saleId pois já está
        // sendo adicionado através do parâmetro.

        $this->userIds = collect($userIds);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->userIds->each(fn($id) =>
            AddSaleClosedToUserJob::dispatch($this->saleId, $id)
        );
    }
}
