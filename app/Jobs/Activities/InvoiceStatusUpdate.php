<?php

namespace App\Jobs\Activities;

use App\Models\Hunt\Invoice;
use Illuminate\Bus\Queueable;
use App\Libraries\ActivityAction;
use Illuminate\Foundation\Auth\User;
use App\Libraries\ActivityRestriction;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Activity\CreateActivity;
use App\Libraries\InvoiceStatus;

class InvoiceStatusUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Hunt\Invoice */
    protected $invoice;

    /** @var \Illuminate\Foundation\Auth\User */
    protected $responsible;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, User $responsible = null)
    {
        $this->invoice = $invoice;

        $this->responsible = $responsible;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity)
    {
        $invoiceData = collect($this->invoice->getAttributes());

        $activityAction = ActivityAction::INVOICE_UPDATED_STATUS;

        // Se a nota for reaberta, tem que mostrar o status como sendo ainda
        // aprovada, mas mudar o tipo de ação.
        if ($this->invoice->isReopened()) {
            $invoiceData->put('status', InvoiceStatus::APPROVED);

            $activityAction = ActivityAction::INVOICE_REOPENED;
        }

        $data = collect([
            'invoice' => $invoiceData->toArray(),
            'user' => $this->invoice->user->getAttributes(),
        ]);

        $activity = collect()
            ->put('action', $activityAction)
            ->when(
                is_null($this->responsible) === false,
                fn ($collection) => $collection->put(
                    'responsible_id', $this->responsible->id
                )
            )
            ->put('restriction', ActivityRestriction::ANY)
            ->put('loggable_type', Invoice::class)
            ->put('loggable_id', $this->invoice->id)
            ->put('user_id', $this->invoice->user_id)
            ->put('data', json_encode($data->toArray()));

        $createActivity->handle($activity->toArray());
    }
}
