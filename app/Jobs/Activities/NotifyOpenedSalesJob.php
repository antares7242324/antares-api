<?php

namespace App\Jobs\Activities;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Actions\Queries\ListOpenedSalesByDate;
use App\Jobs\Activities\ProcessSaleAvailableUsersByChunkJob;

class NotifyOpenedSalesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected Carbon $date){}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sales = (new ListOpenedSalesByDate)
            ->handle($this->date);

        $sales->chunk(10)->each(fn($chunk) =>
            ProcessSaleAvailableUsersByChunkJob::dispatch($chunk)
        );
    }
}
