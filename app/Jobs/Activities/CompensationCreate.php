<?php

namespace App\Jobs\Activities;

use Illuminate\Bus\Queueable;
use App\Libraries\ActivityAction;
use Illuminate\Foundation\Auth\User;
use App\Libraries\ActivityRestriction;
use Illuminate\Queue\SerializesModels;
use App\Models\Transaction\Compensation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Activity\CreateActivity;

class CompensationCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Transaction\Compensation */
    protected $compensation;

    /** @var \Illuminate\Foundation\Auth\User */
    protected $responsible;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Compensation $compensation, User $responsible = null)
    {
        $this->compensation = $compensation;

        $this->responsible = $responsible;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity)
    {
        $data = collect([
            'compensation' => $this->compensation->getAttributes(),
            'user' => $this->compensation->user->getAttributes(),
        ]);

        $activity = collect()
            ->put('action', ActivityAction::COMPENSATION_SYNCED)
            ->when(
                is_null($this->responsible) === false,
                fn($collection) => $collection->put('responsible_id', $this->responsible->id)
            )
            ->put('restriction', ActivityRestriction::CLIENT)
            ->put('loggable_type', Compensation::class)
            ->put('loggable_id', $this->compensation->id)
            ->put('user_id', $this->compensation->user_id)
            ->put('data', json_encode($data->toArray()));

        $createActivity->handle($activity->toArray());
    }
}
