<?php

namespace App\Jobs\Activities;

use App\Libraries\ActivityRestriction;
use App\Models\Order\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Libraries\ActivityAction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Activity\CreateActivity;

class OrderStatusUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Order\Order */
    protected $order;

    /** @var \Illuminate\Foundation\Auth\User */
    protected $responsible;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, User $responsible = null)
    {
        $this->order = $order;

        $this->responsible = $responsible;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity)
    {
        $data = collect([
            'order' => $this->order->getAttributes(),
            'user' => $this->order->user->getAttributes(),
        ]);

        $activity = collect()
            ->put('action', ActivityAction::ORDER_UPDATED_STATUS)
            ->when(
                is_null($this->responsible) === false,
                fn($collection) => $collection->put('responsible_id', $this->responsible->id)
            )
            ->put('restriction', ActivityRestriction::ANY)
            ->put('loggable_type', Order::class)
            ->put('loggable_id', $this->order->id)
            ->put('user_id', $this->order->user_id)
            ->put('data', json_encode($data->toArray()));

        $createActivity->handle($activity->toArray());
    }
}
