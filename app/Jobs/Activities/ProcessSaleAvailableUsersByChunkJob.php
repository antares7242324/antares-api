<?php

namespace App\Jobs\Activities;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Actions\Queries\ListAdminUsers;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Jobs\Activities\AddSaleAvailableToManyUsersJob;

class ProcessSaleAvailableUsersByChunkJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \Illuminate\Database\Eloquent\Collection<\App\Models\Sale\Sale>
     */
    protected $sales;

    /**
     * @var \Illuminate\Support\Collection<integer>
     * @see \App\Models\Account\User
     */
    protected $adminIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $sales)
    {
        $this->sales = $sales;

        $adminIds = (new ListAdminUsers)->handle();

        $this->adminIds = $adminIds->pluck('id');
    }

    public function uniqueId(): string
    {
        return "process-sale-available-users-by-chunk-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sales->each(function ($sale) {
            $users = $sale->users;

            // Adiciona a atividade de promoção disponível
            // e envia o email se possível.
            $users->chunk(10)->each(function ($chunk) use ($sale) {
                // Monta coleção com ids dos usuários.
                $userIds = $chunk->pluck('id');

                AddSaleAvailableToManyUsersJob::dispatch(
                    $userIds->toArray(),
                    $sale->id
                );
            });

            // Adiciona a atividade de promoção a todos os admins.
            $this->adminIds->each(fn($id) =>
                AddSaleAvailableToUserJob::dispatch($sale->id, $id)
            );
        });
    }
}
