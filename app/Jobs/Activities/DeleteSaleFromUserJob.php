<?php

namespace App\Jobs\Activities;

use App\Models\Activity;
use App\Models\Sale\Sale;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class DeleteSaleFromUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        protected int $saleId,
        protected int $userId
    ){}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $activities = Activity::where('user_id', $this->userId)
            ->where('loggable_id', $this->saleId)
            ->where('loggable_type', Sale::class)
            ->get();

        // Remove todas as atividades relacionadas.
        if ($activities) {
            $activities->each(fn($activity) =>
                $activity->delete()
            );
        }
    }
}
