<?php

namespace App\Jobs\Activities;

use App\Jobs\Mail\MailSaleAvailableToUserJob;
use App\Actions\Commands\Activity\CreateActivity;
use App\Actions\Queries\ListUserSaleActivities;

class AddSaleAvailableToUserAndMailJob extends AddSaleAvailableToUserJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity, ListUserSaleActivities $listActivities)
    {
        parent::handle($createActivity, $listActivities);

        if ($this->activities->count() < 1) {
            MailSaleAvailableToUserJob::dispatch(
                $this->sale->id,
                $this->user->id
            );
        }
    }
}
