<?php

namespace App\Jobs\Activities;

use App\Models\Hunt\Invoice;
use Illuminate\Bus\Queueable;
use App\Libraries\ActivityAction;
use Illuminate\Foundation\Auth\User;
use App\Libraries\ActivityRestriction;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Activity\CreateActivity;

class InvoiceCreatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        protected Invoice $invoice, protected ?User $responsible = null
    ){}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity)
    {
        $data = collect([
            'invoice' => $this->invoice->getAttributes(),
            'user' => $this->invoice->user->getAttributes(),
        ]);

        $activity = collect()
            ->put('action', ActivityAction::INVOICE_CREATED)
            ->when(
                is_null($this->responsible) === false,
                fn ($collection) => $collection->put(
                    'responsible_id', $this->responsible->id
                )
            )
            ->put('restriction', ActivityRestriction::ANY)
            ->put('loggable_type', Invoice::class)
            ->put('loggable_id', $this->invoice->id)
            ->put('user_id', $this->invoice->user_id)
            ->put('data', json_encode($data->toArray()));

        $createActivity->handle($activity->toArray());
    }
}
