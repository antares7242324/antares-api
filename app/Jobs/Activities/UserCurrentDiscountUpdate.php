<?php

namespace App\Jobs\Activities;

use Illuminate\Bus\Queueable;
use App\Libraries\ActivityAction;
use Illuminate\Foundation\Auth\User;
use App\Libraries\ActivityRestriction;
use Illuminate\Queue\SerializesModels;
use App\Models\Account\User as Account;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Activity\CreateActivity;

class UserCurrentDiscountUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \Illuminate\Foundation\Auth\User */
    protected $user;

    /** @var \Illuminate\Foundation\Auth\User */
    protected $responsible;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, User $responsible = null)
    {
        $this->user = $user;

        $this->responsible = $responsible;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateActivity $createActivity)
    {
        $data = collect([
            'user' => $this->user->getAttributes(),
            'priceList' => $this->user->priceList->getAttributes(),
        ]);

        $data = collect()
            ->put('action', ActivityAction::USER_UPDATED_CURRENT_DISCOUNT)
            ->when(
                is_null($this->responsible) === false,
                fn($collection) => $collection->put('responsible_id', $this->responsible->id)
            )
            ->put('restriction', ActivityRestriction::CLIENT)
            ->put('loggable_type', Account::class)
            ->put('loggable_id', $this->user->id)
            ->put('user_id', $this->user->id)
            ->put('data', json_encode($data->toArray()));

        $createActivity->handle($data->toArray());
    }
}
