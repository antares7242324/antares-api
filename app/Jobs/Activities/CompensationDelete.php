<?php

namespace App\Jobs\Activities;

use App\Models\Activity;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Models\Transaction\Compensation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CompensationDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Transaction\Compensation */
    protected $compensation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Compensation $compensation)
    {
        $this->compensation = $compensation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Remove a atividade relacionada à compensação.
        $activities = Activity::where('user_id', $this->compensation->user_id)
            ->where('loggable_id', $this->compensation->id)
            ->where('loggable_type', Compensation::class)
            ->get();

        if ($activities) {
            $activities->each(fn($activity) => $activity->delete());
        }
    }
}
