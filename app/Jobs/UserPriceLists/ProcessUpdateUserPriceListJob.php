<?php

namespace App\Jobs\UserPriceLists;

use Illuminate\Support\Str;
use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Portal\GetPriceListPortal;
use App\Jobs\UserPriceLists\CancelOrdersUserPriceListJob;

class ProcessUpdateUserPriceListJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->user = User::findOrFail($userId);
    }

    public function uniqueId(): string
    {
        return "process-update-user-price-list-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(GetPriceListPortal $getPriceListPortal)
    {
        $codeUser = $this->user->completeCodeErp();

        // Usuário tem que possuir loja ativa com código ERP.
        if (is_null($codeUser)) {
            Log::error(
                "ProcessUpdateUserPriceListJob: Usuário ({$this->user->id}) não possui loja ativa com código ERP."
            );
            return;
        }

        // Monta os dados para a consulta da tabela de preços.
        $params = collect([
            'user_code' => $codeUser,
            'page_size' => 1,
        ]);

        // Busca os dados da tabela de preços no Portal Antares ERP.
        $priceList = $getPriceListPortal->handle($params);

        // O método retorna NULL quando ocorre uma exceção.
        if (is_null($priceList)) {
            Log::error(
                "ProcessUpdateUserPriceListJob: Ocorreu algum erro na requisição ao Portal Antares ERP para o usuário ({$this->user->id})."
            );
            return;
        }

        // Caso não encontre nenhum resultado, retorna collection vazia.
        if ($priceList->isEmpty()) {
            Log::error(
                "ProcessUpdateUserPriceListJob: Ocorreu algum erro na requisição ao Portal Antares ERP para o usuário ({$this->user->id})."
            );
            return;
        }

        if (is_null($priceList) || $priceList->has('codigo') === false) {
            Log::alert(
                "ProcessUpdateUserPriceListJob: Não foi encontrada a tabela de preço no Portal Antares ERP para o usuário ({$this->user->id})."
            );

            // Remove a tabela de preços do usuário se ele possuir.
            if ($this->user->priceList()->exists()) {
                Log::alert(
                    "ProcessUpdateUserPriceListJob: Removendo tabela de preços ({$this->user->priceList->id}) do usuário ({$this->user->id}) e cancelando os pedidos em rascunho vinculados."
                );

                $priceListId = $this->user->priceList->id;

                // Cancela todos os pedidos que estiverem em rascunho e
                // estão vinculados a tabela de preços que foi removida.
                CancelOrdersUserPriceListJob::dispatch(
                    $this->user->id, $priceListId, OrderStatus::DRAFT
                );

                $this->user->update([
                    'price_list_id' => null,
                ]);
            }

            return;
        }

        // Busca a tabela de preços no sistema.
        $localPriceList = PriceList::query()
            ->where('code_erp', $priceList->get('codigo'))
            ->orderBy('created_at', 'desc')
            ->first();

        // Apenas vai vincular a nova tabela caso ela exista no sistema.
        if (is_null($localPriceList)) {
            Log::alert(
                "ProcessUpdateUserPriceListJob: Tabela de preços ({$priceList->get('codigo')}) não encontrada no sistema para o usuário ({$this->user->id})."
            );
            return;
        }

        // Se o usuário não possui tabela de preços, vincula a encontrada.
        if ($this->user->priceList()->exists() === false) {
            $this->user->update([
                'price_list_id' => $localPriceList->id,
            ]);
            return;
        }

        // Não precisa fazer nada se for a mesma tabela.
        if ($priceList->get('codigo') == $this->user->priceList->code_erp) {
            return;
        }

        // Cancela os pedidos que estiverem em rascunho e estão
        // vinculados a tabela de preços que foi alterada.
        CancelOrdersUserPriceListJob::dispatch(
            $this->user->id, $this->user->priceList->id, OrderStatus::DRAFT
        );

        // Atualiza para a nova tabela de preços.
        $this->user->update([
            'price_list_id' => $localPriceList->id,
        ]);
    }
}
