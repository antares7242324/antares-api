<?php

namespace App\Jobs\UserPriceLists;

use App\Libraries\UserRole;
use App\Libraries\UserStatus;
use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateUserPriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected int $chunk = 100)
    {
        if ($chunk < 1) $this->chunk = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = 1;

        do {
            $users = User::query()
                ->where('status', UserStatus::ACTIVE)
                ->where('role', UserRole::CLIENT)
                ->whereNotNull('code_erp')
                ->orderBy('id')
                ->paginate($this->chunk, ['*'], 'page', $page);

            $users->each(function ($user) {
                // Apenas processa se o usuário tem a loja cadastrada.
                if (! is_null($user->activeStore())) {
                    ProcessUpdateUserPriceListJob::dispatch($user->id);
                }
            });

            $page++;
        }
        while ($users->hasMorePages());
    }
}
