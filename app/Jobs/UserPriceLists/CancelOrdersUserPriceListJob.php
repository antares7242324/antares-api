<?php

namespace App\Jobs\UserPriceLists;

use Illuminate\Support\Str;
use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CancelOrdersUserPriceListJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        $userId, protected $priceListId, protected ?string $status
    )
    {
        $this->user = User::findOrFail($userId);
    }

    public function uniqueId(): string
    {
        return "cancel-orders-user-price-list-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orders = $this->user->orders()
            ->when(is_null($this->status) === false, fn ($query) =>
                $query->where('status', $this->status)
            )
            ->where('price_list_id', $this->priceListId)
            ->get();

        $orders->each(function ($order) {
            $order->update(['status' => OrderStatus::CANCELLED]);
        });
    }
}
