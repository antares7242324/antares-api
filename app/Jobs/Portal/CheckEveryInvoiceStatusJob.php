<?php

namespace App\Jobs\Portal;

use App\Models\Hunt\Invoice;
use Illuminate\Bus\Queueable;
use App\Libraries\InvoiceStatus;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckEveryInvoiceStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $perPage;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $perPage = 100)
    {
        $this->perPage = $perPage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentPage = 1;

        $checkDate = now()->subDays(
            config('scheduler.update_invoice_status.check_days')
        );

        // Primeira página.
        $invoices = Invoice::query()
            ->whereNotIn('status', [InvoiceStatus::REJECTED])
            ->whereDate('verified_at', '<=', $checkDate->toDateString())
            ->paginate($this->perPage, ['*'], 'page', $currentPage);

        // Faz a consulta usando loop while enquanto houverem itens para buscar.
        while ($currentPage <= $invoices->lastPage()) {

            // Busca as notas fiscais no portal e atualiza se necessário.
            $invoices->each(function ($invoice) {
                UpdateInvoiceStatusJob::dispatch(
                    $invoice->code,
                    $invoice->id
                );
            });

            $currentPage++;

            // Busca a próxima página.
            $invoices = Invoice::whereNotIn('status', [
                InvoiceStatus::REJECTED
            ])->paginate($this->perPage, ['*'], 'page', $currentPage);
        }
    }
}
