<?php

namespace App\Jobs\Portal;

use App\Models\Order\Order;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\Portal\UpdateOrderStatusJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckEveryOrderStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected int $perPage;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $perPage = 100)
    {
        $this->perPage = $perPage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentPage = 1;

        // Primeira página.
        $orders = Order::whereNotNull('code_erp')
            ->whereNotIn('status', [
                OrderStatus::CANCELLED,
                OrderStatus::COMPLETED,
                OrderStatus::FAILED,
                OrderStatus::DRAFT,
            ])
            ->orderBy('id')
            ->paginate($this->perPage, ['*'], 'page', $currentPage);

        Log::info("Check Every Order Status: pedidos encontrados", [
            'orders' => $orders->count(),
        ]);

        // Se houverem pedidos encontrados, então continua a aplicação.
        if ($orders->count()) {
            // Faz a consulta usando loop while enquanto houverem
            // itens para buscar.
            while ($currentPage <= $orders->lastPage()) {
                // Busca os pedidos no portal e atualiza se necessário.
                $orders->each(function ($order) {
                    UpdateOrderStatusJob::dispatch($order->id);
                });

                $currentPage++;

                // Busca a próxima página.
                $orders = Order::whereNotNull('code_erp')
                    ->whereNotIn('status', [
                        OrderStatus::CANCELLED,
                        OrderStatus::COMPLETED,
                        OrderStatus::FAILED,
                        OrderStatus::DRAFT,
                    ])
                    ->paginate($this->perPage, ['*'], 'page', $currentPage);
            }
        }
    }
}
