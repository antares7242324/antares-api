<?php

namespace App\Jobs\Portal;

use App\Jobs\Mail\MailPortalErrorLogJob;
use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateUserPriceListCompleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?User $user;
    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $userId)
    {
        $this->user = User::findOrFail($userId);
        $this->portal = new Portal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = [
            'codigoERP' => $this->user->portalCode(),
        ];

        try {
            // Request GET.
            $response = $this->portal->get('listaprecos', $params);

            // Monta o JSON.
            $responseData = $response->getData();

            // Impede a execução caso haja erro.
            if ($responseData->success === false) return;

            // Monta os dados da resposta.
            $data = collect($responseData->data);

            // Atribui o código da tabela de preços pelo portal.
            $priceListCode = $data->get('codigo');

            // Se não existir o código da tabela de preços, não continua.
            if (is_null($priceListCode)) return;

            // Verifica se já existe a tabela de preços encontrada no portal.
            $priceList = PriceList::where('code_erp', $priceListCode)
                ->first();

            // TODO -> IMPLEMENTAR.
            // Se não encontrar a tabela de preços, tem que criá-la
            // e atualiza todos os preços dos itens vinculados.
            if (is_null($priceList)) return;

            // Se a tabela de preços não mudou, não continua.
            if ($priceList->id == $this->user->price_list_id) return;

            // Vincula a nova tabela de preços.
            $this->user->update(['price_list_id' => $priceList->id]);
        }
        catch (\Exception $e) {
            $message = "Erro ao atualizar a tabela de preços do usuário ({$this->user->id})";

            Log::channel('antares_erp')->error($message, [
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => collect($params)->toArray(),
                'exception' => $e,
            ]);

            Log::error($message, [
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => collect($params)->toArray(),
                'exception' => $e,
            ]);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => isset($response) ? $response->getData() : [],
                'query' => $params ?? [],
                'exception' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
            ])->toArray());

            throw $e;
        }
    }
}
