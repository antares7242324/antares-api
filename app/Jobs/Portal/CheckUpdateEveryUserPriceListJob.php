<?php

namespace App\Jobs\Portal;

use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Jobs\Portal\UpdateUserPriceListCompleteJob;

class CheckUpdateEveryUserPriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $perPage = 100){}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = 1;

        do {
            $users = User::paginate($this->perPage, ['*'], $page);

            $users->each(function ($user) {
                UpdateUserPriceListCompleteJob::dispatch($user->id);
            });

            $page++;
        }
        while ($users->hasMorePages());
    }
}
