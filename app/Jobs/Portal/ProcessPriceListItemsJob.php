<?php

namespace App\Jobs\Portal;

use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Portal\UpdateItemPricesBatchJob;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ProcessPriceListItemsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?PriceList $priceList;
    protected string $portalCode;
    protected int $pageSize;
    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $priceListId, string $portalCode, int $pageSize = 300)
    {
        $this->priceList = PriceList::findOrFail($priceListId);

        $this->portalCode = $portalCode;

        $this->pageSize = $pageSize;

        $this->portal = new Portal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Preparando as variáveis principais.
        $page = 1;

        $hasNext = true;

        try {
            // Faz a consulta usando loop while
            // enquanto houverem itens para buscar.
            while ($hasNext) {

                // Monta os parâmetros.
                $params = [
                    'page' => $page,
                    'pageSize' => $this->pageSize,
                    'codigoERP' => $this->portalCode,
                ];

                // Request GET.
                $response = $this->portal->get('listaprecos', $params);

                // Monta o JSON.
                $responseData = $response->getData();

                // Impede a execução caso haja erro.
                if ($responseData->success === false) return;

                // Monta os dados da resposta.
                $data = $responseData->data;

                $itens = collect($data->itens);

                if ($itens->isNotEmpty())
                    UpdateItemPricesBatchJob::dispatch($itens->toArray(), $this->priceList->id, $hasNext);

                $hasNext = $data->hasNext;

                $page++;
            }
        }
        catch (\Exception $e) {
            $message = "Erro ao processar a tabela de precos ({$this->priceList->id}) no portal";

            Log::channel('antares_erp')
                ->error($message, [
                    'response' => isset($response)
                        ? collect($response->getData())->toArray()
                        : [],
                    'query' => collect($params ?? [])->toArray(),
                    'price_list' => $this->priceList->toArray(),
                    'exception' => $e,
                ]);

            Log::error($message, [
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => collect($params ?? [])->toArray(),
                'price_list' => $this->priceList->toArray(),
                'exception' => $e,
            ]);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => $params ?? [],
                'exception' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
            ]));

            throw $e;
        }
    }
}
