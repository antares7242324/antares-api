<?php

namespace App\Jobs\Portal;

use Illuminate\Support\Str;
use App\Models\Product\Item;
use App\Models\Product\Price;
use Illuminate\Bus\Queueable;
use App\Models\Product\PriceList;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Traits\Portal\DateConversionPortal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateItemPricesBatchJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use DateConversionPortal;

    protected Collection $items;
    protected ?PriceList $priceList;
    protected bool $hasNext;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        array $items = [],
        string $priceListId,
        bool $hasNext
    )
    {
        $this->items = collect($items);

        $this->priceList = PriceList::findOrFail($priceListId);

        $this->hasNext = $hasNext;
    }

    public function uniqueId(): string
    {
        return "update-item-prices-batch-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            // Faz o upsert de cada preço encontrado.
            $this->items->each(function ($element) {
                $data = collect($element);

                $item = Item::where('code', $data->get('codigo'))->first();

                if (is_null($item)) {
                    Log::error("UpdateItemPricesBatchJob: Item não encontrado", [
                        'code' => $data->get('codigo'),
                        'price_list_id' => $this->priceList->id,
                        'price_list_code' => $this->priceList->code,
                    ]);

                    throw new \Exception(
                        "Item com código ({$data->get('codigo')}) não foi encontrado."
                    );
                }

                // Atualiza/cria o objeto do preço do item na tabela.
                $amount = (int) $data->get('preco_base');

                $originalAmount = (int) $data->get('preco_original');

                $startAt = $data->get('inicio_vigencia');

                if ($startAt !== null) {
                    $startAt = $this->toCarbon($startAt);
                }

                $endAt = $data->get('fim_vigencia');

                if ($endAt !== null) {
                    $endAt = $this->toCarbon($endAt);
                }

                $lastUpdateAt = $data->get('ultAlteracao');

                if ($lastUpdateAt !== null) {
                    $lastUpdateAt = $this->toCarbon($lastUpdateAt);
                }

                $params = [
                    'amount' => $amount,
                    'original_amount' => $originalAmount,
                    'start_at' => $startAt,
                    'end_at' => $endAt,
                    'last_update_at' => $lastUpdateAt,
                ];

                $keys = [
                    'item_id' => $item->id,
                    'price_list_id' => $this->priceList->id,
                ];

                Price::updateOrCreate($keys, $params);
            });

            DB::commit();
        }
        catch (\Exception $e) {
            // Rollback se achar algum erro.
            DB::rollback();

            Log::error("UpdateItemPricesBatchJob: Ocorreu algum erro ao atualizar os itens da tabela de preços. ({$this->priceList->code})", [
                'error' => $e->getMessage(),
                'response' => $data->toArray(),
            ]);
        }

        // Se tiver atualizado todos os itens da tabela de
        // preços, informa a data em que a tabela fica ativa.
        if ($this->hasNext === false) {
            $this->priceList->update(['active_at' => now()]);
        }
    }
}
