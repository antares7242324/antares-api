<?php

namespace App\Jobs\Portal;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use App\Models\Product\PriceList;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Portal\UpdatePriceCompleteJob;

class CheckUpdatePriceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $lastUpdatedAt;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        ?string $lastUpdatedAt = null, protected int $perPage = 300
    )
    {
        if ($lastUpdatedAt === null) $lastUpdatedAt = 'now';

        $this->lastUpdatedAt = Carbon::parse($lastUpdatedAt)->startOfDay();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = 1;

        do {
            $priceLists = PriceList::paginate($this->perPage, ['*'], $page);

            $priceLists->each(fn ($priceList) =>
                UpdatePriceCompleteJob::dispatch(
                    $priceList->id,
                    $priceList->code,
                    $this->lastUpdatedAt->format('Y-m-d')
                )
            );

            $page++;
        }
        while ($priceLists->hasMorePages());
    }
}
