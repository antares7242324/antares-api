<?php

namespace App\Jobs\Portal;

use App\Traits\Currency;
use App\Models\Product\Item;
use App\Models\Product\Price;
use Illuminate\Bus\Queueable;
use App\Models\Product\PriceList;
use Illuminate\Support\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Traits\Portal\DateConversionPortal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdatePriceListItemBatchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Currency;
    use DateConversionPortal;

    protected ?PriceList $priceList;
    protected Collection $items;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $priceListId, Collection $items)
    {
        $this->priceList = PriceList::findOrFail($priceListId);

        $this->items = collect($items->get('itens'));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->items->each(function ($element) {
            // Converte para coleção para facilitar a manipulação dos dados.
            $data = collect($element);

            // Dados específicos para o item.
            $itemData = collect()
                ->put('name', $data->get('descricao'))
                ->put('code', $data->get('codigo'))
                ->put('hunt', $data->get('caçada', true));

            // Atualiza ou cria o item.
            $item = Item::updateOrCreate(
                ['code' => $data->get('codigo')],
                $itemData->toArray()
            );

            // Dados específicos para o preço do item na tabela.
            $amount = (int) $data->get('preco_base', 0);

            $originalAmount = (int) $data->get('preco_original', 0);

            $startAt = $data->get('inicio_vigencia');

            $endAt = $data->get('fim_vigencia');

            $lastUpdateAt = $data->get('ultAlteracao');

            if ($startAt !== null) {
                $startAt = $this->toCarbon($startAt);
            }

            if ($endAt !== null) {
                $endAt = $this->toCarbon($endAt);
            }

            if ($lastUpdateAt !== null) {
                $lastUpdateAt = $this->toCarbon($lastUpdateAt);
            }

            $priceData = collect()
                ->put('amount', $amount)
                ->put('original_amount', $originalAmount)
                ->put('start_at', $startAt)
                ->put('end_at', $endAt)
                ->put('last_update_at', $lastUpdateAt);

            // Atualiza ou cria o preço do item nesta tabela de preços.
            Price::updateOrCreate([
                'item_id' => $item->id,
                'price_list_id' => $this->priceList->id
            ], $priceData->toArray());
        });
    }
}
