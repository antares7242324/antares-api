<?php

namespace App\Jobs\Portal;

use Illuminate\Bus\Queueable;
use App\Models\Product\PriceList;
use Illuminate\Queue\SerializesModels;
use App\Actions\Queries\PriceListByCode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Jobs\Portal\UpdatePriceListItemBatchJob;
use App\Actions\Commands\Portal\GetPriceListItemsPortal;

class UpdateCreatePriceListItemsJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected PriceList $priceList;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected string $codeErp)
    {
        $this->priceList = (new PriceListByCode)->handle($codeErp, true);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(GetPriceListItemsPortal $getPriceListItemsPortal)
    {
        // Busca os dados da tabela de preços no Portal Antares ERP.
        // Primeira chamada necessária para buscar as próximas páginas.
        $priceListItems = $getPriceListItemsPortal->handle($this->codeErp);

        // Atualiza a primeira página de itens. Tem que ser feito em
        // separado para poder pegar as próximas páginas.
        UpdatePriceListItemBatchJob::dispatch(
            $this->priceList->id, $priceListItems
        );

        // Agora começa a executar as próximas páginas, se tiver.
        $hasNext = $priceListItems->get('hasNext');

        $page = 1;

        while ($hasNext) {
            $page++;

            $priceListItems = $getPriceListItemsPortal->handle(
                $this->codeErp, $page
            );

            $hasNext = $priceListItems->get('hasNext');

            UpdatePriceListItemBatchJob::dispatch(
                $this->priceList->id, $priceListItems
            );
        }
    }
}
