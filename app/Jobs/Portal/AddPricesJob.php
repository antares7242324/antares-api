<?php

namespace App\Jobs\Portal;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class AddPricesJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?PriceList $priceList;
    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($priceListId, protected $pageSize = 300)
    {
        $this->priceList = PriceList::findOrFail($priceListId);

        $this->portal = new Portal;
    }

    public function uniqueId(): string
    {
        return "add-prices-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Preparando as variáveis principais.
        $page = 1;

        $hasNext = true;

        // Monta os parâmetros.
        $params = collect()
            ->put('tabela', $this->priceList->code_erp)
            ->put('pageSize', $this->pageSize);

        do {
            try {
                // Coloca sempre a página mais atualizada.
                $params->put('page', $page);

                // Executa a requisição.
                $response = $this->portal->get(
                    'listaprecos', $params->toArray()
                );

                // Monta o JSON.
                $responseData = $response->getData();

                // Monta os dados da resposta.
                $data = collect($responseData->data);

                // Coleta os itens.
                $items = collect($data->get('itens', []));

                $hasNext = $data->get('hasNext', false);

                // Adiciona os preços de todos os itens vinculados.
                if ($items->isNotEmpty()) {
                    AddPricesBatchJob::dispatch(
                        $items->toArray(),
                        $this->priceList->id,
                        $hasNext
                    );
                }

                $page++;
            }
            catch (\Exception $e) {
                $hasNext = false;

                $message = "Erro ao adicionar itens na tabela de preços ({$this->priceList->id})";

                Log::channel('antares_erp')
                    ->error($message, [
                        'response' => isset($response)
                            ? collect($response->getData())->toArray()
                            : [],
                        'query' => $params->toArray(),
                        'price_list' => $this->priceList->toArray(),
                        'exception' => $e,
                    ]);

                Log::error($message, [
                    'response' => isset($response)
                        ? collect($response->getData())->toArray()
                        : [],
                    'query' => $params->toArray(),
                    'price_list' => $this->priceList->toArray(),
                    'exception' => $e,
                ]);

                MailPortalErrorLogJob::dispatch(collect([
                    'message' => $message,
                    'response' => isset($response)
                        ? collect($response->getData())->toArray()
                        : [],
                    'query' => $params->toArray(),
                    'exception' => [
                        'message' => $e->getMessage(),
                        'trace' => $e->getTraceAsString(),
                    ],
                ])->toArray());

                throw $e;
            }
        }
        while ($hasNext);
    }
}
