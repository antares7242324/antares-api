<?php

namespace App\Jobs\Portal;

use Illuminate\Support\Str;
use App\Models\Product\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Collection;

class UpdateItemsBatchJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Collection $items;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $items = [])
    {
        $this->items = collect($items);
    }

    public function uniqueId(): string
    {
        return "update-items-batch-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $removedItems = $this->items->filter(fn ($item) =>
            Item::where('code', $item['codigo'])
                ->whereNotNull('deleted_at')
                ->exists()
        );

        $validItems = $this->items->diff($removedItems);

        try {
            DB::beginTransaction();

            $upsertData = $validItems->map(function ($item) {
                $data = collect($item);

                return [
                    'code' => $data->get('codigo'),
                    'name' => $data->get('descricao'),
                    'hunt' => $data->get('caçada', true),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            });

            if ($upsertData->isNotEmpty()) {
                Item::upsert(
                    $upsertData->toArray(),
                    ['code'],
                    ['name', 'hunt', 'updated_at']
                );
            }

            // Recria os items removidos.
            $removedItems->each(function ($item) {
                $data = collect($item);

                Item::create([
                    'code' => $data->get('codigo'),
                    'name' => $data->get('descricao'),
                    'hunt' => $data->get('caçada', true),
                ]);
            });

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();

            // Log the error
            Log::error("UpdateItemsBatchJob: Erro ao atualizar os itens do portal.", [
                'error' => $e->getMessage(),
                'item_count' => $this->items->count(),
                'response' => $this->items->toArray(),
            ]);
        }
    }
}
