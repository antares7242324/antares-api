<?php

namespace App\Jobs\Portal;

use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use Illuminate\Support\Carbon;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Queue\InteractsWithQueue;
use App\Traits\Portal\DateConversionPortal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Support\Portal\PortalPriceListStatus;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ProcessUserPriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use DateConversionPortal;

    protected ?User $user;
    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $userId)
    {
        $this->user = User::findOrFail($userId);

        $this->portal = new Portal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Atribui a combinação do code_erp do usuário e de sua loja.
        $portalCode = $this->user->portalCode();

        $params = [
            'codigoERP' => $portalCode,
        ];

        try {
            // Request GET.
            $response = $this->portal->get('listaprecos', $params);

            // Monta o JSON.
            $responseData = $response->getData();

            // Impede a execução caso haja erro.
            if ($responseData->success === false) return;

            // Monta os dados da resposta.
            $data = collect($responseData->data);

            // Atribui o código da tabela de preços pelo portal.
            $priceListCode = $data->get('codigo');

            // Se não existir o código da tabela de preços, não continua.
            if (is_null($priceListCode)) return;

            // Verifica se já existe a tabela de preços encontrada no portal.
            $priceList = PriceList::where('code_erp', $priceListCode)
                ->first();

            // Se já existe, vincula essa tabela de
            // preços no usuário e termina a execução.
            if ($priceList) {
                $this->user->update(['price_list_id' => $priceList->id]);

                return;
            }

            // Se não existe, cria a tabela de preços.
            $startAt = $data->get('inicio_vigencia');

            if ($startAt !== null) $startAt = $this->toCarbon($startAt);

            $endAt = $data->get('fim_vigencia');

            if ($endAt !== null) $endAt = $this->toCarbon($endAt);

            $priceListData = collect()
                ->put('active', PortalPriceListStatus::isActive($endAt))
                ->put('name', $data->get('descricao'))
                ->put('code_erp', $priceListCode)
                ->put('start_at', $startAt)
                ->put('end_at', $endAt);

            $priceList = PriceList::create($priceListData->toArray());

            // Vincula a nova tabela de preços no usuário.
            $this->user->update(['price_list_id' => $priceList->id]);

            // Executa o job que vai atualizar os preços dos itens.
            ProcessPriceListItemsJob::dispatch($priceList->id, $portalCode);
        }
        catch (\Exception $e) {
            $message = "Erro ao processar a tabela de preços do usuário ({$this->user->id})";

            Log::channel('antares_erp')
                ->error($message, [
                    'response' => collect(
                        isset($response) ? $response->getData() : []
                    )->toArray(),
                    'query' => collect($params ?? [])->toArray(),
                    'user' => $this->user->toArray(),
                    'exception' => $e,
                ]);

            Log::error($message, [
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => collect($params ?? [])->toArray(),
                'user' => $this->user->toArray(),
                'exception' => $e,
            ]);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => isset($response) ? $response->getData() : [],
                'query' => $params ?? [],
                'exception' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
            ])->toArray());

            throw $e;
        }
    }
}
