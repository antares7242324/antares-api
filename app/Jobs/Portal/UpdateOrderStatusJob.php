<?php

namespace App\Jobs\Portal;

use App\Models\Order\Order;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use App\Support\Portal\Portal;
use App\Libraries\OrderSystemNote;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Mail\MailOrderByStatusJob;
use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Queue\InteractsWithQueue;
use App\Support\Portal\PortalOrderStatus;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Hunt\CancelDebitFromOrder;
use App\Actions\Commands\Portal\UpdateCompletedOrder;

class UpdateOrderStatusJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Portal $portal;
    protected ?Order $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->portal = new Portal;

        $this->order = Order::findOrFail($id);

        Log::info("Job constructed with ID: {$id}");
    }

    public function uniqueId(): string
    {
        return "update-order-status-" . $this->order->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CancelDebitFromOrder $cancelDebitFromOrder)
    {
        Log::info('Job handle method started.');

        // Monta os parâmetros.
        $params = collect()->put('numeroerp', $this->order->code_erp);

        Log::info("Update Order Status Job: Iniciado para o code_erp ({$this->order->code_erp})", [
            'code_erp' => $this->order->code_erp,
        ]);

        try {
            // Request GET.
            $response = $this->portal->get(
                'orcamentos/status', $params->toArray()
            );

            // Monta o JSON.
            $responseData = $response->getData();

            // Monta os dados da resposta.
            $data = collect($responseData->data);

            Log::info("Update Order Status Job: Resposta do portal para o code_erp ({$this->order->code_erp})", [
                'code_erp' => $this->order->code_erp,
                'response' => $data->toArray(),
                'query' => $params->toArray(),
            ]);

            // Aconteceu um erro, impede a execução.
            if ($data->has('errorCode')) {
                $message = "Erro ao atualizar o orçamento do pedido ({$this->order->id})";

                Log::channel('antares_erp')
                    ->error($message, [
                        'code_erp' => $this->order->code_erp,
                        'response' => collect($response->getData())->toArray(),
                        'query' => $params->toArray(),
                        'order' => $this->order->toArray(),
                    ]);

                MailPortalErrorLogJob::dispatch(collect([
                    'message' => $message,
                    'response' => $response->getData(),
                    'query' => $params ?? [],
                ])->toArray());

                return;
            }

            // Pega o status do orçamento.
            $status = $data->get('status');

            // Se por algum motivo não encontrou o código, impede a execução.
            if (is_null($status)) {
                $message = "Não encontrou o código para verificar o status do pedido ({$this->order->id}) com o code_erp ({$this->order->code_erp})";
                Log::channel('antares_erp')
                    ->error($message, [
                        'code_erp' => $this->order->code_erp,
                        'response' => collect($response->getData())->toArray(),
                        'query' => $params->toArray(),
                        'order' => $this->order->toArray(),
                    ]);

                MailPortalErrorLogJob::dispatch(collect([
                    'message' => $message,
                    'response' => $response->getData(),
                    'query' => $params,
                ])->toArray());

                return;
            }

            // Converte o status do Portal para o status do sistema.
            $currentStatus = PortalOrderStatus::convert($status);

            // Agora atualiza o pedido.
            if ($this->order->status != $currentStatus) {

                // Atualiza os dados do pedido quando ele for enviado
                // para produção pelo Portal Antares ERP.
                if ($currentStatus === OrderStatus::COMPLETED) {
                    $updated = (new UpdateCompletedOrder)->handle(
                        $this->order, $data
                    );

                    if ($updated) $this->order->refresh();
                }

                DB::transaction(
                    function () use ($currentStatus, $cancelDebitFromOrder) {
                        // Atualiza o pedido e não verifica o crédito.
                        $this->order->update([
                            'status' => $currentStatus,
                            'system_notes' => OrderSystemNote::statusNote(
                                $currentStatus
                            ),
                        ]);

                        $this->order->refresh();

                        // Verifica se está cancelado e remove o débito.
                        $cancelDebitFromOrder->handle($this->order);
                    }
                );

                $this->order->refresh();

                Log::info("Update Order Status Job: Atualizado o pedido ({$this->order->id}) com code_erp ({$this->order->code_erp})", [
                    'code_erp' => $this->order->code_erp,
                    'order' => $this->order->toArray(),
                ]);

                // Manda o email conforme o status se o
                // pedido não esteja com o status REQUESTED.
                if ($this->order->isRequested() === false) {
                    MailOrderByStatusJob::dispatch($this->order->id, $currentStatus);
                }
            }
        }
        catch (\Exception $e) {
            $message = "Erro ao atualizar o status do orçamento do pedido ({$this->order->id}) com code_erp ({$this->order->code_erp})";

            // Salva o log de erro no antares-erp.
            Log::channel('antares_erp')
                ->error($message, [
                    'code_erp' => $this->order->code_erp,
                    'response' => collect(
                        isset($response) ? $response->getData() : []
                    )->toArray(),
                    'query' => $params->toArray(),
                    'order' => $this->order->toArray(),
                    'exception' => $e,
                ]);

            // Repete o log de erro no sistema.
            Log::error($message, [
                'code_erp' => $this->order->code_erp,
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => $params->toArray(),
                'order' => $this->order->toArray(),
                'exception' => $e,
            ]);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => isset($response) ? $response->getData() : [],
                'query' => $params->toArray(),
                'exception' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
            ])->toArray());

            throw $e;
        }
    }
}
