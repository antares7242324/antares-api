<?php

namespace App\Jobs\Portal;

use Illuminate\Support\Str;
use App\Models\Hunt\Invoice;
use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Support\Portal\PortalNfeStatus;
use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Invoice\UpdateInvoice;

class UpdateInvoiceStatusJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected string $code, protected int $id)
    {
        $this->portal = new Portal;
    }

    public function uniqueId(): string
    {
        return "update-invoice-status-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UpdateInvoice $updateInvoice)
    {
        $invoice = Invoice::findOrFail($this->id);

        // Primeiro atualiza a data de quando essa nota foi verificada.
        // Isso tem que acontecer independente de da nota ser consultada
        // no ERP ou ter seu status alterado.
        $invoice->update([
            'verified_at' => now(),
        ]);

        $invoice->refresh();

        // Pega apenas os números.
        $codenumber = str_replace('nfe', '', strtolower($this->code));

        // Monta os parâmetros.
        $params = collect()
            ->put('chavenfe', $codenumber);

        try {
            // Request GET.
            $response = $this->portal->get(
                'consultachavenfe', $params->toArray()
            );

            // Monta o JSON.
            $responseData = $response->getData();

            // Monta os dados da resposta.
            $data = $responseData->data;

            // Pega o código do status da nota fiscal.
            $codigo = $data->codigo ?? null;

            if (!is_null($codigo)) {
                $currentStatus = PortalNfeStatus::convert($codigo);

                // Atualiza a nota fiscal para o novo status.
                if ($invoice->status != $currentStatus) {
                    $updateData = ['status' => $currentStatus];
                    $invoice = $updateInvoice->handle($invoice, $updateData);
                }
            }
        }
        catch (\Exception $e) {
            $message = "Erro ao atualizar o status da nota fiscal ({$invoice->id})";

            Log::channel('antares_erp')
                ->error($message, [
                    'response' => collect(
                        isset($response) ? $response->getData() : []
                    )->toArray(),
                    'query' => $params->toArray(),
                    'invoice' => $invoice->toArray(),
                    'exception' => $e,
                ]);

            Log::error($message, [
                'response' => collect(
                    isset($response) ? $response->getData() : []
                )->toArray(),
                'query' => $params->toArray(),
                'invoice' => $invoice->toArray(),
                'exception' => $e,
            ]);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => isset($response) ? $response->getData() : [],
                'query' => $params ?? [],
                'exception' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
            ])->toArray());

            throw $e;
        }
    }
}
