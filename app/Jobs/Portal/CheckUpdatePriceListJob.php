<?php

namespace App\Jobs\Portal;

use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use Illuminate\Support\Carbon;
use App\Models\Product\PriceList;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Traits\Portal\DateConversionPortal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Support\Portal\PortalPriceListStatus;

class CheckUpdatePriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use DateConversionPortal;

    protected Portal $portal;
    protected Carbon $lastUpdatedAt;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(?string $lastUpdatedAt = null)
    {
        if ($lastUpdatedAt === null) $lastUpdatedAt = 'now';

        $this->lastUpdatedAt = Carbon::parse($lastUpdatedAt)->startOfDay();

        $this->portal = new Portal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $query = [
                'dataAlteracao' => $this->lastUpdatedAt->format('d/m/Y'),
            ];

            // Executa a requisição.
            $response = $this->portal->get('listatabelas', $query);

            // Monta o JSON.
            $responseData = $response->getData();

            // Monta os dados da resposta.
            $data = collect($responseData->data);

            // Pega os códigos das tabelas de preços no sistema para atualizar.
            $priceListCodes = PriceList::all()->pluck('code_erp');

            // Monta a coleção com tabelas.
            $portalPriceLists = collect($data->get('tabelas'));

            $portalPriceLists->map(
                function ($element) use ($priceListCodes) {
                    $data = collect($element);

                    if ($priceListCodes->contains($data->get('codigo'))) {
                        $priceList = PriceList::query()
                            ->where('code_erp', $data->get('codigo'))
                            ->first();

                        $startAt = $data->get('inicio_vigencia');

                        $endAt = $data->get('fim_vigencia');

                        $lastUpdatedAt = $data->get('ultAlteracao');

                        if ($startAt !== null) {
                            $startAt = $this->toCarbon($startAt);
                        }

                        if ($endAt !== null) {
                            $endAt = $this->toCarbon($endAt);
                        }

                        if ($lastUpdatedAt !== null) {
                            $lastUpdatedAt = $this->toCarbon($lastUpdatedAt);
                        }

                        $priceList->update([
                            'active' => PortalPriceListStatus::isActive($endAt),
                            'name' => $data->get('descricao'),
                            'start_at' => $startAt,
                            'end_at' => $endAt,
                            'last_updated_at' => $lastUpdatedAt,
                        ]);
                    }
                }
            );
        }
        catch (\Exception $e) {
            Log::error("CheckUpdateEveryPriceListJob: Ocorreu um erro ao atualizar as tabelas de preços.", [
                'error' => $e->getMessage(),
                'response' => isset($response)
                    ? collect($response->getData())
                    : [],
                ]
            );
        }
    }
}
