<?php

namespace App\Jobs\Portal;

use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use App\Models\Product\PriceList;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Traits\Portal\DateConversionPortal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Support\Portal\PortalPriceListStatus;

class AddPriceListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use DateConversionPortal;

    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->portal = new Portal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Executa a requisição.
        $response = $this->portal->get('listatabelas');

        // Monta o JSON.
        $responseData = $response->getData();

        // Monta os dados da resposta.
        $data = collect(collect($responseData->data)->get('tabelas'));

        // Filtra as tabelas do Portal para apenas as que estão ativas.
        $lists = $data->map(function ($item) {
            $item = collect($item);

            $endAt = $item->get('fim_vigencia');

            if ($endAt !== null) {
                $endAt = $this->toCarbon($endAt);
            }

            $status = PortalPriceListStatus::isActive($endAt);

            return $status ? $item->toArray() : null;
        })->filter()->values();

        // Busca todos os códigos das tabelas de preço.
        $existingCodes = PriceList::query()
            ->where('active', true)
            ->pluck('code_erp');

        // Verifica quais tabelas de preço devem ser adicionadas.
        $newLists = $lists->reject(function ($item) use ($existingCodes) {
            $item = collect($item);
            return in_array($item->get('codigo'), $existingCodes->toArray());
        });

        // Adiciona as novas tabelas de preço encontradas no portal.
        $newLists->each(function ($item) {
            $item = collect($item);

            $startAt = $item->get('inicio_vigencia');

            if ($startAt !== null) {
                $startAt = $this->toCarbon($startAt);
            }

            $endAt = $item->get('fim_vigencia');

            if ($endAt !== null) {
                $endAt = $this->toCarbon($endAt);
            }

            $lastUpdatedAt = $item->get('ultAlteracao');

            if ($lastUpdatedAt !== null) {
                $lastUpdatedAt = $this->toCarbon($lastUpdatedAt);
            }

            $priceList = PriceList::create([
                'active' => PortalPriceListStatus::isActive($endAt),
                'name' => $item->get('descricao'),
                'code_erp' => $item->get('codigo'),
                'start_at' => $startAt,
                'end_at' => $endAt,
                'last_updated_at' => $lastUpdatedAt,
            ]);

            AddPricesJob::dispatch($priceList->id);
        });
    }
}
