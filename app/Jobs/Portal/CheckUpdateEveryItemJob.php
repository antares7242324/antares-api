<?php

namespace App\Jobs\Portal;

use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Bus\Queueable;
use App\Support\Portal\Portal;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Portal\UpdateItemsBatchJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckUpdateEveryItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Portal $portal;
    protected $lastUpdateAt;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        ?string $lastUpdateAt = null,
        protected string|null $searchKey = null,
        protected int $pageSize = 300
    )
    {
        $this->portal = new Portal;

        $this->lastUpdateAt = $lastUpdateAt !== null
            ? Carbon::parse($this->lastUpdateAt)
            : null;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Preparando as variáveis principais.
        $page = 1;

        $hasNext = true;

        // Monta os parâmetros.
        $params = collect()
            // Consulta pelo código se for informado.
            ->when($this->searchKey !== null, fn($collection) =>
                $collection->put('searchKey', $this->searchKey)
            )
            // Consulta pela data de atualização se for informada.
            ->when($this->lastUpdateAt !== null, fn($collection) =>
                $collection->put(
                    'dataAlteracao',
                    $this->lastUpdateAt->format('d/m/Y')
                )
            )
            ->put('pageSize', $this->pageSize);

        do {
            try {
                // Coloca sempre a página mais atualizada.
                $params->put('page', $page);

                // Executa a requisição.
                $response = $this->portal->get(
                    'listaprodutos', $params->toArray()
                );

                // Monta o JSON.
                $responseData = $response->getData();

                // Monta os dados da resposta.
                $data = collect($responseData->data);

                // Coleta os itens para atualizar.
                $itens = collect($data->get('itens', []));

                if ($itens->isNotEmpty()) {
                    UpdateItemsBatchJob::dispatch($itens->toArray());
                }

                $hasNext = $data->get('hasNext', false);

                $page++;
            }
            catch(\Exception $e) {
                $hasNext = false;

                $message = "Ocorreu um erro ao atualizar os produtos do portal";

                Log::channel('antares_erp')
                    ->error($message, [
                        'response' => collect(
                            isset($response) ? $response->getData() : []
                        )->toArray(),
                        'query' => $params->toArray(),
                        'exception' => $e,
                    ]);

                Log::error($message, [
                    'response' => collect(
                            isset($response) ? $response->getData() : []
                        )->toArray(),
                    'query' => $params->toArray(),
                    'exception' => $e,
                ]);

                MailPortalErrorLogJob::dispatch(collect([
                    'message' => $message,
                    'response' => isset($response) ? $response->getData() : [],
                    'query' => $params,
                    'exception' => [
                        'message' => $e->getMessage(),
                        'trace' => $e->getTraceAsString(),
                    ],
                ])->toArray());

                // Impede a execução caso haja erro.
                throw $e;
            }
        }
        while ($hasNext);
    }
}
