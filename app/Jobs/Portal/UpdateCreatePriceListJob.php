<?php

namespace App\Jobs\Portal;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Traits\Portal\DateConversionPortal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Support\Portal\PortalPriceListStatus;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Jobs\Portal\UpdateCreatePriceListItemsJob;
use App\Actions\Commands\Portal\GetPriceListPortal;
use App\Actions\Commands\PriceList\UpdateCreatePriceList;

class UpdateCreatePriceListJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use DateConversionPortal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected string $codeErp) {}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        GetPriceListPortal $getPriceListPortal,
        UpdateCreatePriceList $updateCreatePriceList
    )
    {
        // Monta os dados para a consulta da tabela de preços.
        $params = collect([
            'price_list_code' => $this->codeErp,
            'page_size' => 1,
        ]);

        // Busca os dados da tabela de preços no Portal Antares ERP.
        $priceListPortal = $getPriceListPortal->handle($params);

        // Converte os dados.
        $startAt = $priceListPortal->get('inicio_vigencia');

        $endAt = $priceListPortal->get('fim_vigencia');

        $lastUpdatedAt = $priceListPortal->get('ultAlteracao');

        if ($startAt !== null) {
            $startAt = $this->toCarbon($startAt);
        }

        if ($endAt !== null) {
            $endAt = $this->toCarbon($endAt);
        }

        if ($lastUpdatedAt !== null) {
            $lastUpdatedAt = $this->toCarbon($lastUpdatedAt);
        }

        $active = PortalPriceListStatus::isActive($endAt);

        // Monta os dados para o create / update.
        $data = collect()
            ->put('code_erp', $priceListPortal->get('codigo'))
            ->put('active', $active)
            ->put('name', $priceListPortal->get('descricao'))
            ->put('start_at', $startAt)
            ->put('end_at', $endAt)
            ->put('last_updated_at', $lastUpdatedAt);

        $priceList = $updateCreatePriceList->handle($data);

        if (!$priceList) {
            throw new \Exception(
                "Ocorreu um erro ao atualizar a tabela de preços de código ({$this->codeErp})"
            );
        }

        // Atualiza todos os preços dentro dessa tabela de preços.
        UpdateCreatePriceListItemsJob::dispatch($this->codeErp);
    }
}
