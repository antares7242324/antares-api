<?php

namespace App\Jobs\Portal;

use App\Models\Order\Order;
use Illuminate\Bus\Queueable;
use App\Libraries\OrderStatus;
use App\Support\Portal\Portal;
use App\Libraries\OrderSystemNote;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Mail\MailOrderByStatusJob;
use App\Jobs\Mail\MailPortalErrorLogJob;
use Illuminate\Queue\InteractsWithQueue;
use App\Support\Portal\PortalOrderStatus;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Actions\Commands\Order\SetOrderFailed;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Hunt\CancelDebitFromOrder;

class ProcessOrderRequestJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?Order $order;
    protected Portal $portal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->order = Order::findOrFail($id);

        $this->portal = new Portal;
    }

    public function uniqueId(): string
    {
        return "process-order-request-" . $this->order->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CancelDebitFromOrder $cancelDebitFromOrder)
    {
        Log::info("Process Order Request Job: Iniciado para o pedido ({$this->order->id})", [
            'order' => $this->order->toArray(),
        ]);

        // Apenas processa esse job se o pedido for solicitado.
        if ($this->order->isRequested() === false) {
            Log::error("Process Order Request Job: O pedido ({$this->order->id}) não foi requisitado para a Antares ERP.", [
                'order' => $this->order->toArray(),
            ]);

            throw new \Exception(
                "Ocorreu um erro ao processar o pedido: O pedido ({$this->order->id}) não foi requisitado para a Antares ERP."
            );
        }

        $body = collect()
            ->put('cliente', $this->order->user->code_erp)
            ->put('loja', $this->order->store->code_erp)
            ->put('emissao', $this->order->issuance_at->format('d/m/Y'))
            ->put('tabelaPreco', $this->order->priceList->code_erp)
            ->put('valorDescontoTotal', $this->order->discountValue())
            ->put('percentualDescontoTotal', $this->order->discountPercent());

        // Coleta todos os itens vinculados.
        $items = $this->order->items;

        // Adiciona os itens do pedido caso eles existam.
        $products = collect();

        // Adiciona cada item no body.
        $items->each(function ($product) use ($products) {
            $item = $product->item;

            $deliveryAt = $product->deliveryAt()->format('d/m/Y');

            $products->push([
                'codigo' => $item->code,
                'quantidade' => $product->quantity,
                'valorUnitario' => $product->unitValue(),
                'dataEntrega' => $deliveryAt,
            ]);
        });

        // Adiciona os itens do pedido mesmo se a coleção for vazia.
        $body->put('itens', $products->toArray());

        // Monta os parâmetros da query.
        $query = collect()
            ->put('numeroPortal', $this->order->id);

        Log::info("Process Order Request Job: Dados montados do pedido ({$this->order->id})", [
            'body' => $body->toArray(),
            'query' => $query->toArray(),
            'order' => $this->order->toArray(),
        ]);

        try {
            // Request POST.
            $response = $this->portal->post(
                'orcamentos/incluir',
                $body->toArray(),
                $query->toArray(),
            );
        }
        catch (\Exception $e) {
            $message = "Ocorreu um erro ao incluir orçamento do pedido ({$this->order->id})";

            // Salva o erro no log da antares-erp.
            Log::channel('antares_erp')
                ->error($message, [
                    'response' => collect(
                        isset($response) ? $response->getData() : [],
                    )->toArray(),
                    'query' => $query->toArray(),
                    'body' => $body->toArray(),
                    'order' => $this->order->toArray(),
                    'exception' => $e,
                ]);

            // Salva o erro no log da aplicação.
            Log::error($message, [
                'response' => collect(
                    isset($response) ? $response->getData() : [],
                )->toArray(),
                'query' => $query->toArray(),
                'body' => $body->toArray(),
                'order' => $this->order->toArray(),
                'exception' => $e,
            ]);

            // Coloca o pedido com status de erro.
            $this->order = (new SetOrderFailed)->handle($this->order);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => isset($response) ? $response->getData() : [],
                'query' => $query,
                'body' => $body,
                'exception' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
            ])->toArray());

            throw $e;
        }

        Log::info("Process Order Request Job: Resposta recebida do Portal do pedido ({$this->order->id})", [
            'response' => collect($response->getData())->toArray(),
            'query' => $query->toArray(),
            'body' => $body->toArray(),
            'order' => $this->order->toArray(),
        ]);

        // Monta o objeto do Json Response.
        $responseData = $response->getData();

        // Monta apenas o campo "data".
        $data = collect($responseData->data);

        // Executa este processo quando ocorre um erro na requisição.
        if ($response->isSuccessful() === false) {
            // Salva o log do erro no antares-erp.
            $message = "Erro na criação do orçamento do pedido ({$this->order->id}) no portal";

            Log::channel('antares_erp')
                ->error($message, [
                    'response' => collect($response->getData())->toArray(),
                    'query' => $query->toArray(),
                    'body' => $body->toArray(),
                    'order' => $this->order->toArray(),
                ]);

            // Reproduz o mesmo log do erro no sistema.
            Log::error($message, [
                'response' => collect($response->getData())->toArray(),
                'query' => $query->toArray(),
                'body' => $body->toArray(),
                'order' => $this->order->toArray(),
            ]);

            // Coloca o pedido com status de erro.
            $this->order = (new SetOrderFailed)->handle($this->order);

            MailPortalErrorLogJob::dispatch(collect([
                'message' => $message,
                'response' => $response->getData(),
                'body' => $body,
                'query' => $query,
            ])->toArray());

            throw new \Exception($message);
        }

        // Se retornou o código de cancelado.
        if ($data->get('codigo') !== null) {
            $currentStatus = PortalOrderStatus::convert($data->get('codigo'));

            if ($currentStatus == OrderStatus::CANCELLED) {
                // Pega o código do pedido no portal se ele for enviado.
                $orderCodeErp = $data->get('numeroERP');

                DB::transaction(
                    function () use ($currentStatus, $orderCodeErp, $cancelDebitFromOrder) {
                        // Atualiza o pedido e não verifica o crédito.
                        $data = collect()
                            ->put('status', $currentStatus)
                            ->put('system_notes', OrderSystemNote::statusNote($currentStatus))
                            ->when($orderCodeErp !== null, fn ($data) =>
                                $data->put('code_erp', $orderCodeErp)
                            );

                        $this->order->update($data->toArray());

                        $this->order->refresh();

                        // Verifica se está cancelado e remove o débito.
                        $cancelDebitFromOrder->handle($this->order);
                    }
                );

                $this->order->refresh();

                Log::info("Update Order Status Job: Atualizado o pedido ({$this->order->id}) com code_erp ({$this->order->code})", [
                    'code_erp' => $this->order->code ?? 'não informado',
                    'order' => $this->order->toArray(),
                ]);

                // Manda o email conforme se o status do pedido for CANCELLED.
                if ($this->order->isCancelled()) {
                    MailOrderByStatusJob::dispatch(
                        $this->order->id, $currentStatus
                    );
                }

                return;
            }
        }

        // Executa este processo quando a requisição foi concluída com sucesso.
        $orderCodeErp = $data->get('numeroERP');

        // Atualiza o pedido com a resposta do Portal Antares.
        $this->order->update([
            'code_erp' => $orderCodeErp,
            'status' => OrderStatus::SENT,
            'system_notes' => OrderSystemNote::ANTARES_PENDING,
        ]);

        // Recarrega o pedido.
        $this->order->refresh();

        Log::info("Process Order Request Job: Atualizou o pedido ({$this->order->id})", [
            'code_erp' => $orderCodeErp,
            'order' => $this->order->toArray(),
        ]);

        // Envia o e-mail de notificação.
        MailOrderByStatusJob::dispatch($this->order->id);
    }
}
