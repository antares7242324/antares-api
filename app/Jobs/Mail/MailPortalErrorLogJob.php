<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use App\Mail\PortalErrorLogMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class MailPortalErrorLogJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $data){}

    public function uniqueId(): string
    {
        return "mail-portal-error-log-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $loggerEmail = config('antares.logger_email');

        if (config('antares.logger') && $loggerEmail !== null) {
            Mail::to($loggerEmail)
                ->send(new PortalErrorLogMail($this->data));
        }
    }
}
