<?php

namespace App\Jobs\Mail;

use App\Mail\InvoiceRejectedMail;
use App\Models\Hunt\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class MailInvoiceRejectedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?Invoice $invoice;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $invoiceId)
    {
        $this->invoice = Invoice::findOrFail($invoiceId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->invoice->user->email;

        Mail::to($email)
            ->send(new InvoiceRejectedMail($this->invoice));
    }
}
