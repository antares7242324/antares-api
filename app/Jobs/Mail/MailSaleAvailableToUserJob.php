<?php

namespace App\Jobs\Mail;

use App\Models\Sale\Sale;
use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Mail\SaleAvailableMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class MailSaleAvailableToUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected int $saleId, protected int $userId){}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sale = Sale::findOrFail($this->saleId);

        $user = User::findOrFail($this->userId);

        Mail::to($user->email)->send(new SaleAvailableMail($sale, $user));
    }
}
