<?php

namespace App\Jobs\Mail;

use App\Models\Order\Order;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Commands\Mailer\MailOrderByStatus;

class MailOrderByStatusJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Order $order;
    protected ?string $status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $id, string $status = null)
    {
        $this->order = Order::findOrFail($id);

        $this->status = $status;
    }

    public function uniqueId(): string
    {
        return "mail-order-by-status-" . Str::uuid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailOrderByStatus $mailOrderByStatus)
    {
        $mailOrderByStatus->handle($this->order, $this->status);
    }
}
