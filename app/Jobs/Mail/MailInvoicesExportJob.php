<?php

namespace App\Jobs\Mail;

use App\Models\Account\User;
use Illuminate\Bus\Queueable;
use App\Mail\InvoiceExportMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class MailInvoicesExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $userId, protected string $filename)
    {
        $this->user = User::findOrFail($userId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->email)
            ->send(new InvoiceExportMail($this->user, $this->filename));
    }
}
