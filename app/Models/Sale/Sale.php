<?php

namespace App\Models\Sale;

use Carbon\Carbon;
use App\Models\Account\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sale extends Model
{
    use HasFactory, SoftDeletes;

    const TABLE = 'sales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'discount',
        'start_at',
        'end_at',
    ];

    /**
     * Relationship to model User.
     * @see \App\Models\Account\User
     */
    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    /**
     * Recupera o desconto.
     */
    public function discount()
    {
        return $this->discount * 100;
    }

    /**
     * Recupera a data inicial no formato Carbon.
     */
    public function startAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->start_at);
    }

    /**
     * Recupera a data final no formato Carbon.
     */
    public function endAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->end_at);
    }

    public function isOpen()
    {
        return Carbon::now()->isBetween($this->startAt(), $this->endAt());
    }

    public function isClosed()
    {
        return Carbon::now() > $this->endAt();
    }
}
