<?php

namespace App\Models\Account;

use App\Models\Product\UserItem;
use App\Models\Sale\Sale;
use App\Libraries\UserRole;
use App\Traits\HasUserRole;
use App\Models\Hunt\Invoice;
use App\Libraries\UserStatus;
use App\Models\Product\PriceList;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use App\Models\Transaction\DebitOrder;
use App\Actions\Commands\GetNumberTier;
use App\Models\Transaction\Compensation;
use Illuminate\Notifications\Notifiable;
use App\Models\Transaction\CreditInvoice;
use App\Actions\Commands\GetNextNumberTier;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Actions\Commands\Discount\ListExtraDiscounts;
use App\Models\Order\Order;
use App\Models\Transaction\CreditCanceledOrder;
use App\Models\Transaction\DebitCanceledInvoice;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use HasUserRole;

    const TABLE = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'role',
        'name',
        'email',
        'code_erp',
        'current_discount',
        'average_discount',
        'price_list_id',
        'status',
        'last_status',
        'available_balance',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Relationship to model Store.
     * @see \App\Models\Account\Store
     */
    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    /**
     * Search the active store.
     * OBS: Este método existe apenas por enquanto que o user só pode ter 1 loja.
     * @return \App\Models\Account\Store
     */
    public function activeStore()
    {
        return $this->stores()->latest()->first();
    }

    /**
     * Relationship to model Sale.
     * @see \App\Models\Sale\Sale
     */
    public function sales()
    {
        return $this->belongsToMany(Sale::class)
            ->withTimestamps();
    }

    /**
     * Relationship to model PriceList.
     * @see \App\Models\Product\PriceList
     */
    public function priceList()
    {
        return $this->belongsTo(PriceList::class);
    }

    /**
     * Relationship to model UserItem.
     * @see \App\Models\Product\UserItem
     */
    public function userItems()
    {
        return $this->hasMany(UserItem::class);
    }

    public function latestUpdatedUserItems()
    {
        return $this->userItems()->latest('updated_at');
    }

    /**
     * Relationship to model Invoice.
     * @see \App\Models\Hunt\Invoice
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * Relationship to model DebitOrder.
     * @see \App\Models\Transaction\DebitOrder
     */
    public function debitOrders()
    {
        return $this->hasMany(DebitOrder::class);
    }

    public function creditCanceledOrders(): HasMany
    {
        return $this->hasMany(CreditCanceledOrder::class);
    }

    public function debitCanceledInvoices(): HasMany
    {
        return $this->hasMany(DebitCanceledInvoice::class);
    }

    /**
     * Relationship to model CreditInvoice.
     * @see \App\Models\Transaction\CreditInvoice
     */
    public function creditInvoices()
    {
        return $this->hasMany(CreditInvoice::class);
    }

    /**
     * Relacionamento com model Compensation.
     * @see \App\Models\Transaction\Compensation
     */
    public function compensations()
    {
        return $this->hasMany(Compensation::class);
    }

    /**
     * Relacionamento com model Order.
     * @see \App\Models\Order\Order
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Get an user's first name.
     * @return string|null
     */
    public function firstName()
    {
        if ($this->name == '' || $this->name === null) return $this->name;

        return collect(explode(' ', $this->name))->first();
    }

    /**
     * Check if an user is active.
     * @return boolean
     */
    public function isActive()
    {
        return $this->status == UserStatus::ACTIVE;
    }

    /**
     * Obtem o nome do acesso.
     * @return string
     *  Se for client ou logistics, retorna o nome do campo role.
     *  Se for qualquer outro, será considerado como admin.
     */
    public function accessName()
    {
        if ($this->isClient() || $this->isLogistics())
            return $this->role;

        return UserRole::ADMIN;
    }

    /**
     * Set a new user status and store the old status.
     * @param string $status
     * @return \App\Models\Account\User
     */
    public function setStatus(string $status)
    {
        $this->last_status = $this->status;
        $this->status = $status;
        return $this;
    }

    /**
     * Set a new user password.
     * @param string $password
     * @return \App\Models\Account\User
     */
    public function setPassword(string $password)
    {
        $this->forceFill([
            'password' => Hash::make($password),
        ]);
        return $this;
    }

    /**
     * Soma todos os créditos vinculados ao usuário.
     *
     * @return int O somatório de todos os valores.
     */
    public function sumCredits()
    {
        $creditInvoices = $this->creditInvoices->sum('amount');

        $creditCompensations = $this->compensations
            ->where('amount', '>', 0)
            ->sum('amount');

        $creditCanceledOrders = $this->creditCanceledOrders->sum('amount');

        $total = $creditInvoices + abs($creditCompensations) + $creditCanceledOrders;

        return $total;
    }

    /**
     * Calcula a quantidade de créditos.
     *
     * @return int
     */
    public function countInvoiceCredits()
    {
        return $this->creditInvoices->count();
    }

    /**
     * Soma todos os débitos vinculados ao usuário.
     *
     * @return int O somatório de todos os valores.
     */
    public function sumDebits()
    {
        $debitOrders = $this->debitOrders->sum('amount');

        $debitCompensations = $this->compensations
            ->where('amount', '<', 0)
            ->sum('amount');

        $debitCanceledInvoices = $this->debitCanceledInvoices->sum('amount');

        $total = $debitOrders + abs($debitCompensations) + $debitCanceledInvoices;

        return $total;
    }

    /**
     * Calcula a quantidade de débitos.
     *
     * @return int
     */
    public function countOrderDebits()
    {
        return $this->debitOrders->count();
    }

    public function savedAmount()
    {
        return $this->debitOrders->sum('amount');
    }

    /**
     * Recupera o desconto efetivo definido pelo administrador para o distribuidor. Este desconto é baseado no desconto médio do usuário.
     */
    public function currentDiscount()
    {
        return $this->current_discount * 100;
    }

    /**
     * Recupera a média dos descontos do distribuidor baseados nas notas fiscais adicionadas.
     */
    public function averageDiscount()
    {
        return intval($this->average_discount * 100);
    }

    public function discountTier()
    {
        $extraDiscounts = new ListExtraDiscounts;

        $getNumberTier = new GetNumberTier;

        return $getNumberTier->handle($extraDiscounts->handle(), $this->average_discount);
    }

    public function nextDiscountTier()
    {
        $extraDiscounts = new ListExtraDiscounts;

        $getNextNumberTier = new GetNextNumberTier;

        return $getNextNumberTier->handle($extraDiscounts->handle(), $this->discountTier());
    }

    /**
     * Informa se pode atualizar a lista de preços vinculada ao usuário.
     *
     * @return boolean
     *   TRUE se pode, FALSE caso contrário.
     */
    public function canUpdatePriceList()
    {
        // Não pode ser já estiver vinculada.
        if (! $this->priceList()->exists()) return false;

        // Não pode se não tiver o código ERP vinculado.
        if (empty($this->code_erp)) return false;

        // Não pode se não tiver lojas vinculadas.
        if (! $this->stores()->exists()) return false;

        // Não pode ser o usuário ainda não estiver ativo.
        if (! $this->isActive()) return false;

        return true;
    }

    /**
     * Retorna a combinação do código ERP do usuário e o código ERP da sua primeira loja.
     *
     * @return string|null
     *   Se não houver código ERP, retorna null.
     */
    // TODO: Verificar se este método ainda faz sentido.
    public function portalCode()
    {
        if ($this->canUpdatePriceList() === false) return null;

        // Por enquanto valida apenas a primeira loja.
        $store = $this->stores()->first();

        if (is_null($store->code_erp)) return null;

        return $this->code_erp . $store->code_erp;
    }

    public function completeCodeErp(): string|null
    {
        $store = $this->activeStore();

        if (is_null($store)) return null;

        return $this->code_erp . $store->code_erp;
    }
}