<?php

namespace App\Models\Hunt;

use App\Traits\Currency;
use App\Models\Product\Item;
use App\Models\Product\UserItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InvoiceItem extends Model
{
    use HasFactory, Currency;

    const TABLE = 'invoice_item';

    protected $table = self::TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'invoice_id',
        'item_id',
        'quantity',
        'unit_price',
        'refund',
        'discount',
        'hunt_discount',
        'unit_cost',
        'subtotal',
        'description',
        'code',
        'user_item_id',
    ];

    /**
     * Relationship to model Invoice.
     * @see \App\Models\Hunt\Invoice
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * Relationship to model Item.
     * @see \App\Models\Product\Item
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function userItem(): BelongsTo
    {
        return $this->belongsTo(UserItem::class, 'user_item_id', 'id');
    }

    /**
     * Recupera o desconto dado pelo distribuidor no formato para leitura.
     */
    public function discount()
    {
        return $this->discount * 100;
    }

    /**
     * Recupera o desconto obtido pela caçada no formato para leitura.
     */
    public function huntDiscount()
    {
        return $this->hunt_discount * 100;
    }

    /**
     * Recupera o somatório do valor de custo pela quantidade dos itens.
     *
     * @return int
     */
    public function costSubtotal()
    {
        return $this->unit_cost * $this->quantity;
    }

    /**
     * Recupera o custo efetivo de um item, que é o valor subtotal de custo da quantidade de itens subtraído pelo valor de estorno baseado no desconto da caçada.
     *
     * @return int O valor de custo efetivo.
     */
    public function effectiveCost()
    {
        return $this->costSubtotal() - $this->refund;
    }
}
