<?php

namespace App\Models\Hunt;

use App\Traits\Currency;
use App\Models\File\Media;
use App\Models\Account\User;
use Illuminate\Support\Carbon;
use App\Libraries\InvoiceStatus;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction\CreditInvoice;
use App\Models\Transaction\DebitCanceledInvoice;
use App\Support\Readables\InvoiceStatusReadable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoice extends Model
{
    use HasFactory;
    use Currency;

    const TABLE = 'invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'status',
        'user_id',
        'media_id',
        'amount',
        'code',
        'number',
        'series',
        'cnae',
        'uf',
        'municipality',
        'issuance_at',
    ];

    /**
     * Relacionamento com model User.
     * @see \App\Models\Account\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacionamento com model InvoiceItem.
     * @see \App\Models\Hunt\InvoiceItem
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(InvoiceItem::class, 'invoice_id', 'id');
    }

    /**
     * Relacionamento com model Media.
     * @see \App\Models\File\Media
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function media(): BelongsTo
    {
        return $this->belongsTo(Media::class);
    }

    /**
     * Relacionamento com model CreditInvoice.
     * @see \App\Models\Transaction\CreditInvoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function credits(): HasMany
    {
        return $this->hasMany(CreditInvoice::class);
    }

    public function debits(): HasMany
    {
        return $this->hasMany(DebitCanceledInvoice::class);
    }

    public function lastCredit()
    {
        return $this->credits()->latest()->first();
    }

    public function lastDebit()
    {
        return $this->debits()->latest()->first();
    }

    /**
     * Verifica se Invoice está pendente.
     *
     * @return bool
     *   True se sim. False se não.
     * @see \App\Libraries\InvoiceStatus
     */
    public function isPending(): bool
    {
        return $this->status === InvoiceStatus::PENDING;
    }

    public function isNotPending(): bool
    {
        return !$this->isPending();
    }

    /**
     * Verifica se Invoice está aprovada.
     *
     * @return bool
     *   True se sim. False se não.
     * @see \App\Libraries\InvoiceStatus
     */
    public function isApproved(): bool
    {
        return $this->status === InvoiceStatus::APPROVED;
    }

    public function isNotApproved(): bool
    {
        return !$this->isApproved();
    }

    /**
     * Verifica se Invoice está rejeitada.
     *
     * @return bool
     *   True se sim. False se não.
     * @see \App\Libraries\InvoiceStatus
     */
    public function isRejected(): bool
    {
        return $this->status === InvoiceStatus::REJECTED;
    }

    public function isNotRejected(): bool
    {
        return !$this->isRejected();
    }

    public function isReopened(): bool
    {
        return $this->status === InvoiceStatus::REOPENED;
    }

    public function isNotReopened(): bool
    {
        return !$this->isReopened();
    }

    /**
     * Recupera o status da nota formatado para leitura.
     *
     * @return string
     */
    public function readableStatus(): string
    {
        return InvoiceStatusReadable::toReadable($this->status);
    }

    public function refundValue()
    {
        if ($this->items->isEmpty()) return 0;

        return $this->items()->sum('refund');
    }

    public function creditAmount()
    {
        $credit = $this->lastCredit();

        if (is_null($credit)) return 0;

        return $credit->amount;
    }

    public function finalBalanceAmount()
    {
        $credit = $this->lastCredit();

        if (is_null($credit)) return $this->user->current_balance;

        return $credit->finalBalance();
    }

    public function issuanceAt()
    {
        if (is_null($this->issuance_at)) return null;

        return Carbon::parse($this->issuance_at);
    }
}
