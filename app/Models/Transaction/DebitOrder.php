<?php

namespace App\Models\Transaction;

use App\Models\Account\User;
use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DebitOrder extends Model
{
    use HasFactory;

    const TABLE = 'debit_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'order_id',
        'amount',
        'current_balance',
    ];

    /**
     * Relacionamento com model User.
     * @see \App\Models\Account\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacionamento com model Order.
     * @see \App\Models\Order\Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Calcula o saldo final subtraindo o valor de débito do saldo atual.
     *
     * @return int
     *   O saldo final depois do débito do pedido.
     */
    public function finalBalance()
    {
        return $this->current_balance - $this->amount;
    }
}
