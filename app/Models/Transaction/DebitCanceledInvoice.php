<?php

namespace App\Models\Transaction;

use App\Libraries\DebitCanceledType;
use App\Traits\Currency;
use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DebitCanceledInvoice extends Model
{
    use HasFactory;
    use Currency;

    const TABLE = 'debit_canceled_invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'invoice_id',
        'user_id',
        'amount',
        'current_balance',
        'type',
    ];

    /**
     * Relationship to model User.
     * @see \App\Models\Account\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacionamento com model Invoice.
     * @see \App\Models\Hunt\Invoice
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * Calcula o saldo final adicionando o valor de crédito no saldo atual.
     *
     * @return int
     *   O saldo final depois do crédito da nota fiscal.
     */
    public function finalBalance()
    {
        return $this->current_balance + $this->amount;
    }

    public function byAdmin()
    {
        return $this->type === DebitCanceledType::ADMIN;
    }

    public function notByAdmin()
    {
        return ! $this->byAdmin();
    }
}
