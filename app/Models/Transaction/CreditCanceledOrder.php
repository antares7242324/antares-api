<?php

namespace App\Models\Transaction;

use App\Traits\Currency;
use App\Models\Order\Order;
use App\Models\Account\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CreditCanceledOrder extends Model
{
    use HasFactory;
    use Currency;

    const TABLE = 'credit_canceled_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'order_id',
        'user_id',
        'amount',
        'current_balance',
    ];

    /**
     * Relationship to model User.
     * @see \App\Models\Account\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacionamento com model Order.
     * @see \App\Models\Order\Order
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Calcula o saldo final adicionando o valor de crédito no saldo atual.
     *
     * @return int
     *   O saldo final depois do crédito da nota fiscal.
     */
    public function finalBalance()
    {
        return $this->current_balance + $this->amount;
    }
}
