<?php

namespace App\Models;

use App\Models\Account\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Actions\Commands\Activity\BuildActivityResource;

class Activity extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TABLE = 'activities';

    protected $fillable = [
        'action',
        'responsible_id',
        'restriction',
        'loggable_type',
        'loggable_id',
        'user_id',
        'data',
    ];

    /**
     * Relacionamento com model User para obter o responsável.
     * @see \App\Models\Account\User
     */
    public function responsible()
    {
        return $this->belongsTo(User::class, 'responsible_id');
    }

    /**
     * Relacionamento com model User.
     * @see \App\Models\Account\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function log()
    {
        return (new BuildActivityResource)->handle($this);
    }
}
