<?php

namespace App\Models\Order;

use Carbon\Carbon;
use App\Traits\Currency;
use App\Models\Order\Order;
use App\Models\Product\Item;
use App\Models\Product\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderItem extends Model
{
    use HasFactory;

    use Currency;

    const TABLE = 'order_item';

    protected $table = self::TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'order_id',
        'item_id',
        'price_id',
        'quantity',
        'unit_original_price',
        'unit_discount_price',
        'subtotal',
        'discount',
        'debit',
        'delivery_at',
    ];

    /**
     * Relacionamento com o model Order.
     * @see \App\Models\Order\Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Relacionamento com o model Item.
     * @see \App\Models\Product\Item
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * Relacionamento com o model Price.
     * @see \App\Models\Product\Price
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price()
    {
        return $this->belongsTo(Price::class);
    }

    /**
     * Recupera o valor total do preço original da unidade multiplicada pela quantidade dos itens no pedido.
     *
     * @return int
     *   O subtotal baseado no preço original.
     */
    public function originalSubtotal()
    {
        return $this->unit_original_price * $this->quantity;
    }

    public function discountSubtotal()
    {
        return $this->unit_discount_price * $this->quantity;
    }

    /**
     * Recupera a data de entrega como objeto Carbon.
     *
     * @return \Carbon\Carbon|bool
     *   O objeto da data.
     */
    public function deliveryAt()
    {
        return Carbon::createFromFormat('Y-m-d', $this->delivery_at);
    }

    /**
     * Demonstra o valor do desconto no formato de porcentagem.
     *
     * @return string
     */
    public function readableDiscount()
    {
        $number = $this->discount * 100;

        if ($number < 1) return '0';

        $readableNumber = number_format($this->discount * 100, 2);

        if (strpos($readableNumber, '.') !== false)
            $readableNumber = rtrim($readableNumber, '0');

        return $readableNumber;
    }

    /**
     * Demonstra o valor do desconto no formato de porcentagem.
     *
     * @return int
     */
    public function discount()
    {
        return (int) $this->discount * 100;
    }

    public function discountedValue()
    {
        return $this->unit_original_price - $this->unit_discount_price;
    }

    public function unitValue()
    {
        $unitValue = $this->unit_discount_price > 0
            ? $this->unit_discount_price / 100
            : 0.00;

        return $unitValue;
    }
}
