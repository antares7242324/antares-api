<?php

namespace App\Models\Order;

use App\Traits\Currency;
use App\Models\Sale\Sale;
use App\Mail\OrderSentMail;
use App\Models\Account\User;
use App\Models\Account\Store;
use App\Libraries\OrderStatus;
use App\Models\Order\OrderItem;
use App\Mail\OrderCancelledMail;
use App\Mail\OrderCompletedMail;
use App\Mail\OrderRequestedMail;
use App\Models\Product\PriceList;
use App\Models\Transaction\DebitOrder;
use Illuminate\Database\Eloquent\Model;
use App\Support\Readables\OrderStatusReadable;
use App\Models\Transaction\CreditCanceledOrder;
use App\Support\Readables\PaymentMethodReadable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;
    use Currency;

    const TABLE = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'status',
        'code_erp',
        'user_id',
        'store_id',
        'price_list_id',
        'current_discount',
        'debit',
        'discount',
        'sale_id',
        'sale_discount',
        'payment_method',
        'shipping',
        'description',
        'issuance_at',
        'system_notes',
    ];

    protected $casts = [
        'issuance_at' => 'datetime',
    ];

    /**
     * Relacionamento com model User.
     * @see \App\Models\Account\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacionamento com model Store.
     * @see \App\Models\Account\Store
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Relacionamento com model PriceList.
     * @see \App\Models\Product\PriceList
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function priceList(): BelongsTo
    {
        return $this->belongsTo(PriceList::class);
    }

    /**
     * Relacionamento com o model OrderItem.
     * @see \App\Models\Product\OrderItem
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    /**
     * Relacionamento com model DebitOrder.
     * @see \App\Models\Transaction\DebitOrder
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function debitTransaction(): HasOne
    {
        return $this->hasOne(DebitOrder::class);
    }

    public function creditTransaction(): HasOne
    {
        return $this->hasOne(CreditCanceledOrder::class);
    }

    public function sale(): BelongsTo
    {
        return $this->belongsTo(Sale::class);
    }

    /**
     * Verifica se o pedido é um rascunho.
     *
     * @return bool
     */
    public function isDraft()
    {
        return $this->status == OrderStatus::DRAFT;
    }

    /**
     * Verifica se o pedido foi solicitado.
     *
     * @return bool
     */
    public function isRequested()
    {
        return $this->status == OrderStatus::REQUESTED;
    }

    public function isNotRequested()
    {
        return !$this->isRequested();
    }

    /**
     * Verifica se o pedido foi encaminhado.
     *
     * @return bool
     */
    public function isSent()
    {
        return $this->status == OrderStatus::SENT;
    }

    /**
     * Verifica se o pedido foi concluído.
     *
     * @return bool
     */
    public function isCompleted()
    {
        return $this->status == OrderStatus::COMPLETED;
    }

    /**
     * Verifica se o pedido foi cancelado.
     *
     * @return bool
     */
    public function isCancelled()
    {
        return $this->status == OrderStatus::CANCELLED;
    }

    /**
     * Recupera o valor total de todos os itens no pedido baseado no preço descontado dos itens.
     *
     * @return int O valor somado de todos os itens.
     */
    public function subtotal()
    {
        return $this->items->sum(fn($item) => $item->subtotal);
    }

    /**
     * Recupera o valor total de todos os itens no pedido baseado no preço original dos itens.
     *
     * @return int O valor somado de todos os itens.
     */
    public function originalSubtotal()
    {
        return $this->items->sum(fn($item) => $item->unit_original_price * $item->quantity);
    }

    /**
     * Recupera o método de pagamento formatado para leitura.
     *
     * @return string
     */
    public function paymentMethod()
    {
        return PaymentMethodReadable::toReadable($this->payment_method);
    }

    /**
     * Recupera o status do pedido formatado para leitura.
     *
     * @return string
     */
    public function readableStatus()
    {
        return OrderStatusReadable::toReadable($this->status);
    }

    /**
     * Recupera o valor do débito.
     *
     * @return int
     */
    public function debitAmount()
    {
        $debit = $this->debitTransaction;

        if (is_null($debit)) return 0;

        return $debit->amount;
    }

    /**
     * Recupera o saldo atual do usuário no momento do pedido.
     *
     * @return int
     */
    public function currentBalanceAmount()
    {
        $debit = $this->debitTransaction;

        if (is_null($debit)) return $this->user->current_balance;

        return $debit->current_balance;
    }

    public function finalBalanceAmount()
    {
        $debit = $this->debitTransaction;

        if (is_null($debit)) return $this->user->current_balance;

        return $debit->finalBalance();
    }

    /**
     * Cria o objeto de email com base no status do pedido.
     *
     * @param string $status
     *   O status em que deve ser enviado o email.
     * @return \Illuminate\Mail\Mailable|null
     *   Objeto de email ou NULL se o status
     *   informado não for o mesmo do pedido.
     */
    public function mailByStatus(string $status)
    {
        if ($status == OrderStatus::REQUESTED)
            return new OrderRequestedMail($this);

        if ($status == OrderStatus::SENT)
            return new OrderSentMail($this);

        if ($status == OrderStatus::COMPLETED)
            return new OrderCompletedMail($this);

        if ($status == OrderStatus::CANCELLED)
            return new OrderCancelledMail($this);

        return null;
    }

    public function discountValue()
    {
        $discountValue = $this->debit > 0
            ? $this->debit / 100
            : 0.00;

        return $discountValue;
    }

    public function discountPercent()
    {
        $discountPercent = $this->discount * 100;

        return $discountPercent;
    }
}
