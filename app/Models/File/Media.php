<?php

namespace App\Models\File;

use App\Models\Hunt\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Media extends Model
{
    use HasFactory;

    const TABLE = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'extension',
        'type',
        'storage',
        'path',
    ];

    /**
     * Relationship to model Invoice.
     * @see \App\Models\Hunt\Invoice
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function path()
    {
        return Storage::disk($this->storage)->url($this->path);
    }
}
