<?php

namespace App\Models;

use App\Libraries\TokenAbility;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Sanctum\PersonalAccessToken as SanctumPersonalAccessToken;

class PersonalAccessToken extends SanctumPersonalAccessToken
{
    use HasFactory;

    public function activation()
    {
        if ($this->cant(TokenAbility::ACTIVATE) || $this->expires_at < Carbon::now()) {
            return false;
        }

        return true;
    }

    public function recovering()
    {
        if ($this->cant(TokenAbility::RECOVER) || $this->expires_at < Carbon::now()) {
            return false;
        }

        return true;
    }
}
