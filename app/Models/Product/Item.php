<?php

namespace App\Models\Product;

use App\Models\Order\Order;
use App\Models\Order\OrderItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Item extends Model
{
    use HasFactory, SoftDeletes;

    const TABLE = 'items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'code',
        'hunt',
    ];

    /**
     * Relacionamento com o model PriceList.
     * @see \App\Models\Product\PriceList
     */
    public function priceLists()
    {
        return $this->belongsToMany(PriceList::class, Price::TABLE, 'item_id', 'price_list_id')
            ->withPivot('amount', 'start_at', 'end_at', 'last_update_at')
            ->withTimestamps();
    }

    /**
     * Relacionamento com o model Price.
     * @see \App\Models\Product\Price
     */
    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    /**
     * Relacionamento com o model OrderItem.
     * @see \App\Models\Product\OrderItem
     */
    public function orders()
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * Busca o preço de uma tabela de preços específica.
     *
     * @param int $priceListId
     *   O ID do model PriceList, que é a tabela de preços.
     *   @see \App\Models\Product\PriceList
     * @return \App\Models\Product\Price
     *   O objeto Price.
     */
    public function priceFromList(int $priceListId)
    {
        return $this->prices->where('price_list_id', $priceListId)->first();
    }
}
