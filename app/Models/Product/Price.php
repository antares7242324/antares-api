<?php

namespace App\Models\Product;

use App\Traits\Currency;
use App\Models\Account\User;
use App\Models\Order\OrderItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Price extends Model
{
    use HasFactory;
    use Currency;

    const TABLE = 'prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'price_list_id',
        'item_id',
        'amount',
        'original_amount',
        'start_at',
        'end_at',
        'last_update_at',
    ];

    /**
     * Relacionamento com model Item.
     * @see \App\Models\Product\Item
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * Relacionamento com model PriceList.
     * @see \App\Models\Product\PriceList
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function priceList()
    {
        return $this->belongsTo(PriceList::class);
    }

    /**
     * Relacionamento com model OrderItem.
     * @see \App\Models\Order\OrderItem
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderItem()
    {
        return $this->hasMany(OrderItem::class, 'price_id');
    }
}
