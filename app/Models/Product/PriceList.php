<?php

namespace App\Models\Product;

use App\Models\Order\Order;
use App\Models\Account\User;
use App\Models\Product\Item;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PriceList extends Model
{
    use HasFactory;

    const TABLE = 'price_lists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'active',
        'name',
        'code_erp',
        'start_at',
        'end_at',
        'last_updated_at',
    ];

    /**
     * Relationship to model User.
     * @see \App\Models\Account\User
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Relationship to model Item.
     * @see \App\Models\Product\Item
     */
    public function items()
    {
        return $this->belongsToMany(Item::class, Price::TABLE, 'price_list_id', 'item_id')
            ->withPivot('id', 'amount', 'start_at', 'end_at', 'last_update_at')
            ->withTimestamps();
    }

    public function isActive()
    {
        return $this->active;
    }

    public function isNotActive()
    {
        return ! $this->active;
    }

    public function hasStarted()
    {
        $startAt = Carbon::parse($this->start_at);

        return $startAt <= Carbon::now();
    }

    public function hasNotStarted()
    {
        return ! $this->hasStarted();
    }

    public function hasEnded()
    {
        if (is_null($this->end_at)) return false;

        $endAt = Carbon::parse($this->end_at);

        return $endAt < Carbon::now();
    }

    public function hasNotEnded()
    {
        return ! $this->hasEnded();
    }
}
