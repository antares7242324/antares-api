<?php

namespace App\Models\Product;

use App\Models\Account\User;
use App\Models\Hunt\Invoice;
use App\Models\Hunt\InvoiceItem;
use App\Models\Product\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class UserItem extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TABLE = 'user_items';

    protected $fillable = [
        'user_id',
        'item_id',
        'confirmed',
        'code',
        'name',
    ];

    protected $casts = [
        'confirmed' => 'boolean',
    ];

    /**
     * Relacionamento com model User.
     * @see \App\Models\Account\User
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relacionamento com model Item.
     * @see \App\Models\Product\Item
     */
    public function item(): BelongsTo
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * Relacionamento com model InvoiceItem.
     * @see \App\Models\Hunt\InvoiceItem
     */
    public function invoices(): HasManyThrough
    {
        return $this->hasManyThrough(
            Invoice::class,
            InvoiceItem::class,
            'user_item_id',
            'id',
            'id',
            'invoice_id'
        );
    }
}
