<?php

namespace App\Traits;

use App\Libraries\UserRole;

trait HasUserRole
{
   public function isAdmin()
   {
      return $this->role == UserRole::ADMIN;
   }

   public function isClient()
   {
      return $this->role == UserRole::CLIENT;
   }

   public function isDev()
   {
      return $this->role == UserRole::DEV;
   }

   public function isLogistics()
   {
      return $this->role == UserRole::LOGISTICS;
   }

   public function isMeeg()
   {
      return $this->role == UserRole::MEEG;
   }
}