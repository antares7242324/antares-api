<?php

namespace App\Traits\Portal;

use Illuminate\Support\Carbon;

trait DateConversionPortal
{
    public function toCarbon(string $date)
    {
        // Try to parse the date with the format 'd/m/Y'
        $carbonDate = Carbon::createFromFormat('d/m/Y', $date);

        // Check if the date is valid
        if ($date == $carbonDate->format('d/m/Y')) {
            return $carbonDate;
        }

        // Try to parse the date with the format 'd/m/y'
        $carbonDate = Carbon::createFromFormat('d/m/y', $date);

        // Check if the date is valid
        if ($date == $carbonDate->format('d/m/y')) {
            return $carbonDate;
        }

        return false;
    }
}