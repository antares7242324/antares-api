<?php

namespace App\Traits;

trait Currency
{
    /**
     * Converte o dinheiro em centavos Brasileiros para formato monetário.
     *
     * @param integer $amount O valor para converter em centavos.
     * @return string A representação da monetária.
     */
    public function toCurrency(int $cents = null)
    {
        if (is_null($cents)) return '0';

        return number_format($cents / 100, 2, ',', '.');
    }

    /**
     * Adicionar um desconto ao valor em centavos baseado num percentual.
     *
     * @param int $cents O valor em centavos.
     * @param float $percent O valor percentual para descontar.
     * @return int O valor final descontado em centavos.
     */
    public function toDiscount(int $cents, float $percent)
    {
        $discount = intval($cents * $percent);

        $discounted = $cents - $discount;

        return (int) $discounted;
    }

    /**
     * Converter um valor em formato monetário para centavos Brasileiros.
     *
     * @param string $amount O valor no formato monetário '1234.56'
     * @return int O valor final em centavos.
     */
    public function toCents(string $amount)
    {
        // Removendo separdor de milhar (vírgula).
        $strippedAmount = str_replace(',', '', $amount);

        // Separando o inteiro do decimal.
        $amountParts = collect(explode('.', $strippedAmount));

        // Extraindo apenas os dois primeiros valores da parte decimal.
        $decimalPart = substr($amountParts->last(), 0, 2);

        // Concatena as duas partes.
        $finalAmount = $amountParts->first() . $decimalPart;

        return (int) $finalAmount;
    }

    public function roundDecimalPercent(float $value)
    {
        $value *= 100;

        $rounded = round($value);

        return $rounded / 100;
    }
}