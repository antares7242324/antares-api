<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait RequestParamsTrait
{
    public function requestHasInclude(string $name, ?Request $request = null)
    {
        if (is_null($request)) $request = request();

        if ($request->has('include') === false) return false;

        $include = str_replace(' ', '', $request->input('include'));

        return in_array($name, explode(',', $include));
    }

    public function requestIncludeArray(?Request $request = null)
    {
        if (is_null($request)) $request = request();

        if ($request->has('include') === false) return [];

        $include = str_replace(' ', '', $request->input('include'));

        return explode(',', $include);
    }

    public function requestHasFilter(string $name, ?Request $request = null)
    {
        if (is_null($request)) $request = request();

        if (is_null($request->input("filter.{$name}"))) return false;

        return true;
    }

    public function requestFilterArray(string $name, ?Request $request = null)
    {
        if (is_null($request)) $request = request();

        $filter = str_replace(' ', '', $request->input("filter.{$name}"));

        return explode(',', $filter);
    }
}