<?php

namespace App\Traits;

trait HttpResponses {
    protected function success($data, $message = null, $code = 200)
    {
        return response()->json([
            'data' => $data,
            'success' => true,
            'message' => $message,
        ], $code);
    }

    protected function error($data = [], $message = null, $code = 500, $errorCode = null)
    {
        $response = collect()
            ->put('data', $data)
            ->put('success', false)
            ->when(is_null($errorCode) === false, fn ($response) =>
                $response->put('error_code', $errorCode)
            )
            ->put('message', $message);

        return response()->json($response->toArray(), $code);
    }
}