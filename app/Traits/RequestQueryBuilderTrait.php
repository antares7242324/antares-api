<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Spatie\QueryBuilder\QueryBuilderRequest;

trait RequestQueryBuilderTrait
{
    /**
     * Recupera todos os includes usando o Spatie Query Builder.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function queryBuilderIncludes(Request $request): Collection
    {
        // Recupera todos os includes que foram usados.
        $includes = QueryBuilderRequest::fromRequest($request)->includes();

        return $includes;
    }
}