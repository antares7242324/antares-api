<?php

namespace App\Traits;

use App\Models\Account\User;

trait AccountTrait
{
    public function loadUserAccount(): User|null
    {
        return User::find(auth()->user()->id);
    }
}