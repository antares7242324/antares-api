<?php

namespace App\QueryBuilder\Filters;

use Carbon\Carbon;
use App\Models\Product\Price;
use App\Models\Product\PriceList;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterPriceItemIdList implements Filter
{
    protected $priceList = PriceList::TABLE;

    protected $price = Price::TABLE;

    public function __invoke(Builder $query, $value, string $property)
    {
        $now = Carbon::now();

        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);

        // Adiciona as condições para quando for ativo.
        $query->when($value, function ($query) use ($now) {
            $query->where("$this->priceList.active", true)
                ->where(function ($query) use ($now) {
                    $query->where(function ($query) use ($now) {
                            $query->where("$this->price.start_at", '<=', $now)
                                ->orWhereNull("$this->price.start_at");
                        })
                        ->where(function ($query) use ($now) {
                            $query->where("$this->price.end_at", '>=', $now)
                                ->orWhereNull("$this->price.end_at");
                        });
            });
        });

        // Adiciona as condições para quando for inativo.
        $query->when($value === false, function ($query) use ($now) {
            $query->where("$this->priceList.active", false)
                ->orWhere("$this->price.start_at", '>', $now)
                ->orWhere("$this->price.end_at", '<', $now);
        });
    }
}