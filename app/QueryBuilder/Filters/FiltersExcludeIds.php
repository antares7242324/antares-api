<?php
namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FiltersExcludeIds implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        // Quando possui uma lista de IDs.
        $query->when(is_array($value), function ($query) use ($value) {
            $ids = collect($value)->map(fn($id) => str_replace(' ', '', $id));
            $query->whereNotIn('id', $ids->toArray());
        });

        // Quando possui apenas um ID informado.
        $query->when(is_array($value) === false, function ($query) use ($value) {
            $id = collect($value);
            $query->whereNotIn('id', $id->toArray());
        });
    }
}