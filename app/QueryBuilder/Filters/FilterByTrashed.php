<?php

namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterByTrashed implements Filter
{
   const NAME = 'trashed';

   public function __invoke(Builder $query, $value, string $property)
   {
      if ((bool)$value) {
         return $query->whereNull('deleted_at');
      }

      return $query->whereNotNull('deleted_at');
   }
}