<?php

namespace App\QueryBuilder\Sorts;

use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Contracts\Database\Eloquent\Builder;

class SortByStatus implements Sort
{
   const NAME = 'status';

   public function __invoke(Builder $query, bool $descending, string $property)
   {
      $rawQuery = $descending
         ? 'ISNULL(deleted_at), deleted_at'
         : 'deleted_at, ISNULL(deleted_at) DESC';

      $query->orderByRaw($rawQuery);
   }
}