<?php

namespace App\QueryBuilder\Sorts;

use App\Models\Product\Item;
use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Contracts\Database\Eloquent\Builder;

class SortByItemName implements Sort
{
    const NAME = 'items.name';

    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'DESC' : 'ASC';

        $query->join(
            Item::TABLE,
            Item::TABLE.'.id', '=', $query->from.'.item_id'
        );

        $query->orderBy(Item::TABLE . '.name', $direction);
    }
}