<?php

namespace App\Console;

use App\Jobs\Portal\AddPriceListJob;
use App\Jobs\Portal\CheckUpdatePriceJob;
use App\Console\Commands\NotifySaleStatus;
use Illuminate\Console\Scheduling\Schedule;
use App\Jobs\Portal\CheckUpdateEveryItemJob;
use App\Jobs\Portal\CheckUpdatePriceListJob;
use App\Jobs\Portal\CheckEveryOrderStatusJob;
use App\Jobs\Portal\CheckEveryInvoiceStatusJob;
use App\Jobs\Portal\CheckEveryRequestedOrderJob;
use App\Jobs\UserPriceLists\UpdateUserPriceListJob;
use App\Jobs\Orders\CancelOrdersPriceListInactiveJob;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Envia emails para as promoções ativas.
        if (config('scheduler.notify_sale_status.enabled')) {
            Log::info("Notify Sales Status: Iniciado.");

            $schedule->command(NotifySaleStatus::class)
                ->dailyAt(config('scheduler.notify_sale_status.daily_hour'));
        }

        // Sincroniza os dados dos itens com o portal.
        if (config('scheduler.update_item.enabled')) {
            Log::info("Check Update Every Item: Iniciado.");

            $schedule->job(fn () => CheckUpdateEveryItemJob::dispatch())
                ->dailyAt(config('scheduler.update_item.daily_hour'));
        }

        // Sincroniza os dados das tabelas de preço com o portal.
        if (config('scheduler.update_list.enabled')) {
            $updatedAt = now()->subDays(1)->format('Y-m-d');

            Log::info("Check Update Price List: Iniciado.");

            $schedule->job(fn () =>
                CheckUpdatePriceListJob::dispatch($updatedAt))
                    ->dailyAt(config('scheduler.update_list.daily_hour'));
        }

        // Adiciona as novas tabelas de preço encontradas no portal.
        if (config('scheduler.add_list.enabled')) {
            Log::info("Add Price List: Iniciado.");

            $schedule->job(fn () =>
                AddPriceListJob::dispatch())
                    ->dailyAt(config('scheduler.add_list.daily_hour'));
        }

        // Sincroniza todos os preços dos itens nas tabelas do portal.
        if (config('scheduler.update_price.enabled')) {
            $updatedAt = now()->subDays(1)->format('Y-m-d');

            Log::info("Check Update Price: Iniciado.");

            $schedule->job(fn () =>
                CheckUpdatePriceJob::dispatch($updatedAt))
                    ->dailyAt(config('scheduler.update_price.daily_hour'));
        }

        // Verifica quais tabelas de preço estão desativadas para cada
        // usuário e cancela os seus pedidos em rascunho que estejam
        // vinculados a essas tabelas.
        if (config('scheduler.cancel_order_price_list_inactive.enabled')) {
            Log::info("Cancel Orders Price List Inactive: Iniciado.");

            $schedule->job(CancelOrdersPriceListInactiveJob::class)
                ->dailyAt(config('scheduler.cancel_order_price_list_inactive.daily_hour'));
        }

        // Verifica e atualiza as tabelas de preço do usuário, caso necessário.
        // Se a tabela atual for trocada, cancela todos os pedidos vinculados
        // e atribui a nova tabela de preços.
        if (config('scheduler.update_user_price_list.enabled')) {
            Log::info("Update User Price List: Iniciado.");

            $schedule->job(UpdateUserPriceListJob::class)
                ->dailyAt(config('scheduler.update_user_price_list.daily_hour'));
        }

        // Verifica e atualiza os status de todas as notas fiscais no portal.
        if (config('scheduler.update_invoice_status.enabled')) {
            Log::info("Check Every Invoice Status: Iniciado.");

            $schedule->job(CheckEveryInvoiceStatusJob::class)->everyTwoHours();
        }

        // Verifica e atualiza os status de todos os pedidos no portal.
        if (config('scheduler.update_order_status.enabled')) {
            Log::info("Check Every Order Status: Iniciado.");

            $schedule->job(CheckEveryOrderStatusJob::class)->everyTwoHours();
        }

        // Verifica quais pedidos estão pendentes e envia para o portal.
        if (config('scheduler.send_requested_order.enabled')) {
            Log::info("Check Every Requested Order Status: Iniciado.");

            $schedule->job(CheckEveryRequestedOrderJob::class)->hourly();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
