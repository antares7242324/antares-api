<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ResetDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antares:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset the database removing every record in every table.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            // Busca o nome de todas as tabelas do sistema.
            $tables = DB::connection()
                ->getDoctrineSchemaManager()
                ->listTableNames();

            // Desativa a verificação de chaves estrangeiras.
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            foreach ($tables as $table) {
                // Não pode zerar a tabela de migrations.
                if ($table !== 'migrations') {
                    DB::table($table)->truncate();
                }

                $this->info("Table {$table} truncated.");
            }

            // Reativa a verificação de chaves estrangeiras.
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            $this->info('All tables have been truncated.');
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $this->error("An error occurred while truncating tables: {$e->getMessage()}");
        }
    }
}
