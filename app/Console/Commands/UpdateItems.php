<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\Portal\CheckUpdateEveryItemJob;
use Illuminate\Support\Carbon;

class UpdateItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antares:items.update
                            {--updated= : Date in format "YYYY-MM-DD H:i:s". If not informed, it will update items from any date.}
                            {--searchkey= : Item Antares ERP key. If not informed, it will update all items from the portal.}
                            {--pagesize= : Items processed per page. If not informed, it will proccess 300 items per page.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all items from the Antares ERP Portal';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $updatedAt = $this->option('updated');

        if (is_null($updatedAt) === false)
            $updatedAt = Carbon::parse($updatedAt);

        $searchKey = $this->option('searchkey');

        $pageSize = is_null($this->option('pagesize'))
            ? 300
            : (int) $this->option('pagesize');

        try {
            CheckUpdateEveryItemJob::dispatch(
                $updatedAt, $searchKey, $pageSize
            );

            $this->info("The job has been dispatched.");

            return Command::SUCCESS;
        }
        catch (\Exception $e) {
            $this->error(
                "An error occurred while dispatching the job: {$e->getMessage()}"
            );

            return Command::FAILURE;
        }
    }
}
