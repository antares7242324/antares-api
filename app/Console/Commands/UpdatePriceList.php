<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\Portal\UpdateCreatePriceListJob;
use App\Actions\Commands\Portal\ShowPriceLists;

class UpdatePriceList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antares:prices.update
                            {--code= : The price list code from Antares ERP.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the informed price list.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $priceListCode = $this->option('code');

        if ($priceListCode) {
            UpdateCreatePriceListJob::dispatch($priceListCode);

            $this->info("Tabela de preços {$priceListCode} está sendo atualizada. Aguarde um momento.");

            return Command::SUCCESS;
        }

        $priceLists = (new ShowPriceLists)->handle();

        try {
            $priceLists->each(function ($priceList) {
                UpdateCreatePriceListJob::dispatch($priceList->codigo);

                $this->info("Tabela de preços {$priceList->codigo} está sendo atualizada. Aguarde um momento.");
            });

            return Command::SUCCESS;
        }
        catch (\Exception $e) {
            $this->error($e->getMessage());

            return Command::FAILURE;
        }
    }
}
