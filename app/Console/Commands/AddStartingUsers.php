<?php

namespace App\Console\Commands;

use App\Libraries\UserRole;
use App\Models\Account\User;
use App\Libraries\UserStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AddStartingUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antares:users.start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the starting users to the database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            // Cria o admin.
            User::create([
                'name' => 'Meeg',
                'email' => 'admin@meeg.app',
                'role' => UserRole::MEEG,
                'status' => UserStatus::ACTIVE,
                'email_verified_at' => now(),
                'password' => Hash::make('admin#1234'),
            ]);

            // Cria o distribuidor para testes.
            $clientUser = User::create([
                'name' => 'Distribuidor Teste',
                'email' => 'jean@meeg.app',
                'role' => UserRole::CLIENT,
                'status' => UserStatus::ACTIVE,
                'email_verified_at' => now(),
                'password' => Hash::make('distribuidor#1234'),
                'code_erp' => '000011',
            ]);

            $clientUser->stores()->create([
                'name' => 'Teste',
                'cnpj' => '65338741000122',
                'code_erp' => '01',
            ]);

            $this->info('Starting users created');

            return Command::SUCCESS;
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $this->error($e->getMessage());

            return Command::FAILURE;
        }
    }
}
