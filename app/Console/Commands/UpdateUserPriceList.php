<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UserPriceLists\UpdateUserPriceListJob;

class UpdateUserPriceList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antares:users.priceup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza todos os itens na tabela de preços do usuário.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UpdateUserPriceListJob::dispatch();

        $this->info("Atualizando as tabelas de preços de todos os usuários. Aguarde um momento.");
    }
}
