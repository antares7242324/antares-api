<?php

namespace App\Console\Commands;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use App\Jobs\Activities\NotifyClosedSalesJob;
use App\Jobs\Activities\NotifyOpenedSalesJob;

class NotifySaleStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antares:notify-sales {datetime?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifica o status da promoção com base na data informada. Se não for informada a data, usará a data atual.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = is_null($this->argument('datetime'))
            ? Carbon::now()
            : Carbon::parse($this->argument('datetime'));

        NotifyOpenedSalesJob::dispatch($date);

        NotifyClosedSalesJob::dispatch($date);

        return 0;
    }
}
