@include('mails.parts.email-header')
        <tr>
            <td>
                <table style="width: 100%;padding: 40px 5vw 50px 5vw;">
                    <tr>
                        <td>
                            <table style="margin: auto;">
                                <tr>
                                    <td style="margin-right: 10px;display: block;">
                                        <div style="padding: 20px;background-color: #282828;border-radius: 5px;">
                                            <img src="https://res.cloudinary.com/dqp1ooitz/image/upload/v1684149163/Antares/percent_hxxh9e.png" alt="">
                                        </div>
                                    </td>
                                    <td>
                                        <div style="padding: 20px;background-color: #282828;border-radius: 5px;">
                                            <img src="https://res.cloudinary.com/dqp1ooitz/image/upload/v1680100312/Antares/Grupo_1794_yobc7o.png" alt="">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#323232;border-radius: 0px 0px 5px 5px;">
                            <div style="text-align: center;margin-top:30px;">
                                <h1 style="color: #ffffff;font-size: 24px;margin: 0 0 20px 0;">Compre com ainda mais desconto, {{ $user->firstName() }}!</h1>
                                <p style="color: #FFFFFF;font-size: 16px;margin: 0;font-weight: 700;">Além dos seus descontos habituais, conte ainda com a promoção {{ $sale->name }} que adiciona +{{ $sale->discount() }}% de desconto extra.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;background-color: #D4D4D420;padding: 3vw 5vw 5vw 5vw;">
        <tr>
            <td>
                <p style="font-size: 16px;color: #585858;margin: 0 0 0px 0; text-align:center;"> Adicionamos um novo desconto promocional que acrescenta {{ $sale->discount() }}% em suas compras. Entre na sua conta para utilizá-lo, pois ele já está disponível.</p>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border-radius: 10px;width: 100%;border-spacing: 0px;margin-top: 30px;background-color: #FFF;margin-top: 30px;margin-bottom:30px;">
                    <tr>
                        <td style="padding-left: 20px;background-position: left center;background-size: 100% 100%;background-repeat: no-repeat;background-image: url(https://res.cloudinary.com/dqp1ooitz/image/upload/v1684152692/Antares/ticket-big_kltipr.png);">
                            <p style="text-align: left;font-size: 16px;color: #FFF;margin: 10px 0;">{{ $sale->name }}</p>
                        </td>
                        <td>
                            <h1 style="border-right: 1px solid #33333320;font-weight: 100;text-align: center;font-size: 36px;margin: 10px 0;">+{{ $sale->discount() }}% OFF</h1>
                        </td>
                        <td>
                            <p style="text-align: center;font-size: 16px;margin: 10px 0;">
                                De {{ $sale->startAt()->format('d/m/Y')}} à {{ $sale->endAt()->format('d/m/Y') }}
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0; text-align:center;">Caso deseje acompanhar seu saldo ou fazer um novo upload de nota, acesse o portal através do link abaixo. Caso não seja redirecionado para a plataforma ANTARES, copie o link abaixo e cole em seu navegador:</p>
                <a style="font-size: 16px;display: block;color: #333333;margin: 0;font-weight: 700;text-align:center;" href="{{ config('app.frontend_url') }}">{{ config('app.frontend_url') }}</a>
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')