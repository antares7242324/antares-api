@include('mails.parts.email-header')
        <tr>
            <td style="padding: 3vw">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; margin-bottom: 30; text-align: center;">
                            <h1 style="color: #ffffff;font-size: 24px;">Seu arquivo de pedidos foi gerado, {{ $user->name }}!</h1>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding: 5vw; background-color: #D4D4D420;">
        <tr>
            <td style="text-align: center;display: block;">
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0;font-weight: 600;">Seu arquivo está pronto. Você pode encontrar o seu arquivo CSV anexo nesta mensagem.</p>
                <p style="font-size: 16px;color: #585858;margin: 0;">Obrigado por usar nosso serviço.</p>
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')
