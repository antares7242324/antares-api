@include('mails.parts.email-header')
        <tr>
            <td style="padding: 5vw">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; margin-bottom: 30; text-align: center;">
                            <h1 style="color: #ffffff;font-size: 24px;margin: 0 0 30px 0;">Recupere a sua conta, {{ $user->firstName() }}!</h1>
                            <p style="color: #FFFFFF; opacity: 80%; font-size: 16px; margin-bottom: 24px; margin: 0;font-weight: 700;">Você está próximo de ter acesso à plataforma ANTARES novamente.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding: 5vw; background-color: #D4D4D420;">
        <tr>
            <td style="text-align: center;display: block;">
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0;">Você está recebendo este e-mail porque recebemos uma solicitação de redefinição de senha. Para prosseguir, clique no botão abaixo e digite sua nova senha:</p>
                <a style="font-size: 16px;margin: 0 auto 20px auto;background-color: #FECD00;text-decoration: none;padding: 20px;display: block;width: 164px;font-weight: 700;color: black;border-radius: 5px;" href="{{ config('app.frontend_url') }}/recoverAccount?token=@removeTokenIdPrefix($token->plainTextToken)">Redefinir senha</a>
                <a style="font-size: 16px;display: block;color: #333333;margin: 0 0 20px 0;font-weight: 700;" href="{{ config('app.frontend_url') }}/recoverAccount?token=@removeTokenIdPrefix($token->plainTextToken)">{{ config('app.frontend_url') }}/recoverAccount?token=@removeTokenIdPrefix($token->plainTextToken)</a>
                <p style="font-size: 16px;color: #585858;margin: 0;margin-bottom: 20px;">Caso não tenha realizado a solicitação de redefinição de senha, outra pessoa possivelmente tentando acessar a sua conta. Para sua segurança, ignore este e-mail e não o encaminhe para qualquer outra pessoa.</p>
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')