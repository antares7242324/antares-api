        @include('mails.parts.email-header')
        <tr>
            <td>
                <table style="width: 100%;margin-top: 40px;">
                    <tr>
                        <td>
                            <table style="margin: auto;">
                                <tr>
                                    <td style="margin-right: 10px;display: block;">
                                        <div style="padding: 20px;background-color: #282828;border-radius: 5px;">
                                            <img src="https://res.cloudinary.com/dqp1ooitz/image/upload/v1680036789/Antares/8686855_ic_fluent_box_regular_icon_x4eajn.png" alt="">
                                        </div>
                                    </td>
                                    <td>
                                        <div style="padding: 20px;background-color: #282828;border-radius: 5px;">
                                            <img src="https://res.cloudinary.com/dqp1ooitz/image/upload/v1680100312/Antares/Grupo_1794_yobc7o.png" alt="">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#323232;border-radius: 0px 0px 5px 5px;padding-bottom: 5vw;">
                            <div style="text-align: center;margin-top:30px;">
                                <h1 style="color: #ffffff;font-size: 24px;margin: 0 0 20px 0;">Seu saldo de cashback foi atualizado, {{ $user->firstName() }}!</h1>
                                <p style="color: #FFFFFF;font-size: 16px;margin: 0;font-weight: 700;">Agora você já pode usá-lo como desconto em seu próximo pedido realizado pelo portal.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p style="font-size: 16px;color: #585858;margin: 0;background-color: #D4D4D420;padding: 5vw 5vw 0 5vw;">Recebemos sua nota e ela foi aprovada para uso. Seu saldo de cashback já está atualizado e pode ser usado em pedidos futuros.</p>
    @include('mails.parts.invoice-table')
    <table style="width: 100%;background-color: #D4D4D420;padding: 0vw 5vw 5vw 5vw;">
        <tr>
            <td>
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0; text-align:center;">Caso deseje acompanhar o seu saldo ou fazer um novo upload de nota, acesse o portal através do link abaixo. Caso não seja redirecionado para a plataforma ANTARES, copie o link abaixo e cole em seu navegador:</p>
                <a style="font-size: 16px;display: block;color: #333333;margin: 0;font-weight: 700;text-align:center;" href="{{ config('app.frontend_url') }}">{{ config('app.frontend_url') }}</a>
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')