@include('mails.parts.email-header')
        <tr>
            <td style="padding: 3vw">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; margin-bottom: 30; text-align: center;">
                            <h1 style="color: #ffffff;font-size: 24px;">Pedido solicitado para {{ $user->firstName() }}!</h1>
                            {{-- <p style="color: #FFFFFF;font-size: 16px;margin: 0;font-weight: 700;">Você pode acompanhá-lo pelo painel ANTARES.</p> --}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p style="font-size: 16px;color: #585858;margin: 0;background-color: #D4D4D420;padding: 5vw 5vw 0 5vw;">Recebemos o pedido para {{ $user->name }} com o código ERP {{ $user->code_erp }}.</p>
    <table style="width: 100%;background-color: #D4D4D420;">
        <tr>
            <td style="padding: 40px 5vw 5vw 5vw;">
                @include('mails.parts.order-table')
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')