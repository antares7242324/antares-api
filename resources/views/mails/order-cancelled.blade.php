@include('mails.parts.email-header')
        <tr>
            <td style="padding: 3vw">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; margin-bottom: 30; text-align: center;">
                            <h1 style="color: #ffffff;font-size: 24px;">Seu pedido foi cancelado, {{ $user->firstName() }}!</h1>
                            <p style="color: #FFFFFF;font-size: 16px;margin: 0;font-weight: 700;">Todos os pedidos solicitados necessitam passar por uma aprovação. Recebemos o seu pedido, mas ele infelizmente não foi aprovado. Caso precisa de maiores informações, contete nossa equipe.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;background-color: #D4D4D420;">
        <tr>
            <td style="padding: 40px 5vw 5vw 5vw;">
                @include('mails.parts.order-table')
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')