<h1>{{ $messageTitle }}</h1>

<fieldset>
    <p><strong>Dados da resposta:</strong></p>
    <pre>{{ $response }}</pre>
</fieldset>

<fieldset>
    <p><strong>Dados da query:</strong></p>
    <pre>{{ $query }}</pre>
</fieldset>

<fieldset>
    <p><strong>Dados do body:</strong></p>
    <pre>{{ $body }}</pre>
</fieldset>

<fieldset>
    <p><strong>Dados da exceção:</strong></p>
    @if ($exception->isNotEmpty())
        @if ($exception->has('message'))
            <p><strong>{{ $exception->get('message') }}</strong></p>
        @endif
        @if ($exception->has('trace'))
            <pre>{{ $exception->get('trace') }}</pre>
        @endif
        @if (!$exception->has('trace') && !$exception->has('message'))
            <p>Nenhuma exceção foi encontrada.</p>
        @endif
    @endif

    @if ($exception->isEmpty())
        <p>Nenhuma exceção foi encontrada.</p>
    @endif
</fieldset>