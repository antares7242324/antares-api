@include('mails.parts.email-header')
        <tr>
            <td style="padding: 5vw">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%; margin-bottom: 30; text-align: center;">
                            <h1 style="color: #ffffff;font-size: 24px;margin: 0 0 30px 0;">Bem-vindo à ANTARES, {{ $user->firstName() }}!</h1>
                            <p style="color: #FFFFFF; opacity: 80%; font-size: 16px; margin-bottom: 24px; margin: 0 0 24px 0;font-weight: 700;">Você está recebendo um convite especial para entrar na plataforma.</p>
                            <p style="color: #FFFFFF; opacity: 80%; font-size: 16px; margin: 0;">Seu e-mail foi cadastrado em nossa base para que você possa realizar e acompanhar seus pedidos, além de poder utilizar seus descontos adicionais por meio do saldo de cashback.</p>
                        </td>
                    </tr>
                    <tr>
                        <table style="width: 100%;margin-top: 30px;">
                            <tr>
                                <td style="padding: 24px;display: block;background-color: #9D9D9D20;margin-right: 20px;border-radius: 5px;width: auto;">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="margin:auto;width:fit-content;">
                                                    <img src="https://res.cloudinary.com/dqp1ooitz/image/upload/v1680036789/Antares/8686855_ic_fluent_box_regular_icon_x4eajn.png" alt="">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    <p style="color: #FFFFFF;font-size: 16px; margin-bottom:0;text-align:center;">Edite e acompanhe seus pedidos facilmente pelo painel.</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 24px;background-color: #9D9D9D20;border-radius: 5px;width: auto;">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="margin:auto;width:fit-content;">
                                                    <img src="https://res.cloudinary.com/dqp1ooitz/image/upload/v1680036789/Antares/8687850_ic_fluent_tag_regular_icon_ao3ghw.png" alt="">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    <p style="color: #FFFFFF;font-size: 16px;margin-bottom:0;text-align:center;">Faça upload das suas notas de venda e ganhe descontos.</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;padding: 5vw; background-color: #D4D4D420;">
        <tr>
            <td style="text-align: center;display: block;">
                <h2 style="font-size: 24px;color: #333333;margin: 0 0 15px 0;">Entre e defina sua senha para começar a usá-la!</h2>
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0;">Clique no link abaixo e defina sua senha para entrar na plataforma:</p>
                <a style="font-size: 16px;margin: 0 auto 20px auto;background-color: #FECD00;text-decoration: none;padding: 20px;display: block;width: 164px;font-weight: 700;color: black;border-radius: 5px;" href="{{ config('app.frontend_url') }}/definePassword?token=@removeTokenIdPrefix($token->plainTextToken)">Definir senha</a>
                <a style="font-size: 16px;display: block;color: #333333;margin: 0 0 20px 0;font-weight: 700;" href="{{ config('app.frontend_url') }}/definePassword?token=@removeTokenIdPrefix($token->plainTextToken)">{{ config('app.frontend_url') }}/definePassword?token=@removeTokenIdPrefix($token->plainTextToken)</a>
                <p style="font-size: 16px;color: #585858;margin: 0;">Se o link de acesso não abrir diretamente em seu navegador, copie e cole o link acima para ser redirecionado para a tela de definição de senha e comece a usar a plataforma.</p>
            </td>
        </tr>
        {{-- <tr>
            <td style="margin-top: 30px;text-align: center;display: block;">
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0;text-align: center;">Caso desconheça esse convite ou não deseje participar da plataforma, você pode negar essa solicitação clicando no link abaixo:</p>
                <a style="font-size: 16px;margin: 0 auto 20px auto;background-color: #FECD00;text-decoration: none;padding: 20px;display: block;width: 164px;font-weight: 700;color: black;border-radius: 5px;" href="">Desconheço</a>
                <a style="font-size: 16px;display: block;color: #333333;margin: 0 0 20px 0;font-weight: 700;" href="https://www.antaresacoplamentos.com.br/painel/convite/ka02840202jbknso72024567">https://www.antaresacoplamentos.com.br/painel/convite/ka02840202jbknso72024567</a>
                <p style="font-size: 16px;color: #585858;margin: 0;">Se o link de acesso não abrir diretamente em seu navegador, copie e cole o link acima para ser redirecionado para a tela de definição de senha e comece a usar a plataforma.</p>
            </td>
        </tr> --}}
    </table>
@include('mails.parts.email-footer')