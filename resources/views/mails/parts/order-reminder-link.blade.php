<table style="width: 100%;background-color: #D4D4D420;padding: 0vw 5vw 5vw 5vw;">
    <tr>
        <td>
            <p style="font-size: 14px;color: #585858;margin: 0 0 20px 0; text-align:center;">Caso deseje acompanhar os seus pedidos e o seu saldo de cashback, acesse o portal através do link abaixo. Caso não seja redirecionado para a plataforma ANTARES, copie o link abaixo e cole em seu navegador:</p>
            <a style="font-size: 14px;display: block;color: #333333;margin: 0;font-weight: 700;text-align:center;" href="{{ route('my.orders.show', ['order' => $order->id]) }}">{{ route('my.orders.show', ['order' => $order->id]) }} meus pedidos</a>
        </td>
    </tr>
</table>