<fieldset>

    <p>
        <strong>Número:</strong> {{ $order->code }}
        <br>
        <strong>Loja:</strong> {{ $order->store->name }}
        <br>
        <strong>Pagamento:</strong> {{ $order->paymentMethod() }}
        <br>
        <strong>Status:</strong> {{ $order->readableStatus() }}
    </p>

    <p>
        <strong>Descrição</strong>
        <br>{{ $order->description }}
    </p>

    <p>
        @foreach ($items as $item)
            <fieldset>

                <p>
                    <strong>Qtd:</strong> {{ $item->quantity }}
                    <br>
                    <strong>Itens:</strong> {{ $item->item->name }}
                    <br>
                    <strong>Preço:</strong> R${{ $item->toCurrency($item->originalSubtotal()) }} | R${{ $item->toCurrency($item->subtotal) }}
                    <br>
                    <strong>Entrega:</strong> {{ $item->deliveryAt()->format('d/m/Y') }}
                </p>

            </fieldset>
        @endforeach
    </p>

    <p>
        <strong>Saldo usado no pedido:</strong> R${{ $order->toCurrency($order->debitAmount()) }}
        <br>
        <strong>Saldo original:</strong> R${{ $order->toCurrency($order->currentBalanceAmount()) }}
        <br>
        <strong>Saldo restante:</strong> R${{ $order->toCurrency($order->finalBalanceAmount()) }}
        <br>
        <strong>Total do pedido:</strong> R${{ $order->toCurrency($order->originalSubtotal()) }} | R${{ $order->toCurrency($order->subtotal()) }}
    </p>

</fieldset>