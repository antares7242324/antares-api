@inject('calculation', '\App\Support\Hunt\HuntCalculation')
@php $refundTotal = 0; @endphp
<table style="width: 100%;background-color: #D4D4D420;">
    <tr>
        <td style="padding: 40px 5vw 5vw 5vw;">
            <div style="border-radius: 5px;background-color: #FFFFFF;padding: 36px 36px 45px 36px;">
                <table style="width: 100%;text-align: left;border-collapse: collapse;">
                    <tr>
                        <th style="padding: 20px;">
                            <p style="color: #333333;font-size: 16px;margin: 0;">Qtd</p>
                        </th>
                        <th style="padding: 20px;">
                            <p style="color: #333333;font-size: 16px;margin: 0;">Itens</p>
                        </th>
                        <th style="padding: 20px;">
                            <p style="color: #333333;font-size: 16px;margin: 0;">Desconto aplicado</p>
                        </th>
                        <th style="padding: 20px;">
                            <p style="color: #333333;font-size: 16px;margin: 0;">Custo original</p>
                        </th>
                        <th style="text-align: right;padding: 20px;">
                            <p style="color: #333333;font-size: 16px;margin: 0;">Cashback</p>
                        </th>
                    </tr>
                    @foreach ($items as $item)
                        @php $refundTotal = $refundTotal + $item->refund; @endphp
                        <tr style="border-radius: 5px;background-color: #D4D4D430;border-bottom: 5px solid white;">
                        <td style="padding: 20px;">
                            <p style="color: #585858;font-size: 16px;margin: 0">{{ $item->quantity }}</p>
                        </td>
                        <td style="padding: 20px;">
                            <p style="color: #585858;font-size: 16px;margin: 0">{{ $item->description }}</p>
                        </td>
                        <td style="padding: 20px;">
                            {{ $item->discount() }}%
                        </td>
                        <td style="padding: 20px;">
                            R$ {{ $item->toCurrency($item->effectiveCost()) }}%
                        </td>
                        <td style="text-align: right;padding: 20px;">
                            <p style="color: #585858;font-size: 16px;margin: 0">R$ {{ $item->toCurrency($item->refund) }}</p>
                        </td>
                        </tr>
                    @endforeach
                </table>
                <table style="margin-top: 40px;width: 100%;">
                    <tr>
                        <td style="width: 50%;">
                        </td>
                        <td style="width: 50%;text-align: right;">
                            <table style="border-collapse: collapse;width: 100%;">
                                <tr style="border-radius: 5px;background-color: #D4D4D430;">
                                    <td style="padding: 20px;text-align: left;">
                                        <h3 style="color: #333333;font-size: 16px;margin: 0;">Total</h3>
                                    </td>
                                    <td style="padding: 20px;width: fit-content;display: inline-block;text-align: left;">
                                        <p style="color: #333333;font-size: 16px;margin: 0;font-weight: 700">R$ {{ $calculation->toCurrency($refundTotal) }}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>