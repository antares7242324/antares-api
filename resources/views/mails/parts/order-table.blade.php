<div style="border-radius: 5px;background-color: #FFFFFF;padding: 36px 36px 45px 36px;">
    <table style="width: 100%;text-align: left;">
        <tr>
            <th style="width: 50%;">
                <p style="color: #333333;font-size: 16px;margin: 0 0 15px 0">Identificação do pedido</p>
            </th>
            <th style="width: 15%;">
                <p style="color: #333333;font-size: 16px;margin: 0 0 15px 0">Loja</p>
            </th>
            <th style="text-align: right;">
                <p style="color: #333333;font-size: 16px;margin: 0 0 15px 0">Status</p>
            </th>
        </tr>
        <tr>
            <td>
                <p style="color: #585858;font-size: 14px;margin: 0">{{ $order->description }}</p>
            </td>
            <td>
                <p style="color: #585858;font-size: 14px;margin: 0">{{ $order->store->name }}</p>
            </td>
            <td style="text-align: right;">
                <div>
                    <table style="width: 100%;text-align: right;">
                        @if ($order->isRequested())
                        <tr style="width: fit-content;display: inline-block;background-color: #FFDD1F30;border-radius: 20px;">
                            <td>
                                <div style="margin-right: 0px;background-color: #FECD00;width: 12px;height: 12px;margin-left: 2px;border-radius: 50%;"></div>
                            </td>
                            <td>
                                <p style="color: #C6A108;font-size: 14px;margin: 0 2px 0 0;">{{ $order->readableStatus() }}</p>
                            </td>
                        </tr>
                        @elseif ($order->isSent())
                        <tr style="width: fit-content;display: inline-block;background-color: #FFDD1F30;border-radius: 20px;">
                            <td>
                                <div style="margin-right: 0px;background-color: #FECD00;width: 12px;height: 12px;margin-left: 2px;border-radius: 50%;"></div>
                            </td>
                            <td>
                                <p style="color: #C6A108;font-size: 14px;margin: 0 2px 0 0;">{{ $order->readableStatus() }}</p>
                            </td>
                        </tr>
                        @elseif ($order->isCompleted())
                        <tr style="width: fit-content;display: inline-block;background-color: #5AD86630;border-radius: 20px;">
                            <td>
                                <div style="margin-right: 0px;background-color: #5AD866;width: 12px;height: 12px;margin-left: 2px;border-radius: 50%;"></div>
                            </td>
                            <td>
                                <p style="color: #62B46B;font-size: 14px;margin: 0 2px 0 0;">{{ $order->readableStatus() }}</p>
                            </td>
                        </tr>
                        @elseif ($order->isCancelled())
                        <tr style="width: fit-content;display: inline-block;background-color: #F4484833;border-radius: 20px;">
                            <td>
                                <div style="margin-right: 0px;background-color: #F44848;width: 12px;height: 12px;margin-left: 2px;border-radius: 50%;"></div>
                            </td>
                            <td>
                                <p style="color: #F44848;font-size: 14px;margin: 0 2px 0 0;">{{ $order->readableStatus() }}</p>
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;text-align: left;border-collapse: collapse;">
        <tr>
            <th style="padding: 20px;">
                <p style="color: #333333;font-size: 16px;margin: 0;">Qtd</p>
            </th>
            <th style="padding: 20px;">
                <p style="color: #333333;font-size: 16px;margin: 0;">Itens</p>
            </th>
            <th style="padding: 20px;">
                <p style="color: #333333;font-size: 16px;margin: 0;">Preço</p>
            </th>
            <th style="text-align: right;padding: 20px;">
                <p style="color: #333333;font-size: 16px;margin: 0;">Entrega</p>
            </th>
        </tr>
        @foreach ($items as $item)
            <tr style="border-radius: 5px;background-color: #D4D4D430;border-bottom: 5px solid white;">
                <td style="padding: 20px;">
                    <p style="color: #585858;font-size: 14px;margin: 0">
                        {{ $item->quantity }}
                    </p>
                </td>
                <td style="padding: 20px;">
                    <p style="color: #585858;font-size: 14px;margin: 0">
                        {{ $item->item->name }}
                    </p>
                </td>
                <td style="padding: 20px;">
                    <p style="color: #585858;font-size: 14px;margin: 0">
                        R${{ $item->toCurrency($item->subtotal) }}
                    </p>
                </td>
                <td style="text-align: right;padding: 20px;">
                    <p style="color: #585858;font-size: 14px;margin: 0">
                        {{ $item->deliveryAt()->format('d/m/Y') }}
                    </p>
                </td>
            </tr>
        @endforeach
    </table>
    <table style="margin-top: 40px;width: 100%;">
        <tr>
            <td style="width: 50%;">
                <table>
                    <tr >
                        <td style="width: 134px;height: 50px;background-image: url('https://res.cloudinary.com/dqp1ooitz/image/upload/v1680168420/Antares/Componente_3_10_ombgzt.png');background-repeat: no-repeat;background-size: contain;background-position: left center;">
                            <p style="color: #FFFFFF;z-index: 1;position: relative;margin: 10px 10px 5px;font-weight: 700;font-size: 8px;">Desconto atual</p>
                            <h3 style="color: #FFFFFF;z-index: 1;position: relative;font-size: 16px;margin: 0px 10px 10px;font-weight: 700;">{{ $user->currentDiscount() }}% OFF</h3>
                        </td>
                        <td style="display: block;padding: 12px 10px 10px;">
                            <h3 style="margin: 0;font-weight: 100;margin-bottom: 5px;font-size: 12px;">Utilizando</h3>
                            <p style="margin: 0;font-weight: 700;font-size: 8px;">
                                R${{ $order->toCurrency($order->debitAmount()) }} de R${{ $order->toCurrency($order->currentBalanceAmount()) }}
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%;text-align: right;">
                <table style="border-collapse: collapse;width: 100%;">
                    <tr style="border-radius: 5px;background-color: #D4D4D430;">
                        <td style="padding: 20px;text-align: left;">
                            <h3 style="color: #333333;font-size: 16px;margin: 0;">Total</h3>
                        </td>
                        <td style="text-align: right;">
                            <span style="font-size: 12px;color: #333333;text-decoration: line-through;">R${{ $order->toCurrency($order->originalSubtotal()) }}</span>
                        </td>
                        <td style="padding: 20px 20px 20px 0;width: fit-content;display: inline-block;text-align: left;float: right;">
                            <p style="color: #333333;font-size: 16px;margin: 0;font-weight: 700">
                                R${{ $order->toCurrency($order->subtotal()) }}
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>