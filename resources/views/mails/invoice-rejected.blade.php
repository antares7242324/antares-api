        @include('mails.parts.email-header')
        <tr>
            <td>
                <table style="width: 100%;margin-top: 40px;">
                    <tr>
                        <td style="background-color:#323232;border-radius: 0px 0px 5px 5px;padding-bottom: 5vw;">
                            <div style="text-align: center;margin-top:30px;">
                                <h1 style="color: #ffffff;font-size: 24px;margin: 0 0 20px 0;">Sua nota foi cancelada, {{ $invoice->user->firstName() }}</h1>
                                <p style="color: #FFFFFF;font-size: 16px;margin: 0;font-weight: 700;">Isso afeta o seu saldo de cashback.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p style="font-size: 16px;color: #585858;margin: 0;background-color: #D4D4D420;padding: 5vw 5vw 0 5vw;">Após passar por uma verificação, a nota descrita abaixo foi cancelada e o crédito relativo a ela foi retirado do seu saldo de cashback.</p>
    @include('mails.parts.invoice-table')
    <table style="width: 100%;background-color: #D4D4D420;padding: 0vw 5vw 5vw 5vw;">
        <tr>
            <td>
                <p style="font-size: 16px;color: #585858;margin: 0 0 20px 0; text-align:center;">Caso deseje acompanhar o seu saldo ou fazer um novo upload de nota, acesse o portal através do link abaixo. Caso não seja redirecionado para a plataforma ANTARES, copie o link abaixo e cole em seu navegador:</p>
                <a style="font-size: 16px;display: block;color: #333333;margin: 0;font-weight: 700;text-align:center;" href="{{ config('app.frontend_url') }}">{{ config('app.frontend_url') }}</a>
            </td>
        </tr>
    </table>
@include('mails.parts.email-footer')