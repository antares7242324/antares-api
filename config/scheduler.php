<?php

$notifySaleStatus = collect([
    'enabled' => filter_var(env('SCHEDULE_NOTIFY_SALE_STATUS', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_NOTIFY_SALE_STATUS_DAILY_HOUR', '12:00'),
]);

$updateItem = collect([
    'enabled' => filter_var(env('SCHEDULE_UPDATE_ITEM', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_UPDATE_ITEM_DAILY_HOUR', '00:05'),
]);

$updateList = collect([
    'enabled' => filter_var(env('SCHEDULE_UPDATE_LIST', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_UPDATE_LIST_DAILY_HOUR', '02:05'),
]);

$addList = collect([
    'enabled' => filter_var(env('SCHEDULE_ADD_LIST', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_ADD_LIST_DAILY_HOUR', '02:35'),
]);

$updatePrice = collect([
    'enabled' => filter_var(env('SCHEDULE_UPDATE_PRICE', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_UPDATE_PRICE_DAILY_HOUR', '03:05'),
]);

$cancelOrderPriceListInactive = collect([
    'enabled' => filter_var(env('SCHEDULE_CANCEL_ORDER_PRICE_LIST_INACTIVE', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_CANCEL_ORDER_PRICE_LIST_INACTIVE_DAILY_HOUR', '04:35'),
]);

$updateUserPriceList = collect([
    'enabled' => filter_var(env('SCHEDULE_UPDATE_USER_PRICE_LIST', true), FILTER_VALIDATE_BOOLEAN),
    'daily_hour' => env('SCHEDULE_UPDATE_USER_PRICE_LIST_DAILY_HOUR', '06:35'),
]);

$updateInvoiceStatus = collect([
    'enabled' => filter_var(env('SCHEDULE_UPDATE_INVOICE_STATUS', true), FILTER_VALIDATE_BOOLEAN),
    'check_days' => (int) env('SCHEDULE_UPDATE_INVOICE_STATUS_CHECK_DAYS', 24),
]);

$updateOrderStatus = collect([
    'enabled' => filter_var(env('SCHEDULE_UPDATE_ORDER_STATUS', true), FILTER_VALIDATE_BOOLEAN),
]);

$sendRequestedOrder = collect([
    'enabled' => filter_var(env('SCHEDULE_SEND_REQUESTED_ORDER', true), FILTER_VALIDATE_BOOLEAN),
]);

return [
    'notify_sale_status' => [
        'enabled' => $notifySaleStatus->get('enabled'),
        'daily_hour' => $notifySaleStatus->get('daily_hour'),
    ],

    'update_item' => [
        'enabled' => $updateItem->get('enabled'),
        'daily_hour' => $updateItem->get('daily_hour'),
    ],

    'update_list' => [
        'enabled' => $updateList->get('enabled'),
        'daily_hour' => $updateList->get('daily_hour'),
    ],

    'add_list' => [
        'enable' => $addList->get('enable'),
        'daily_hour' => $addList->get('daily_hour'),
    ],

    'update_price' => [
        'enabled' => $updatePrice->get('enabled'),
        'daily_hour' => $updatePrice->get('daily_hour'),
    ],

    'cancel_order_price_list_inactive' => [
        'enabled' => $cancelOrderPriceListInactive->get('enabled'),
        'daily_hour' => $cancelOrderPriceListInactive->get('daily_hour'),
    ],

    'update_user_price_list' => [
        'enabled' => $updateUserPriceList->get('enabled'),
        'daily_hour' => $updateUserPriceList->get('daily_hour'),
    ],

    'update_invoice_status' => [
        'enabled' => $updateInvoiceStatus->get('enabled'),
        'check_days' => $updateInvoiceStatus->get('check_days'),
    ],

    'update_order_status' => [
        'enabled' => $updateOrderStatus->get('enabled'),
    ],

    'send_requested_order' => [
        'enabled' => $sendRequestedOrder->get('enabled'),
    ],
];
