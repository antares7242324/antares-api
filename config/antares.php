<?php

$blockedCnae = env('ANTARES_BLOCKED_CNAE', null);

if (is_null($blockedCnae) || strlen($blockedCnae) < 1) $blockedCnae = [];

if (is_array($blockedCnae) === false) {
    $blockedCnae = explode(',', $blockedCnae);
}

return [
    'extra_discounts' => env('ANTARES_EXTRA_DISCOUNTS', '0.00,0.10,0.15,0.20,0.25'),

    'hunt_alpha' => env('ANTARES_HUNT_ALPHA', '0.85'),

    'hunt_tare' => env('ANTARES_HUNT_TARE', '0.02'),

    'blocked_cnae' => $blockedCnae,

    'logger' => filter_var(env('ANTARES_LOGGER', 'false'), FILTER_VALIDATE_BOOLEAN),

    'logger_email' => env('ANTARES_LOGGER_EMAIL', null),

    'email' => env('ANTARES_EMAIL', null),
];
